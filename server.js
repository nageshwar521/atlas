'use strict';

const Hapi = require('hapi');
const jsonServer = require('json-server');
const db = require('./db.json');
const _ = require('lodash');

// Create a server with a host and port
const server = Hapi.server({
  host: 'localhost',
  port: 8000,
  routes: { cors: true }
});

// utils
const getHostnames = (vnfs) => {
  const sites = vnfs.sites.map(site => site.vms);
  return _.flatten(sites);
}

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/device',
  handler: function (request, h) {
    const queryParams = request.query;
    const { page, sizePerPage } = queryParams;
    let devices = db.devicesMock;
    return devices.slice(page, sizePerPage);
  }
});

// Add the route
server.route({
  method: 'POST',
  path: '/api/v1/platform/device',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'PUT',
  path: '/api/v1/platform/device',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'DELETE',
  path: '/api/v1/platform/device/delete',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'DELETE',
  path: '/api/v1/platform/device/deleteMultiple',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/site',
  handler: function (request, h) {
    const queryParams = request.query;
    const { page, sizePerPage } = queryParams;
    let sites = db.sitesMock;
    return sites.slice(page, sizePerPage);
  }
});

// Add the route
server.route({
  method: 'POST',
  path: '/api/v1/platform/site',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'PUT',
  path: '/api/v1/platform/site',
  handler: function (request, h) {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/lob',
  handler: function (request, h) {
    const queryParams = request.query;
    let lobs = db.deployments.organizations;
    return Object.keys(lobs);
  }
});

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/project',
  handler: function (request, h) {
    const queryParams = request.query;
    const { lob } = queryParams;
    let lobs = db.deployments.organizations;
    let projects = _.omit(lobs[lob], ['totalVnfs']);
    return Object.keys(projects);
  }
});

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/vnf',
  handler: function (request, h) {
    const queryParams = request.query;
    const { lob, project } = queryParams;
    let lobs = db.deployments.organizations;
    let projects = lobs[lob];
    let vnfs = projects[project];
    return Object.keys(vnfs);
  }
});

// Add the route
server.route({
  method: 'GET',
  path: '/api/v1/platform/siteids',
  handler: function (request, h) {
    return [
      "BOASTON",
      "Sacramento",
      "MADRID",
      "Texas",
      "Arlington"
    ];
  }
});

server.route({
  method: 'GET',
  path: '/api/v1/platform/scheduleBackup/{deviceId}',
  handler: (request, h) => {
    return {
      "deviceId": "DEVICE_47",
      "time": "10:00",
      "startDate": "02/25/2019",
      "endDate": "05/25/2019",
      "frequency": "Daily"
    };
  }
});

server.route({
  method: 'POST',
  path: '/api/v1/platform/scheduleBackup/{deviceId}',
  handler: (request, h) => {
    const queryParams = request.payload;
    return queryParams;
  }
});

server.route({
  method: 'POST',
  path: '/api/v1/platform/backupNow/{deviceId}',
  handler: (request, h) => {
    const queryParams = request.payload;
    return queryParams;
  }
});

// Start the server
const start = async function () {

  try {
    await server.start();
  }
  catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log('Server running at:', server.info.uri);
};

start();