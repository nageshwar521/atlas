'use strict';

import url         from 'url';
import browserSync from 'browser-sync';
import gulp        from 'gulp';
import config      from '../config';

gulp.task('browserSync', function() {

  const DEFAULT_FILE = 'index.html';
 const ASSET_EXTENSION_REGEX = new RegExp(`\\b(?!\\?)\\.(${config.assetExtensions.join('|')})\\b(?!\\.)`, 'i');
  browserSync.create();
  browserSync.init({
    server: {
      baseDir: config.buildDir,
      middleware: function(req, res, next) {
        const fileHref = url.parse(req.url).href;

        if ( !ASSET_EXTENSION_REGEX.test(fileHref)) {
          req.url = '/' + DEFAULT_FILE;
        }

        return next();
      }
    },
    port: config.browserPort,
    ghostMode: {
      links: false
    }
  });

});
// var browserSync = require('browser-sync').create();

// // Static server
// gulp.task('browserSync', function() {
//     browserSync.init({
//         server: {
//             baseDir: config.buildDir
//         },
//         port: config.browserPort
//     });
// });