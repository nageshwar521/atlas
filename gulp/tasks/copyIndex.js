'use strict';

import gulp   from 'gulp';
import config from '../config';

gulp.task('copyIndex', function() {

  return gulp.src('index.html').pipe(gulp.dest(config.buildDir));

});
