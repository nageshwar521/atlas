'use strict';

var gulp = require('gulp'),
    uncache = require('gulp-uncache');
	
gulp.task('uncache', function () {
    return gulp.src('build/index.html')
        .pipe(uncache({
            append:'time',
            srcDir:'build',
            distDir:'build'
        }))
        .pipe(gulp.dest('build'));
});