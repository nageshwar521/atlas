'use strict';

import gulp from 'gulp';
import config from '../config';
import livereload from 'gulp-livereload';

gulp.task('watch', ['browserSync'], function () {

  livereload.listen();
  // Scripts are automatically watched by Watchify inside Browserify task
  gulp.watch(config.styles.src, ['less']);
  gulp.watch('./src/app.less', ['less']);
  gulp.watch('index.html', ['copyIndex']);

});