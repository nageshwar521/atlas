import gulp        from 'gulp';
import runSequence from 'run-sequence';

gulp.task('dev', ['clean'], function(cb) {

  cb = cb || function() {};

  global.isProd = false;

  return runSequence(['less', 'fonts', 'imagemin', 'browserify', 'copyIndex', 'uncache'], 'watch', 'uncache', cb);

});