'use strict';

var gulp = require('gulp');

// Fonts
gulp.task('fonts', function () {
  return gulp.src([
    './node_modules/**/fonts/**/*.{ttf,woff,eot,svg}'])
    .pipe(gulp.dest('./build/css/fonts/'));
});