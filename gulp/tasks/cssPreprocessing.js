'use strict';

import gulp from 'gulp';
import less from 'gulp-less';
import config from '../config';
import autoprefix from 'less-plugin-autoprefix'

gulp.task('less', function () {
  return gulp.src('./less/index.less')
    .pipe(less({
      plugins: [autoprefix]
    }))
    .pipe(gulp.dest('./build/css'));
});
