'use strict';

const config = {

  browserPort: 3000,

  scripts: {
    src: './src/**/*.js',
    dest: './build/js/',
    gulp: './gulp/**/*.js'
  },

  images: {
    src: './img/**/*.{jpeg,jpg,png,gif,pdf}',
    dest: './build/img/'
  },

  styles: {
    src: ['./less/**/*.less'],
    dest: './build/css/'
  },

  fonts: {
    src: ['./node_modules/**/fonts/**/*.{ttf,woff,eot,svg}'],
    dest: './build/fonts/'
  },

  sourceDir: './src/',

  buildDir: './build/',

  assetExtensions: [
    'js',
    'css',
    'map',
    'png',
    'jpe?g',
    'gif',
    'svg',
    'eot',
    'otf',
    'ttc',
    'ttf',
    'woff2?',
    'pdf'
  ]
};

export default config;