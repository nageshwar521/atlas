import axios from 'axios';

const api = (url, config) => {
  const token = 'YWRtaW46VmVyaXpvbjE=';
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Basic ${token}`
  };
  const defaultConfig = {
    baseURL: url,
    headers,
    method: 'get'
  };
  const options = Object.assign({}, defaultConfig, config);
  // console.log(options);
  const request = axios(options);
  return request;
}

export default api;