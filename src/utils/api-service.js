import axios from 'axios';

const onSuccess = response => {
  const { data } = response;
  if (data && data.error) {
    return Promise.reject(data.error);
  }
  return data;
};

const onError = err => {
  console.log(err)
};

const settingDefaultConfig = apiConfig => {

  const config = apiConfig;
  config.method = config.method || 'get';
  config.responseType = config.responseType || 'json';
  config.noCache = config.noCache || false;
  if (config.method.toLowerCase() !== 'get') {
    config.data = config.data || {};

    if (typeof config.headers === 'object' && config.headers !== null) {
      if (!config.headers['Content-Type']) {
        config.headers['Content-Type'] = 'application/json';
      }
    } else {
      config.headers = { 'Content-Type': 'application/json' };
    }
  }
  return config;
};

const api = apiConfig => {
  const axiosRef = axios.create();
  const config = settingDefaultConfig(apiConfig);

  if (config.noCache) {
    const temp = {
      'Cache-Control': 'no-cache'
    };
    config.headers = config.headers ? config.headers : {};
    config.headers = { ...config.headers, ...temp };
  }


  const token = 'YWRtaW46VmVyaXpvbjE=';
  if (config.headers) {
    if (!config.headers.Authorization) {
      config.headers.Authorization = `Basic ${token}`;
    }
  } else {
    config.headers = { Authorization: `Basic ${token}` };
  }

  config.customErrorHandeler = config.customErrorHandeler || config.onError;
  config.customSuccessHandler = config.customSuccessHandler || onSuccess;
  config.url = config.url || null;

  return axiosRef(config).then(config.customSuccessHandler, config.customErrorHandeler);
};


const service = (apiConfig) => {
  const config = apiConfig || {};
  return api(config);
};

const Get = (url, headers, responseType, onError) => service({
  url,
  headers,
  responseType,
  method: 'GET',
  onError
})

export { Get }
