import React from 'react';
import { NotificationManager, listNotify } from 'react-notifications';

// import { toastr } from 'react-redux-toastr'; 
// import { library } from '@fortawesome/fontawesome-svg-core';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCheckCircle, faTimesCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

// library.add(faCheckCircle, faTimesCircle, faInfoCircle);

const createNotification = (type, message) => {
    console.log(listNotify);
    return () => {
        switch (type) {
            case 'info':
                NotificationManager.info(message);
                break;
            case 'success':
                NotificationManager.success(message);
                break;
            case 'error':
                NotificationManager.error(message);
                break;
        }
    };
};

export const successNotification = (message) => {
    return createNotification('success', message)();
}

export const errorNotification = (message) => {
    return createNotification('error', message)();
}

export const infoNotification = (message) => {
    return createNotification('info', message)();
}

const mobile = () => (window.innerWidth < 768)?true:false;

const filterData = (data, searchText) => {
    return data.filter((dataItem) => {
        return Object.values(dataItem).join(" ").toLowerCase().includes(searchText.toLowerCase());
    });
}

export default {
    mobile,
    filterData,
    successNotification,
    errorNotification,
    infoNotification
}