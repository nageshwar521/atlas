import React, { Component, Fragment } from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Routes from './routes';

// Store config
import configureStore from './store/configureStore';

const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        {/* <ReduxToastr
          timeOut={5000}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          closeOnToastrClick /> */}
        <NotificationContainer />
        <Router>
          <Route component={Routes}></Route>
        </Router>
      </Provider>
    )
  }
}
