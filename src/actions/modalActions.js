import * as types from './actionTypes';

export const openModal = (modalData) => {
  return dispatch => {
    dispatch({
      type: types.OPEN_MODAL,
      payload: modalData
    });
  }
}

export const closeModal = () => {
  return dispatch => {
    dispatch({
      type: types.CLOSE_MODAL
    });
  }
}

export default {
  openModal,
  closeModal
}