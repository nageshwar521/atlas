import * as types from './actionTypes';

export const setActiveTab = (tabName) => {
  return dispatch => {
    dispatch({
      type: types.SET_ACTIVE_TAB,
      tabName
    });
  }
}

export default {
  setActiveTab
}