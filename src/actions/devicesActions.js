import * as types from './actionTypes';
import { filterData, successNotification, errorNotification, infoNotification } from '../utils/common';

import { 
  devicesUrl,
  lobsUrl,
  projectsUrl,
  vnfsUrl,
  siteIdsUrl,
  scheduleBackupUrl,
  backupNowUrl
} from '../config'
import api from '../utils/api';

export const showNewDeviceForm = () => {
  return dispatch => {
    dispatch({
      type: types.SHOW_NEW_DEVICE_FORM
    });
  }
}

export const hideNewDeviceForm = () => {
  return dispatch => {
    dispatch({
      type: types.HIDE_NEW_DEVICE_FORM
    });
  }
}

export const showDeviceDetails = ({deviceData, isEditable}) => {
  return dispatch => {
    dispatch({
      type: types.SHOW_DEVICE_DETAILS,
      payload: { deviceData, isEditable }
    });
  }
}

export const hideDeviceDetails = () => {
  return dispatch => {
    dispatch({
      type: types.HIDE_DEVICE_DETAILS
    });
  }
}

const fetchDevicesRequest = () => {
  return {
    type: types.FETCH_DEVICES_REQUEST
  };
}

const fetchDevicesSuccess = (sites) => {
  return {
    type: types.FETCH_DEVICES_SUCCESS,
    payload: sites
  };
}

const fetchDevicesFailure = (error) => {
  return {
    type: types.FETCH_DEVICES_FAILURE,
    error
  };
}

export const fetchDevices = (currentPage = 1, sizePerPage = 10, params = "") => {
  return dispatch => {
    dispatch(fetchDevicesRequest());
    api(`${devicesUrl}?page=${currentPage - 1}&sizePerPage=${sizePerPage}${params}`)
      .then(response => {
        dispatch(fetchDevicesSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchDevicesFailure(error));
      });
  }
}

export const handleSelectedDevices = (id, isSelect, selectedDevices) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_DEVICES,
      payload: {
        id,
        isSelect,
        selectedDevices
      }
    });
  }
}

export const handleSelectedAllDevices = (ids, isSelect, selectedDevices) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_ALL_DEVICES,
      payload: {
        ids,
        isSelect,
        selectedDevices
      }
    });
  }
}

export const filterDevices = (filteredDevices) => {
  return dispatch => {
    dispatch({
      type: types.FILTER_DEVICES,
      payload: filteredDevices
    });
  };
}

export const clearSelectedDevices = () => {
  return dispatch => {
    dispatch({
      type: types.CLEAR_DEVICES
    });
  }
}

export const handleSearch = (searchText) => {
  return (dispatch, getState) => {
    const { devicesReducer } = getState();
    const { devices } = devicesReducer;
    const filteredDataIds = filterData(devices, searchText);
    // console.log('handleSearch');
    // console.log(filteredDataIds);
    dispatch({
      type: types.HANDLE_SEARCH,
      payload: filteredDataIds
    });
  }
}

const fetchScheduleBackupDataRequest = () => {
  return {
    type: types.FETCH_SCHEDULE_BACKUP_DATA_REQUEST
  };
}

const fetchScheduleBackupDataSuccess = (scheduleBackupData) => {
  return {
    type: types.FETCH_SCHEDULE_BACKUP_DATA_SUCCESS,
    payload: scheduleBackupData
  };
}

const fetchScheduleBackupDataFailure = (error) => {
  return {
    type: types.FETCH_SCHEDULE_BACKUP_DATA_FAILURE,
    error
  };
}

export const fetchScheduleBackupData = (deviceId) => {
  return dispatch => {
    dispatch(fetchScheduleBackupDataRequest());
    api(`${scheduleBackupUrl}/${deviceId}`)
      .then(response => {
        console.log("fetchScheduleBackupData", response);
        dispatch(fetchScheduleBackupDataSuccess(response.data));
        // openModal(modalData)(dispatch);
      })
      .catch(error => {
        console.log("fetchScheduleBackupData error", error);
        dispatch(fetchScheduleBackupDataFailure(error));
      });
  }
}

export const closeScheculeBackupModal = () => {
  return {
    type: types.CLOSE_SCHEDULE_BACKUP_MODAL
  };
}

const fetchLobsDataRequest = () => {
  return {
    type: types.FETCH_LOBS_DATA_REQUEST
  };
}

const fetchLobsDataSuccess = (lobs) => {
  return {
    type: types.FETCH_LOBS_DATA_SUCCESS,
    payload: lobs
  };
}

const fetchLobsDataFailure = (error) => {
  return {
    type: types.FETCH_LOBS_DATA_FAILURE,
    error
  };
}

export const fetchLobsData = () => {
  return dispatch => {
    dispatch(fetchLobsDataRequest());
    api(lobsUrl)
      .then(response => {
        dispatch(fetchLobsDataSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchLobsDataFailure(error));
      });
  }
}

const fetchProjectsDataRequest = () => {
  return {
    type: types.FETCH_PROJECTS_DATA_REQUEST
  };
}

const fetchProjectsDataSuccess = (projects) => {
  return {
    type: types.FETCH_PROJECTS_DATA_SUCCESS,
    payload: projects
  };
}

const fetchProjectsDataFailure = (error) => {
  return {
    type: types.FETCH_PROJECTS_DATA_FAILURE,
    error
  };
}

export const fetchProjectsData = (params) => {
  return dispatch => {
    dispatch(fetchProjectsDataRequest());
    api(projectsUrl + params)
      .then(response => {
        dispatch(fetchProjectsDataSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchProjectsDataFailure(error));
      });
  }
}

const fetchVnfsDataRequest = () => {
  return {
    type: types.FETCH_VNFS_DATA_REQUEST
  };
}

const fetchVnfsDataSuccess = (vnfs) => {
  return {
    type: types.FETCH_VNFS_DATA_SUCCESS,
    payload: vnfs
  };
}

const fetchVnfsDataFailure = (error) => {
  return {
    type: types.FETCH_VNFS_DATA_FAILURE,
    error
  };
}

export const fetchVnfsData = (params) => {
  return dispatch => {
    dispatch(fetchVnfsDataRequest());
    api(vnfsUrl + params)
      .then(response => {
        dispatch(fetchVnfsDataSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchVnfsDataFailure(error));
      });
  }
}

const fetchSitesDataRequest = () => {
  return {
    type: types.FETCH_SITES_DATA_REQUEST
  };
}

const fetchSitesDataSuccess = (sites) => {
  return {
    type: types.FETCH_SITES_DATA_SUCCESS,
    payload: sites
  };
}

const fetchSitesDataFailure = (error) => {
  return {
    type: types.FETCH_SITES_DATA_FAILURE,
    error
  };
}

export const fetchSitesData = () => {
  return dispatch => {
    dispatch(fetchSitesDataRequest());
    api(siteIdsUrl)
      .then(response => {
        dispatch(fetchSitesDataSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchSitesDataFailure(error));
      });
  }
}

export const addDeviceRequest = () => {
  return {
    type: types.ADD_DEVICE_REQUEST
  };
}

const addDeviceSuccess = (response) => {
  return {
    type: types.ADD_DEVICE_SUCCESS,
    payload: response
  };
}

const addDeviceFailure = (error) => {
  return {
    type: types.ADD_DEVICE_FAILURE,
    error
  };
}

export const addDevice = (formData) => {
  return dispatch => {
    dispatch(addDeviceRequest());
    const options = {
      method: 'POST',
      data: JSON.stringify(formData),
      'Content-Type': 'application/json'
    };
    api(`${devicesUrl}`, options)
      .then(response => {
        successNotification(`Adding ${formData.hostName} success`);
        dispatch(addDeviceSuccess(response));
      })
      .catch(error => {
        errorNotification(`Adding ${formData.hostName} error`);
        dispatch(addDeviceFailure(error));
      });
  }
}

export const updateDeviceDetails = (formData) => {
  return dispatch => {
    dispatch({
      type: types.UPDATE_DEVICE_DETAILS,
      payload: formData
    });
  }
}

export const updateDeviceRequest = () => {
  return {
    type: types.UPDATE_DEVICE_REQUEST
  };
}

const updateDeviceSuccess = (response) => {
  return {
    type: types.UPDATE_DEVICE_SUCCESS,
    payload: response
  };
}

const updateDeviceFailure = (error) => {
  return {
    type: types.UPDATE_DEVICE_FAILURE,
    error
  };
}

export const updateDevice = (formData) => {
  return dispatch => {
    dispatch(updateDeviceRequest());
    const options = {
      method: 'PUT',
      data: JSON.stringify(formData),
      'Content-Type': 'application/json'
    };
    api(`${devicesUrl}`, options)
      .then(response => {
        successNotification(`Updating ${formData.hostName} success`);
        dispatch(updateDeviceSuccess(response));
      })
      .catch(error => {
        errorNotification(`Updating ${formData.hostName} error`);
        dispatch(updateDeviceFailure(error));
      });
  }
}

export const updateScheduleBackupData = (formData) => {
  return dispatch => {
    dispatch({
      type: types.UPDATE_SCHEDULE_BACKUP_DATA,
      payload: formData
    })
  }
}

export const postScheduleBackupDataRequest = () => {
  return {
    type: types.POST_SCHEDULE_BACKUP_DATA_REQUEST
  };
}

const postScheduleBackupDataSuccess = (response) => {
  return {
    type: types.POST_SCHEDULE_BACKUP_DATA_SUCCESS,
    payload: response
  };
}

const postScheduleBackupDataFailure = (error) => {
  return {
    type: types.POST_SCHEDULE_BACKUP_DATA_FAILURE,
    error
  };
}

export const postScheduleBackupData = (formData) => {
  // console.log(formData)
  return dispatch => {
    dispatch(postScheduleBackupDataRequest());
    const options = {
      method: 'POST',
      data: JSON.stringify(formData),
      'Content-Type': 'application/json'
    };
    api(`${scheduleBackupUrl}`, options)
      .then(response => {
        successNotification(`${formData.deviceId} Schedule backup data success`);
        dispatch(postScheduleBackupDataSuccess(response));
      })
      .catch(error => {
        errorNotification(`${formData.deviceId} Schedule backup data error`);
        dispatch(postScheduleBackupDataFailure(error));
      });
  }
}

export const postBackupNowDataRequest = () => {
  return {
    type: types.POST_BACKUP_NOW_DATA_REQUEST
  };
}

const postBackupNowDataSuccess = (response) => {
  return {
    type: types.POST_BACKUP_NOW_DATA_SUCCESS,
    payload: response
  };
}

const postBackupNowDataFailure = (error) => {
  return {
    type: types.POST_BACKUP_NOW_DATA_FAILURE,
    error
  };
}

export const postBackupNowData = (deviceId) => {
  return dispatch => {
    dispatch(postBackupNowDataRequest());
    api(`${backupNowUrl}/${deviceId}`)
      .then(response => {
        successNotification(`${deviceId} backup now data success`);
        dispatch(postBackupNowDataSuccess(response));
      })
      .catch(error => {
        errorNotification(`${deviceId} backup now data error`);
        dispatch(postBackupNowDataFailure(error));
      });
  }
}

export const openBackupNowModal = () => {
  return {
    type: types.OPEN_BACKUP_NOW_MODAL
  };
}

export const closeBackupNowModal = () => {
  return {
    type: types.CLOSE_BACKUP_NOW_MODAL
  };
}

export const deleteSingleDeviceRequest = () => {
  return {
    type: types.DELETE_SINGLE_DEVICE_REQUEST
  };
}

const deleteSingleDeviceSuccess = (response) => {
  return {
    type: types.DELETE_SINGLE_DEVICE_SUCCESS,
    payload: response
  };
}

const deleteSingleDeviceFailure = (error) => {
  return {
    type: types.DELETE_SINGLE_DEVICE_FAILURE,
    error
  };
}

export const deleteSingleDevice = (device) => {
  return dispatch => {
    dispatch(deleteSingleDeviceRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json'
    };
    api(`${devicesUrl}/delete?deviceId=${device[0].deviceId}`, options)
      .then(response => {
        successNotification(`Deleting ${device[0].deviceId} success`);
        dispatch(deleteSingleDeviceSuccess(response));
      })
      .catch(error => {
        errorNotification(`Deleting ${device[0].deviceId} error`);
        dispatch(deleteSingleDeviceFailure(error));
      });
  }
}

const deleteMultipleDevicesRequest = () => {
  return {
    type: types.DELETE_MULTIPLE_DEVICES_REQUEST
  };
}

const deleteMultipleDevicesSuccess = (response) => {
  return {
    type: types.DELETE_MULTIPLE_DEVICES_SUCCESS,
    payload: response
  };
}

const deleteMultipleDevicesFailure = (error) => {
  return {
    type: types.DELETE_MULTIPLE_DEVICES_FAILURE,
    error
  };
}

export const deleteMultipleDevices = (devices) => {
  const inputData = {
    ids:[]
  }
  devices.forEach(device => {
    inputData.ids.push(device.deviceId);
  });
  return dispatch => {
    dispatch(deleteMultipleDevicesRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json',
      data: inputData
    };
    api(`${devicesUrl}/deleteMultiple`, options)
      .then(response => {
        successNotification(`Deleting ${inputData.ids.split(",")} success`);
        dispatch(deleteMultipleDevicesSuccess(response));
      })
      .catch(error => {
        successNotification(`Deleting ${inputData.ids.split(",")} error`);
        dispatch(deleteMultipleDevicesFailure(error));
      });
  }
}

export const resetDevices = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_DEVICE_TAB
    });
  }
}

// export const singleDeviceDelete = (device) => {
//   const SingleDeviceDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/device/delete?deviceId="
//   return dispatch => {
//   axios({
//     url: `${SingleDeviceDeleteUrl}${device[0].deviceId}`,
//     method: 'DELETE',
//     headers: {
//       Authorization: `Basic YWRtaW46VmVyaXpvbjE=`
//     },
//   })
//   .then((response) => {
//     if (response.data.status == "success") {
//     }
//   })
//   .catch(error => {
//         //dispatch(fetchSnapshotsFailure(error));
//         console.log( error, "505 Internal Server Error");
//   });
//  };
// }

// export const multipleDevicesDelete = (devices) => {
//   const MultipleDeviceDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/device/deleteMultiple"
//   const inputData = {
//     ids:[]
//   }
//     devices.forEach(device => {
//     inputData.ids.push(device.deviceId);
//       });
//   return dispatch => {
//   axios({
//     method: 'DELETE',
//     url: `${MultipleDeviceDeleteUrl}`,
//     headers: { 'Content-Type': 'application/json' , 'Authorization': 'Basic YWRtaW46VmVyaXpvbjE=' },
//     data: inputData
//   })
//   .then((response) => {
//     if (response.data.status == "success") {
//     }
//   })
//   .catch(error => {
//         //dispatch(fetchSnapshotsFailure(error));
//         console.log( error, "505 Internal Server Error");
//   });
//  };
// }

export default {
  showNewDeviceForm,
  hideNewDeviceForm,
  showDeviceDetails,
  hideDeviceDetails,
  fetchDevices,
  handleSelectedDevices,
  handleSelectedAllDevices,
  filterDevices,
  clearSelectedDevices,
  handleSearch,
  fetchScheduleBackupData,
  fetchLobsData,
  fetchProjectsData,
  fetchVnfsData,
  fetchSitesData,
  addDeviceRequest,
  addDevice,
  updateScheduleBackupData,
  postScheduleBackupDataRequest,
  postScheduleBackupData,
  closeScheculeBackupModal,
  postBackupNowDataRequest,
  postBackupNowData,
  openBackupNowModal,
  closeBackupNowModal,
  deleteSingleDeviceRequest,
  deleteSingleDevice,
  deleteMultipleDevicesRequest,
  deleteMultipleDevices,
  updateDeviceDetails,
  updateDeviceRequest,
  updateDevice,
  resetDevices
}