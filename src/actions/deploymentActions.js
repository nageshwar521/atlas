//import axios from 'axios';
import * as types from './actionTypes';
import { deploymentsUrl } from '../config';
import { Get } from '../utils/api-service';

export const fetchDeploymentsRequest = () => {
  return { 
    type: types.FETCH_DEPLOYMENTS_DATA_REQUEST 
  };
}

export const fetchDeploymentsSuccess = (response) => {
  return {
    type: types.FETCH_DEPLOYMENTS_DATA_SUCCESS,
    payload: response
  };
}

export const fetchDeploymentsFailure = (error) => {
  return {
    type: types.FETCH_DEPLOYMENTS_DATA_FAILURE,
    error
  };
}

export const fetchDeployments = () => {
  return dispatch => {
    dispatch(fetchDeploymentsRequest());
     Get(deploymentsUrl)
     //axios.get(deploymentsUrl)
      .then(response => {
        dispatch(fetchDeploymentsSuccess(response));
        //dispatch(fetchDeploymentsSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchDeploymentsFailure(error));
      });
  };
}

export default {
  fetchDeployments
}