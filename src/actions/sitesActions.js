import * as types from './actionTypes';
import { filterData, successNotification, errorNotification } from '../utils/common';
import { sitesUrl } from '../config'
import api from '../utils/api';

export const showNewSiteForm = () => {
  return dispatch => {
    dispatch({
      type: types.SHOW_NEW_SITE_FORM
    });
  }
}

export const hideNewSiteForm = () => {
  return dispatch => {
    dispatch({
      type: types.HIDE_NEW_SITE_FORM
    });
  }
}

export const showSiteDetails = ({ siteData, isEditable }) => {
  return dispatch => {
    dispatch({
      type: types.SHOW_SITE_DETAILS,
      payload: { siteData, isEditable }
    });
  }
}

export const hideSiteDetails = () => {
  return dispatch => {
    dispatch({
      type: types.HIDE_SITE_DETAILS
    });
  }
}

const fetchSitesRequest = () => {
  return {
    type: types.FETCH_SITES_REQUEST
  };
}

const fetchSitesSuccess = (sites) => {
  return {
    type: types.FETCH_SITES_SUCCESS,
    payload: sites
  };
}

const fetchSitesFailure = (error) => {
  return {
    type: types.FETCH_SITES_FAILURE,
    error
  };
}

export const fetchSites = (currentPage = 1, sizePerPage = 10, params = "") => {
  return dispatch => {
    dispatch(fetchSitesRequest());
    api(`${sitesUrl}?page=${currentPage - 1}&sizePerPage=${sizePerPage}${params}`)
    .then(response => {
      // console.log("fetchSites", response);
      dispatch(fetchSitesSuccess(response.data));
    })
    .catch(error => {
      // console.log("fetchSites error", error);
      dispatch(fetchSitesFailure(error));
    })
  }
}

export const handleSelectedSites = (id, isSelect, selectedSites) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_SITES,
      payload: {
        id,
        isSelect,
        selectedSites
      }
    });
  }
}

export const handleSelectedAllSites = (ids, isSelect, selectedSites) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_ALL_SITES,
      payload: {
        ids,
        isSelect,
        selectedSites
      }
    });
  }
}

export const filterSites = (filteredSites) => {
  return dispatch => {
    dispatch({
      type: types.FILTER_SITES,
      payload: filteredSites
    });
  };
}

export const clearSelectedSites = () => {
  return dispatch => {
    dispatch({
      type: types.CLEAR_SITES
    });
  }
}

export const handleSearch = (searchText) => {
  return (dispatch, getState) => {
    const { sitesReducer } = getState();
    const { sites } = sitesReducer;
    const filteredDataIds = filterData(sites, searchText);
    // console.log('handleSearch');
    // console.log(filteredDataIds);
    dispatch({
      type: types.HANDLE_SEARCH,
      payload: filteredDataIds
    });
  }
}

export const addSiteRequest = () => {
  return {
    type: types.ADD_SITE_REQUEST
  };
}

const addSiteSuccess = (response) => {
  return {
    type: types.ADD_SITE_SUCCESS,
    payload: response
  };
}

const addSiteFailure = (error) => {
  return {
    type: types.ADD_SITE_FAILURE,
    error
  };
}

export const addSite = (formData) => {
  return dispatch => {
    dispatch(addSiteRequest());
    const options = {
      method: 'POST',
      data: JSON.stringify(formData),
      'Content-Type': 'application/json'
    };
    api(`${sitesUrl}`, options)
      .then(response => {
        successNotification(`Adding ${formData.siteName} success`);
        dispatch(addSiteSuccess(response));
      })
      .catch(error => {
        errorNotification(`Adding ${formData.siteName} error`);
        dispatch(addSiteFailure(error));
      });
  }
}

export const updateSiteDetails = (formData) => {
  return dispatch => {
    dispatch({
      type: types.UPDATE_SITE_DETAILS,
      payload: formData
    });
  }
}

export const updateSiteRequest = () => {
  return {
    type: types.UPDATE_SITE_REQUEST
  };
}

const updateSiteSuccess = (response) => {
  return {
    type: types.UPDATE_SITE_SUCCESS,
    payload: response
  };
}

const updateSiteFailure = (error) => {
  return {
    type: types.UPDATE_SITE_FAILURE,
    error
  };
}

export const updateSite = (formData) => {
  return dispatch => {
    console.log(formData);
    dispatch(updateSiteRequest());
    const options = {
      method: 'PUT',
      data: JSON.stringify(formData),
      'Content-Type': 'application/json'
    };
    api(`${sitesUrl}`, options)
      .then(response => {
        successNotification(`Updating ${formData.siteName} success`);
        dispatch(updateSiteSuccess(response));
      })
      .catch(error => {
        errorNotification(`Updating ${formData.siteName} error`);
        dispatch(updateSiteFailure(error));
      });
  }
}

const deleteSingleSiteRequest = () => {
  return {
    type: types.DELETE_SINGLE_SITE_REQUEST
  };
}

const deleteSingleSiteSuccess = (response) => {
  return {
    type: types.DELETE_SINGLE_SITE_SUCCESS,
    payload: response
  };
}

const deleteSingleSiteFailure = (error) => {
  return {
    type: types.DELETE_SINGLE_SITE_FAILURE,
    error
  };
}

export const deleteSingleSite = (site) => {
  return dispatch => {
    dispatch(deleteSingleSiteRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json'
    };
    api(`${sitesUrl}/delete?siteInternalId=${site[0].siteInternalId}`, options)
      .then(response => {
        successNotification(`Deleting ${site[0].siteInternalId} success`);
        dispatch(deleteSingleSiteSuccess(response));
      })
      .catch(error => {
        successNotification(`Deleting ${site[0].siteInternalId} error`);
        dispatch(deleteSingleSiteFailure(error));
      });
  }
}

const deleteMultipleSitesRequest = () => {
  return {
    type: types.DELETE_MULTIPLE_SITES_REQUEST
  };
}

const deleteMultipleSitesSuccess = (response) => {
  return {
    type: types.DELETE_MULTIPLE_SITES_SUCCESS,
    payload: response
  };
}

const deleteMultipleSitesFailure = (error) => {
  return {
    type: types.DELETE_MULTIPLE_SITES_FAILURE,
    error
  };
}

export const deleteMultipleSites = (sites) => {
  const inputData = {
    ids: []
  }
  sites.forEach(site => {
    inputData.ids.push(site.siteInternalId);
  });
  return dispatch => {
    dispatch(deleteMultipleSitesRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json',
      data: inputData
    };
    api(`${sitesUrl}/deleteMultiple`, options)
      .then(response => {
        successNotification(`Deleting ${inputData.ids.join(", ")} success`);
        dispatch(deleteMultipleSitesSuccess(response));
      })
      .catch(error => {
        successNotification(`Deleting ${inputData.ids.join(", ")} error`);
        dispatch(deleteMultipleSitesFailure(error));
      });
  }
}

export const resetSites = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_SITE_TAB
    });
  }
}

// export const singleSiteDelete = (site) => {
//   const SingleSiteDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/site/delete?siteInternalId="
//   return dispatch => {
//   axios({
//     url: `${SingleSiteDeleteUrl}${site[0].siteInternalId}`,
//     method: 'DELETE',
//     headers: {
//       Authorization: `Basic YWRtaW46VmVyaXpvbjE=`
//     },
//   })
//   .then((response) => {
//     if (response.data.status == "success") {
//       //alert("Selcted SITE Delted, Please refresh the page");
//     }
//   })
//   .catch(error => {
//         //dispatch(fetchSnapshotsFailure(error));
//         alert( error, "505 Internal Server Error");
//   });
//  };
// }


// export const multipleSitesDelete = (sites) => {
//   const MultipleSitesDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/site/deleteMultiple"
//   const inputData = {
//     ids:[]
//   }
//     sites.forEach(site => {
//     inputData.ids.push(site.siteInternalId);
//       });
//   return dispatch => {
//   axios({
//     method: 'DELETE',
//     url: `${MultipleSitesDeleteUrl}`,
//     headers: { 'Content-Type': 'application/json' , 'Authorization': 'Basic YWRtaW46VmVyaXpvbjE=' },
//     data: inputData
//   })
//   .then((response) => {
//     if (response.data.status == "success") {
//       //alert("Selected SITES Deleted, Please refresh the page");
//     }
//   })
//   .catch(error => {
//         //dispatch(fetchSnapshotsFailure(error));
//         alert( error, "505 Internal Server Error");
//   });
//  };
// }

export default {
  showNewSiteForm,
  hideNewSiteForm,
  showSiteDetails,
  hideSiteDetails,
  fetchSites,
  handleSelectedSites,
  handleSelectedAllSites,
  filterSites,
  clearSelectedSites,
  handleSearch,
  addSiteRequest,
  addSite,
  updateSiteRequest,
  updateSite,
  updateSiteDetails,
  deleteSingleSiteRequest,
  deleteSingleSite,
  deleteMultipleSitesRequest,
  deleteMultipleSites,
  resetSites
}