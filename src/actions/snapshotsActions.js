import axios from 'axios';
import * as types from './actionTypes';
import { snapshotsUrl } from '../config';
import { Get } from '../utils/api-service';
import { toastr } from 'react-redux-toastr';
import { filterData, successNotification, errorNotification, infoNotification } from '../utils/common';
import api from '../utils/api';

export const fetchSnapshotsRequest = () => {
  return {
    type: types.FETCH_SNAPSHOTS_REQUEST
  };
}

export const fetchSnapshotsSuccess = (response) => {
  return {
    type: types.FETCH_SNAPSHOTS_SUCCESS,
    payload: response
  };
}

export const fetchSnapshotsFailure = (error) => {
  return {
    type: types.FETCH_SNAPSHOTS_FAILURE,
    error
  };
}

export const fetchSnapshots = (currentPage = 1, sizePerPage = 10, params = "") => {
  return dispatch => {
    dispatch(fetchSnapshotsRequest());
    Get(`${snapshotsUrl}/?page=${currentPage - 1 }&sizePerPage=${sizePerPage}${params}`)
      //axios.get(deploymentsUrl)
      .then(response => {
        dispatch(fetchSnapshotsSuccess(response));
        //dispatch(fetchSnapshotsSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchSnapshotsFailure(error));
      });
  };
}

export const handleSelectedSnapshots = (id, isSelect, selectedSnapshots) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_SNAPSHOTS,
      payload: {
        id,
        isSelect,
        selectedSnapshots
      }
    });
  }
}

export const handleSelectedAllSnapshots = (ids, isSelect, selectedSnapshots) => {
  return dispatch => {
    dispatch({
      type: types.SELECTED_ALL_SNAPSHOTS,
      payload: {
        ids,
        isSelect,
        selectedSnapshots
      }
    });
  }
}

export const filterSnapshots = (filteredRows) => {
  return dispatch => {
    dispatch({
      type: types.FILTER_SNAPSHOTS,
      payload: filteredRows
    });
  };
}

export const clearSelectedSnapshots = () => {
  return dispatch => {
    dispatch({
      type: types.CLEAR_SNAPSHOTS
    });
  }
}

export const downloadSingleSnapshotRequest = () => {
  return {
    type: types.DOWNLOAD_SINGLE_SNAPSHOT_REQUEST
  };
}

const downloadSingleSnapshotSuccess = (response) => {
  return {
    type: types.DOWNLOAD_SINGLE_SNAPSHOT_SUCCESS,
    payload: response
  };
}

const downloadSingleSnapshotFailure = (error) => {
  return {
    type: types.DOWNLOAD_SINGLE_SNAPSHOT_FAILURE,
    error
  };
}

export const downloadSingleSnapshot = (snapshot) => {
  return dispatch => {
    const infoNote = infoNotification(`Downloading ${snapshot[0].snapshotName} is in progress`);
    console.log("infoNote");
    console.log(infoNote);
    dispatch(downloadSingleSnapshotRequest());
    const options = {
      method: 'GET',
      responseType: 'blob'
    };
    api(`${snapshotsUrl}/download?path=${snapshot[0].path}`, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `${snapshot[0].snapshotName}`);
        document.body.appendChild(link);
        link.click();
        infoNotification(`Downloading ${snapshot[0].snapshotName} success`);
        dispatch(downloadSingleSnapshotSuccess(response));
      })
      .catch(error => {
        errorNotification(`Downloading ${snapshot[0].snapshotName} failed`);
        dispatch(downloadSingleSnapshotFailure(error));
      });
  }
}

export const downloadMultipleSnapshotsRequest = () => {
  return {
    type: types.DOWNLOAD_MULTIPLE_SNAPSHOTS_REQUEST
  };
}

const downloadMultipleSnapshotsSuccess = (response) => {
  return {
    type: types.DOWNLOAD_MULTIPLE_SNAPSHOTS_SUCCESS,
    payload: response
  };
}

const downloadMultipleSnapshotsFailure = (error) => {
  return {
    type: types.DOWNLOAD_MULTIPLE_SNAPSHOTS_FAILURE,
    error
  };
}

export const downloadMultipleSnapshots = (snapshots) => {
  const inputData = {
    ids: []
  }
  snapshots.forEach(snapshot => {
    inputData.ids.push(snapshot.path);
  });
  return dispatch => {
    infoNotification(`Downloading ${inputData.ids.join(",")} in progress`);
    dispatch(downloadMultipleSnapshotsRequest());
    const options = {
      method: 'POST',
      responseType: 'blob',
      data: inputData
    };
    api(`${snapshotsUrl}/downloadMultiple`, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Atlas-Snapshots.zip`);
        document.body.appendChild(link);
        link.click();
        successNotification(`Downloading ${inputData.ids.join(", ")} success`);
        dispatch(downloadMultipleSnapshotsSuccess(response));
      })
      .catch(error => {
        errorNotification(`Downloading ${inputData.ids.join(", ")} failed`);
        dispatch(downloadMultipleSnapshotsFailure(error));
      });
  }
}

// export const singleSnapshotDownload = (snapshot) => {
//   const snapshotDownloadUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot/download?path="
//   return dispatch => {
//     axios({
//       url: `${snapshotDownloadUrl}${snapshot[0].path}`,
//       method: 'GET',
//       headers: {
//         Authorization: `Basic YWRtaW46VmVyaXpvbjE=`
//       },
//       responseType: 'blob',
//     }).then((response) => {
//       const url = window.URL.createObjectURL(new Blob([response.data]));
//       const link = document.createElement('a');
//       link.href = url;
//       link.setAttribute('download', `${snapshot[0].snapshotName}`);
//       document.body.appendChild(link);
//       link.click();
//     });
//   };
// }

// export const multipleSnapshotsDownload = (snapshots) => {
//   const snapshotDownloadUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot/downloadMultiple/"
//   const inputData = {
//     paths: []
//   }
//   snapshots.forEach(snapshot => {
//     inputData.paths.push(snapshot.path);
//   });
//   return dispatch => {
//     axios({
//       url: `${snapshotDownloadUrl}`,
//       method: 'POST',
//       headers: {
//         Authorization: `Basic YWRtaW46VmVyaXpvbjE=`
//       },
//       data: inputData,
//       responseType: 'blob',
//     }).then((response) => {
//       const url = window.URL.createObjectURL(new Blob([response.data]));
//       const link = document.createElement('a');
//       link.href = url;
//       link.setAttribute('download', `Atlas-Snapshots.zip`);
//       document.body.appendChild(link);
//       link.click();
//     });
//   };
// }

// export const singleSnapshotDelete = (snapshot) => {
//   const SingleDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot/delete?path="
//   return dispatch => {
//     axios({
//       url: `${SingleDeleteUrl}${snapshot[0].path}`,
//       method: 'DELETE',
//       headers: {
//         Authorization: `Basic YWRtaW46VmVyaXpvbjE=`
//       },
//     }).then((response) => {
//       if (response.data.status == "success") {
//       }
//     });
//   }
// }


// export const multipleSnapshotsDelete = (snapshots) => {
//   const MultipleDeleteUrl = "https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot/deleteMultiple"
//   const inputData = {
//     paths: []
//   }
//   snapshots.forEach(snapshot => {
//     inputData.paths.push(snapshot.path);
//   });
//   return dispatch => {
//     axios({
//       method: 'DELETE',
//       url: `${MultipleDeleteUrl}`,
//       headers: { 'Content-Type': 'application/json', 'Authorization': 'Basic YWRtaW46VmVyaXpvbjE=' },
//       data: inputData
//     })
//       .then((response) => {
//         if (response.data.status == "success") {
//           alert("Deleted selected Snapshots, Please refresh the page");
//         }
//       })
//       .catch(error => {
//         dispatch(fetchSnapshotsFailure(error));
//         alert(error, "505 Internal Server Error");
//       });
//   };
// }

export const deleteSingleSnapshotRequest = () => {
  return {
    type: types.DELETE_SINGLE_SNAPSHOT_REQUEST
  };
}

const deleteSingleSnapshotSuccess = (response) => {
  return {
    type: types.DELETE_SINGLE_SNAPSHOT_SUCCESS,
    payload: response
  };
}

const deleteSingleSnapshotFailure = (error) => {
  return {
    type: types.DELETE_SINGLE_SNAPSHOT_FAILURE,
    error
  };
}

export const deleteSingleSnapshot = (snapshot) => {
  return dispatch => {
    dispatch(deleteSingleSnapshotRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json'
    };
    api(`${snapshotsUrl}/delete?path=${snapshot[0].path}`, options)
      .then(response => {
        successNotification(`Deleting ${snapshot[0].path} success`);
        dispatch(deleteSingleSnapshotSuccess(response));
      })
      .catch(error => {
        errorNotification(`Deleting ${snapshot[0].path} error`);
        dispatch(deleteSingleSnapshotFailure(error));
      });
  }
}

export const deleteMultipleSnapshotsRequest = () => {
  return {
    type: types.DELETE_MULTIPLE_SNAPSHOTS_REQUEST
  };
}

const deleteMultipleSnapshotsSuccess = (response) => {
  return {
    type: types.DELETE_MULTIPLE_SNAPSHOTS_SUCCESS,
    payload: response
  };
}

const deleteMultipleSnapshotsFailure = (error) => {
  return {
    type: types.DELETE_MULTIPLE_SNAPSHOTS_FAILURE,
    error
  };
}

export const deleteMultipleSnapshots = (snapshots) => {
  const inputData = {
    ids: []
  }
  snapshots.forEach(snapshot => {
    inputData.ids.push(snapshot.path);
  });
  return dispatch => {
    dispatch(deleteMultipleSnapshotsRequest());
    const options = {
      method: 'DELETE',
      'Content-Type': 'application/json',
      data: inputData
    };
    api(`${snapshotsUrl}/deleteMultiple`, options)
      .then(response => {
        successNotification(`Deleting ${inputData.ids.join(", ")} success`);
        dispatch(deleteMultipleSnapshotsSuccess(response));
      })
      .catch(error => {
        errorNotification(`Deleting ${inputData.ids.join(", ")} error`);
        dispatch(deleteMultipleSnapshotsFailure(error));
      });
  }
}

export const handleSearch = (searchText) => {
  return (dispatch, getState) => {
    const { snapshotsReducer } = getState();
    const { snapshots } = snapshotsReducer;
    const filteredDataIds = filterData(snapshots, searchText);
    dispatch({
      type: types.HANDLE_SEARCH,
      payload: filteredDataIds
    });
  }
}

export const resetSnapshots = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_SNAPSHOT_TAB
    });
  }
}

export const resetDownloadSingleSnapshot = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_DOWNLOAD_SINGLE_SNAPSHOT
    });
  }
}

export const resetDownloadMultipleSnapshot = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_DOWNLOAD_MULTIPLE_SNAPSHOT
    });
  }
}

export const resetDeleteSingleSnapshot = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_DELETE_SINGLE_SNAPSHOT
    });
  }
}

export const resetDeleteMultipleSnapshot = () => {
  return dispatch => {
    dispatch({
      type: types.RESET_DELETE_MULTIPLE_SNAPSHOT
    });
  }
}

export default {
  fetchSnapshots,
  filterSnapshots,
  handleSelectedSnapshots,
  handleSelectedAllSnapshots,
  clearSelectedSnapshots,
  downloadSingleSnapshotRequest,
  downloadSingleSnapshot,
  downloadMultipleSnapshotsRequest,
  downloadMultipleSnapshots,
  deleteSingleSnapshotRequest,
  deleteSingleSnapshot,
  deleteMultipleSnapshotsRequest,
  deleteMultipleSnapshots,
  handleSearch,
  resetSnapshots,
  resetDeleteSingleSnapshot,
  resetDeleteMultipleSnapshot,
  resetDownloadSingleSnapshot,
  resetDownloadMultipleSnapshot
}