module.exports = {

// //Dashboard
//     deploymentsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/deployments`,
//     calendarUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/calendar`,
//     usersCountUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/usercount`,
    
// //Audit Page
//     auditActivityUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/audit`,
//     auditEventUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/audit/`,
   
// //Config management 
//     hostnamesUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot`,
//     snapshotsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/snapshot`,
//     devicesUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/device`,
//     sitesUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/site`,

// //Fetch Lobs, Projects, Vnfs, Sites, SiteIds
//     lobsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/lob`,
//     projectsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/project`,
//     vnfsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/vnf`,
//     siteIdsUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/siteids`,

// //Device Actions
//     scheduleBackupUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/scheduleBackup`,
//     backupNowUrl: `https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/backupNow`,


//MOCK - API's
    mapUrl: `http://localhost:3002/map`,
    snapshotsUrl: `http://localhost:3002/hostnames`,
    deploymentsUrl: `http://localhost:3002/deployments`,
    hostnamesUrl: `http://localhost:3002/hostnames`,
    calendarUrl: `http://localhost:3002/calendar`,
    usersCountUrl: `http://localhost:3002/users`,
    auditActivityUrl: `http://localhost:3002/Activities`,
    auditEventUrl: `http://localhost:3002/Event`,

    devicesUrl: `http://localhost:8000/api/v1/platform/device`,
    sitesUrl: `http://localhost:8000/api/v1/platform/site`,
    lobsUrl: `http://localhost:8000/api/v1/platform/lob`,
    projectsUrl: `http://localhost:8000/api/v1/platform/project`,
    vnfsUrl: `http://localhost:8000/api/v1/platform/vnf`,
    siteIdsUrl: `http://localhost:8000/api/v1/platform/siteids`,
    scheduleBackupUrl: `http://localhost:8000/api/v1/platform/scheduleBackup`,
    backupNowUrl: `http://localhost:8000/api/v1/platform/backupNow`,



}
