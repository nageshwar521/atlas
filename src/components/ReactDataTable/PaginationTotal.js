import React from 'react';

const PaginationTotal = ({from, to, total}) => {
  return (
    <div className="react-datatable--pagination-total">
      Showing {from} to {to < total ? to : total} of {total} results
    </div>
  );
}

export default PaginationTotal;