import React from 'react';

const SearchField = (props) => {
  return (
    <div className="input-group pull-right config-management--search-box">
      <input
        className="form-control config-search"
        placeholder="Search..."
        onChange={evt => props.onSearch(evt.target.value)}
        type="text"
      />
    </div>
  );
};

export default SearchField;