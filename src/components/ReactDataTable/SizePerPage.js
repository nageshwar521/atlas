import React from 'react';
import ReactSelect from '../ReactSelect/ReactSelect';
import cn from 'classnames';
import { css } from 'emotion';

const Option = ({ cx, children, getStyles, innerRef, ...props }) => {
  const optionClass = cx(
    css(getStyles('option', props)),
    {
      'option': true,
      'option--is-disabled': props.isDisabled,
      'option--is-focused': props.isFocused,
      'option--is-selected': props.isSelected,
    },
    'reactSelect--custom-option'
  );
  return (
    <div className={optionClass} {...props.innerProps}>
      <span className="reactSelect--custom-option--text">{props.data.label}</span>
    </div>
  );
}

const getOptions = (props) => {
  let options = [
    {
      label: '10', value: 10
    }, {
      label: '20', value: 20
    }, {
      label: 'All', value: props.totalCount
    }
  ];
  return options;
}

const SizePerPage = (props) => {
  const {
    optionRenderer,
    isSearchable = false,
    onChange,
    selectedPageSize,
    ...restProps
  } = props;
  const options = getOptions(props);
  const components = {
    Option: Option
  };
  const customStyles = {
    option: (provided) => ({
      ...provided,
      width: '80px'
    }),
  };
  return (
    <div className="react-datatable--size-per-page">
      <ReactSelect
        customStyles={customStyles}
        isSearchable={isSearchable}
        options={options}
        components={components}
        handleChange={onChange}
        selectedOption={selectedPageSize}
        {...restProps} />
    </div>
  );
}

export default SizePerPage;