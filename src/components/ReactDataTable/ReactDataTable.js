import React from 'react';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import BootstrapTable from 'react-bootstrap-table-next';
import Pagination from "react-js-pagination";
import cn from 'classnames';
import { css } from 'emotion';
import moment from 'moment';
import ActionsDropdown from './ActionsDropdown';
import SearchField from './SearchField';
import PaginationTotal from './PaginationTotal';
import SizePerPage from './SizePerPage';

class ReactDataTable extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedPageSize: 10
    };
    this.onSizePerPageChange = this.onSizePerPageChange.bind(this);
  }
  selectRow() {
    const { selectedRows = [] } = this.props;
    return {
      mode: 'checkbox',
      classes: 'selection-row',
      selected: selectedRows.map(row => row.id),
      onSelect: this.props.onSelectRow,
      onSelectAll: this.props.onSelectAllRows
    };
  }
  isOptionDisabled(option) {
    // console.log(option);
    return option.isOptionDisabled ? option.isOptionDisabled : false;
  }
  expandRow() {
    return {
      renderer: row => (
        <BootstrapTable
          keyField='id'
          data={this.props.subtableData}
          columns={this.props.columns}
        />
      ),
      showExpandColumn: true,
      expandHeaderColumnRenderer: ({ isAnyExpands }) => {
        return null;
      },
      expandColumnRenderer: ({ expanded }) => {
        if (expanded) {
          return (
            <i className="fa fa-caret-down" />
          );
        }
        return (
          <i className="fa fa-caret-right" />
        );
      }
    };
  }
  onSizePerPageChange(selectedPageSize) {
    this.setState({
      selectedPageSize: selectedPageSize.value
    });
  }
  render() {
    const { 
      data = [], 
      columns = [], 
      header,
      sizePerPage, 
      currentPage,
      selectedRows = [],
      rowSelectable = false,
      titleText = "",
      selectedAction,
      totalRows = 0,
      search = false,
      options,
      hiddenRows
    } = this.props;
    const {
      selectedPageSize
    } = this.state;
    // console.log("ReactDataTable this.props");
    // console.log(this.props);
    return (
      <div className="react-datatable--container">
        <ToolkitProvider
          keyField='id'
          data={data}
          columns={columns}
        >
          {
            props => {
              // console.log(props);
              return (
                <React.Fragment>
                  <div className="react-datatable--header clearfix">
                    <h5 className="react-datatable--title-text col-xs-3 pull-left no-padding-left">
                      {selectedRows.length} {titleText}
                    </h5>
                    <div className="react-datatable--table-controls col-xs-9 pull-right no-padding">
                      <ActionsDropdown
                        options={options}
                        selectedRows={selectedRows}
                        onChange={this.props.onActionChange}
                        isOptionDisabled={this.isOptionDisabled}
                      />
                      <React.Fragment>
                        {search ? <SearchField onSearch={this.props.onSearch} /> : null}
                        {header}
                      </React.Fragment>
                    </div>
                  </div>
                  <div className="react-datatable--table-wrapper">
                    <BootstrapTable
                      selectRow={rowSelectable && this.selectRow()}
                      noDataIndication={'No Results Found'}
                      {...props.baseProps}
                    />
                  </div>
                  <div className="react-datatable--footer">
                    <div className="react-datatable--footer--wrapper">
                      <div className="react-datatable--footer--pagination-total">
                        <PaginationTotal
                          from={(((currentPage - 1) * sizePerPage) + 1)}
                          to={(totalRows < (currentPage * sizePerPage)) ? totalRows : (currentPage * sizePerPage)}
                          total={totalRows}
                        />
                      </div>
                      <div className="react-datatable--footer--pagination clearfix">
                        <Pagination
                          innerClass="pagination react-datatable--pagination"
                          activePage={currentPage}
                          itemsCountPerPage={sizePerPage}
                          totalItemsCount={totalRows}
                          pageRangeDisplayed={10}
                          hideFirstLastPages={true}
                          onChange={this.props.onPageChange} />
                      </div>
                      <div className="react-datatable--footer--size-per-page">
                        <SizePerPage
                          selectedPageSize={selectedPageSize}
                          onChange={this.onSizePerPageChange}
                          totalCount={totalRows}
                        />
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              );
            }
          }
        </ToolkitProvider>
      </div>
    );
  }
}

export default ReactDataTable;