import React, { Component } from 'react';
import ReactSelect from '../ReactSelect/ReactSelect';
import cn from 'classnames';
import { css } from 'emotion';

// const getOptions = (props) => {
//   // console.log(props);
//   const { options, selectedRows = [] } = props;
//   return options.map((option) => {
//     if (selectedRows.length >= 1) {
//       if ((option.value === "Download") || (option.value === "Delete")) {
//         option.isOptionDisabled = false;
//       } else {
//         option.isOptionDisabled = true;
//       }
//     } else {
//       option.isOptionDisabled = true;
//     }
//     return option;
//   });
// };

const Option = ({ cx, children, getStyles, innerRef, ...props }) => {
    const optionClass = cx(
      css(getStyles('option', props)),
      {
        'option': true,
        'option--is-disabled': props.isDisabled,
        'option--is-focused': props.isFocused,
        'option--is-selected': props.isSelected,
      },
      'reactSelect--custom-option'
    );
    return (
      <div className={optionClass} {...props.innerProps}>
        <span className={cn("reactSelect--custom-option--icon", props.data.iconClass)} />
        <span className="reactSelect--custom-option--text">{props.data.label}</span>
      </div>
    );
  }

const ActionsDropdown = (props) => {
  const { 
    optionRenderer,
    isSearchable = false, 
    onChange, 
    placeholder = "Actions",
    selectedAction,
    options,
    ...restProps 
  } = props;
  // const options = getOptions(props);
  const components = {
    Option: optionRenderer ? optionRenderer : Option
  };
  const customStyles = {
    option: (provided) => ({
      ...provided,
      width: '150px'
    }),
  };
  return (
    <div className="react-datatable--actions-dropdown pull-right">
      <ReactSelect
        customStyles={customStyles}
        placeholder={placeholder}
        isSearchable={isSearchable}
        options={options}
        components={components}
        handleChange={onChange}
        selectedOption={selectedAction}
        {...restProps} />
    </div>
  );
}

export default ActionsDropdown;