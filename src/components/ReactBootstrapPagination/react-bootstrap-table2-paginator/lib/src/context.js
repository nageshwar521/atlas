'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _const = require('./const');

var _const2 = _interopRequireDefault(_const);

var _bootstrap = require('./bootstrap');

var _pagination = require('./pagination');

var _pagination2 = _interopRequireDefault(_pagination);

var _page = require('./page');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/prop-types: 0 */
/* eslint react/require-default-props: 0 */
/* eslint no-lonely-if: 0 */


exports.default = function (isRemotePagination, handleRemotePageChange) {
  var PaginationContext = _react2.default.createContext();

  var PaginationProvider = function (_React$Component) {
    _inherits(PaginationProvider, _React$Component);

    function PaginationProvider(props) {
      _classCallCheck(this, PaginationProvider);

      var _this = _possibleConstructorReturn(this, (PaginationProvider.__proto__ || Object.getPrototypeOf(PaginationProvider)).call(this, props));

      _this.handleChangePage = _this.handleChangePage.bind(_this);
      _this.handleChangeSizePerPage = _this.handleChangeSizePerPage.bind(_this);
      _this.onDropdownToggle = _this.onDropdownToggle.bind(_this);

      var currPage = void 0;
      var currSizePerPage = void 0;
      var options = props.pagination.options;

      var sizePerPageList = options.sizePerPageList || _const2.default.SIZE_PER_PAGE_LIST;

      // initialize current page
      if (typeof options.page !== 'undefined') {
        currPage = options.page;
      } else if (typeof options.pageStartIndex !== 'undefined') {
        currPage = options.pageStartIndex;
      } else {
        currPage = _const2.default.PAGE_START_INDEX;
      }

      // initialize current sizePerPage
      if (typeof options.sizePerPage !== 'undefined') {
        currSizePerPage = options.sizePerPage;
      } else if (_typeof(sizePerPageList[0]) === 'object') {
        currSizePerPage = sizePerPageList[0].value;
      } else {
        currSizePerPage = sizePerPageList[0];
      }

      _this.currPage = currPage;
      _this.currSizePerPage = currSizePerPage;
      return _this;
    }

    _createClass(PaginationProvider, [{
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(nextProps) {
        var needNewState = false;
        var currPage = this.currPage;
        var currSizePerPage = this.currSizePerPage;
        var onPageChange = nextProps.pagination.options.onPageChange;


        var pageStartIndex = typeof nextProps.pagination.options.pageStartIndex !== 'undefined' ? nextProps.pagination.options.pageStartIndex : _const2.default.PAGE_START_INDEX;

        // user should align the page when the page is not fit to the data size when remote enable
        if (!isRemotePagination()) {
          var newPage = (0, _page.alignPage)(nextProps.data, currPage, currSizePerPage, pageStartIndex);
          if (currPage !== newPage) {
            currPage = newPage;
            needNewState = true;
          }
        } else {
          this.currPage = nextProps.pagination.options.page;
          this.currSizePerPage = nextProps.pagination.options.sizePerPage;
        }

        if (needNewState) {
          if (onPageChange) {
            onPageChange(currPage, currSizePerPage);
          }
          this.currPage = currPage;
          this.currSizePerPage = currSizePerPage;
        }
      }
    }, {
        key: 'onDropdownToggle',
        value: function onDropdownToggle(state) {
          var options = this.props.pagination.options;
          if (options.onDropdownToggle) {
            options.onDropdownToggle(state);
          }
        }
    }, {
      key: 'handleChangePage',
      value: function handleChangePage(currPage) {
        var currSizePerPage = this.currSizePerPage;
        var options = this.props.pagination.options;


        if (options.onPageChange) {
          options.onPageChange(currPage, currSizePerPage);
        }

        this.currPage = currPage;

        if (isRemotePagination()) {
          handleRemotePageChange(currPage, currSizePerPage);
          return;
        }
        this.forceUpdate();
      }
    }, {
      key: 'handleChangeSizePerPage',
      value: function handleChangeSizePerPage(currSizePerPage, currPage) {
        var options = this.props.pagination.options;


        if (options.onSizePerPageChange) {
          options.onSizePerPageChange(currSizePerPage, currPage);
        }

        this.currPage = currPage;
        this.currSizePerPage = currSizePerPage;

        if (isRemotePagination()) {
          handleRemotePageChange(currPage, currSizePerPage);
          return;
        }
        this.forceUpdate();
      }
    }, {
      key: 'render',
      value: function render() {
        var data = this.props.data;
        var _props = this.props,
            options = _props.pagination.options,
            bootstrap4 = _props.bootstrap4;
        var currPage = this.currPage,
            currSizePerPage = this.currSizePerPage;

        var withFirstAndLast = typeof options.withFirstAndLast === 'undefined' ? _const2.default.With_FIRST_AND_LAST : options.withFirstAndLast;
        var alwaysShowAllBtns = typeof options.alwaysShowAllBtns === 'undefined' ? _const2.default.SHOW_ALL_PAGE_BTNS : options.alwaysShowAllBtns;
        var hideSizePerPage = typeof options.hideSizePerPage === 'undefined' ? _const2.default.HIDE_SIZE_PER_PAGE : options.hideSizePerPage;
        var hidePageListOnlyOnePage = typeof options.hidePageListOnlyOnePage === 'undefined' ? _const2.default.HIDE_PAGE_LIST_ONLY_ONE_PAGE : options.hidePageListOnlyOnePage;
        var pageStartIndex = typeof options.pageStartIndex === 'undefined' ? _const2.default.PAGE_START_INDEX : options.pageStartIndex;

        data = isRemotePagination() ? data : (0, _page.getByCurrPage)(data, currPage, currSizePerPage, pageStartIndex);

        return _react2.default.createElement(
          PaginationContext.Provider,
          { value: { data: data } },
          this.props.children,
          _react2.default.createElement(
            _bootstrap.BootstrapContext.Provider,
            { value: { bootstrap4: bootstrap4 } },
            _react2.default.createElement(_pagination2.default, {
              key: 'pagination',
              dataSize: options.totalSize || this.props.data.length,
              currPage: currPage,
              currSizePerPage: currSizePerPage,
              onPageChange: this.handleChangePage,
              onSizePerPageChange: this.handleChangeSizePerPage,
              onDropdownToggle: this.onDropdownToggle,
              sizePerPageList: options.sizePerPageList || _const2.default.SIZE_PER_PAGE_LIST,
              paginationSize: options.paginationSize || _const2.default.PAGINATION_SIZE,
              pageStartIndex: pageStartIndex,
              withFirstAndLast: withFirstAndLast,
              alwaysShowAllBtns: alwaysShowAllBtns,
              hideSizePerPage: hideSizePerPage,
              hidePageListOnlyOnePage: hidePageListOnlyOnePage,
              showTotal: options.showTotal,
              paginationTotalRenderer: options.paginationTotalRenderer,
              firstPageText: options.firstPageText || _const2.default.FIRST_PAGE_TEXT,
              prePageText: options.prePageText || _const2.default.PRE_PAGE_TEXT,
              nextPageText: options.nextPageText || _const2.default.NEXT_PAGE_TEXT,
              lastPageText: options.lastPageText || _const2.default.LAST_PAGE_TEXT,
              prePageTitle: options.prePageTitle || _const2.default.PRE_PAGE_TITLE,
              nextPageTitle: options.nextPageTitle || _const2.default.NEXT_PAGE_TITLE,
              firstPageTitle: options.firstPageTitle || _const2.default.FIRST_PAGE_TITLE,
              lastPageTitle: options.lastPageTitle || _const2.default.LAST_PAGE_TITLE
            })
          )
        );
      }
    }]);

    return PaginationProvider;
  }(_react2.default.Component);

  PaginationProvider.propTypes = {
    data: _propTypes2.default.array.isRequired
  };


  return {
    Provider: PaginationProvider,
    Consumer: PaginationContext.Consumer
  };
};