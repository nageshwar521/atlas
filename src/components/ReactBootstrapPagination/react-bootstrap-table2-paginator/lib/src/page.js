"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var getNormalizedPage = function getNormalizedPage(page, pageStartIndex) {
  var offset = Math.abs(1 - pageStartIndex);
  return page + offset;
};

var endIndex = function endIndex(page, sizePerPage, pageStartIndex) {
  return getNormalizedPage(page, pageStartIndex) * sizePerPage - 1;
};

var startIndex = function startIndex(end, sizePerPage) {
  return end - (sizePerPage - 1);
};

var alignPage = exports.alignPage = function alignPage(data, page, sizePerPage, pageStartIndex) {
  var dataSize = data.length;

  if (page < pageStartIndex || page > Math.floor(dataSize / sizePerPage) + pageStartIndex) {
    return pageStartIndex;
  }
  return page;
};

var getByCurrPage = exports.getByCurrPage = function getByCurrPage(data, page, sizePerPage, pageStartIndex) {
  var dataSize = data.length;
  if (!dataSize) return [];

  var end = endIndex(page, sizePerPage, pageStartIndex);
  var start = startIndex(end, sizePerPage);

  var result = [];
  for (var i = start; i <= end; i += 1) {
    result.push(data[i]);
    if (i + 1 === dataSize) break;
  }
  return result;
};