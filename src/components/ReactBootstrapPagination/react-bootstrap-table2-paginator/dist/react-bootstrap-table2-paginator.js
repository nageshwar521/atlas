(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["react"], factory);
	else if(typeof exports === 'object')
		exports["ReactBootstrapTable2Paginator"] = factory(require("react"));
	else
		root["ReactBootstrapTable2Paginator"] = factory(root["React"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

if (false) {
  var REACT_ELEMENT_TYPE = (typeof Symbol === 'function' &&
    Symbol.for &&
    Symbol.for('react.element')) ||
    0xeac7;

  var isValidElement = function(object) {
    return typeof object === 'object' &&
      object !== null &&
      object.$$typeof === REACT_ELEMENT_TYPE;
  };

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = require('./factoryWithTypeCheckers')(isValidElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = __webpack_require__(7)();
}


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  PAGINATION_SIZE: 5,
  PAGE_START_INDEX: 1,
  With_FIRST_AND_LAST: true,
  SHOW_ALL_PAGE_BTNS: false,
  SHOW_TOTAL: false,
  PAGINATION_TOTAL: null,
  FIRST_PAGE_TEXT: '<<',
  PRE_PAGE_TEXT: '<',
  NEXT_PAGE_TEXT: '>',
  LAST_PAGE_TEXT: '>>',
  NEXT_PAGE_TITLE: 'next page',
  LAST_PAGE_TITLE: 'last page',
  PRE_PAGE_TITLE: 'previous page',
  FIRST_PAGE_TITLE: 'first page',
  SIZE_PER_PAGE_LIST: [10, 25, 30, 50],
  HIDE_SIZE_PER_PAGE: false,
  HIDE_PAGE_LIST_ONLY_ONE_PAGE: false
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(null, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function () {
			return classNames;
		}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BootstrapContext = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// consider to have a common lib?1
var BootstrapContext = exports.BootstrapContext = _react2.default.createContext({
  bootstrap4: false
});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _context = __webpack_require__(6);

var _context2 = _interopRequireDefault(_context);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return {
    createContext: _context2.default,
    options: options
  };
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _const = __webpack_require__(2);

var _const2 = _interopRequireDefault(_const);

var _bootstrap = __webpack_require__(4);

var _pagination = __webpack_require__(11);

var _pagination2 = _interopRequireDefault(_pagination);

var _page = __webpack_require__(18);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/prop-types: 0 */
/* eslint react/require-default-props: 0 */
/* eslint no-lonely-if: 0 */


exports.default = function (isRemotePagination, handleRemotePageChange) {
  var PaginationContext = _react2.default.createContext();

  var PaginationProvider = function (_React$Component) {
    _inherits(PaginationProvider, _React$Component);

    function PaginationProvider(props) {
      _classCallCheck(this, PaginationProvider);

      var _this = _possibleConstructorReturn(this, (PaginationProvider.__proto__ || Object.getPrototypeOf(PaginationProvider)).call(this, props));

      _this.handleChangePage = _this.handleChangePage.bind(_this);
      _this.handleChangeSizePerPage = _this.handleChangeSizePerPage.bind(_this);

      var currPage = void 0;
      var currSizePerPage = void 0;
      var options = props.pagination.options;

      var sizePerPageList = options.sizePerPageList || _const2.default.SIZE_PER_PAGE_LIST;

      // initialize current page
      if (typeof options.page !== 'undefined') {
        currPage = options.page;
      } else if (typeof options.pageStartIndex !== 'undefined') {
        currPage = options.pageStartIndex;
      } else {
        currPage = _const2.default.PAGE_START_INDEX;
      }

      // initialize current sizePerPage
      if (typeof options.sizePerPage !== 'undefined') {
        currSizePerPage = options.sizePerPage;
      } else if (_typeof(sizePerPageList[0]) === 'object') {
        currSizePerPage = sizePerPageList[0].value;
      } else {
        currSizePerPage = sizePerPageList[0];
      }

      _this.currPage = currPage;
      _this.currSizePerPage = currSizePerPage;
      return _this;
    }

    _createClass(PaginationProvider, [{
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(nextProps) {
        var needNewState = false;
        var currPage = this.currPage;
        var currSizePerPage = this.currSizePerPage;
        var onPageChange = nextProps.pagination.options.onPageChange;


        var pageStartIndex = typeof nextProps.pagination.options.pageStartIndex !== 'undefined' ? nextProps.pagination.options.pageStartIndex : _const2.default.PAGE_START_INDEX;

        // user should align the page when the page is not fit to the data size when remote enable
        if (!isRemotePagination()) {
          var newPage = (0, _page.alignPage)(nextProps.data, currPage, currSizePerPage, pageStartIndex);
          if (currPage !== newPage) {
            currPage = newPage;
            needNewState = true;
          }
        } else {
          this.currPage = nextProps.pagination.options.page;
          this.currSizePerPage = nextProps.pagination.options.sizePerPage;
        }

        if (needNewState) {
          if (onPageChange) {
            onPageChange(currPage, currSizePerPage);
          }
          this.currPage = currPage;
          this.currSizePerPage = currSizePerPage;
        }
      }
    }, {
      key: 'handleChangePage',
      value: function handleChangePage(currPage) {
        var currSizePerPage = this.currSizePerPage;
        var options = this.props.pagination.options;


        if (options.onPageChange) {
          options.onPageChange(currPage, currSizePerPage);
        }

        this.currPage = currPage;

        if (isRemotePagination()) {
          handleRemotePageChange(currPage, currSizePerPage);
          return;
        }
        this.forceUpdate();
      }
    }, {
      key: 'handleChangeSizePerPage',
      value: function handleChangeSizePerPage(currSizePerPage, currPage) {
        var options = this.props.pagination.options;


        if (options.onSizePerPageChange) {
          options.onSizePerPageChange(currSizePerPage, currPage);
        }

        this.currPage = currPage;
        this.currSizePerPage = currSizePerPage;

        if (isRemotePagination()) {
          handleRemotePageChange(currPage, currSizePerPage);
          return;
        }
        this.forceUpdate();
      }
    }, {
      key: 'render',
      value: function render() {
        var data = this.props.data;
        var _props = this.props,
            options = _props.pagination.options,
            bootstrap4 = _props.bootstrap4;
        var currPage = this.currPage,
            currSizePerPage = this.currSizePerPage;

        var withFirstAndLast = typeof options.withFirstAndLast === 'undefined' ? _const2.default.With_FIRST_AND_LAST : options.withFirstAndLast;
        var alwaysShowAllBtns = typeof options.alwaysShowAllBtns === 'undefined' ? _const2.default.SHOW_ALL_PAGE_BTNS : options.alwaysShowAllBtns;
        var hideSizePerPage = typeof options.hideSizePerPage === 'undefined' ? _const2.default.HIDE_SIZE_PER_PAGE : options.hideSizePerPage;
        var hidePageListOnlyOnePage = typeof options.hidePageListOnlyOnePage === 'undefined' ? _const2.default.HIDE_PAGE_LIST_ONLY_ONE_PAGE : options.hidePageListOnlyOnePage;
        var pageStartIndex = typeof options.pageStartIndex === 'undefined' ? _const2.default.PAGE_START_INDEX : options.pageStartIndex;

        data = isRemotePagination() ? data : (0, _page.getByCurrPage)(data, currPage, currSizePerPage, pageStartIndex);

        return _react2.default.createElement(
          PaginationContext.Provider,
          { value: { data: data } },
          this.props.children,
          _react2.default.createElement(
            _bootstrap.BootstrapContext.Provider,
            { value: { bootstrap4: bootstrap4 } },
            _react2.default.createElement(_pagination2.default, {
              key: 'pagination',
              dataSize: options.totalSize || this.props.data.length,
              currPage: currPage,
              currSizePerPage: currSizePerPage,
              onPageChange: this.handleChangePage,
              onSizePerPageChange: this.handleChangeSizePerPage,
              sizePerPageList: options.sizePerPageList || _const2.default.SIZE_PER_PAGE_LIST,
              paginationSize: options.paginationSize || _const2.default.PAGINATION_SIZE,
              pageStartIndex: pageStartIndex,
              withFirstAndLast: withFirstAndLast,
              alwaysShowAllBtns: alwaysShowAllBtns,
              hideSizePerPage: hideSizePerPage,
              hidePageListOnlyOnePage: hidePageListOnlyOnePage,
              showTotal: options.showTotal,
              paginationTotalRenderer: options.paginationTotalRenderer,
              firstPageText: options.firstPageText || _const2.default.FIRST_PAGE_TEXT,
              prePageText: options.prePageText || _const2.default.PRE_PAGE_TEXT,
              nextPageText: options.nextPageText || _const2.default.NEXT_PAGE_TEXT,
              lastPageText: options.lastPageText || _const2.default.LAST_PAGE_TEXT,
              prePageTitle: options.prePageTitle || _const2.default.PRE_PAGE_TITLE,
              nextPageTitle: options.nextPageTitle || _const2.default.NEXT_PAGE_TITLE,
              firstPageTitle: options.firstPageTitle || _const2.default.FIRST_PAGE_TITLE,
              lastPageTitle: options.lastPageTitle || _const2.default.LAST_PAGE_TITLE
            })
          )
        );
      }
    }]);

    return PaginationProvider;
  }(_react2.default.Component);

  PaginationProvider.propTypes = {
    data: _propTypes2.default.array.isRequired
  };


  return {
    Provider: PaginationProvider,
    Consumer: PaginationContext.Consumer
  };
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */



var emptyFunction = __webpack_require__(8);
var invariant = __webpack_require__(9);
var ReactPropTypesSecret = __webpack_require__(10);

module.exports = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    invariant(
      false,
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
  };
  shim.isRequired = shim;
  function getShim() {
    return shim;
  };
  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim
  };

  ReactPropTypes.checkPropTypes = emptyFunction;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

function makeEmptyFunction(arg) {
  return function () {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
var emptyFunction = function emptyFunction() {};

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function () {
  return this;
};
emptyFunction.thatReturnsArgument = function (arg) {
  return arg;
};

module.exports = emptyFunction;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



/**
 * Use invariant() to assert state which your program assumes to be true.
 *
 * Provide sprintf-style format (only %s is supported) and arguments
 * to provide information about what broke and what you were
 * expecting.
 *
 * The invariant message will be stripped in production, but the invariant
 * will remain to ensure logic does not differ in production.
 */

var validateFormat = function validateFormat(format) {};

if (false) {
  validateFormat = function validateFormat(format) {
    if (format === undefined) {
      throw new Error('invariant requires an error message argument');
    }
  };
}

function invariant(condition, format, a, b, c, d, e, f) {
  validateFormat(format);

  if (!condition) {
    var error;
    if (format === undefined) {
      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
    } else {
      var args = [a, b, c, d, e, f];
      var argIndex = 0;
      error = new Error(format.replace(/%s/g, function () {
        return args[argIndex++];
      }));
      error.name = 'Invariant Violation';
    }

    error.framesToPop = 1; // we don't care about invariant's own frame
    throw error;
  }
}

module.exports = invariant;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _classnames = __webpack_require__(3);

var _classnames2 = _interopRequireDefault(_classnames);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _pageResolver2 = __webpack_require__(12);

var _pageResolver3 = _interopRequireDefault(_pageResolver2);

var _sizePerPageDropdown = __webpack_require__(13);

var _sizePerPageDropdown2 = _interopRequireDefault(_sizePerPageDropdown);

var _paginationList = __webpack_require__(15);

var _paginationList2 = _interopRequireDefault(_paginationList);

var _paginationTotal = __webpack_require__(17);

var _paginationTotal2 = _interopRequireDefault(_paginationTotal);

var _const = __webpack_require__(2);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/require-default-props: 0 */
/* eslint arrow-body-style: 0 */


var Pagination = function (_pageResolver) {
  _inherits(Pagination, _pageResolver);

  function Pagination(props) {
    _classCallCheck(this, Pagination);

    var _this = _possibleConstructorReturn(this, (Pagination.__proto__ || Object.getPrototypeOf(Pagination)).call(this, props));

    _this.defaultTotal = function (from, to, size) {
      return _react2.default.createElement(_paginationTotal2.default, {
        from: from,
        to: to,
        dataSize: size
      });
    };

    _this.setTotal = function (from, to, size, total) {
      if (total && typeof total === 'function') {
        return total(from, to, size);
      }

      return _this.defaultTotal(from, to, size);
    };

    _this.closeDropDown = _this.closeDropDown.bind(_this);
    _this.toggleDropDown = _this.toggleDropDown.bind(_this);
    _this.handleChangePage = _this.handleChangePage.bind(_this);
    _this.handleChangeSizePerPage = _this.handleChangeSizePerPage.bind(_this);
    _this.state = _this.initialState();
    return _this;
  }

  _createClass(Pagination, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var dataSize = nextProps.dataSize,
          currSizePerPage = nextProps.currSizePerPage;

      if (currSizePerPage !== this.props.currSizePerPage || dataSize !== this.props.dataSize) {
        var totalPages = this.calculateTotalPage(currSizePerPage, dataSize);
        var lastPage = this.calculateLastPage(totalPages);
        this.setState({ totalPages: totalPages, lastPage: lastPage });
      }
    }
  }, {
    key: 'toggleDropDown',
    value: function toggleDropDown() {
      var dropdownOpen = !this.state.dropdownOpen;
      this.setState(function () {
        return { dropdownOpen: dropdownOpen };
      });
    }
  }, {
    key: 'closeDropDown',
    value: function closeDropDown() {
      this.setState(function () {
        return { dropdownOpen: false };
      });
    }
  }, {
    key: 'handleChangeSizePerPage',
    value: function handleChangeSizePerPage(sizePerPage) {
      var _props = this.props,
          currSizePerPage = _props.currSizePerPage,
          onSizePerPageChange = _props.onSizePerPageChange;

      var selectedSize = typeof sizePerPage === 'string' ? parseInt(sizePerPage, 10) : sizePerPage;
      var currPage = this.props.currPage;

      if (selectedSize !== currSizePerPage) {
        var newTotalPages = this.calculateTotalPage(selectedSize);
        var newLastPage = this.calculateLastPage(newTotalPages);
        if (currPage > newLastPage) currPage = newLastPage;
        onSizePerPageChange(selectedSize, currPage);
      }
      this.closeDropDown();
    }
  }, {
    key: 'handleChangePage',
    value: function handleChangePage(newPage) {
      var page = void 0;
      var _props2 = this.props,
          currPage = _props2.currPage,
          pageStartIndex = _props2.pageStartIndex,
          prePageText = _props2.prePageText,
          nextPageText = _props2.nextPageText,
          lastPageText = _props2.lastPageText,
          firstPageText = _props2.firstPageText,
          onPageChange = _props2.onPageChange;
      var lastPage = this.state.lastPage;


      if (newPage === prePageText) {
        page = this.backToPrevPage();
      } else if (newPage === nextPageText) {
        page = currPage + 1 > lastPage ? lastPage : currPage + 1;
      } else if (newPage === lastPageText) {
        page = lastPage;
      } else if (newPage === firstPageText) {
        page = pageStartIndex;
      } else {
        page = parseInt(newPage, 10);
      }

      // if (keepSizePerPageState) { this.closeDropDown(); }

      if (page !== currPage) {
        onPageChange(page);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          totalPages = _state.totalPages,
          lastPage = _state.lastPage,
          open = _state.dropdownOpen;
      var _props3 = this.props,
          showTotal = _props3.showTotal,
          dataSize = _props3.dataSize,
          paginationTotalRenderer = _props3.paginationTotalRenderer,
          sizePerPageList = _props3.sizePerPageList,
          currSizePerPage = _props3.currSizePerPage,
          hideSizePerPage = _props3.hideSizePerPage,
          hidePageListOnlyOnePage = _props3.hidePageListOnlyOnePage;

      var pages = this.calculatePageStatus(this.calculatePages(totalPages), lastPage);

      var _calculateFromTo = this.calculateFromTo(),
          _calculateFromTo2 = _slicedToArray(_calculateFromTo, 2),
          from = _calculateFromTo2[0],
          to = _calculateFromTo2[1];

      var pageListClass = (0, _classnames2.default)('react-bootstrap-table-pagination-list', 'col-md-6 col-xs-6 col-sm-6 col-lg-6', {
        'react-bootstrap-table-pagination-list-hidden': hidePageListOnlyOnePage && totalPages === 1
      });
      return _react2.default.createElement(
        'div',
        { className: 'row react-bootstrap-table-pagination' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-6 col-xs-6 col-sm-6 col-lg-6' },
          sizePerPageList.length > 1 && !hideSizePerPage ? _react2.default.createElement(_sizePerPageDropdown2.default, {
            currSizePerPage: '' + currSizePerPage,
            options: this.calculateSizePerPageStatus(),
            onSizePerPageChange: this.handleChangeSizePerPage,
            onClick: this.toggleDropDown,
            onBlur: this.closeDropDown,
            open: open
          }) : null,
          showTotal ? this.setTotal(from, to, dataSize, paginationTotalRenderer) : null
        ),
        _react2.default.createElement(
          'div',
          { className: pageListClass },
          _react2.default.createElement(_paginationList2.default, { pages: pages, onPageChange: this.handleChangePage })
        )
      );
    }
  }]);

  return Pagination;
}((0, _pageResolver3.default)(_react.Component));

Pagination.propTypes = {
  dataSize: _propTypes2.default.number.isRequired,
  sizePerPageList: _propTypes2.default.array.isRequired,
  currPage: _propTypes2.default.number.isRequired,
  currSizePerPage: _propTypes2.default.number.isRequired,
  onPageChange: _propTypes2.default.func.isRequired,
  onSizePerPageChange: _propTypes2.default.func.isRequired,
  pageStartIndex: _propTypes2.default.number,
  paginationSize: _propTypes2.default.number,
  showTotal: _propTypes2.default.bool,
  paginationTotalRenderer: _propTypes2.default.func,
  firstPageText: _propTypes2.default.string,
  prePageText: _propTypes2.default.string,
  nextPageText: _propTypes2.default.string,
  lastPageText: _propTypes2.default.string,
  nextPageTitle: _propTypes2.default.string,
  prePageTitle: _propTypes2.default.string,
  firstPageTitle: _propTypes2.default.string,
  lastPageTitle: _propTypes2.default.string,
  withFirstAndLast: _propTypes2.default.bool,
  alwaysShowAllBtns: _propTypes2.default.bool,
  hideSizePerPage: _propTypes2.default.bool,
  hidePageListOnlyOnePage: _propTypes2.default.bool
};

Pagination.defaultProps = {
  pageStartIndex: _const2.default.PAGE_START_INDEX,
  paginationSize: _const2.default.PAGINATION_SIZE,
  withFirstAndLast: _const2.default.With_FIRST_AND_LAST,
  alwaysShowAllBtns: _const2.default.SHOW_ALL_PAGE_BTNS,
  showTotal: _const2.default.SHOW_TOTAL,
  paginationTotalRenderer: _const2.default.PAGINATION_TOTAL,
  firstPageText: _const2.default.FIRST_PAGE_TEXT,
  prePageText: _const2.default.PRE_PAGE_TEXT,
  nextPageText: _const2.default.NEXT_PAGE_TEXT,
  lastPageText: _const2.default.LAST_PAGE_TEXT,
  sizePerPageList: _const2.default.SIZE_PER_PAGE_LIST,
  nextPageTitle: _const2.default.NEXT_PAGE_TITLE,
  prePageTitle: _const2.default.PRE_PAGE_TITLE,
  firstPageTitle: _const2.default.FIRST_PAGE_TITLE,
  lastPageTitle: _const2.default.LAST_PAGE_TITLE,
  hideSizePerPage: _const2.default.HIDE_SIZE_PER_PAGE,
  hidePageListOnlyOnePage: _const2.default.HIDE_PAGE_LIST_ONLY_ONE_PAGE
};

exports.default = Pagination;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _const = __webpack_require__(2);

var _const2 = _interopRequireDefault(_const);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint no-mixed-operators: 0 */


exports.default = function (ExtendBase) {
  return function (_ExtendBase) {
    _inherits(PageResolver, _ExtendBase);

    function PageResolver() {
      _classCallCheck(this, PageResolver);

      return _possibleConstructorReturn(this, (PageResolver.__proto__ || Object.getPrototypeOf(PageResolver)).apply(this, arguments));
    }

    _createClass(PageResolver, [{
      key: 'backToPrevPage',
      value: function backToPrevPage() {
        var _props = this.props,
            currPage = _props.currPage,
            pageStartIndex = _props.pageStartIndex;

        return currPage - 1 < pageStartIndex ? pageStartIndex : currPage - 1;
      }
    }, {
      key: 'goToNextPage',
      value: function goToNextPage() {
        var currPage = this.props.currPage;
        var lastPage = this.state.lastPage;

        return currPage + 1 > lastPage ? lastPage : currPage + 1;
      }
    }, {
      key: 'initialState',
      value: function initialState() {
        var totalPages = this.calculateTotalPage();
        var lastPage = this.calculateLastPage(totalPages);
        return { totalPages: totalPages, lastPage: lastPage, dropdownOpen: false };
      }
    }, {
      key: 'calculateTotalPage',
      value: function calculateTotalPage() {
        var sizePerPage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props.currSizePerPage;
        var dataSize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.props.dataSize;

        return Math.ceil(dataSize / sizePerPage);
      }
    }, {
      key: 'calculateLastPage',
      value: function calculateLastPage(totalPages) {
        var pageStartIndex = this.props.pageStartIndex;

        return pageStartIndex + totalPages - 1;
      }
    }, {
      key: 'calculateFromTo',
      value: function calculateFromTo() {
        var _props2 = this.props,
            dataSize = _props2.dataSize,
            currPage = _props2.currPage,
            currSizePerPage = _props2.currSizePerPage,
            pageStartIndex = _props2.pageStartIndex;

        var offset = Math.abs(_const2.default.PAGE_START_INDEX - pageStartIndex);

        var from = (currPage - pageStartIndex) * currSizePerPage;
        from = dataSize === 0 ? 0 : from + 1;
        var to = Math.min(currSizePerPage * (currPage + offset), dataSize);
        if (to > dataSize) to = dataSize;

        return [from, to];
      }
    }, {
      key: 'calculatePages',
      value: function calculatePages() {
        var totalPages = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.totalPages;
        var lastPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.state.lastPage;
        var _props3 = this.props,
            currPage = _props3.currPage,
            paginationSize = _props3.paginationSize,
            pageStartIndex = _props3.pageStartIndex,
            withFirstAndLast = _props3.withFirstAndLast,
            firstPageText = _props3.firstPageText,
            prePageText = _props3.prePageText,
            nextPageText = _props3.nextPageText,
            lastPageText = _props3.lastPageText,
            alwaysShowAllBtns = _props3.alwaysShowAllBtns;


        var pages = void 0;
        var endPage = totalPages;
        if (endPage <= 0) return [];

        var startPage = Math.max(currPage - Math.floor(paginationSize / 2), pageStartIndex);
        endPage = startPage + paginationSize - 1;

        if (endPage > lastPage) {
          endPage = lastPage;
          startPage = endPage - paginationSize + 1;
        }

        if (startPage !== pageStartIndex && totalPages > paginationSize && withFirstAndLast) {
          pages = [firstPageText, prePageText];
        } else if (totalPages > 1 || alwaysShowAllBtns) {
          pages = [prePageText];
        } else {
          pages = [];
        }

        for (var i = startPage; i <= endPage; i += 1) {
          if (i >= pageStartIndex) pages.push(i);
        }

        if (endPage <= lastPage && pages.length > 1) {
          pages.push(nextPageText);
        }
        if (endPage !== lastPage && withFirstAndLast) {
          pages.push(lastPageText);
        }
        return pages;
      }
    }, {
      key: 'calculatePageStatus',
      value: function calculatePageStatus() {
        var _this2 = this;

        var pages = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        var lastPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.state.lastPage;
        var _props4 = this.props,
            currPage = _props4.currPage,
            pageStartIndex = _props4.pageStartIndex,
            firstPageText = _props4.firstPageText,
            prePageText = _props4.prePageText,
            nextPageText = _props4.nextPageText,
            lastPageText = _props4.lastPageText,
            alwaysShowAllBtns = _props4.alwaysShowAllBtns;

        var isStart = function isStart(page) {
          return currPage === pageStartIndex && (page === firstPageText || page === prePageText);
        };
        var isEnd = function isEnd(page) {
          return currPage === lastPage && (page === nextPageText || page === lastPageText);
        };

        return pages.filter(function (page) {
          if (alwaysShowAllBtns) {
            return true;
          }
          return !(isStart(page) || isEnd(page));
        }).map(function (page) {
          var title = void 0;
          var active = page === currPage;
          var disabled = isStart(page) || isEnd(page);

          if (page === nextPageText) {
            title = _this2.props.nextPageTitle;
          } else if (page === prePageText) {
            title = _this2.props.prePageTitle;
          } else if (page === firstPageText) {
            title = _this2.props.firstPageTitle;
          } else if (page === lastPageText) {
            title = _this2.props.lastPageTitle;
          } else {
            title = '' + page;
          }

          return { page: page, active: active, disabled: disabled, title: title };
        });
      }
    }, {
      key: 'calculateSizePerPageStatus',
      value: function calculateSizePerPageStatus() {
        var sizePerPageList = this.props.sizePerPageList;

        return sizePerPageList.map(function (_sizePerPage) {
          var pageText = _sizePerPage.text || _sizePerPage;
          var pageNumber = _sizePerPage.value || _sizePerPage;
          return {
            text: '' + pageText,
            page: pageNumber
          };
        });
      }
    }]);

    return PageResolver;
  }(ExtendBase);
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(3);

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _bootstrap = __webpack_require__(4);

var _sizePerPageOption = __webpack_require__(14);

var _sizePerPageOption2 = _interopRequireDefault(_sizePerPageOption);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sizePerPageDefaultClass = 'react-bs-table-sizePerPage-dropdown';

var SizePerPageDropDown = function SizePerPageDropDown(props) {
  var open = props.open,
      hidden = props.hidden,
      onClick = props.onClick,
      onBlur = props.onBlur,
      options = props.options,
      className = props.className,
      variation = props.variation,
      btnContextual = props.btnContextual,
      currSizePerPage = props.currSizePerPage,
      onSizePerPageChange = props.onSizePerPageChange;


  var dropDownStyle = { visibility: hidden ? 'hidden' : 'visible' };
  var openClass = open ? 'open show' : '';
  var dropdownClasses = (0, _classnames2.default)(openClass, sizePerPageDefaultClass, variation, className);

  return _react2.default.createElement(
    _bootstrap.BootstrapContext.Consumer,
    null,
    function (_ref) {
      var bootstrap4 = _ref.bootstrap4;
      return _react2.default.createElement(
        'span',
        {
          style: dropDownStyle,
          className: dropdownClasses
        },
        _react2.default.createElement(
          'button',
          {
            id: 'pageDropDown',
            className: 'btn ' + btnContextual + ' dropdown-toggle',
            'data-toggle': 'dropdown',
            'aria-expanded': open,
            onClick: onClick,
            onBlur: onBlur
          },
          currSizePerPage,
          ' ',
          bootstrap4 ? null : _react2.default.createElement(
            'span',
            null,
            _react2.default.createElement('span', { className: 'caret' })
          )
        ),
        _react2.default.createElement(
          'ul',
          {
            className: 'dropdown-menu ' + openClass,
            role: 'menu',
            'aria-labelledby': 'pageDropDown'
          },
          options.map(function (option) {
            return _react2.default.createElement(_sizePerPageOption2.default, _extends({}, option, {
              key: option.text,
              bootstrap4: bootstrap4,
              onSizePerPageChange: onSizePerPageChange
            }));
          })
        )
      );
    }
  );
};

SizePerPageDropDown.propTypes = {
  currSizePerPage: _propTypes2.default.string.isRequired,
  options: _propTypes2.default.array.isRequired,
  onClick: _propTypes2.default.func.isRequired,
  onBlur: _propTypes2.default.func.isRequired,
  onSizePerPageChange: _propTypes2.default.func.isRequired,
  open: _propTypes2.default.bool,
  hidden: _propTypes2.default.bool,
  btnContextual: _propTypes2.default.string,
  variation: _propTypes2.default.oneOf(['dropdown', 'dropup']),
  className: _propTypes2.default.string
};
SizePerPageDropDown.defaultProps = {
  open: false,
  hidden: false,
  btnContextual: 'btn-default btn-secondary',
  variation: 'dropdown',
  className: ''
};

exports.default = SizePerPageDropDown;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint jsx-a11y/href-no-hash: 0 */
var SizePerPageOption = function SizePerPageOption(_ref) {
  var text = _ref.text,
      page = _ref.page,
      onSizePerPageChange = _ref.onSizePerPageChange,
      bootstrap4 = _ref.bootstrap4;
  return bootstrap4 ? _react2.default.createElement(
    'a',
    {
      href: '#',
      tabIndex: '-1',
      role: 'menuitem',
      className: 'dropdown-item',
      'data-page': page,
      onMouseDown: function onMouseDown(e) {
        e.preventDefault();
        onSizePerPageChange(page);
      }
    },
    text
  ) : _react2.default.createElement(
    'li',
    {
      key: text,
      role: 'presentation',
      className: 'dropdown-item'
    },
    _react2.default.createElement(
      'a',
      {
        href: '#',
        tabIndex: '-1',
        role: 'menuitem',
        'data-page': page,
        onMouseDown: function onMouseDown(e) {
          e.preventDefault();
          onSizePerPageChange(page);
        }
      },
      text
    )
  );
};

SizePerPageOption.propTypes = {
  text: _propTypes2.default.string.isRequired,
  page: _propTypes2.default.number.isRequired,
  onSizePerPageChange: _propTypes2.default.func.isRequired,
  bootstrap4: _propTypes2.default.bool
};

SizePerPageOption.defaultProps = {
  bootstrap4: false
};

exports.default = SizePerPageOption;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _pageButton = __webpack_require__(16);

var _pageButton2 = _interopRequireDefault(_pageButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PaginatonList = function PaginatonList(props) {
  return _react2.default.createElement(
    'ul',
    { className: 'pagination react-bootstrap-table-page-btns-ul' },
    props.pages.map(function (pageProps) {
      return _react2.default.createElement(_pageButton2.default, _extends({
        key: pageProps.page
      }, pageProps, {
        onPageChange: props.onPageChange
      }));
    })
  );
};

PaginatonList.propTypes = {
  pages: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    page: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
    active: _propTypes2.default.bool,
    disable: _propTypes2.default.bool,
    title: _propTypes2.default.string
  })).isRequired,
  onPageChange: _propTypes2.default.func.isRequired
};

exports.default = PaginatonList;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _classnames = __webpack_require__(3);

var _classnames2 = _interopRequireDefault(_classnames);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/require-default-props: 0 */
/* eslint jsx-a11y/href-no-hash: 0 */


var PageButton = function (_Component) {
  _inherits(PageButton, _Component);

  function PageButton(props) {
    _classCallCheck(this, PageButton);

    var _this = _possibleConstructorReturn(this, (PageButton.__proto__ || Object.getPrototypeOf(PageButton)).call(this, props));

    _this.handleClick = _this.handleClick.bind(_this);
    return _this;
  }

  _createClass(PageButton, [{
    key: 'handleClick',
    value: function handleClick(e) {
      e.preventDefault();
      this.props.onPageChange(this.props.page);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          page = _props.page,
          title = _props.title,
          active = _props.active,
          disabled = _props.disabled;

      var classes = (0, _classnames2.default)({
        active: active,
        disabled: disabled,
        'page-item': true
      });

      return _react2.default.createElement(
        'li',
        { className: classes, title: title },
        _react2.default.createElement(
          'a',
          { href: '#', onClick: this.handleClick, className: 'page-link' },
          page
        )
      );
    }
  }]);

  return PageButton;
}(_react.Component);

PageButton.propTypes = {
  onPageChange: _propTypes2.default.func.isRequired,
  page: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]).isRequired,
  active: _propTypes2.default.bool.isRequired,
  disabled: _propTypes2.default.bool.isRequired,
  title: _propTypes2.default.string
};

exports.default = PageButton;

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PaginationTotal = function PaginationTotal(props) {
  return _react2.default.createElement(
    'span',
    { className: 'react-bootstrap-table-pagination-total' },
    '\xA0Showing rows ',
    props.from,
    ' to\xA0',
    props.to,
    ' of\xA0',
    props.dataSize
  );
};

PaginationTotal.propTypes = {
  from: _propTypes2.default.number.isRequired,
  to: _propTypes2.default.number.isRequired,
  dataSize: _propTypes2.default.number.isRequired
};

exports.default = PaginationTotal;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var getNormalizedPage = function getNormalizedPage(page, pageStartIndex) {
  var offset = Math.abs(1 - pageStartIndex);
  return page + offset;
};

var endIndex = function endIndex(page, sizePerPage, pageStartIndex) {
  return getNormalizedPage(page, pageStartIndex) * sizePerPage - 1;
};

var startIndex = function startIndex(end, sizePerPage) {
  return end - (sizePerPage - 1);
};

var alignPage = exports.alignPage = function alignPage(data, page, sizePerPage, pageStartIndex) {
  var dataSize = data.length;

  if (page < pageStartIndex || page > Math.floor(dataSize / sizePerPage) + pageStartIndex) {
    return pageStartIndex;
  }
  return page;
};

var getByCurrPage = exports.getByCurrPage = function getByCurrPage(data, page, sizePerPage, pageStartIndex) {
  var dataSize = data.length;
  if (!dataSize) return [];

  var end = endIndex(page, sizePerPage, pageStartIndex);
  var start = startIndex(end, sizePerPage);

  var result = [];
  for (var i = start; i <= end; i += 1) {
    result.push(data[i]);
    if (i + 1 === dataSize) break;
  }
  return result;
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCA2OWY1Y2FhMjIzOTExMmY2ZDZkYSIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwge1wicm9vdFwiOlwiUmVhY3RcIixcImNvbW1vbmpzMlwiOlwicmVhY3RcIixcImNvbW1vbmpzXCI6XCJyZWFjdFwiLFwiYW1kXCI6XCJyZWFjdFwifSIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJvcC10eXBlcy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9zcmMvY29uc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NsYXNzbmFtZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL2Jvb3RzdHJhcC5qcyIsIndlYnBhY2s6Ly8vLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9zcmMvY29udGV4dC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJvcC10eXBlcy9mYWN0b3J5V2l0aFRocm93aW5nU2hpbXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2ZianMvbGliL2VtcHR5RnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2ZianMvbGliL2ludmFyaWFudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJvcC10eXBlcy9saWIvUmVhY3RQcm9wVHlwZXNTZWNyZXQuanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3BhZ2luYXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3BhZ2UtcmVzb2x2ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3NpemUtcGVyLXBhZ2UtZHJvcGRvd24uanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3NpemUtcGVyLXBhZ2Utb3B0aW9uLmpzIiwid2VicGFjazovLy8uL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdpbmF0aW9uLWxpc3QuanMiLCJ3ZWJwYWNrOi8vLy4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3BhZ2UtYnV0dG9uLmpzIiwid2VicGFjazovLy8uL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdpbmF0aW9uLXRvdGFsLmpzIiwid2VicGFjazovLy8uL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdlLmpzIl0sIm5hbWVzIjpbIlBBR0lOQVRJT05fU0laRSIsIlBBR0VfU1RBUlRfSU5ERVgiLCJXaXRoX0ZJUlNUX0FORF9MQVNUIiwiU0hPV19BTExfUEFHRV9CVE5TIiwiU0hPV19UT1RBTCIsIlBBR0lOQVRJT05fVE9UQUwiLCJGSVJTVF9QQUdFX1RFWFQiLCJQUkVfUEFHRV9URVhUIiwiTkVYVF9QQUdFX1RFWFQiLCJMQVNUX1BBR0VfVEVYVCIsIk5FWFRfUEFHRV9USVRMRSIsIkxBU1RfUEFHRV9USVRMRSIsIlBSRV9QQUdFX1RJVExFIiwiRklSU1RfUEFHRV9USVRMRSIsIlNJWkVfUEVSX1BBR0VfTElTVCIsIkhJREVfU0laRV9QRVJfUEFHRSIsIkhJREVfUEFHRV9MSVNUX09OTFlfT05FX1BBR0UiLCJCb290c3RyYXBDb250ZXh0IiwiY3JlYXRlQ29udGV4dCIsImJvb3RzdHJhcDQiLCJvcHRpb25zIiwiaXNSZW1vdGVQYWdpbmF0aW9uIiwiaGFuZGxlUmVtb3RlUGFnZUNoYW5nZSIsIlBhZ2luYXRpb25Db250ZXh0IiwiUGFnaW5hdGlvblByb3ZpZGVyIiwicHJvcHMiLCJoYW5kbGVDaGFuZ2VQYWdlIiwiYmluZCIsImhhbmRsZUNoYW5nZVNpemVQZXJQYWdlIiwiY3VyclBhZ2UiLCJjdXJyU2l6ZVBlclBhZ2UiLCJwYWdpbmF0aW9uIiwic2l6ZVBlclBhZ2VMaXN0IiwicGFnZSIsInBhZ2VTdGFydEluZGV4Iiwic2l6ZVBlclBhZ2UiLCJ2YWx1ZSIsIm5leHRQcm9wcyIsIm5lZWROZXdTdGF0ZSIsIm9uUGFnZUNoYW5nZSIsIm5ld1BhZ2UiLCJkYXRhIiwiZm9yY2VVcGRhdGUiLCJvblNpemVQZXJQYWdlQ2hhbmdlIiwid2l0aEZpcnN0QW5kTGFzdCIsImFsd2F5c1Nob3dBbGxCdG5zIiwiaGlkZVNpemVQZXJQYWdlIiwiaGlkZVBhZ2VMaXN0T25seU9uZVBhZ2UiLCJjaGlsZHJlbiIsInRvdGFsU2l6ZSIsImxlbmd0aCIsInBhZ2luYXRpb25TaXplIiwic2hvd1RvdGFsIiwicGFnaW5hdGlvblRvdGFsUmVuZGVyZXIiLCJmaXJzdFBhZ2VUZXh0IiwicHJlUGFnZVRleHQiLCJuZXh0UGFnZVRleHQiLCJsYXN0UGFnZVRleHQiLCJwcmVQYWdlVGl0bGUiLCJuZXh0UGFnZVRpdGxlIiwiZmlyc3RQYWdlVGl0bGUiLCJsYXN0UGFnZVRpdGxlIiwiQ29tcG9uZW50IiwicHJvcFR5cGVzIiwiYXJyYXkiLCJpc1JlcXVpcmVkIiwiUHJvdmlkZXIiLCJDb25zdW1lciIsIlBhZ2luYXRpb24iLCJkZWZhdWx0VG90YWwiLCJmcm9tIiwidG8iLCJzaXplIiwic2V0VG90YWwiLCJ0b3RhbCIsImNsb3NlRHJvcERvd24iLCJ0b2dnbGVEcm9wRG93biIsInN0YXRlIiwiaW5pdGlhbFN0YXRlIiwiZGF0YVNpemUiLCJ0b3RhbFBhZ2VzIiwiY2FsY3VsYXRlVG90YWxQYWdlIiwibGFzdFBhZ2UiLCJjYWxjdWxhdGVMYXN0UGFnZSIsInNldFN0YXRlIiwiZHJvcGRvd25PcGVuIiwic2VsZWN0ZWRTaXplIiwicGFyc2VJbnQiLCJuZXdUb3RhbFBhZ2VzIiwibmV3TGFzdFBhZ2UiLCJiYWNrVG9QcmV2UGFnZSIsIm9wZW4iLCJwYWdlcyIsImNhbGN1bGF0ZVBhZ2VTdGF0dXMiLCJjYWxjdWxhdGVQYWdlcyIsImNhbGN1bGF0ZUZyb21UbyIsInBhZ2VMaXN0Q2xhc3MiLCJjYWxjdWxhdGVTaXplUGVyUGFnZVN0YXR1cyIsIm51bWJlciIsImZ1bmMiLCJib29sIiwic3RyaW5nIiwiZGVmYXVsdFByb3BzIiwiTWF0aCIsImNlaWwiLCJvZmZzZXQiLCJhYnMiLCJtaW4iLCJlbmRQYWdlIiwic3RhcnRQYWdlIiwibWF4IiwiZmxvb3IiLCJpIiwicHVzaCIsImlzU3RhcnQiLCJpc0VuZCIsImZpbHRlciIsIm1hcCIsInRpdGxlIiwiYWN0aXZlIiwiZGlzYWJsZWQiLCJfc2l6ZVBlclBhZ2UiLCJwYWdlVGV4dCIsInRleHQiLCJwYWdlTnVtYmVyIiwiRXh0ZW5kQmFzZSIsInNpemVQZXJQYWdlRGVmYXVsdENsYXNzIiwiU2l6ZVBlclBhZ2VEcm9wRG93biIsImhpZGRlbiIsIm9uQ2xpY2siLCJvbkJsdXIiLCJjbGFzc05hbWUiLCJ2YXJpYXRpb24iLCJidG5Db250ZXh0dWFsIiwiZHJvcERvd25TdHlsZSIsInZpc2liaWxpdHkiLCJvcGVuQ2xhc3MiLCJkcm9wZG93bkNsYXNzZXMiLCJvcHRpb24iLCJvbmVPZiIsIlNpemVQZXJQYWdlT3B0aW9uIiwiZSIsInByZXZlbnREZWZhdWx0IiwiUGFnaW5hdG9uTGlzdCIsInBhZ2VQcm9wcyIsImFycmF5T2YiLCJzaGFwZSIsIm9uZU9mVHlwZSIsImRpc2FibGUiLCJQYWdlQnV0dG9uIiwiaGFuZGxlQ2xpY2siLCJjbGFzc2VzIiwiUGFnaW5hdGlvblRvdGFsIiwiZ2V0Tm9ybWFsaXplZFBhZ2UiLCJlbmRJbmRleCIsInN0YXJ0SW5kZXgiLCJlbmQiLCJhbGlnblBhZ2UiLCJnZXRCeUN1cnJQYWdlIiwic3RhcnQiLCJyZXN1bHQiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO0FDVkE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7QUM3REEsK0M7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O2tCQzdCZTtBQUNiQSxtQkFBaUIsQ0FESjtBQUViQyxvQkFBa0IsQ0FGTDtBQUdiQyx1QkFBcUIsSUFIUjtBQUliQyxzQkFBb0IsS0FKUDtBQUtiQyxjQUFZLEtBTEM7QUFNYkMsb0JBQWtCLElBTkw7QUFPYkMsbUJBQWlCLElBUEo7QUFRYkMsaUJBQWUsR0FSRjtBQVNiQyxrQkFBZ0IsR0FUSDtBQVViQyxrQkFBZ0IsSUFWSDtBQVdiQyxtQkFBaUIsV0FYSjtBQVliQyxtQkFBaUIsV0FaSjtBQWFiQyxrQkFBZ0IsZUFiSDtBQWNiQyxvQkFBa0IsWUFkTDtBQWViQyxzQkFBb0IsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxFQUFiLENBZlA7QUFnQmJDLHNCQUFvQixLQWhCUDtBQWlCYkMsZ0NBQThCO0FBakJqQixDOzs7Ozs7QUNBZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxnQkFBZ0I7O0FBRWhCO0FBQ0E7O0FBRUEsaUJBQWlCLHNCQUFzQjtBQUN2QztBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFBQTtBQUNILEVBQUU7QUFDRjtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDL0NEOzs7Ozs7QUFFQTtBQUNPLElBQU1DLDhDQUFtQixnQkFBTUMsYUFBTixDQUFvQjtBQUNsREMsY0FBWTtBQURzQyxDQUFwQixDQUF6QixDOzs7Ozs7Ozs7Ozs7O0FDSFA7Ozs7OztrQkFFZTtBQUFBLE1BQUNDLE9BQUQsdUVBQVcsRUFBWDtBQUFBLFNBQW1CO0FBQ2hDRixvQ0FEZ0M7QUFFaENFO0FBRmdDLEdBQW5CO0FBQUEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNDZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7QUFDQTs7OztBQUNBOzs7Ozs7OzsrZUFUQTtBQUNBO0FBQ0E7OztrQkFTZSxVQUNiQyxrQkFEYSxFQUViQyxzQkFGYSxFQUdWO0FBQ0gsTUFBTUMsb0JBQW9CLGdCQUFNTCxhQUFOLEVBQTFCOztBQURHLE1BR0dNLGtCQUhIO0FBQUE7O0FBUUQsZ0NBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSwwSUFDWEEsS0FEVzs7QUFFakIsWUFBS0MsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0JDLElBQXRCLE9BQXhCO0FBQ0EsWUFBS0MsdUJBQUwsR0FBK0IsTUFBS0EsdUJBQUwsQ0FBNkJELElBQTdCLE9BQS9COztBQUVBLFVBQUlFLGlCQUFKO0FBQ0EsVUFBSUMsd0JBQUo7QUFOaUIsVUFPVFYsT0FQUyxHQU9HSyxNQUFNTSxVQVBULENBT1RYLE9BUFM7O0FBUWpCLFVBQU1ZLGtCQUFrQlosUUFBUVksZUFBUixJQUEyQixnQkFBTWxCLGtCQUF6RDs7QUFFQTtBQUNBLFVBQUksT0FBT00sUUFBUWEsSUFBZixLQUF3QixXQUE1QixFQUF5QztBQUN2Q0osbUJBQVdULFFBQVFhLElBQW5CO0FBQ0QsT0FGRCxNQUVPLElBQUksT0FBT2IsUUFBUWMsY0FBZixLQUFrQyxXQUF0QyxFQUFtRDtBQUN4REwsbUJBQVdULFFBQVFjLGNBQW5CO0FBQ0QsT0FGTSxNQUVBO0FBQ0xMLG1CQUFXLGdCQUFNNUIsZ0JBQWpCO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFJLE9BQU9tQixRQUFRZSxXQUFmLEtBQStCLFdBQW5DLEVBQWdEO0FBQzlDTCwwQkFBa0JWLFFBQVFlLFdBQTFCO0FBQ0QsT0FGRCxNQUVPLElBQUksUUFBT0gsZ0JBQWdCLENBQWhCLENBQVAsTUFBOEIsUUFBbEMsRUFBNEM7QUFDakRGLDBCQUFrQkUsZ0JBQWdCLENBQWhCLEVBQW1CSSxLQUFyQztBQUNELE9BRk0sTUFFQTtBQUNMTiwwQkFBa0JFLGdCQUFnQixDQUFoQixDQUFsQjtBQUNEOztBQUVELFlBQUtILFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0EsWUFBS0MsZUFBTCxHQUF1QkEsZUFBdkI7QUE3QmlCO0FBOEJsQjs7QUF0Q0E7QUFBQTtBQUFBLGdEQXdDeUJPLFNBeEN6QixFQXdDb0M7QUFDbkMsWUFBSUMsZUFBZSxLQUFuQjtBQURtQyxZQUU3QlQsUUFGNkIsR0FFaEIsSUFGZ0IsQ0FFN0JBLFFBRjZCO0FBQUEsWUFHM0JDLGVBSDJCLEdBR1AsSUFITyxDQUczQkEsZUFIMkI7QUFBQSxZQUkzQlMsWUFKMkIsR0FJVkYsVUFBVU4sVUFBVixDQUFxQlgsT0FKWCxDQUkzQm1CLFlBSjJCOzs7QUFNbkMsWUFBTUwsaUJBQWlCLE9BQU9HLFVBQVVOLFVBQVYsQ0FBcUJYLE9BQXJCLENBQTZCYyxjQUFwQyxLQUF1RCxXQUF2RCxHQUNyQkcsVUFBVU4sVUFBVixDQUFxQlgsT0FBckIsQ0FBNkJjLGNBRFIsR0FDeUIsZ0JBQU1qQyxnQkFEdEQ7O0FBR0E7QUFDQSxZQUFJLENBQUNvQixvQkFBTCxFQUEyQjtBQUN6QixjQUFNbUIsVUFBVSxxQkFBVUgsVUFBVUksSUFBcEIsRUFBMEJaLFFBQTFCLEVBQW9DQyxlQUFwQyxFQUFxREksY0FBckQsQ0FBaEI7QUFDQSxjQUFJTCxhQUFhVyxPQUFqQixFQUEwQjtBQUN4QlgsdUJBQVdXLE9BQVg7QUFDQUYsMkJBQWUsSUFBZjtBQUNEO0FBQ0YsU0FORCxNQU1PO0FBQ0wsZUFBS1QsUUFBTCxHQUFnQlEsVUFBVU4sVUFBVixDQUFxQlgsT0FBckIsQ0FBNkJhLElBQTdDO0FBQ0EsZUFBS0gsZUFBTCxHQUF1Qk8sVUFBVU4sVUFBVixDQUFxQlgsT0FBckIsQ0FBNkJlLFdBQXBEO0FBQ0Q7O0FBRUQsWUFBSUcsWUFBSixFQUFrQjtBQUNoQixjQUFJQyxZQUFKLEVBQWtCO0FBQ2hCQSx5QkFBYVYsUUFBYixFQUF1QkMsZUFBdkI7QUFDRDtBQUNELGVBQUtELFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0EsZUFBS0MsZUFBTCxHQUF1QkEsZUFBdkI7QUFDRDtBQUNGO0FBcEVBO0FBQUE7QUFBQSx1Q0FzRWdCRCxRQXRFaEIsRUFzRTBCO0FBQUEsWUFDakJDLGVBRGlCLEdBQ0csSUFESCxDQUNqQkEsZUFEaUI7QUFBQSxZQUVIVixPQUZHLEdBRVcsS0FBS0ssS0FGaEIsQ0FFakJNLFVBRmlCLENBRUhYLE9BRkc7OztBQUl6QixZQUFJQSxRQUFRbUIsWUFBWixFQUEwQjtBQUN4Qm5CLGtCQUFRbUIsWUFBUixDQUFxQlYsUUFBckIsRUFBK0JDLGVBQS9CO0FBQ0Q7O0FBRUQsYUFBS0QsUUFBTCxHQUFnQkEsUUFBaEI7O0FBRUEsWUFBSVIsb0JBQUosRUFBMEI7QUFDeEJDLGlDQUF1Qk8sUUFBdkIsRUFBaUNDLGVBQWpDO0FBQ0E7QUFDRDtBQUNELGFBQUtZLFdBQUw7QUFDRDtBQXJGQTtBQUFBO0FBQUEsOENBdUZ1QlosZUF2RnZCLEVBdUZ3Q0QsUUF2RnhDLEVBdUZrRDtBQUFBLFlBQzNCVCxPQUQyQixHQUNiLEtBQUtLLEtBRFEsQ0FDekNNLFVBRHlDLENBQzNCWCxPQUQyQjs7O0FBR2pELFlBQUlBLFFBQVF1QixtQkFBWixFQUFpQztBQUMvQnZCLGtCQUFRdUIsbUJBQVIsQ0FBNEJiLGVBQTVCLEVBQTZDRCxRQUE3QztBQUNEOztBQUVELGFBQUtBLFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0EsYUFBS0MsZUFBTCxHQUF1QkEsZUFBdkI7O0FBRUEsWUFBSVQsb0JBQUosRUFBMEI7QUFDeEJDLGlDQUF1Qk8sUUFBdkIsRUFBaUNDLGVBQWpDO0FBQ0E7QUFDRDtBQUNELGFBQUtZLFdBQUw7QUFDRDtBQXRHQTtBQUFBO0FBQUEsK0JBd0dRO0FBQUEsWUFDREQsSUFEQyxHQUNRLEtBQUtoQixLQURiLENBQ0RnQixJQURDO0FBQUEscUJBRXlDLEtBQUtoQixLQUY5QztBQUFBLFlBRWVMLE9BRmYsVUFFQ1csVUFGRCxDQUVlWCxPQUZmO0FBQUEsWUFFMEJELFVBRjFCLFVBRTBCQSxVQUYxQjtBQUFBLFlBR0NVLFFBSEQsR0FHK0IsSUFIL0IsQ0FHQ0EsUUFIRDtBQUFBLFlBR1dDLGVBSFgsR0FHK0IsSUFIL0IsQ0FHV0EsZUFIWDs7QUFJUCxZQUFNYyxtQkFBbUIsT0FBT3hCLFFBQVF3QixnQkFBZixLQUFvQyxXQUFwQyxHQUN2QixnQkFBTTFDLG1CQURpQixHQUNLa0IsUUFBUXdCLGdCQUR0QztBQUVBLFlBQU1DLG9CQUFvQixPQUFPekIsUUFBUXlCLGlCQUFmLEtBQXFDLFdBQXJDLEdBQ3hCLGdCQUFNMUMsa0JBRGtCLEdBQ0dpQixRQUFReUIsaUJBRHJDO0FBRUEsWUFBTUMsa0JBQWtCLE9BQU8xQixRQUFRMEIsZUFBZixLQUFtQyxXQUFuQyxHQUN0QixnQkFBTS9CLGtCQURnQixHQUNLSyxRQUFRMEIsZUFEckM7QUFFQSxZQUFNQywwQkFBMEIsT0FBTzNCLFFBQVEyQix1QkFBZixLQUEyQyxXQUEzQyxHQUM5QixnQkFBTS9CLDRCQUR3QixHQUNPSSxRQUFRMkIsdUJBRC9DO0FBRUEsWUFBTWIsaUJBQWlCLE9BQU9kLFFBQVFjLGNBQWYsS0FBa0MsV0FBbEMsR0FDckIsZ0JBQU1qQyxnQkFEZSxHQUNJbUIsUUFBUWMsY0FEbkM7O0FBR0FPLGVBQU9wQix1QkFDTG9CLElBREssR0FFTCx5QkFDRUEsSUFERixFQUVFWixRQUZGLEVBR0VDLGVBSEYsRUFJRUksY0FKRixDQUZGOztBQVNBLGVBQ0U7QUFBQywyQkFBRCxDQUFtQixRQUFuQjtBQUFBLFlBQTRCLE9BQVEsRUFBRU8sVUFBRixFQUFwQztBQUNJLGVBQUtoQixLQUFMLENBQVd1QixRQURmO0FBRUU7QUFBQSx3Q0FBa0IsUUFBbEI7QUFBQSxjQUEyQixPQUFRLEVBQUU3QixzQkFBRixFQUFuQztBQUNFO0FBQ0UsbUJBQUksWUFETjtBQUVFLHdCQUFXQyxRQUFRNkIsU0FBUixJQUFxQixLQUFLeEIsS0FBTCxDQUFXZ0IsSUFBWCxDQUFnQlMsTUFGbEQ7QUFHRSx3QkFBV3JCLFFBSGI7QUFJRSwrQkFBa0JDLGVBSnBCO0FBS0UsNEJBQWUsS0FBS0osZ0JBTHRCO0FBTUUsbUNBQXNCLEtBQUtFLHVCQU43QjtBQU9FLCtCQUFrQlIsUUFBUVksZUFBUixJQUEyQixnQkFBTWxCLGtCQVByRDtBQVFFLDhCQUFpQk0sUUFBUStCLGNBQVIsSUFBMEIsZ0JBQU1uRCxlQVJuRDtBQVNFLDhCQUFpQmtDLGNBVG5CO0FBVUUsZ0NBQW1CVSxnQkFWckI7QUFXRSxpQ0FBb0JDLGlCQVh0QjtBQVlFLCtCQUFrQkMsZUFacEI7QUFhRSx1Q0FBMEJDLHVCQWI1QjtBQWNFLHlCQUFZM0IsUUFBUWdDLFNBZHRCO0FBZUUsdUNBQTBCaEMsUUFBUWlDLHVCQWZwQztBQWdCRSw2QkFBZ0JqQyxRQUFRa0MsYUFBUixJQUF5QixnQkFBTWhELGVBaEJqRDtBQWlCRSwyQkFBY2MsUUFBUW1DLFdBQVIsSUFBdUIsZ0JBQU1oRCxhQWpCN0M7QUFrQkUsNEJBQWVhLFFBQVFvQyxZQUFSLElBQXdCLGdCQUFNaEQsY0FsQi9DO0FBbUJFLDRCQUFlWSxRQUFRcUMsWUFBUixJQUF3QixnQkFBTWhELGNBbkIvQztBQW9CRSw0QkFBZVcsUUFBUXNDLFlBQVIsSUFBd0IsZ0JBQU05QyxjQXBCL0M7QUFxQkUsNkJBQWdCUSxRQUFRdUMsYUFBUixJQUF5QixnQkFBTWpELGVBckJqRDtBQXNCRSw4QkFBaUJVLFFBQVF3QyxjQUFSLElBQTBCLGdCQUFNL0MsZ0JBdEJuRDtBQXVCRSw2QkFBZ0JPLFFBQVF5QyxhQUFSLElBQXlCLGdCQUFNbEQ7QUF2QmpEO0FBREY7QUFGRixTQURGO0FBZ0NEO0FBaEtBOztBQUFBO0FBQUEsSUFHOEIsZ0JBQU1tRCxTQUhwQzs7QUFHR3RDLG9CQUhILENBSU11QyxTQUpOLEdBSWtCO0FBQ2pCdEIsVUFBTSxvQkFBVXVCLEtBQVYsQ0FBZ0JDO0FBREwsR0FKbEI7OztBQW1LSCxTQUFPO0FBQ0xDLGNBQVUxQyxrQkFETDtBQUVMMkMsY0FBVTVDLGtCQUFrQjRDO0FBRnZCLEdBQVA7QUFJRCxDOzs7Ozs7O0FDckxEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDMURBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDZDQUE2QztBQUM3QztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQjs7Ozs7OztBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRDtBQUNyRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQSwwQkFBMEI7QUFDMUI7QUFDQTtBQUNBOztBQUVBLDJCOzs7Ozs7O0FDcERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1hBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7K2VBVEE7QUFDQTs7O0lBVU1DLFU7OztBQUNKLHNCQUFZM0MsS0FBWixFQUFtQjtBQUFBOztBQUFBLHdIQUNYQSxLQURXOztBQUFBLFVBNkVuQjRDLFlBN0VtQixHQTZFSixVQUFDQyxJQUFELEVBQU9DLEVBQVAsRUFBV0MsSUFBWDtBQUFBLGFBQ2I7QUFDRSxjQUFPRixJQURUO0FBRUUsWUFBS0MsRUFGUDtBQUdFLGtCQUFXQztBQUhiLFFBRGE7QUFBQSxLQTdFSTs7QUFBQSxVQXFGbkJDLFFBckZtQixHQXFGUixVQUFDSCxJQUFELEVBQU9DLEVBQVAsRUFBV0MsSUFBWCxFQUFpQkUsS0FBakIsRUFBMkI7QUFDcEMsVUFBSUEsU0FBVSxPQUFPQSxLQUFQLEtBQWlCLFVBQS9CLEVBQTRDO0FBQzFDLGVBQU9BLE1BQU1KLElBQU4sRUFBWUMsRUFBWixFQUFnQkMsSUFBaEIsQ0FBUDtBQUNEOztBQUVELGFBQU8sTUFBS0gsWUFBTCxDQUFrQkMsSUFBbEIsRUFBd0JDLEVBQXhCLEVBQTRCQyxJQUE1QixDQUFQO0FBQ0QsS0EzRmtCOztBQUVqQixVQUFLRyxhQUFMLEdBQXFCLE1BQUtBLGFBQUwsQ0FBbUJoRCxJQUFuQixPQUFyQjtBQUNBLFVBQUtpRCxjQUFMLEdBQXNCLE1BQUtBLGNBQUwsQ0FBb0JqRCxJQUFwQixPQUF0QjtBQUNBLFVBQUtELGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCQyxJQUF0QixPQUF4QjtBQUNBLFVBQUtDLHVCQUFMLEdBQStCLE1BQUtBLHVCQUFMLENBQTZCRCxJQUE3QixPQUEvQjtBQUNBLFVBQUtrRCxLQUFMLEdBQWEsTUFBS0MsWUFBTCxFQUFiO0FBTmlCO0FBT2xCOzs7OzhDQUV5QnpDLFMsRUFBVztBQUFBLFVBQzNCMEMsUUFEMkIsR0FDRzFDLFNBREgsQ0FDM0IwQyxRQUQyQjtBQUFBLFVBQ2pCakQsZUFEaUIsR0FDR08sU0FESCxDQUNqQlAsZUFEaUI7O0FBRW5DLFVBQUlBLG9CQUFvQixLQUFLTCxLQUFMLENBQVdLLGVBQS9CLElBQWtEaUQsYUFBYSxLQUFLdEQsS0FBTCxDQUFXc0QsUUFBOUUsRUFBd0Y7QUFDdEYsWUFBTUMsYUFBYSxLQUFLQyxrQkFBTCxDQUF3Qm5ELGVBQXhCLEVBQXlDaUQsUUFBekMsQ0FBbkI7QUFDQSxZQUFNRyxXQUFXLEtBQUtDLGlCQUFMLENBQXVCSCxVQUF2QixDQUFqQjtBQUNBLGFBQUtJLFFBQUwsQ0FBYyxFQUFFSixzQkFBRixFQUFjRSxrQkFBZCxFQUFkO0FBQ0Q7QUFDRjs7O3FDQUVnQjtBQUNmLFVBQU1HLGVBQWUsQ0FBQyxLQUFLUixLQUFMLENBQVdRLFlBQWpDO0FBQ0EsV0FBS0QsUUFBTCxDQUFjLFlBQU07QUFDbEIsZUFBTyxFQUFFQywwQkFBRixFQUFQO0FBQ0QsT0FGRDtBQUdEOzs7b0NBRWU7QUFDZCxXQUFLRCxRQUFMLENBQWMsWUFBTTtBQUNsQixlQUFPLEVBQUVDLGNBQWMsS0FBaEIsRUFBUDtBQUNELE9BRkQ7QUFHRDs7OzRDQUV1QmxELFcsRUFBYTtBQUFBLG1CQUNjLEtBQUtWLEtBRG5CO0FBQUEsVUFDM0JLLGVBRDJCLFVBQzNCQSxlQUQyQjtBQUFBLFVBQ1ZhLG1CQURVLFVBQ1ZBLG1CQURVOztBQUVuQyxVQUFNMkMsZUFBZSxPQUFPbkQsV0FBUCxLQUF1QixRQUF2QixHQUFrQ29ELFNBQVNwRCxXQUFULEVBQXNCLEVBQXRCLENBQWxDLEdBQThEQSxXQUFuRjtBQUZtQyxVQUc3Qk4sUUFINkIsR0FHaEIsS0FBS0osS0FIVyxDQUc3QkksUUFINkI7O0FBSW5DLFVBQUl5RCxpQkFBaUJ4RCxlQUFyQixFQUFzQztBQUNwQyxZQUFNMEQsZ0JBQWdCLEtBQUtQLGtCQUFMLENBQXdCSyxZQUF4QixDQUF0QjtBQUNBLFlBQU1HLGNBQWMsS0FBS04saUJBQUwsQ0FBdUJLLGFBQXZCLENBQXBCO0FBQ0EsWUFBSTNELFdBQVc0RCxXQUFmLEVBQTRCNUQsV0FBVzRELFdBQVg7QUFDNUI5Qyw0QkFBb0IyQyxZQUFwQixFQUFrQ3pELFFBQWxDO0FBQ0Q7QUFDRCxXQUFLOEMsYUFBTDtBQUNEOzs7cUNBRWdCbkMsTyxFQUFTO0FBQ3hCLFVBQUlQLGFBQUo7QUFEd0Isb0JBV3BCLEtBQUtSLEtBWGU7QUFBQSxVQUd0QkksUUFIc0IsV0FHdEJBLFFBSHNCO0FBQUEsVUFJdEJLLGNBSnNCLFdBSXRCQSxjQUpzQjtBQUFBLFVBS3RCcUIsV0FMc0IsV0FLdEJBLFdBTHNCO0FBQUEsVUFNdEJDLFlBTnNCLFdBTXRCQSxZQU5zQjtBQUFBLFVBT3RCQyxZQVBzQixXQU90QkEsWUFQc0I7QUFBQSxVQVF0QkgsYUFSc0IsV0FRdEJBLGFBUnNCO0FBQUEsVUFTdEJmLFlBVHNCLFdBU3RCQSxZQVRzQjtBQUFBLFVBWWhCMkMsUUFaZ0IsR0FZSCxLQUFLTCxLQVpGLENBWWhCSyxRQVpnQjs7O0FBY3hCLFVBQUkxQyxZQUFZZSxXQUFoQixFQUE2QjtBQUMzQnRCLGVBQU8sS0FBS3lELGNBQUwsRUFBUDtBQUNELE9BRkQsTUFFTyxJQUFJbEQsWUFBWWdCLFlBQWhCLEVBQThCO0FBQ25DdkIsZUFBUUosV0FBVyxDQUFaLEdBQWlCcUQsUUFBakIsR0FBNEJBLFFBQTVCLEdBQXVDckQsV0FBVyxDQUF6RDtBQUNELE9BRk0sTUFFQSxJQUFJVyxZQUFZaUIsWUFBaEIsRUFBOEI7QUFDbkN4QixlQUFPaUQsUUFBUDtBQUNELE9BRk0sTUFFQSxJQUFJMUMsWUFBWWMsYUFBaEIsRUFBK0I7QUFDcENyQixlQUFPQyxjQUFQO0FBQ0QsT0FGTSxNQUVBO0FBQ0xELGVBQU9zRCxTQUFTL0MsT0FBVCxFQUFrQixFQUFsQixDQUFQO0FBQ0Q7O0FBRUQ7O0FBRUEsVUFBSVAsU0FBU0osUUFBYixFQUF1QjtBQUNyQlUscUJBQWFOLElBQWI7QUFDRDtBQUNGOzs7NkJBa0JRO0FBQUEsbUJBQzhDLEtBQUs0QyxLQURuRDtBQUFBLFVBQ0NHLFVBREQsVUFDQ0EsVUFERDtBQUFBLFVBQ2FFLFFBRGIsVUFDYUEsUUFEYjtBQUFBLFVBQ3FDUyxJQURyQyxVQUN1Qk4sWUFEdkI7QUFBQSxvQkFVSCxLQUFLNUQsS0FWRjtBQUFBLFVBR0wyQixTQUhLLFdBR0xBLFNBSEs7QUFBQSxVQUlMMkIsUUFKSyxXQUlMQSxRQUpLO0FBQUEsVUFLTDFCLHVCQUxLLFdBS0xBLHVCQUxLO0FBQUEsVUFNTHJCLGVBTkssV0FNTEEsZUFOSztBQUFBLFVBT0xGLGVBUEssV0FPTEEsZUFQSztBQUFBLFVBUUxnQixlQVJLLFdBUUxBLGVBUks7QUFBQSxVQVNMQyx1QkFUSyxXQVNMQSx1QkFUSzs7QUFXUCxVQUFNNkMsUUFBUSxLQUFLQyxtQkFBTCxDQUF5QixLQUFLQyxjQUFMLENBQW9CZCxVQUFwQixDQUF6QixFQUEwREUsUUFBMUQsQ0FBZDs7QUFYTyw2QkFZWSxLQUFLYSxlQUFMLEVBWlo7QUFBQTtBQUFBLFVBWUF6QixJQVpBO0FBQUEsVUFZTUMsRUFaTjs7QUFhUCxVQUFNeUIsZ0JBQWdCLDBCQUNwQix1Q0FEb0IsRUFFcEIscUNBRm9CLEVBRW1CO0FBQ3JDLHdEQUFpRGpELDJCQUEyQmlDLGVBQWU7QUFEdEQsT0FGbkIsQ0FBdEI7QUFLQSxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVUsc0NBQWY7QUFDRTtBQUFBO0FBQUEsWUFBSyxXQUFVLHFDQUFmO0FBRUloRCwwQkFBZ0JrQixNQUFoQixHQUF5QixDQUF6QixJQUE4QixDQUFDSixlQUEvQixHQUVJO0FBQ0Usa0NBQXFCaEIsZUFEdkI7QUFFRSxxQkFBVSxLQUFLbUUsMEJBQUwsRUFGWjtBQUdFLGlDQUFzQixLQUFLckUsdUJBSDdCO0FBSUUscUJBQVUsS0FBS2dELGNBSmpCO0FBS0Usb0JBQVMsS0FBS0QsYUFMaEI7QUFNRSxrQkFBT2dCO0FBTlQsWUFGSixHQVVNLElBWlY7QUFlSXZDLHNCQUNFLEtBQUtxQixRQUFMLENBQ0VILElBREYsRUFFRUMsRUFGRixFQUdFUSxRQUhGLEVBSUUxQix1QkFKRixDQURGLEdBTU07QUFyQlYsU0FERjtBQXlCRTtBQUFBO0FBQUEsWUFBSyxXQUFZMkMsYUFBakI7QUFDRSxvRUFBZ0IsT0FBUUosS0FBeEIsRUFBZ0MsY0FBZSxLQUFLbEUsZ0JBQXBEO0FBREY7QUF6QkYsT0FERjtBQStCRDs7OztFQS9Jc0IsNkM7O0FBa0p6QjBDLFdBQVdMLFNBQVgsR0FBdUI7QUFDckJnQixZQUFVLG9CQUFVbUIsTUFBVixDQUFpQmpDLFVBRE47QUFFckJqQyxtQkFBaUIsb0JBQVVnQyxLQUFWLENBQWdCQyxVQUZaO0FBR3JCcEMsWUFBVSxvQkFBVXFFLE1BQVYsQ0FBaUJqQyxVQUhOO0FBSXJCbkMsbUJBQWlCLG9CQUFVb0UsTUFBVixDQUFpQmpDLFVBSmI7QUFLckIxQixnQkFBYyxvQkFBVTRELElBQVYsQ0FBZWxDLFVBTFI7QUFNckJ0Qix1QkFBcUIsb0JBQVV3RCxJQUFWLENBQWVsQyxVQU5mO0FBT3JCL0Isa0JBQWdCLG9CQUFVZ0UsTUFQTDtBQVFyQi9DLGtCQUFnQixvQkFBVStDLE1BUkw7QUFTckI5QyxhQUFXLG9CQUFVZ0QsSUFUQTtBQVVyQi9DLDJCQUF5QixvQkFBVThDLElBVmQ7QUFXckI3QyxpQkFBZSxvQkFBVStDLE1BWEo7QUFZckI5QyxlQUFhLG9CQUFVOEMsTUFaRjtBQWFyQjdDLGdCQUFjLG9CQUFVNkMsTUFiSDtBQWNyQjVDLGdCQUFjLG9CQUFVNEMsTUFkSDtBQWVyQjFDLGlCQUFlLG9CQUFVMEMsTUFmSjtBQWdCckIzQyxnQkFBYyxvQkFBVTJDLE1BaEJIO0FBaUJyQnpDLGtCQUFnQixvQkFBVXlDLE1BakJMO0FBa0JyQnhDLGlCQUFlLG9CQUFVd0MsTUFsQko7QUFtQnJCekQsb0JBQWtCLG9CQUFVd0QsSUFuQlA7QUFvQnJCdkQscUJBQW1CLG9CQUFVdUQsSUFwQlI7QUFxQnJCdEQsbUJBQWlCLG9CQUFVc0QsSUFyQk47QUFzQnJCckQsMkJBQXlCLG9CQUFVcUQ7QUF0QmQsQ0FBdkI7O0FBeUJBaEMsV0FBV2tDLFlBQVgsR0FBMEI7QUFDeEJwRSxrQkFBZ0IsZ0JBQU1qQyxnQkFERTtBQUV4QmtELGtCQUFnQixnQkFBTW5ELGVBRkU7QUFHeEI0QyxvQkFBa0IsZ0JBQU0xQyxtQkFIQTtBQUl4QjJDLHFCQUFtQixnQkFBTTFDLGtCQUpEO0FBS3hCaUQsYUFBVyxnQkFBTWhELFVBTE87QUFNeEJpRCwyQkFBeUIsZ0JBQU1oRCxnQkFOUDtBQU94QmlELGlCQUFlLGdCQUFNaEQsZUFQRztBQVF4QmlELGVBQWEsZ0JBQU1oRCxhQVJLO0FBU3hCaUQsZ0JBQWMsZ0JBQU1oRCxjQVRJO0FBVXhCaUQsZ0JBQWMsZ0JBQU1oRCxjQVZJO0FBV3hCdUIsbUJBQWlCLGdCQUFNbEIsa0JBWEM7QUFZeEI2QyxpQkFBZSxnQkFBTWpELGVBWkc7QUFheEJnRCxnQkFBYyxnQkFBTTlDLGNBYkk7QUFjeEJnRCxrQkFBZ0IsZ0JBQU0vQyxnQkFkRTtBQWV4QmdELGlCQUFlLGdCQUFNbEQsZUFmRztBQWdCeEJtQyxtQkFBaUIsZ0JBQU0vQixrQkFoQkM7QUFpQnhCZ0MsMkJBQXlCLGdCQUFNL0I7QUFqQlAsQ0FBMUI7O2tCQW9CZW9ELFU7Ozs7Ozs7Ozs7Ozs7OztBQ3pNZjs7Ozs7Ozs7OzsrZUFEQTs7O2tCQUdlO0FBQUE7QUFBQTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBLHVDQUVNO0FBQUEscUJBQ3NCLEtBQUszQyxLQUQzQjtBQUFBLFlBQ1BJLFFBRE8sVUFDUEEsUUFETztBQUFBLFlBQ0dLLGNBREgsVUFDR0EsY0FESDs7QUFFZixlQUFRTCxXQUFXLENBQVosR0FBaUJLLGNBQWpCLEdBQWtDQSxjQUFsQyxHQUFtREwsV0FBVyxDQUFyRTtBQUNEO0FBTFU7QUFBQTtBQUFBLHFDQU9JO0FBQUEsWUFDTEEsUUFESyxHQUNRLEtBQUtKLEtBRGIsQ0FDTEksUUFESztBQUFBLFlBRUxxRCxRQUZLLEdBRVEsS0FBS0wsS0FGYixDQUVMSyxRQUZLOztBQUdiLGVBQVFyRCxXQUFXLENBQVosR0FBaUJxRCxRQUFqQixHQUE0QkEsUUFBNUIsR0FBdUNyRCxXQUFXLENBQXpEO0FBQ0Q7QUFYVTtBQUFBO0FBQUEscUNBYUk7QUFDYixZQUFNbUQsYUFBYSxLQUFLQyxrQkFBTCxFQUFuQjtBQUNBLFlBQU1DLFdBQVcsS0FBS0MsaUJBQUwsQ0FBdUJILFVBQXZCLENBQWpCO0FBQ0EsZUFBTyxFQUFFQSxzQkFBRixFQUFjRSxrQkFBZCxFQUF3QkcsY0FBYyxLQUF0QyxFQUFQO0FBQ0Q7QUFqQlU7QUFBQTtBQUFBLDJDQW1Ca0Y7QUFBQSxZQUExRWxELFdBQTBFLHVFQUE1RCxLQUFLVixLQUFMLENBQVdLLGVBQWlEO0FBQUEsWUFBaENpRCxRQUFnQyx1RUFBckIsS0FBS3RELEtBQUwsQ0FBV3NELFFBQVU7O0FBQzNGLGVBQU93QixLQUFLQyxJQUFMLENBQVV6QixXQUFXNUMsV0FBckIsQ0FBUDtBQUNEO0FBckJVO0FBQUE7QUFBQSx3Q0F1Qk82QyxVQXZCUCxFQXVCbUI7QUFBQSxZQUNwQjlDLGNBRG9CLEdBQ0QsS0FBS1QsS0FESixDQUNwQlMsY0FEb0I7O0FBRTVCLGVBQU9BLGlCQUFpQjhDLFVBQWpCLEdBQThCLENBQXJDO0FBQ0Q7QUExQlU7QUFBQTtBQUFBLHdDQTRCTztBQUFBLHNCQU1aLEtBQUt2RCxLQU5PO0FBQUEsWUFFZHNELFFBRmMsV0FFZEEsUUFGYztBQUFBLFlBR2RsRCxRQUhjLFdBR2RBLFFBSGM7QUFBQSxZQUlkQyxlQUpjLFdBSWRBLGVBSmM7QUFBQSxZQUtkSSxjQUxjLFdBS2RBLGNBTGM7O0FBT2hCLFlBQU11RSxTQUFTRixLQUFLRyxHQUFMLENBQVMsZ0JBQU16RyxnQkFBTixHQUF5QmlDLGNBQWxDLENBQWY7O0FBRUEsWUFBSW9DLE9BQVEsQ0FBQ3pDLFdBQVdLLGNBQVosSUFBOEJKLGVBQTFDO0FBQ0F3QyxlQUFPUyxhQUFhLENBQWIsR0FBaUIsQ0FBakIsR0FBcUJULE9BQU8sQ0FBbkM7QUFDQSxZQUFJQyxLQUFLZ0MsS0FBS0ksR0FBTCxDQUFTN0UsbUJBQW1CRCxXQUFXNEUsTUFBOUIsQ0FBVCxFQUFnRDFCLFFBQWhELENBQVQ7QUFDQSxZQUFJUixLQUFLUSxRQUFULEVBQW1CUixLQUFLUSxRQUFMOztBQUVuQixlQUFPLENBQUNULElBQUQsRUFBT0MsRUFBUCxDQUFQO0FBQ0Q7QUEzQ1U7QUFBQTtBQUFBLHVDQStDdUI7QUFBQSxZQURoQ1MsVUFDZ0MsdUVBRG5CLEtBQUtILEtBQUwsQ0FBV0csVUFDUTtBQUFBLFlBQWhDRSxRQUFnQyx1RUFBckIsS0FBS0wsS0FBTCxDQUFXSyxRQUFVO0FBQUEsc0JBVzVCLEtBQUt6RCxLQVh1QjtBQUFBLFlBRTlCSSxRQUY4QixXQUU5QkEsUUFGOEI7QUFBQSxZQUc5QnNCLGNBSDhCLFdBRzlCQSxjQUg4QjtBQUFBLFlBSTlCakIsY0FKOEIsV0FJOUJBLGNBSjhCO0FBQUEsWUFLOUJVLGdCQUw4QixXQUs5QkEsZ0JBTDhCO0FBQUEsWUFNOUJVLGFBTjhCLFdBTTlCQSxhQU44QjtBQUFBLFlBTzlCQyxXQVA4QixXQU85QkEsV0FQOEI7QUFBQSxZQVE5QkMsWUFSOEIsV0FROUJBLFlBUjhCO0FBQUEsWUFTOUJDLFlBVDhCLFdBUzlCQSxZQVQ4QjtBQUFBLFlBVTlCWixpQkFWOEIsV0FVOUJBLGlCQVY4Qjs7O0FBYWhDLFlBQUkrQyxjQUFKO0FBQ0EsWUFBSWdCLFVBQVU1QixVQUFkO0FBQ0EsWUFBSTRCLFdBQVcsQ0FBZixFQUFrQixPQUFPLEVBQVA7O0FBRWxCLFlBQUlDLFlBQVlOLEtBQUtPLEdBQUwsQ0FBU2pGLFdBQVcwRSxLQUFLUSxLQUFMLENBQVc1RCxpQkFBaUIsQ0FBNUIsQ0FBcEIsRUFBb0RqQixjQUFwRCxDQUFoQjtBQUNBMEUsa0JBQVVDLFlBQVkxRCxjQUFaLEdBQTZCLENBQXZDOztBQUVBLFlBQUl5RCxVQUFVMUIsUUFBZCxFQUF3QjtBQUN0QjBCLG9CQUFVMUIsUUFBVjtBQUNBMkIsc0JBQVlELFVBQVV6RCxjQUFWLEdBQTJCLENBQXZDO0FBQ0Q7O0FBRUQsWUFBSTBELGNBQWMzRSxjQUFkLElBQWdDOEMsYUFBYTdCLGNBQTdDLElBQStEUCxnQkFBbkUsRUFBcUY7QUFDbkZnRCxrQkFBUSxDQUFDdEMsYUFBRCxFQUFnQkMsV0FBaEIsQ0FBUjtBQUNELFNBRkQsTUFFTyxJQUFJeUIsYUFBYSxDQUFiLElBQWtCbkMsaUJBQXRCLEVBQXlDO0FBQzlDK0Msa0JBQVEsQ0FBQ3JDLFdBQUQsQ0FBUjtBQUNELFNBRk0sTUFFQTtBQUNMcUMsa0JBQVEsRUFBUjtBQUNEOztBQUVELGFBQUssSUFBSW9CLElBQUlILFNBQWIsRUFBd0JHLEtBQUtKLE9BQTdCLEVBQXNDSSxLQUFLLENBQTNDLEVBQThDO0FBQzVDLGNBQUlBLEtBQUs5RSxjQUFULEVBQXlCMEQsTUFBTXFCLElBQU4sQ0FBV0QsQ0FBWDtBQUMxQjs7QUFFRCxZQUFJSixXQUFXMUIsUUFBWCxJQUF1QlUsTUFBTTFDLE1BQU4sR0FBZSxDQUExQyxFQUE2QztBQUMzQzBDLGdCQUFNcUIsSUFBTixDQUFXekQsWUFBWDtBQUNEO0FBQ0QsWUFBSW9ELFlBQVkxQixRQUFaLElBQXdCdEMsZ0JBQTVCLEVBQThDO0FBQzVDZ0QsZ0JBQU1xQixJQUFOLENBQVd4RCxZQUFYO0FBQ0Q7QUFDRCxlQUFPbUMsS0FBUDtBQUNEO0FBM0ZVO0FBQUE7QUFBQSw0Q0E2RnFEO0FBQUE7O0FBQUEsWUFBNUNBLEtBQTRDLHVFQUFwQyxFQUFvQztBQUFBLFlBQWhDVixRQUFnQyx1RUFBckIsS0FBS0wsS0FBTCxDQUFXSyxRQUFVO0FBQUEsc0JBUzFELEtBQUt6RCxLQVRxRDtBQUFBLFlBRTVESSxRQUY0RCxXQUU1REEsUUFGNEQ7QUFBQSxZQUc1REssY0FINEQsV0FHNURBLGNBSDREO0FBQUEsWUFJNURvQixhQUo0RCxXQUk1REEsYUFKNEQ7QUFBQSxZQUs1REMsV0FMNEQsV0FLNURBLFdBTDREO0FBQUEsWUFNNURDLFlBTjRELFdBTTVEQSxZQU40RDtBQUFBLFlBTzVEQyxZQVA0RCxXQU81REEsWUFQNEQ7QUFBQSxZQVE1RFosaUJBUjRELFdBUTVEQSxpQkFSNEQ7O0FBVTlELFlBQU1xRSxVQUFVLFNBQVZBLE9BQVU7QUFBQSxpQkFDYnJGLGFBQWFLLGNBQWIsS0FBZ0NELFNBQVNxQixhQUFULElBQTBCckIsU0FBU3NCLFdBQW5FLENBRGE7QUFBQSxTQUFoQjtBQUVBLFlBQU00RCxRQUFRLFNBQVJBLEtBQVE7QUFBQSxpQkFDWHRGLGFBQWFxRCxRQUFiLEtBQTBCakQsU0FBU3VCLFlBQVQsSUFBeUJ2QixTQUFTd0IsWUFBNUQsQ0FEVztBQUFBLFNBQWQ7O0FBR0EsZUFBT21DLE1BQ0p3QixNQURJLENBQ0csVUFBQ25GLElBQUQsRUFBVTtBQUNoQixjQUFJWSxpQkFBSixFQUF1QjtBQUNyQixtQkFBTyxJQUFQO0FBQ0Q7QUFDRCxpQkFBTyxFQUFFcUUsUUFBUWpGLElBQVIsS0FBaUJrRixNQUFNbEYsSUFBTixDQUFuQixDQUFQO0FBQ0QsU0FOSSxFQU9Kb0YsR0FQSSxDQU9BLFVBQUNwRixJQUFELEVBQVU7QUFDYixjQUFJcUYsY0FBSjtBQUNBLGNBQU1DLFNBQVN0RixTQUFTSixRQUF4QjtBQUNBLGNBQU0yRixXQUFZTixRQUFRakYsSUFBUixLQUFpQmtGLE1BQU1sRixJQUFOLENBQW5DOztBQUVBLGNBQUlBLFNBQVN1QixZQUFiLEVBQTJCO0FBQ3pCOEQsb0JBQVEsT0FBSzdGLEtBQUwsQ0FBV2tDLGFBQW5CO0FBQ0QsV0FGRCxNQUVPLElBQUkxQixTQUFTc0IsV0FBYixFQUEwQjtBQUMvQitELG9CQUFRLE9BQUs3RixLQUFMLENBQVdpQyxZQUFuQjtBQUNELFdBRk0sTUFFQSxJQUFJekIsU0FBU3FCLGFBQWIsRUFBNEI7QUFDakNnRSxvQkFBUSxPQUFLN0YsS0FBTCxDQUFXbUMsY0FBbkI7QUFDRCxXQUZNLE1BRUEsSUFBSTNCLFNBQVN3QixZQUFiLEVBQTJCO0FBQ2hDNkQsb0JBQVEsT0FBSzdGLEtBQUwsQ0FBV29DLGFBQW5CO0FBQ0QsV0FGTSxNQUVBO0FBQ0x5RCx5QkFBV3JGLElBQVg7QUFDRDs7QUFFRCxpQkFBTyxFQUFFQSxVQUFGLEVBQVFzRixjQUFSLEVBQWdCQyxrQkFBaEIsRUFBMEJGLFlBQTFCLEVBQVA7QUFDRCxTQXpCSSxDQUFQO0FBMEJEO0FBdElVO0FBQUE7QUFBQSxtREF3SWtCO0FBQUEsWUFDbkJ0RixlQURtQixHQUNDLEtBQUtQLEtBRE4sQ0FDbkJPLGVBRG1COztBQUUzQixlQUFPQSxnQkFBZ0JxRixHQUFoQixDQUFvQixVQUFDSSxZQUFELEVBQWtCO0FBQzNDLGNBQU1DLFdBQVdELGFBQWFFLElBQWIsSUFBcUJGLFlBQXRDO0FBQ0EsY0FBTUcsYUFBYUgsYUFBYXJGLEtBQWIsSUFBc0JxRixZQUF6QztBQUNBLGlCQUFPO0FBQ0xFLHVCQUFTRCxRQURKO0FBRUx6RixrQkFBTTJGO0FBRkQsV0FBUDtBQUlELFNBUE0sQ0FBUDtBQVFEO0FBbEpVOztBQUFBO0FBQUEsSUFDY0MsVUFEZDtBQUFBLEM7Ozs7Ozs7Ozs7Ozs7OztBQ0hmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOzs7Ozs7QUFFQSxJQUFNQywwQkFBMEIscUNBQWhDOztBQUVBLElBQU1DLHNCQUFzQixTQUF0QkEsbUJBQXNCLENBQUN0RyxLQUFELEVBQVc7QUFBQSxNQUVuQ2tFLElBRm1DLEdBWWpDbEUsS0FaaUMsQ0FFbkNrRSxJQUZtQztBQUFBLE1BR25DcUMsTUFIbUMsR0FZakN2RyxLQVppQyxDQUduQ3VHLE1BSG1DO0FBQUEsTUFJbkNDLE9BSm1DLEdBWWpDeEcsS0FaaUMsQ0FJbkN3RyxPQUptQztBQUFBLE1BS25DQyxNQUxtQyxHQVlqQ3pHLEtBWmlDLENBS25DeUcsTUFMbUM7QUFBQSxNQU1uQzlHLE9BTm1DLEdBWWpDSyxLQVppQyxDQU1uQ0wsT0FObUM7QUFBQSxNQU9uQytHLFNBUG1DLEdBWWpDMUcsS0FaaUMsQ0FPbkMwRyxTQVBtQztBQUFBLE1BUW5DQyxTQVJtQyxHQVlqQzNHLEtBWmlDLENBUW5DMkcsU0FSbUM7QUFBQSxNQVNuQ0MsYUFUbUMsR0FZakM1RyxLQVppQyxDQVNuQzRHLGFBVG1DO0FBQUEsTUFVbkN2RyxlQVZtQyxHQVlqQ0wsS0FaaUMsQ0FVbkNLLGVBVm1DO0FBQUEsTUFXbkNhLG1CQVhtQyxHQVlqQ2xCLEtBWmlDLENBV25Da0IsbUJBWG1DOzs7QUFjckMsTUFBTTJGLGdCQUFnQixFQUFFQyxZQUFZUCxTQUFTLFFBQVQsR0FBb0IsU0FBbEMsRUFBdEI7QUFDQSxNQUFNUSxZQUFZN0MsT0FBTyxXQUFQLEdBQXFCLEVBQXZDO0FBQ0EsTUFBTThDLGtCQUFrQiwwQkFDdEJELFNBRHNCLEVBRXRCVix1QkFGc0IsRUFHdEJNLFNBSHNCLEVBSXRCRCxTQUpzQixDQUF4Qjs7QUFPQSxTQUNFO0FBQUEsZ0NBQWtCLFFBQWxCO0FBQUE7QUFFSTtBQUFBLFVBQUdoSCxVQUFILFFBQUdBLFVBQUg7QUFBQSxhQUNFO0FBQUE7QUFBQTtBQUNFLGlCQUFRbUgsYUFEVjtBQUVFLHFCQUFZRztBQUZkO0FBSUU7QUFBQTtBQUFBO0FBQ0UsZ0JBQUcsY0FETDtBQUVFLGdDQUFtQkosYUFBbkIscUJBRkY7QUFHRSwyQkFBWSxVQUhkO0FBSUUsNkJBQWdCMUMsSUFKbEI7QUFLRSxxQkFBVXNDLE9BTFo7QUFNRSxvQkFBU0M7QUFOWDtBQVFJcEcseUJBUko7QUFTSSxhQVRKO0FBV0lYLHVCQUFhLElBQWIsR0FDRTtBQUFBO0FBQUE7QUFDRSxvREFBTSxXQUFVLE9BQWhCO0FBREY7QUFaTixTQUpGO0FBc0JFO0FBQUE7QUFBQTtBQUNFLDBDQUE2QnFILFNBRC9CO0FBRUUsa0JBQUssTUFGUDtBQUdFLCtCQUFnQjtBQUhsQjtBQU1JcEgsa0JBQVFpRyxHQUFSLENBQVk7QUFBQSxtQkFDVix3RUFDT3FCLE1BRFA7QUFFRSxtQkFBTUEsT0FBT2YsSUFGZjtBQUdFLDBCQUFheEcsVUFIZjtBQUlFLG1DQUFzQndCO0FBSnhCLGVBRFU7QUFBQSxXQUFaO0FBTko7QUF0QkYsT0FERjtBQUFBO0FBRkosR0FERjtBQStDRCxDQXRFRDs7QUF3RUFvRixvQkFBb0JoRSxTQUFwQixHQUFnQztBQUM5QmpDLG1CQUFpQixvQkFBVXVFLE1BQVYsQ0FBaUJwQyxVQURKO0FBRTlCN0MsV0FBUyxvQkFBVTRDLEtBQVYsQ0FBZ0JDLFVBRks7QUFHOUJnRSxXQUFTLG9CQUFVOUIsSUFBVixDQUFlbEMsVUFITTtBQUk5QmlFLFVBQVEsb0JBQVUvQixJQUFWLENBQWVsQyxVQUpPO0FBSzlCdEIsdUJBQXFCLG9CQUFVd0QsSUFBVixDQUFlbEMsVUFMTjtBQU05QjBCLFFBQU0sb0JBQVVTLElBTmM7QUFPOUI0QixVQUFRLG9CQUFVNUIsSUFQWTtBQVE5QmlDLGlCQUFlLG9CQUFVaEMsTUFSSztBQVM5QitCLGFBQVcsb0JBQVVPLEtBQVYsQ0FBZ0IsQ0FBQyxVQUFELEVBQWEsUUFBYixDQUFoQixDQVRtQjtBQVU5QlIsYUFBVyxvQkFBVTlCO0FBVlMsQ0FBaEM7QUFZQTBCLG9CQUFvQnpCLFlBQXBCLEdBQW1DO0FBQ2pDWCxRQUFNLEtBRDJCO0FBRWpDcUMsVUFBUSxLQUZ5QjtBQUdqQ0ssaUJBQWUsMkJBSGtCO0FBSWpDRCxhQUFXLFVBSnNCO0FBS2pDRCxhQUFXO0FBTHNCLENBQW5DOztrQkFTZUosbUI7Ozs7Ozs7Ozs7Ozs7QUNwR2Y7Ozs7QUFDQTs7Ozs7O0FBRkE7QUFJQSxJQUFNYSxvQkFBb0IsU0FBcEJBLGlCQUFvQjtBQUFBLE1BQ3hCakIsSUFEd0IsUUFDeEJBLElBRHdCO0FBQUEsTUFFeEIxRixJQUZ3QixRQUV4QkEsSUFGd0I7QUFBQSxNQUd4QlUsbUJBSHdCLFFBR3hCQSxtQkFId0I7QUFBQSxNQUl4QnhCLFVBSndCLFFBSXhCQSxVQUp3QjtBQUFBLFNBS25CQSxhQUNMO0FBQUE7QUFBQTtBQUNFLFlBQUssR0FEUDtBQUVFLGdCQUFTLElBRlg7QUFHRSxZQUFLLFVBSFA7QUFJRSxpQkFBVSxlQUpaO0FBS0UsbUJBQVljLElBTGQ7QUFNRSxtQkFBYyxxQkFBQzRHLENBQUQsRUFBTztBQUNuQkEsVUFBRUMsY0FBRjtBQUNBbkcsNEJBQW9CVixJQUFwQjtBQUNEO0FBVEg7QUFXSTBGO0FBWEosR0FESyxHQWVMO0FBQUE7QUFBQTtBQUNFLFdBQU1BLElBRFI7QUFFRSxZQUFLLGNBRlA7QUFHRSxpQkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsY0FBSyxHQURQO0FBRUUsa0JBQVMsSUFGWDtBQUdFLGNBQUssVUFIUDtBQUlFLHFCQUFZMUYsSUFKZDtBQUtFLHFCQUFjLHFCQUFDNEcsQ0FBRCxFQUFPO0FBQ25CQSxZQUFFQyxjQUFGO0FBQ0FuRyw4QkFBb0JWLElBQXBCO0FBQ0Q7QUFSSDtBQVVJMEY7QUFWSjtBQUxGLEdBcEJ3QjtBQUFBLENBQTFCOztBQXdDQWlCLGtCQUFrQjdFLFNBQWxCLEdBQThCO0FBQzVCNEQsUUFBTSxvQkFBVXRCLE1BQVYsQ0FBaUJwQyxVQURLO0FBRTVCaEMsUUFBTSxvQkFBVWlFLE1BQVYsQ0FBaUJqQyxVQUZLO0FBRzVCdEIsdUJBQXFCLG9CQUFVd0QsSUFBVixDQUFlbEMsVUFIUjtBQUk1QjlDLGNBQVksb0JBQVVpRjtBQUpNLENBQTlCOztBQU9Bd0Msa0JBQWtCdEMsWUFBbEIsR0FBaUM7QUFDL0JuRixjQUFZO0FBRG1CLENBQWpDOztrQkFJZXlILGlCOzs7Ozs7Ozs7Ozs7Ozs7QUN2RGY7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNRyxnQkFBZ0IsU0FBaEJBLGFBQWdCO0FBQUEsU0FDcEI7QUFBQTtBQUFBLE1BQUksV0FBVSwrQ0FBZDtBQUVJdEgsVUFBTW1FLEtBQU4sQ0FBWXlCLEdBQVosQ0FBZ0I7QUFBQSxhQUNkO0FBQ0UsYUFBTTJCLFVBQVUvRztBQURsQixTQUVPK0csU0FGUDtBQUdFLHNCQUFldkgsTUFBTWM7QUFIdkIsU0FEYztBQUFBLEtBQWhCO0FBRkosR0FEb0I7QUFBQSxDQUF0Qjs7QUFjQXdHLGNBQWNoRixTQUFkLEdBQTBCO0FBQ3hCNkIsU0FBTyxvQkFBVXFELE9BQVYsQ0FBa0Isb0JBQVVDLEtBQVYsQ0FBZ0I7QUFDdkNqSCxVQUFNLG9CQUFVa0gsU0FBVixDQUFvQixDQUFDLG9CQUFVakQsTUFBWCxFQUFtQixvQkFBVUcsTUFBN0IsQ0FBcEIsQ0FEaUM7QUFFdkNrQixZQUFRLG9CQUFVbkIsSUFGcUI7QUFHdkNnRCxhQUFTLG9CQUFVaEQsSUFIb0I7QUFJdkNrQixXQUFPLG9CQUFVakI7QUFKc0IsR0FBaEIsQ0FBbEIsRUFLSHBDLFVBTm9CO0FBT3hCMUIsZ0JBQWMsb0JBQVU0RCxJQUFWLENBQWVsQztBQVBMLENBQTFCOztrQkFVZThFLGE7Ozs7Ozs7Ozs7Ozs7OztBQzNCZjs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7K2VBSkE7QUFDQTs7O0lBS01NLFU7OztBQUNKLHNCQUFZNUgsS0FBWixFQUFtQjtBQUFBOztBQUFBLHdIQUNYQSxLQURXOztBQUVqQixVQUFLNkgsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCM0gsSUFBakIsT0FBbkI7QUFGaUI7QUFHbEI7Ozs7Z0NBRVdrSCxDLEVBQUc7QUFDYkEsUUFBRUMsY0FBRjtBQUNBLFdBQUtySCxLQUFMLENBQVdjLFlBQVgsQ0FBd0IsS0FBS2QsS0FBTCxDQUFXUSxJQUFuQztBQUNEOzs7NkJBRVE7QUFBQSxtQkFNSCxLQUFLUixLQU5GO0FBQUEsVUFFTFEsSUFGSyxVQUVMQSxJQUZLO0FBQUEsVUFHTHFGLEtBSEssVUFHTEEsS0FISztBQUFBLFVBSUxDLE1BSkssVUFJTEEsTUFKSztBQUFBLFVBS0xDLFFBTEssVUFLTEEsUUFMSzs7QUFPUCxVQUFNK0IsVUFBVSwwQkFBRztBQUNqQmhDLHNCQURpQjtBQUVqQkMsMEJBRmlCO0FBR2pCLHFCQUFhO0FBSEksT0FBSCxDQUFoQjs7QUFNQSxhQUNFO0FBQUE7QUFBQSxVQUFJLFdBQVkrQixPQUFoQixFQUEwQixPQUFRakMsS0FBbEM7QUFDRTtBQUFBO0FBQUEsWUFBRyxNQUFLLEdBQVIsRUFBWSxTQUFVLEtBQUtnQyxXQUEzQixFQUF5QyxXQUFVLFdBQW5EO0FBQWlFckg7QUFBakU7QUFERixPQURGO0FBS0Q7Ozs7OztBQUdIb0gsV0FBV3RGLFNBQVgsR0FBdUI7QUFDckJ4QixnQkFBYyxvQkFBVTRELElBQVYsQ0FBZWxDLFVBRFI7QUFFckJoQyxRQUFNLG9CQUFVa0gsU0FBVixDQUFvQixDQUFDLG9CQUFVakQsTUFBWCxFQUFtQixvQkFBVUcsTUFBN0IsQ0FBcEIsRUFBMERwQyxVQUYzQztBQUdyQnNELFVBQVEsb0JBQVVuQixJQUFWLENBQWVuQyxVQUhGO0FBSXJCdUQsWUFBVSxvQkFBVXBCLElBQVYsQ0FBZW5DLFVBSko7QUFLckJxRCxTQUFPLG9CQUFVakI7QUFMSSxDQUF2Qjs7a0JBUWVnRCxVOzs7Ozs7Ozs7Ozs7O0FDOUNmOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU1HLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxTQUN0QjtBQUFBO0FBQUEsTUFBTSxXQUFVLHdDQUFoQjtBQUFBO0FBQ3VCL0gsVUFBTTZDLElBRDdCO0FBQUE7QUFDOEM3QyxVQUFNOEMsRUFEcEQ7QUFBQTtBQUNtRTlDLFVBQU1zRDtBQUR6RSxHQURzQjtBQUFBLENBQXhCOztBQU1BeUUsZ0JBQWdCekYsU0FBaEIsR0FBNEI7QUFDMUJPLFFBQU0sb0JBQVU0QixNQUFWLENBQWlCakMsVUFERztBQUUxQk0sTUFBSSxvQkFBVTJCLE1BQVYsQ0FBaUJqQyxVQUZLO0FBRzFCYyxZQUFVLG9CQUFVbUIsTUFBVixDQUFpQmpDO0FBSEQsQ0FBNUI7O2tCQU1ldUYsZTs7Ozs7Ozs7Ozs7O0FDZmYsSUFBTUMsb0JBQW9CLFNBQXBCQSxpQkFBb0IsQ0FDeEJ4SCxJQUR3QixFQUV4QkMsY0FGd0IsRUFHckI7QUFDSCxNQUFNdUUsU0FBU0YsS0FBS0csR0FBTCxDQUFTLElBQUl4RSxjQUFiLENBQWY7QUFDQSxTQUFPRCxPQUFPd0UsTUFBZDtBQUNELENBTkQ7O0FBUUEsSUFBTWlELFdBQVcsU0FBWEEsUUFBVyxDQUNmekgsSUFEZSxFQUVmRSxXQUZlLEVBR2ZELGNBSGU7QUFBQSxTQUlYdUgsa0JBQWtCeEgsSUFBbEIsRUFBd0JDLGNBQXhCLElBQTBDQyxXQUEzQyxHQUEwRCxDQUo5QztBQUFBLENBQWpCOztBQU1BLElBQU13SCxhQUFhLFNBQWJBLFVBQWEsQ0FDakJDLEdBRGlCLEVBRWpCekgsV0FGaUI7QUFBQSxTQUdkeUgsT0FBT3pILGNBQWMsQ0FBckIsQ0FIYztBQUFBLENBQW5COztBQUtPLElBQU0wSCxnQ0FBWSxTQUFaQSxTQUFZLENBQ3ZCcEgsSUFEdUIsRUFFdkJSLElBRnVCLEVBR3ZCRSxXQUh1QixFQUl2QkQsY0FKdUIsRUFLcEI7QUFDSCxNQUFNNkMsV0FBV3RDLEtBQUtTLE1BQXRCOztBQUVBLE1BQUlqQixPQUFPQyxjQUFQLElBQXlCRCxPQUFRc0UsS0FBS1EsS0FBTCxDQUFXaEMsV0FBVzVDLFdBQXRCLElBQXFDRCxjQUExRSxFQUEyRjtBQUN6RixXQUFPQSxjQUFQO0FBQ0Q7QUFDRCxTQUFPRCxJQUFQO0FBQ0QsQ0FaTTs7QUFjQSxJQUFNNkgsd0NBQWdCLFNBQWhCQSxhQUFnQixDQUMzQnJILElBRDJCLEVBRTNCUixJQUYyQixFQUczQkUsV0FIMkIsRUFJM0JELGNBSjJCLEVBS3hCO0FBQ0gsTUFBTTZDLFdBQVd0QyxLQUFLUyxNQUF0QjtBQUNBLE1BQUksQ0FBQzZCLFFBQUwsRUFBZSxPQUFPLEVBQVA7O0FBRWYsTUFBTTZFLE1BQU1GLFNBQVN6SCxJQUFULEVBQWVFLFdBQWYsRUFBNEJELGNBQTVCLENBQVo7QUFDQSxNQUFNNkgsUUFBUUosV0FBV0MsR0FBWCxFQUFnQnpILFdBQWhCLENBQWQ7O0FBRUEsTUFBTTZILFNBQVMsRUFBZjtBQUNBLE9BQUssSUFBSWhELElBQUkrQyxLQUFiLEVBQW9CL0MsS0FBSzRDLEdBQXpCLEVBQThCNUMsS0FBSyxDQUFuQyxFQUFzQztBQUNwQ2dELFdBQU8vQyxJQUFQLENBQVl4RSxLQUFLdUUsQ0FBTCxDQUFaO0FBQ0EsUUFBSUEsSUFBSSxDQUFKLEtBQVVqQyxRQUFkLEVBQXdCO0FBQ3pCO0FBQ0QsU0FBT2lGLE1BQVA7QUFDRCxDQWxCTSxDIiwiZmlsZSI6InJlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL2Rpc3QvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KHJlcXVpcmUoXCJyZWFjdFwiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXCJyZWFjdFwiXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJSZWFjdEJvb3RzdHJhcFRhYmxlMlBhZ2luYXRvclwiXSA9IGZhY3RvcnkocmVxdWlyZShcInJlYWN0XCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJSZWFjdEJvb3RzdHJhcFRhYmxlMlBhZ2luYXRvclwiXSA9IGZhY3Rvcnkocm9vdFtcIlJlYWN0XCJdKTtcbn0pKHRoaXMsIGZ1bmN0aW9uKF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfMF9fKSB7XG5yZXR1cm4gXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDY5ZjVjYWEyMjM5MTEyZjZkNmRhIiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFXzBfXztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyBleHRlcm5hbCB7XCJyb290XCI6XCJSZWFjdFwiLFwiY29tbW9uanMyXCI6XCJyZWFjdFwiLFwiY29tbW9uanNcIjpcInJlYWN0XCIsXCJhbWRcIjpcInJlYWN0XCJ9XG4vLyBtb2R1bGUgaWQgPSAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiLyoqXG4gKiBDb3B5cmlnaHQgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKi9cblxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgdmFyIFJFQUNUX0VMRU1FTlRfVFlQRSA9ICh0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmXG4gICAgU3ltYm9sLmZvciAmJlxuICAgIFN5bWJvbC5mb3IoJ3JlYWN0LmVsZW1lbnQnKSkgfHxcbiAgICAweGVhYzc7XG5cbiAgdmFyIGlzVmFsaWRFbGVtZW50ID0gZnVuY3Rpb24ob2JqZWN0KSB7XG4gICAgcmV0dXJuIHR5cGVvZiBvYmplY3QgPT09ICdvYmplY3QnICYmXG4gICAgICBvYmplY3QgIT09IG51bGwgJiZcbiAgICAgIG9iamVjdC4kJHR5cGVvZiA9PT0gUkVBQ1RfRUxFTUVOVF9UWVBFO1xuICB9O1xuXG4gIC8vIEJ5IGV4cGxpY2l0bHkgdXNpbmcgYHByb3AtdHlwZXNgIHlvdSBhcmUgb3B0aW5nIGludG8gbmV3IGRldmVsb3BtZW50IGJlaGF2aW9yLlxuICAvLyBodHRwOi8vZmIubWUvcHJvcC10eXBlcy1pbi1wcm9kXG4gIHZhciB0aHJvd09uRGlyZWN0QWNjZXNzID0gdHJ1ZTtcbiAgbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2ZhY3RvcnlXaXRoVHlwZUNoZWNrZXJzJykoaXNWYWxpZEVsZW1lbnQsIHRocm93T25EaXJlY3RBY2Nlc3MpO1xufSBlbHNlIHtcbiAgLy8gQnkgZXhwbGljaXRseSB1c2luZyBgcHJvcC10eXBlc2AgeW91IGFyZSBvcHRpbmcgaW50byBuZXcgcHJvZHVjdGlvbiBiZWhhdmlvci5cbiAgLy8gaHR0cDovL2ZiLm1lL3Byb3AtdHlwZXMtaW4tcHJvZFxuICBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vZmFjdG9yeVdpdGhUaHJvd2luZ1NoaW1zJykoKTtcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Byb3AtdHlwZXMvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJleHBvcnQgZGVmYXVsdCB7XG4gIFBBR0lOQVRJT05fU0laRTogNSxcbiAgUEFHRV9TVEFSVF9JTkRFWDogMSxcbiAgV2l0aF9GSVJTVF9BTkRfTEFTVDogdHJ1ZSxcbiAgU0hPV19BTExfUEFHRV9CVE5TOiBmYWxzZSxcbiAgU0hPV19UT1RBTDogZmFsc2UsXG4gIFBBR0lOQVRJT05fVE9UQUw6IG51bGwsXG4gIEZJUlNUX1BBR0VfVEVYVDogJzw8JyxcbiAgUFJFX1BBR0VfVEVYVDogJzwnLFxuICBORVhUX1BBR0VfVEVYVDogJz4nLFxuICBMQVNUX1BBR0VfVEVYVDogJz4+JyxcbiAgTkVYVF9QQUdFX1RJVExFOiAnbmV4dCBwYWdlJyxcbiAgTEFTVF9QQUdFX1RJVExFOiAnbGFzdCBwYWdlJyxcbiAgUFJFX1BBR0VfVElUTEU6ICdwcmV2aW91cyBwYWdlJyxcbiAgRklSU1RfUEFHRV9USVRMRTogJ2ZpcnN0IHBhZ2UnLFxuICBTSVpFX1BFUl9QQUdFX0xJU1Q6IFsxMCwgMjUsIDMwLCA1MF0sXG4gIEhJREVfU0laRV9QRVJfUEFHRTogZmFsc2UsXG4gIEhJREVfUEFHRV9MSVNUX09OTFlfT05FX1BBR0U6IGZhbHNlXG59O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL2NvbnN0LmpzIiwiLyohXG4gIENvcHlyaWdodCAoYykgMjAxNiBKZWQgV2F0c29uLlxuICBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UgKE1JVCksIHNlZVxuICBodHRwOi8vamVkd2F0c29uLmdpdGh1Yi5pby9jbGFzc25hbWVzXG4qL1xuLyogZ2xvYmFsIGRlZmluZSAqL1xuXG4oZnVuY3Rpb24gKCkge1xuXHQndXNlIHN0cmljdCc7XG5cblx0dmFyIGhhc093biA9IHt9Lmhhc093blByb3BlcnR5O1xuXG5cdGZ1bmN0aW9uIGNsYXNzTmFtZXMgKCkge1xuXHRcdHZhciBjbGFzc2VzID0gW107XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGFyZyA9IGFyZ3VtZW50c1tpXTtcblx0XHRcdGlmICghYXJnKSBjb250aW51ZTtcblxuXHRcdFx0dmFyIGFyZ1R5cGUgPSB0eXBlb2YgYXJnO1xuXG5cdFx0XHRpZiAoYXJnVHlwZSA9PT0gJ3N0cmluZycgfHwgYXJnVHlwZSA9PT0gJ251bWJlcicpIHtcblx0XHRcdFx0Y2xhc3Nlcy5wdXNoKGFyZyk7XG5cdFx0XHR9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkoYXJnKSkge1xuXHRcdFx0XHRjbGFzc2VzLnB1c2goY2xhc3NOYW1lcy5hcHBseShudWxsLCBhcmcpKTtcblx0XHRcdH0gZWxzZSBpZiAoYXJnVHlwZSA9PT0gJ29iamVjdCcpIHtcblx0XHRcdFx0Zm9yICh2YXIga2V5IGluIGFyZykge1xuXHRcdFx0XHRcdGlmIChoYXNPd24uY2FsbChhcmcsIGtleSkgJiYgYXJnW2tleV0pIHtcblx0XHRcdFx0XHRcdGNsYXNzZXMucHVzaChrZXkpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiBjbGFzc2VzLmpvaW4oJyAnKTtcblx0fVxuXG5cdGlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xuXHRcdG1vZHVsZS5leHBvcnRzID0gY2xhc3NOYW1lcztcblx0fSBlbHNlIGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBkZWZpbmUuYW1kID09PSAnb2JqZWN0JyAmJiBkZWZpbmUuYW1kKSB7XG5cdFx0Ly8gcmVnaXN0ZXIgYXMgJ2NsYXNzbmFtZXMnLCBjb25zaXN0ZW50IHdpdGggbnBtIHBhY2thZ2UgbmFtZVxuXHRcdGRlZmluZSgnY2xhc3NuYW1lcycsIFtdLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRyZXR1cm4gY2xhc3NOYW1lcztcblx0XHR9KTtcblx0fSBlbHNlIHtcblx0XHR3aW5kb3cuY2xhc3NOYW1lcyA9IGNsYXNzTmFtZXM7XG5cdH1cbn0oKSk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jbGFzc25hbWVzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcblxuLy8gY29uc2lkZXIgdG8gaGF2ZSBhIGNvbW1vbiBsaWI/MVxuZXhwb3J0IGNvbnN0IEJvb3RzdHJhcENvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KHtcbiAgYm9vdHN0cmFwNDogZmFsc2Vcbn0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL2Jvb3RzdHJhcC5qcyIsImltcG9ydCBjcmVhdGVDb250ZXh0IGZyb20gJy4vc3JjL2NvbnRleHQnO1xuXG5leHBvcnQgZGVmYXVsdCAob3B0aW9ucyA9IHt9KSA9PiAoe1xuICBjcmVhdGVDb250ZXh0LFxuICBvcHRpb25zXG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL2luZGV4LmpzIiwiLyogZXNsaW50IHJlYWN0L3Byb3AtdHlwZXM6IDAgKi9cbi8qIGVzbGludCByZWFjdC9yZXF1aXJlLWRlZmF1bHQtcHJvcHM6IDAgKi9cbi8qIGVzbGludCBuby1sb25lbHktaWY6IDAgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgQ29uc3QgZnJvbSAnLi9jb25zdCc7XG5pbXBvcnQgeyBCb290c3RyYXBDb250ZXh0IH0gZnJvbSAnLi9ib290c3RyYXAnO1xuaW1wb3J0IFBhZ2luYXRpb24gZnJvbSAnLi9wYWdpbmF0aW9uJztcbmltcG9ydCB7IGdldEJ5Q3VyclBhZ2UsIGFsaWduUGFnZSB9IGZyb20gJy4vcGFnZSc7XG5cbmV4cG9ydCBkZWZhdWx0IChcbiAgaXNSZW1vdGVQYWdpbmF0aW9uLFxuICBoYW5kbGVSZW1vdGVQYWdlQ2hhbmdlXG4pID0+IHtcbiAgY29uc3QgUGFnaW5hdGlvbkNvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KCk7XG5cbiAgY2xhc3MgUGFnaW5hdGlvblByb3ZpZGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgICAgZGF0YTogUHJvcFR5cGVzLmFycmF5LmlzUmVxdWlyZWRcbiAgICB9XG5cbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgdGhpcy5oYW5kbGVDaGFuZ2VQYWdlID0gdGhpcy5oYW5kbGVDaGFuZ2VQYWdlLmJpbmQodGhpcyk7XG4gICAgICB0aGlzLmhhbmRsZUNoYW5nZVNpemVQZXJQYWdlID0gdGhpcy5oYW5kbGVDaGFuZ2VTaXplUGVyUGFnZS5iaW5kKHRoaXMpO1xuXG4gICAgICBsZXQgY3VyclBhZ2U7XG4gICAgICBsZXQgY3VyclNpemVQZXJQYWdlO1xuICAgICAgY29uc3QgeyBvcHRpb25zIH0gPSBwcm9wcy5wYWdpbmF0aW9uO1xuICAgICAgY29uc3Qgc2l6ZVBlclBhZ2VMaXN0ID0gb3B0aW9ucy5zaXplUGVyUGFnZUxpc3QgfHwgQ29uc3QuU0laRV9QRVJfUEFHRV9MSVNUO1xuXG4gICAgICAvLyBpbml0aWFsaXplIGN1cnJlbnQgcGFnZVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLnBhZ2UgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGN1cnJQYWdlID0gb3B0aW9ucy5wYWdlO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2Ygb3B0aW9ucy5wYWdlU3RhcnRJbmRleCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY3VyclBhZ2UgPSBvcHRpb25zLnBhZ2VTdGFydEluZGV4O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY3VyclBhZ2UgPSBDb25zdC5QQUdFX1NUQVJUX0lOREVYO1xuICAgICAgfVxuXG4gICAgICAvLyBpbml0aWFsaXplIGN1cnJlbnQgc2l6ZVBlclBhZ2VcbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5zaXplUGVyUGFnZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY3VyclNpemVQZXJQYWdlID0gb3B0aW9ucy5zaXplUGVyUGFnZTtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHNpemVQZXJQYWdlTGlzdFswXSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgY3VyclNpemVQZXJQYWdlID0gc2l6ZVBlclBhZ2VMaXN0WzBdLnZhbHVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY3VyclNpemVQZXJQYWdlID0gc2l6ZVBlclBhZ2VMaXN0WzBdO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmN1cnJQYWdlID0gY3VyclBhZ2U7XG4gICAgICB0aGlzLmN1cnJTaXplUGVyUGFnZSA9IGN1cnJTaXplUGVyUGFnZTtcbiAgICB9XG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgICAgbGV0IG5lZWROZXdTdGF0ZSA9IGZhbHNlO1xuICAgICAgbGV0IHsgY3VyclBhZ2UgfSA9IHRoaXM7XG4gICAgICBjb25zdCB7IGN1cnJTaXplUGVyUGFnZSB9ID0gdGhpcztcbiAgICAgIGNvbnN0IHsgb25QYWdlQ2hhbmdlIH0gPSBuZXh0UHJvcHMucGFnaW5hdGlvbi5vcHRpb25zO1xuXG4gICAgICBjb25zdCBwYWdlU3RhcnRJbmRleCA9IHR5cGVvZiBuZXh0UHJvcHMucGFnaW5hdGlvbi5vcHRpb25zLnBhZ2VTdGFydEluZGV4ICE9PSAndW5kZWZpbmVkJyA/XG4gICAgICAgIG5leHRQcm9wcy5wYWdpbmF0aW9uLm9wdGlvbnMucGFnZVN0YXJ0SW5kZXggOiBDb25zdC5QQUdFX1NUQVJUX0lOREVYO1xuXG4gICAgICAvLyB1c2VyIHNob3VsZCBhbGlnbiB0aGUgcGFnZSB3aGVuIHRoZSBwYWdlIGlzIG5vdCBmaXQgdG8gdGhlIGRhdGEgc2l6ZSB3aGVuIHJlbW90ZSBlbmFibGVcbiAgICAgIGlmICghaXNSZW1vdGVQYWdpbmF0aW9uKCkpIHtcbiAgICAgICAgY29uc3QgbmV3UGFnZSA9IGFsaWduUGFnZShuZXh0UHJvcHMuZGF0YSwgY3VyclBhZ2UsIGN1cnJTaXplUGVyUGFnZSwgcGFnZVN0YXJ0SW5kZXgpO1xuICAgICAgICBpZiAoY3VyclBhZ2UgIT09IG5ld1BhZ2UpIHtcbiAgICAgICAgICBjdXJyUGFnZSA9IG5ld1BhZ2U7XG4gICAgICAgICAgbmVlZE5ld1N0YXRlID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jdXJyUGFnZSA9IG5leHRQcm9wcy5wYWdpbmF0aW9uLm9wdGlvbnMucGFnZTtcbiAgICAgICAgdGhpcy5jdXJyU2l6ZVBlclBhZ2UgPSBuZXh0UHJvcHMucGFnaW5hdGlvbi5vcHRpb25zLnNpemVQZXJQYWdlO1xuICAgICAgfVxuXG4gICAgICBpZiAobmVlZE5ld1N0YXRlKSB7XG4gICAgICAgIGlmIChvblBhZ2VDaGFuZ2UpIHtcbiAgICAgICAgICBvblBhZ2VDaGFuZ2UoY3VyclBhZ2UsIGN1cnJTaXplUGVyUGFnZSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jdXJyUGFnZSA9IGN1cnJQYWdlO1xuICAgICAgICB0aGlzLmN1cnJTaXplUGVyUGFnZSA9IGN1cnJTaXplUGVyUGFnZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBoYW5kbGVDaGFuZ2VQYWdlKGN1cnJQYWdlKSB7XG4gICAgICBjb25zdCB7IGN1cnJTaXplUGVyUGFnZSB9ID0gdGhpcztcbiAgICAgIGNvbnN0IHsgcGFnaW5hdGlvbjogeyBvcHRpb25zIH0gfSA9IHRoaXMucHJvcHM7XG5cbiAgICAgIGlmIChvcHRpb25zLm9uUGFnZUNoYW5nZSkge1xuICAgICAgICBvcHRpb25zLm9uUGFnZUNoYW5nZShjdXJyUGFnZSwgY3VyclNpemVQZXJQYWdlKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5jdXJyUGFnZSA9IGN1cnJQYWdlO1xuXG4gICAgICBpZiAoaXNSZW1vdGVQYWdpbmF0aW9uKCkpIHtcbiAgICAgICAgaGFuZGxlUmVtb3RlUGFnZUNoYW5nZShjdXJyUGFnZSwgY3VyclNpemVQZXJQYWdlKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xuICAgIH1cblxuICAgIGhhbmRsZUNoYW5nZVNpemVQZXJQYWdlKGN1cnJTaXplUGVyUGFnZSwgY3VyclBhZ2UpIHtcbiAgICAgIGNvbnN0IHsgcGFnaW5hdGlvbjogeyBvcHRpb25zIH0gfSA9IHRoaXMucHJvcHM7XG5cbiAgICAgIGlmIChvcHRpb25zLm9uU2l6ZVBlclBhZ2VDaGFuZ2UpIHtcbiAgICAgICAgb3B0aW9ucy5vblNpemVQZXJQYWdlQ2hhbmdlKGN1cnJTaXplUGVyUGFnZSwgY3VyclBhZ2UpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmN1cnJQYWdlID0gY3VyclBhZ2U7XG4gICAgICB0aGlzLmN1cnJTaXplUGVyUGFnZSA9IGN1cnJTaXplUGVyUGFnZTtcblxuICAgICAgaWYgKGlzUmVtb3RlUGFnaW5hdGlvbigpKSB7XG4gICAgICAgIGhhbmRsZVJlbW90ZVBhZ2VDaGFuZ2UoY3VyclBhZ2UsIGN1cnJTaXplUGVyUGFnZSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcbiAgICB9XG5cbiAgICByZW5kZXIoKSB7XG4gICAgICBsZXQgeyBkYXRhIH0gPSB0aGlzLnByb3BzO1xuICAgICAgY29uc3QgeyBwYWdpbmF0aW9uOiB7IG9wdGlvbnMgfSwgYm9vdHN0cmFwNCB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgY3VyclBhZ2UsIGN1cnJTaXplUGVyUGFnZSB9ID0gdGhpcztcbiAgICAgIGNvbnN0IHdpdGhGaXJzdEFuZExhc3QgPSB0eXBlb2Ygb3B0aW9ucy53aXRoRmlyc3RBbmRMYXN0ID09PSAndW5kZWZpbmVkJyA/XG4gICAgICAgIENvbnN0LldpdGhfRklSU1RfQU5EX0xBU1QgOiBvcHRpb25zLndpdGhGaXJzdEFuZExhc3Q7XG4gICAgICBjb25zdCBhbHdheXNTaG93QWxsQnRucyA9IHR5cGVvZiBvcHRpb25zLmFsd2F5c1Nob3dBbGxCdG5zID09PSAndW5kZWZpbmVkJyA/XG4gICAgICAgIENvbnN0LlNIT1dfQUxMX1BBR0VfQlROUyA6IG9wdGlvbnMuYWx3YXlzU2hvd0FsbEJ0bnM7XG4gICAgICBjb25zdCBoaWRlU2l6ZVBlclBhZ2UgPSB0eXBlb2Ygb3B0aW9ucy5oaWRlU2l6ZVBlclBhZ2UgPT09ICd1bmRlZmluZWQnID9cbiAgICAgICAgQ29uc3QuSElERV9TSVpFX1BFUl9QQUdFIDogb3B0aW9ucy5oaWRlU2l6ZVBlclBhZ2U7XG4gICAgICBjb25zdCBoaWRlUGFnZUxpc3RPbmx5T25lUGFnZSA9IHR5cGVvZiBvcHRpb25zLmhpZGVQYWdlTGlzdE9ubHlPbmVQYWdlID09PSAndW5kZWZpbmVkJyA/XG4gICAgICAgIENvbnN0LkhJREVfUEFHRV9MSVNUX09OTFlfT05FX1BBR0UgOiBvcHRpb25zLmhpZGVQYWdlTGlzdE9ubHlPbmVQYWdlO1xuICAgICAgY29uc3QgcGFnZVN0YXJ0SW5kZXggPSB0eXBlb2Ygb3B0aW9ucy5wYWdlU3RhcnRJbmRleCA9PT0gJ3VuZGVmaW5lZCcgP1xuICAgICAgICBDb25zdC5QQUdFX1NUQVJUX0lOREVYIDogb3B0aW9ucy5wYWdlU3RhcnRJbmRleDtcblxuICAgICAgZGF0YSA9IGlzUmVtb3RlUGFnaW5hdGlvbigpID9cbiAgICAgICAgZGF0YSA6XG4gICAgICAgIGdldEJ5Q3VyclBhZ2UoXG4gICAgICAgICAgZGF0YSxcbiAgICAgICAgICBjdXJyUGFnZSxcbiAgICAgICAgICBjdXJyU2l6ZVBlclBhZ2UsXG4gICAgICAgICAgcGFnZVN0YXJ0SW5kZXhcbiAgICAgICAgKTtcblxuICAgICAgcmV0dXJuIChcbiAgICAgICAgPFBhZ2luYXRpb25Db250ZXh0LlByb3ZpZGVyIHZhbHVlPXsgeyBkYXRhIH0gfT5cbiAgICAgICAgICB7IHRoaXMucHJvcHMuY2hpbGRyZW4gfVxuICAgICAgICAgIDxCb290c3RyYXBDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXsgeyBib290c3RyYXA0IH0gfT5cbiAgICAgICAgICAgIDxQYWdpbmF0aW9uXG4gICAgICAgICAgICAgIGtleT1cInBhZ2luYXRpb25cIlxuICAgICAgICAgICAgICBkYXRhU2l6ZT17IG9wdGlvbnMudG90YWxTaXplIHx8IHRoaXMucHJvcHMuZGF0YS5sZW5ndGggfVxuICAgICAgICAgICAgICBjdXJyUGFnZT17IGN1cnJQYWdlIH1cbiAgICAgICAgICAgICAgY3VyclNpemVQZXJQYWdlPXsgY3VyclNpemVQZXJQYWdlIH1cbiAgICAgICAgICAgICAgb25QYWdlQ2hhbmdlPXsgdGhpcy5oYW5kbGVDaGFuZ2VQYWdlIH1cbiAgICAgICAgICAgICAgb25TaXplUGVyUGFnZUNoYW5nZT17IHRoaXMuaGFuZGxlQ2hhbmdlU2l6ZVBlclBhZ2UgfVxuICAgICAgICAgICAgICBzaXplUGVyUGFnZUxpc3Q9eyBvcHRpb25zLnNpemVQZXJQYWdlTGlzdCB8fCBDb25zdC5TSVpFX1BFUl9QQUdFX0xJU1QgfVxuICAgICAgICAgICAgICBwYWdpbmF0aW9uU2l6ZT17IG9wdGlvbnMucGFnaW5hdGlvblNpemUgfHwgQ29uc3QuUEFHSU5BVElPTl9TSVpFIH1cbiAgICAgICAgICAgICAgcGFnZVN0YXJ0SW5kZXg9eyBwYWdlU3RhcnRJbmRleCB9XG4gICAgICAgICAgICAgIHdpdGhGaXJzdEFuZExhc3Q9eyB3aXRoRmlyc3RBbmRMYXN0IH1cbiAgICAgICAgICAgICAgYWx3YXlzU2hvd0FsbEJ0bnM9eyBhbHdheXNTaG93QWxsQnRucyB9XG4gICAgICAgICAgICAgIGhpZGVTaXplUGVyUGFnZT17IGhpZGVTaXplUGVyUGFnZSB9XG4gICAgICAgICAgICAgIGhpZGVQYWdlTGlzdE9ubHlPbmVQYWdlPXsgaGlkZVBhZ2VMaXN0T25seU9uZVBhZ2UgfVxuICAgICAgICAgICAgICBzaG93VG90YWw9eyBvcHRpb25zLnNob3dUb3RhbCB9XG4gICAgICAgICAgICAgIHBhZ2luYXRpb25Ub3RhbFJlbmRlcmVyPXsgb3B0aW9ucy5wYWdpbmF0aW9uVG90YWxSZW5kZXJlciB9XG4gICAgICAgICAgICAgIGZpcnN0UGFnZVRleHQ9eyBvcHRpb25zLmZpcnN0UGFnZVRleHQgfHwgQ29uc3QuRklSU1RfUEFHRV9URVhUIH1cbiAgICAgICAgICAgICAgcHJlUGFnZVRleHQ9eyBvcHRpb25zLnByZVBhZ2VUZXh0IHx8IENvbnN0LlBSRV9QQUdFX1RFWFQgfVxuICAgICAgICAgICAgICBuZXh0UGFnZVRleHQ9eyBvcHRpb25zLm5leHRQYWdlVGV4dCB8fCBDb25zdC5ORVhUX1BBR0VfVEVYVCB9XG4gICAgICAgICAgICAgIGxhc3RQYWdlVGV4dD17IG9wdGlvbnMubGFzdFBhZ2VUZXh0IHx8IENvbnN0LkxBU1RfUEFHRV9URVhUIH1cbiAgICAgICAgICAgICAgcHJlUGFnZVRpdGxlPXsgb3B0aW9ucy5wcmVQYWdlVGl0bGUgfHwgQ29uc3QuUFJFX1BBR0VfVElUTEUgfVxuICAgICAgICAgICAgICBuZXh0UGFnZVRpdGxlPXsgb3B0aW9ucy5uZXh0UGFnZVRpdGxlIHx8IENvbnN0Lk5FWFRfUEFHRV9USVRMRSB9XG4gICAgICAgICAgICAgIGZpcnN0UGFnZVRpdGxlPXsgb3B0aW9ucy5maXJzdFBhZ2VUaXRsZSB8fCBDb25zdC5GSVJTVF9QQUdFX1RJVExFIH1cbiAgICAgICAgICAgICAgbGFzdFBhZ2VUaXRsZT17IG9wdGlvbnMubGFzdFBhZ2VUaXRsZSB8fCBDb25zdC5MQVNUX1BBR0VfVElUTEUgfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L0Jvb3RzdHJhcENvbnRleHQuUHJvdmlkZXI+XG4gICAgICAgIDwvUGFnaW5hdGlvbkNvbnRleHQuUHJvdmlkZXI+XG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgUHJvdmlkZXI6IFBhZ2luYXRpb25Qcm92aWRlcixcbiAgICBDb25zdW1lcjogUGFnaW5hdGlvbkNvbnRleHQuQ29uc3VtZXJcbiAgfTtcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9zcmMvY29udGV4dC5qcyIsIi8qKlxuICogQ29weXJpZ2h0IDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGVtcHR5RnVuY3Rpb24gPSByZXF1aXJlKCdmYmpzL2xpYi9lbXB0eUZ1bmN0aW9uJyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG52YXIgUmVhY3RQcm9wVHlwZXNTZWNyZXQgPSByZXF1aXJlKCcuL2xpYi9SZWFjdFByb3BUeXBlc1NlY3JldCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCkge1xuICBmdW5jdGlvbiBzaGltKHByb3BzLCBwcm9wTmFtZSwgY29tcG9uZW50TmFtZSwgbG9jYXRpb24sIHByb3BGdWxsTmFtZSwgc2VjcmV0KSB7XG4gICAgaWYgKHNlY3JldCA9PT0gUmVhY3RQcm9wVHlwZXNTZWNyZXQpIHtcbiAgICAgIC8vIEl0IGlzIHN0aWxsIHNhZmUgd2hlbiBjYWxsZWQgZnJvbSBSZWFjdC5cbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaW52YXJpYW50KFxuICAgICAgZmFsc2UsXG4gICAgICAnQ2FsbGluZyBQcm9wVHlwZXMgdmFsaWRhdG9ycyBkaXJlY3RseSBpcyBub3Qgc3VwcG9ydGVkIGJ5IHRoZSBgcHJvcC10eXBlc2AgcGFja2FnZS4gJyArXG4gICAgICAnVXNlIFByb3BUeXBlcy5jaGVja1Byb3BUeXBlcygpIHRvIGNhbGwgdGhlbS4gJyArXG4gICAgICAnUmVhZCBtb3JlIGF0IGh0dHA6Ly9mYi5tZS91c2UtY2hlY2stcHJvcC10eXBlcydcbiAgICApO1xuICB9O1xuICBzaGltLmlzUmVxdWlyZWQgPSBzaGltO1xuICBmdW5jdGlvbiBnZXRTaGltKCkge1xuICAgIHJldHVybiBzaGltO1xuICB9O1xuICAvLyBJbXBvcnRhbnQhXG4gIC8vIEtlZXAgdGhpcyBsaXN0IGluIHN5bmMgd2l0aCBwcm9kdWN0aW9uIHZlcnNpb24gaW4gYC4vZmFjdG9yeVdpdGhUeXBlQ2hlY2tlcnMuanNgLlxuICB2YXIgUmVhY3RQcm9wVHlwZXMgPSB7XG4gICAgYXJyYXk6IHNoaW0sXG4gICAgYm9vbDogc2hpbSxcbiAgICBmdW5jOiBzaGltLFxuICAgIG51bWJlcjogc2hpbSxcbiAgICBvYmplY3Q6IHNoaW0sXG4gICAgc3RyaW5nOiBzaGltLFxuICAgIHN5bWJvbDogc2hpbSxcblxuICAgIGFueTogc2hpbSxcbiAgICBhcnJheU9mOiBnZXRTaGltLFxuICAgIGVsZW1lbnQ6IHNoaW0sXG4gICAgaW5zdGFuY2VPZjogZ2V0U2hpbSxcbiAgICBub2RlOiBzaGltLFxuICAgIG9iamVjdE9mOiBnZXRTaGltLFxuICAgIG9uZU9mOiBnZXRTaGltLFxuICAgIG9uZU9mVHlwZTogZ2V0U2hpbSxcbiAgICBzaGFwZTogZ2V0U2hpbVxuICB9O1xuXG4gIFJlYWN0UHJvcFR5cGVzLmNoZWNrUHJvcFR5cGVzID0gZW1wdHlGdW5jdGlvbjtcbiAgUmVhY3RQcm9wVHlwZXMuUHJvcFR5cGVzID0gUmVhY3RQcm9wVHlwZXM7XG5cbiAgcmV0dXJuIFJlYWN0UHJvcFR5cGVzO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Byb3AtdHlwZXMvZmFjdG9yeVdpdGhUaHJvd2luZ1NoaW1zLmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogXG4gKi9cblxuZnVuY3Rpb24gbWFrZUVtcHR5RnVuY3Rpb24oYXJnKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGFyZztcbiAgfTtcbn1cblxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIGFjY2VwdHMgYW5kIGRpc2NhcmRzIGlucHV0czsgaXQgaGFzIG5vIHNpZGUgZWZmZWN0cy4gVGhpcyBpc1xuICogcHJpbWFyaWx5IHVzZWZ1bCBpZGlvbWF0aWNhbGx5IGZvciBvdmVycmlkYWJsZSBmdW5jdGlvbiBlbmRwb2ludHMgd2hpY2hcbiAqIGFsd2F5cyBuZWVkIHRvIGJlIGNhbGxhYmxlLCBzaW5jZSBKUyBsYWNrcyBhIG51bGwtY2FsbCBpZGlvbSBhbGEgQ29jb2EuXG4gKi9cbnZhciBlbXB0eUZ1bmN0aW9uID0gZnVuY3Rpb24gZW1wdHlGdW5jdGlvbigpIHt9O1xuXG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zID0gbWFrZUVtcHR5RnVuY3Rpb247XG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zRmFsc2UgPSBtYWtlRW1wdHlGdW5jdGlvbihmYWxzZSk7XG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zVHJ1ZSA9IG1ha2VFbXB0eUZ1bmN0aW9uKHRydWUpO1xuZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJuc051bGwgPSBtYWtlRW1wdHlGdW5jdGlvbihudWxsKTtcbmVtcHR5RnVuY3Rpb24udGhhdFJldHVybnNUaGlzID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcztcbn07XG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zQXJndW1lbnQgPSBmdW5jdGlvbiAoYXJnKSB7XG4gIHJldHVybiBhcmc7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGVtcHR5RnVuY3Rpb247XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZW1wdHlGdW5jdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBVc2UgaW52YXJpYW50KCkgdG8gYXNzZXJ0IHN0YXRlIHdoaWNoIHlvdXIgcHJvZ3JhbSBhc3N1bWVzIHRvIGJlIHRydWUuXG4gKlxuICogUHJvdmlkZSBzcHJpbnRmLXN0eWxlIGZvcm1hdCAob25seSAlcyBpcyBzdXBwb3J0ZWQpIGFuZCBhcmd1bWVudHNcbiAqIHRvIHByb3ZpZGUgaW5mb3JtYXRpb24gYWJvdXQgd2hhdCBicm9rZSBhbmQgd2hhdCB5b3Ugd2VyZVxuICogZXhwZWN0aW5nLlxuICpcbiAqIFRoZSBpbnZhcmlhbnQgbWVzc2FnZSB3aWxsIGJlIHN0cmlwcGVkIGluIHByb2R1Y3Rpb24sIGJ1dCB0aGUgaW52YXJpYW50XG4gKiB3aWxsIHJlbWFpbiB0byBlbnN1cmUgbG9naWMgZG9lcyBub3QgZGlmZmVyIGluIHByb2R1Y3Rpb24uXG4gKi9cblxudmFyIHZhbGlkYXRlRm9ybWF0ID0gZnVuY3Rpb24gdmFsaWRhdGVGb3JtYXQoZm9ybWF0KSB7fTtcblxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgdmFsaWRhdGVGb3JtYXQgPSBmdW5jdGlvbiB2YWxpZGF0ZUZvcm1hdChmb3JtYXQpIHtcbiAgICBpZiAoZm9ybWF0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignaW52YXJpYW50IHJlcXVpcmVzIGFuIGVycm9yIG1lc3NhZ2UgYXJndW1lbnQnKTtcbiAgICB9XG4gIH07XG59XG5cbmZ1bmN0aW9uIGludmFyaWFudChjb25kaXRpb24sIGZvcm1hdCwgYSwgYiwgYywgZCwgZSwgZikge1xuICB2YWxpZGF0ZUZvcm1hdChmb3JtYXQpO1xuXG4gIGlmICghY29uZGl0aW9uKSB7XG4gICAgdmFyIGVycm9yO1xuICAgIGlmIChmb3JtYXQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoJ01pbmlmaWVkIGV4Y2VwdGlvbiBvY2N1cnJlZDsgdXNlIHRoZSBub24tbWluaWZpZWQgZGV2IGVudmlyb25tZW50ICcgKyAnZm9yIHRoZSBmdWxsIGVycm9yIG1lc3NhZ2UgYW5kIGFkZGl0aW9uYWwgaGVscGZ1bCB3YXJuaW5ncy4nKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIGFyZ3MgPSBbYSwgYiwgYywgZCwgZSwgZl07XG4gICAgICB2YXIgYXJnSW5kZXggPSAwO1xuICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZm9ybWF0LnJlcGxhY2UoLyVzL2csIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIGFyZ3NbYXJnSW5kZXgrK107XG4gICAgICB9KSk7XG4gICAgICBlcnJvci5uYW1lID0gJ0ludmFyaWFudCBWaW9sYXRpb24nO1xuICAgIH1cblxuICAgIGVycm9yLmZyYW1lc1RvUG9wID0gMTsgLy8gd2UgZG9uJ3QgY2FyZSBhYm91dCBpbnZhcmlhbnQncyBvd24gZnJhbWVcbiAgICB0aHJvdyBlcnJvcjtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGludmFyaWFudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9pbnZhcmlhbnQuanNcbi8vIG1vZHVsZSBpZCA9IDlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIvKipcbiAqIENvcHlyaWdodCAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBSZWFjdFByb3BUeXBlc1NlY3JldCA9ICdTRUNSRVRfRE9fTk9UX1BBU1NfVEhJU19PUl9ZT1VfV0lMTF9CRV9GSVJFRCc7XG5cbm1vZHVsZS5leHBvcnRzID0gUmVhY3RQcm9wVHlwZXNTZWNyZXQ7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcm9wLXR5cGVzL2xpYi9SZWFjdFByb3BUeXBlc1NlY3JldC5qc1xuLy8gbW9kdWxlIGlkID0gMTBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIvKiBlc2xpbnQgcmVhY3QvcmVxdWlyZS1kZWZhdWx0LXByb3BzOiAwICovXG4vKiBlc2xpbnQgYXJyb3ctYm9keS1zdHlsZTogMCAqL1xuaW1wb3J0IGNzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgcGFnZVJlc29sdmVyIGZyb20gJy4vcGFnZS1yZXNvbHZlcic7XG5pbXBvcnQgU2l6ZVBlclBhZ2VEcm9wRG93biBmcm9tICcuL3NpemUtcGVyLXBhZ2UtZHJvcGRvd24nO1xuaW1wb3J0IFBhZ2luYXRpb25MaXN0IGZyb20gJy4vcGFnaW5hdGlvbi1saXN0JztcbmltcG9ydCBQYWdpbmF0aW9uVG90YWwgZnJvbSAnLi9wYWdpbmF0aW9uLXRvdGFsJztcbmltcG9ydCBDb25zdCBmcm9tICcuL2NvbnN0JztcblxuY2xhc3MgUGFnaW5hdGlvbiBleHRlbmRzIHBhZ2VSZXNvbHZlcihDb21wb25lbnQpIHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5jbG9zZURyb3BEb3duID0gdGhpcy5jbG9zZURyb3BEb3duLmJpbmQodGhpcyk7XG4gICAgdGhpcy50b2dnbGVEcm9wRG93biA9IHRoaXMudG9nZ2xlRHJvcERvd24uYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUNoYW5nZVBhZ2UgPSB0aGlzLmhhbmRsZUNoYW5nZVBhZ2UuYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUNoYW5nZVNpemVQZXJQYWdlID0gdGhpcy5oYW5kbGVDaGFuZ2VTaXplUGVyUGFnZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMuc3RhdGUgPSB0aGlzLmluaXRpYWxTdGF0ZSgpO1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICBjb25zdCB7IGRhdGFTaXplLCBjdXJyU2l6ZVBlclBhZ2UgfSA9IG5leHRQcm9wcztcbiAgICBpZiAoY3VyclNpemVQZXJQYWdlICE9PSB0aGlzLnByb3BzLmN1cnJTaXplUGVyUGFnZSB8fCBkYXRhU2l6ZSAhPT0gdGhpcy5wcm9wcy5kYXRhU2l6ZSkge1xuICAgICAgY29uc3QgdG90YWxQYWdlcyA9IHRoaXMuY2FsY3VsYXRlVG90YWxQYWdlKGN1cnJTaXplUGVyUGFnZSwgZGF0YVNpemUpO1xuICAgICAgY29uc3QgbGFzdFBhZ2UgPSB0aGlzLmNhbGN1bGF0ZUxhc3RQYWdlKHRvdGFsUGFnZXMpO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IHRvdGFsUGFnZXMsIGxhc3RQYWdlIH0pO1xuICAgIH1cbiAgfVxuXG4gIHRvZ2dsZURyb3BEb3duKCkge1xuICAgIGNvbnN0IGRyb3Bkb3duT3BlbiA9ICF0aGlzLnN0YXRlLmRyb3Bkb3duT3BlbjtcbiAgICB0aGlzLnNldFN0YXRlKCgpID0+IHtcbiAgICAgIHJldHVybiB7IGRyb3Bkb3duT3BlbiB9O1xuICAgIH0pO1xuICB9XG5cbiAgY2xvc2VEcm9wRG93bigpIHtcbiAgICB0aGlzLnNldFN0YXRlKCgpID0+IHtcbiAgICAgIHJldHVybiB7IGRyb3Bkb3duT3BlbjogZmFsc2UgfTtcbiAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZUNoYW5nZVNpemVQZXJQYWdlKHNpemVQZXJQYWdlKSB7XG4gICAgY29uc3QgeyBjdXJyU2l6ZVBlclBhZ2UsIG9uU2l6ZVBlclBhZ2VDaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3Qgc2VsZWN0ZWRTaXplID0gdHlwZW9mIHNpemVQZXJQYWdlID09PSAnc3RyaW5nJyA/IHBhcnNlSW50KHNpemVQZXJQYWdlLCAxMCkgOiBzaXplUGVyUGFnZTtcbiAgICBsZXQgeyBjdXJyUGFnZSB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoc2VsZWN0ZWRTaXplICE9PSBjdXJyU2l6ZVBlclBhZ2UpIHtcbiAgICAgIGNvbnN0IG5ld1RvdGFsUGFnZXMgPSB0aGlzLmNhbGN1bGF0ZVRvdGFsUGFnZShzZWxlY3RlZFNpemUpO1xuICAgICAgY29uc3QgbmV3TGFzdFBhZ2UgPSB0aGlzLmNhbGN1bGF0ZUxhc3RQYWdlKG5ld1RvdGFsUGFnZXMpO1xuICAgICAgaWYgKGN1cnJQYWdlID4gbmV3TGFzdFBhZ2UpIGN1cnJQYWdlID0gbmV3TGFzdFBhZ2U7XG4gICAgICBvblNpemVQZXJQYWdlQ2hhbmdlKHNlbGVjdGVkU2l6ZSwgY3VyclBhZ2UpO1xuICAgIH1cbiAgICB0aGlzLmNsb3NlRHJvcERvd24oKTtcbiAgfVxuXG4gIGhhbmRsZUNoYW5nZVBhZ2UobmV3UGFnZSkge1xuICAgIGxldCBwYWdlO1xuICAgIGNvbnN0IHtcbiAgICAgIGN1cnJQYWdlLFxuICAgICAgcGFnZVN0YXJ0SW5kZXgsXG4gICAgICBwcmVQYWdlVGV4dCxcbiAgICAgIG5leHRQYWdlVGV4dCxcbiAgICAgIGxhc3RQYWdlVGV4dCxcbiAgICAgIGZpcnN0UGFnZVRleHQsXG4gICAgICBvblBhZ2VDaGFuZ2VcbiAgICAgIC8vIGtlZXBTaXplUGVyUGFnZVN0YXRlXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBsYXN0UGFnZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGlmIChuZXdQYWdlID09PSBwcmVQYWdlVGV4dCkge1xuICAgICAgcGFnZSA9IHRoaXMuYmFja1RvUHJldlBhZ2UoKTtcbiAgICB9IGVsc2UgaWYgKG5ld1BhZ2UgPT09IG5leHRQYWdlVGV4dCkge1xuICAgICAgcGFnZSA9IChjdXJyUGFnZSArIDEpID4gbGFzdFBhZ2UgPyBsYXN0UGFnZSA6IGN1cnJQYWdlICsgMTtcbiAgICB9IGVsc2UgaWYgKG5ld1BhZ2UgPT09IGxhc3RQYWdlVGV4dCkge1xuICAgICAgcGFnZSA9IGxhc3RQYWdlO1xuICAgIH0gZWxzZSBpZiAobmV3UGFnZSA9PT0gZmlyc3RQYWdlVGV4dCkge1xuICAgICAgcGFnZSA9IHBhZ2VTdGFydEluZGV4O1xuICAgIH0gZWxzZSB7XG4gICAgICBwYWdlID0gcGFyc2VJbnQobmV3UGFnZSwgMTApO1xuICAgIH1cblxuICAgIC8vIGlmIChrZWVwU2l6ZVBlclBhZ2VTdGF0ZSkgeyB0aGlzLmNsb3NlRHJvcERvd24oKTsgfVxuXG4gICAgaWYgKHBhZ2UgIT09IGN1cnJQYWdlKSB7XG4gICAgICBvblBhZ2VDaGFuZ2UocGFnZSk7XG4gICAgfVxuICB9XG5cbiAgZGVmYXVsdFRvdGFsID0gKGZyb20sIHRvLCBzaXplKSA9PiAoXG4gICAgPFBhZ2luYXRpb25Ub3RhbFxuICAgICAgZnJvbT17IGZyb20gfVxuICAgICAgdG89eyB0byB9XG4gICAgICBkYXRhU2l6ZT17IHNpemUgfVxuICAgIC8+XG4gICk7XG5cbiAgc2V0VG90YWwgPSAoZnJvbSwgdG8sIHNpemUsIHRvdGFsKSA9PiB7XG4gICAgaWYgKHRvdGFsICYmICh0eXBlb2YgdG90YWwgPT09ICdmdW5jdGlvbicpKSB7XG4gICAgICByZXR1cm4gdG90YWwoZnJvbSwgdG8sIHNpemUpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmRlZmF1bHRUb3RhbChmcm9tLCB0bywgc2l6ZSk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgdG90YWxQYWdlcywgbGFzdFBhZ2UsIGRyb3Bkb3duT3Blbjogb3BlbiB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7XG4gICAgICBzaG93VG90YWwsXG4gICAgICBkYXRhU2l6ZSxcbiAgICAgIHBhZ2luYXRpb25Ub3RhbFJlbmRlcmVyLFxuICAgICAgc2l6ZVBlclBhZ2VMaXN0LFxuICAgICAgY3VyclNpemVQZXJQYWdlLFxuICAgICAgaGlkZVNpemVQZXJQYWdlLFxuICAgICAgaGlkZVBhZ2VMaXN0T25seU9uZVBhZ2VcbiAgICB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwYWdlcyA9IHRoaXMuY2FsY3VsYXRlUGFnZVN0YXR1cyh0aGlzLmNhbGN1bGF0ZVBhZ2VzKHRvdGFsUGFnZXMpLCBsYXN0UGFnZSk7XG4gICAgY29uc3QgW2Zyb20sIHRvXSA9IHRoaXMuY2FsY3VsYXRlRnJvbVRvKCk7XG4gICAgY29uc3QgcGFnZUxpc3RDbGFzcyA9IGNzKFxuICAgICAgJ3JlYWN0LWJvb3RzdHJhcC10YWJsZS1wYWdpbmF0aW9uLWxpc3QnLFxuICAgICAgJ2NvbC1tZC02IGNvbC14cy02IGNvbC1zbS02IGNvbC1sZy02Jywge1xuICAgICAgICAncmVhY3QtYm9vdHN0cmFwLXRhYmxlLXBhZ2luYXRpb24tbGlzdC1oaWRkZW4nOiAoaGlkZVBhZ2VMaXN0T25seU9uZVBhZ2UgJiYgdG90YWxQYWdlcyA9PT0gMSlcbiAgICAgIH0pO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyByZWFjdC1ib290c3RyYXAtdGFibGUtcGFnaW5hdGlvblwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC02IGNvbC14cy02IGNvbC1zbS02IGNvbC1sZy02XCI+XG4gICAgICAgICAge1xuICAgICAgICAgICAgc2l6ZVBlclBhZ2VMaXN0Lmxlbmd0aCA+IDEgJiYgIWhpZGVTaXplUGVyUGFnZSA/XG4gICAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICA8U2l6ZVBlclBhZ2VEcm9wRG93blxuICAgICAgICAgICAgICAgICAgY3VyclNpemVQZXJQYWdlPXsgYCR7Y3VyclNpemVQZXJQYWdlfWAgfVxuICAgICAgICAgICAgICAgICAgb3B0aW9ucz17IHRoaXMuY2FsY3VsYXRlU2l6ZVBlclBhZ2VTdGF0dXMoKSB9XG4gICAgICAgICAgICAgICAgICBvblNpemVQZXJQYWdlQ2hhbmdlPXsgdGhpcy5oYW5kbGVDaGFuZ2VTaXplUGVyUGFnZSB9XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXsgdGhpcy50b2dnbGVEcm9wRG93biB9XG4gICAgICAgICAgICAgICAgICBvbkJsdXI9eyB0aGlzLmNsb3NlRHJvcERvd24gfVxuICAgICAgICAgICAgICAgICAgb3Blbj17IG9wZW4gfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICkgOiBudWxsXG4gICAgICAgICAgfVxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHNob3dUb3RhbCA/XG4gICAgICAgICAgICAgIHRoaXMuc2V0VG90YWwoXG4gICAgICAgICAgICAgICAgZnJvbSxcbiAgICAgICAgICAgICAgICB0byxcbiAgICAgICAgICAgICAgICBkYXRhU2l6ZSxcbiAgICAgICAgICAgICAgICBwYWdpbmF0aW9uVG90YWxSZW5kZXJlclxuICAgICAgICAgICAgICApIDogbnVsbFxuICAgICAgICAgIH1cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgcGFnZUxpc3RDbGFzcyB9PlxuICAgICAgICAgIDxQYWdpbmF0aW9uTGlzdCBwYWdlcz17IHBhZ2VzIH0gb25QYWdlQ2hhbmdlPXsgdGhpcy5oYW5kbGVDaGFuZ2VQYWdlIH0gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblBhZ2luYXRpb24ucHJvcFR5cGVzID0ge1xuICBkYXRhU2l6ZTogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICBzaXplUGVyUGFnZUxpc3Q6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxuICBjdXJyUGFnZTogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICBjdXJyU2l6ZVBlclBhZ2U6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgb25QYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvblNpemVQZXJQYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBwYWdlU3RhcnRJbmRleDogUHJvcFR5cGVzLm51bWJlcixcbiAgcGFnaW5hdGlvblNpemU6IFByb3BUeXBlcy5udW1iZXIsXG4gIHNob3dUb3RhbDogUHJvcFR5cGVzLmJvb2wsXG4gIHBhZ2luYXRpb25Ub3RhbFJlbmRlcmVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgZmlyc3RQYWdlVGV4dDogUHJvcFR5cGVzLnN0cmluZyxcbiAgcHJlUGFnZVRleHQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG5leHRQYWdlVGV4dDogUHJvcFR5cGVzLnN0cmluZyxcbiAgbGFzdFBhZ2VUZXh0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICBuZXh0UGFnZVRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBwcmVQYWdlVGl0bGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGZpcnN0UGFnZVRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBsYXN0UGFnZVRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB3aXRoRmlyc3RBbmRMYXN0OiBQcm9wVHlwZXMuYm9vbCxcbiAgYWx3YXlzU2hvd0FsbEJ0bnM6IFByb3BUeXBlcy5ib29sLFxuICBoaWRlU2l6ZVBlclBhZ2U6IFByb3BUeXBlcy5ib29sLFxuICBoaWRlUGFnZUxpc3RPbmx5T25lUGFnZTogUHJvcFR5cGVzLmJvb2xcbn07XG5cblBhZ2luYXRpb24uZGVmYXVsdFByb3BzID0ge1xuICBwYWdlU3RhcnRJbmRleDogQ29uc3QuUEFHRV9TVEFSVF9JTkRFWCxcbiAgcGFnaW5hdGlvblNpemU6IENvbnN0LlBBR0lOQVRJT05fU0laRSxcbiAgd2l0aEZpcnN0QW5kTGFzdDogQ29uc3QuV2l0aF9GSVJTVF9BTkRfTEFTVCxcbiAgYWx3YXlzU2hvd0FsbEJ0bnM6IENvbnN0LlNIT1dfQUxMX1BBR0VfQlROUyxcbiAgc2hvd1RvdGFsOiBDb25zdC5TSE9XX1RPVEFMLFxuICBwYWdpbmF0aW9uVG90YWxSZW5kZXJlcjogQ29uc3QuUEFHSU5BVElPTl9UT1RBTCxcbiAgZmlyc3RQYWdlVGV4dDogQ29uc3QuRklSU1RfUEFHRV9URVhULFxuICBwcmVQYWdlVGV4dDogQ29uc3QuUFJFX1BBR0VfVEVYVCxcbiAgbmV4dFBhZ2VUZXh0OiBDb25zdC5ORVhUX1BBR0VfVEVYVCxcbiAgbGFzdFBhZ2VUZXh0OiBDb25zdC5MQVNUX1BBR0VfVEVYVCxcbiAgc2l6ZVBlclBhZ2VMaXN0OiBDb25zdC5TSVpFX1BFUl9QQUdFX0xJU1QsXG4gIG5leHRQYWdlVGl0bGU6IENvbnN0Lk5FWFRfUEFHRV9USVRMRSxcbiAgcHJlUGFnZVRpdGxlOiBDb25zdC5QUkVfUEFHRV9USVRMRSxcbiAgZmlyc3RQYWdlVGl0bGU6IENvbnN0LkZJUlNUX1BBR0VfVElUTEUsXG4gIGxhc3RQYWdlVGl0bGU6IENvbnN0LkxBU1RfUEFHRV9USVRMRSxcbiAgaGlkZVNpemVQZXJQYWdlOiBDb25zdC5ISURFX1NJWkVfUEVSX1BBR0UsXG4gIGhpZGVQYWdlTGlzdE9ubHlPbmVQYWdlOiBDb25zdC5ISURFX1BBR0VfTElTVF9PTkxZX09ORV9QQUdFXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQYWdpbmF0aW9uO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3BhZ2luYXRpb24uanMiLCIvKiBlc2xpbnQgbm8tbWl4ZWQtb3BlcmF0b3JzOiAwICovXG5pbXBvcnQgQ29uc3QgZnJvbSAnLi9jb25zdCc7XG5cbmV4cG9ydCBkZWZhdWx0IEV4dGVuZEJhc2UgPT5cbiAgY2xhc3MgUGFnZVJlc29sdmVyIGV4dGVuZHMgRXh0ZW5kQmFzZSB7XG4gICAgYmFja1RvUHJldlBhZ2UoKSB7XG4gICAgICBjb25zdCB7IGN1cnJQYWdlLCBwYWdlU3RhcnRJbmRleCB9ID0gdGhpcy5wcm9wcztcbiAgICAgIHJldHVybiAoY3VyclBhZ2UgLSAxKSA8IHBhZ2VTdGFydEluZGV4ID8gcGFnZVN0YXJ0SW5kZXggOiBjdXJyUGFnZSAtIDE7XG4gICAgfVxuXG4gICAgZ29Ub05leHRQYWdlKCkge1xuICAgICAgY29uc3QgeyBjdXJyUGFnZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgbGFzdFBhZ2UgfSA9IHRoaXMuc3RhdGU7XG4gICAgICByZXR1cm4gKGN1cnJQYWdlICsgMSkgPiBsYXN0UGFnZSA/IGxhc3RQYWdlIDogY3VyclBhZ2UgKyAxO1xuICAgIH1cblxuICAgIGluaXRpYWxTdGF0ZSgpIHtcbiAgICAgIGNvbnN0IHRvdGFsUGFnZXMgPSB0aGlzLmNhbGN1bGF0ZVRvdGFsUGFnZSgpO1xuICAgICAgY29uc3QgbGFzdFBhZ2UgPSB0aGlzLmNhbGN1bGF0ZUxhc3RQYWdlKHRvdGFsUGFnZXMpO1xuICAgICAgcmV0dXJuIHsgdG90YWxQYWdlcywgbGFzdFBhZ2UsIGRyb3Bkb3duT3BlbjogZmFsc2UgfTtcbiAgICB9XG5cbiAgICBjYWxjdWxhdGVUb3RhbFBhZ2Uoc2l6ZVBlclBhZ2UgPSB0aGlzLnByb3BzLmN1cnJTaXplUGVyUGFnZSwgZGF0YVNpemUgPSB0aGlzLnByb3BzLmRhdGFTaXplKSB7XG4gICAgICByZXR1cm4gTWF0aC5jZWlsKGRhdGFTaXplIC8gc2l6ZVBlclBhZ2UpO1xuICAgIH1cblxuICAgIGNhbGN1bGF0ZUxhc3RQYWdlKHRvdGFsUGFnZXMpIHtcbiAgICAgIGNvbnN0IHsgcGFnZVN0YXJ0SW5kZXggfSA9IHRoaXMucHJvcHM7XG4gICAgICByZXR1cm4gcGFnZVN0YXJ0SW5kZXggKyB0b3RhbFBhZ2VzIC0gMTtcbiAgICB9XG5cbiAgICBjYWxjdWxhdGVGcm9tVG8oKSB7XG4gICAgICBjb25zdCB7XG4gICAgICAgIGRhdGFTaXplLFxuICAgICAgICBjdXJyUGFnZSxcbiAgICAgICAgY3VyclNpemVQZXJQYWdlLFxuICAgICAgICBwYWdlU3RhcnRJbmRleFxuICAgICAgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCBvZmZzZXQgPSBNYXRoLmFicyhDb25zdC5QQUdFX1NUQVJUX0lOREVYIC0gcGFnZVN0YXJ0SW5kZXgpO1xuXG4gICAgICBsZXQgZnJvbSA9ICgoY3VyclBhZ2UgLSBwYWdlU3RhcnRJbmRleCkgKiBjdXJyU2l6ZVBlclBhZ2UpO1xuICAgICAgZnJvbSA9IGRhdGFTaXplID09PSAwID8gMCA6IGZyb20gKyAxO1xuICAgICAgbGV0IHRvID0gTWF0aC5taW4oY3VyclNpemVQZXJQYWdlICogKGN1cnJQYWdlICsgb2Zmc2V0KSwgZGF0YVNpemUpO1xuICAgICAgaWYgKHRvID4gZGF0YVNpemUpIHRvID0gZGF0YVNpemU7XG5cbiAgICAgIHJldHVybiBbZnJvbSwgdG9dO1xuICAgIH1cblxuICAgIGNhbGN1bGF0ZVBhZ2VzKFxuICAgICAgdG90YWxQYWdlcyA9IHRoaXMuc3RhdGUudG90YWxQYWdlcyxcbiAgICAgIGxhc3RQYWdlID0gdGhpcy5zdGF0ZS5sYXN0UGFnZSkge1xuICAgICAgY29uc3Qge1xuICAgICAgICBjdXJyUGFnZSxcbiAgICAgICAgcGFnaW5hdGlvblNpemUsXG4gICAgICAgIHBhZ2VTdGFydEluZGV4LFxuICAgICAgICB3aXRoRmlyc3RBbmRMYXN0LFxuICAgICAgICBmaXJzdFBhZ2VUZXh0LFxuICAgICAgICBwcmVQYWdlVGV4dCxcbiAgICAgICAgbmV4dFBhZ2VUZXh0LFxuICAgICAgICBsYXN0UGFnZVRleHQsXG4gICAgICAgIGFsd2F5c1Nob3dBbGxCdG5zXG4gICAgICB9ID0gdGhpcy5wcm9wcztcblxuICAgICAgbGV0IHBhZ2VzO1xuICAgICAgbGV0IGVuZFBhZ2UgPSB0b3RhbFBhZ2VzO1xuICAgICAgaWYgKGVuZFBhZ2UgPD0gMCkgcmV0dXJuIFtdO1xuXG4gICAgICBsZXQgc3RhcnRQYWdlID0gTWF0aC5tYXgoY3VyclBhZ2UgLSBNYXRoLmZsb29yKHBhZ2luYXRpb25TaXplIC8gMiksIHBhZ2VTdGFydEluZGV4KTtcbiAgICAgIGVuZFBhZ2UgPSBzdGFydFBhZ2UgKyBwYWdpbmF0aW9uU2l6ZSAtIDE7XG5cbiAgICAgIGlmIChlbmRQYWdlID4gbGFzdFBhZ2UpIHtcbiAgICAgICAgZW5kUGFnZSA9IGxhc3RQYWdlO1xuICAgICAgICBzdGFydFBhZ2UgPSBlbmRQYWdlIC0gcGFnaW5hdGlvblNpemUgKyAxO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RhcnRQYWdlICE9PSBwYWdlU3RhcnRJbmRleCAmJiB0b3RhbFBhZ2VzID4gcGFnaW5hdGlvblNpemUgJiYgd2l0aEZpcnN0QW5kTGFzdCkge1xuICAgICAgICBwYWdlcyA9IFtmaXJzdFBhZ2VUZXh0LCBwcmVQYWdlVGV4dF07XG4gICAgICB9IGVsc2UgaWYgKHRvdGFsUGFnZXMgPiAxIHx8IGFsd2F5c1Nob3dBbGxCdG5zKSB7XG4gICAgICAgIHBhZ2VzID0gW3ByZVBhZ2VUZXh0XTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBhZ2VzID0gW107XG4gICAgICB9XG5cbiAgICAgIGZvciAobGV0IGkgPSBzdGFydFBhZ2U7IGkgPD0gZW5kUGFnZTsgaSArPSAxKSB7XG4gICAgICAgIGlmIChpID49IHBhZ2VTdGFydEluZGV4KSBwYWdlcy5wdXNoKGkpO1xuICAgICAgfVxuXG4gICAgICBpZiAoZW5kUGFnZSA8PSBsYXN0UGFnZSAmJiBwYWdlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgIHBhZ2VzLnB1c2gobmV4dFBhZ2VUZXh0KTtcbiAgICAgIH1cbiAgICAgIGlmIChlbmRQYWdlICE9PSBsYXN0UGFnZSAmJiB3aXRoRmlyc3RBbmRMYXN0KSB7XG4gICAgICAgIHBhZ2VzLnB1c2gobGFzdFBhZ2VUZXh0KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBwYWdlcztcbiAgICB9XG5cbiAgICBjYWxjdWxhdGVQYWdlU3RhdHVzKHBhZ2VzID0gW10sIGxhc3RQYWdlID0gdGhpcy5zdGF0ZS5sYXN0UGFnZSkge1xuICAgICAgY29uc3Qge1xuICAgICAgICBjdXJyUGFnZSxcbiAgICAgICAgcGFnZVN0YXJ0SW5kZXgsXG4gICAgICAgIGZpcnN0UGFnZVRleHQsXG4gICAgICAgIHByZVBhZ2VUZXh0LFxuICAgICAgICBuZXh0UGFnZVRleHQsXG4gICAgICAgIGxhc3RQYWdlVGV4dCxcbiAgICAgICAgYWx3YXlzU2hvd0FsbEJ0bnNcbiAgICAgIH0gPSB0aGlzLnByb3BzO1xuICAgICAgY29uc3QgaXNTdGFydCA9IHBhZ2UgPT5cbiAgICAgICAgKGN1cnJQYWdlID09PSBwYWdlU3RhcnRJbmRleCAmJiAocGFnZSA9PT0gZmlyc3RQYWdlVGV4dCB8fCBwYWdlID09PSBwcmVQYWdlVGV4dCkpO1xuICAgICAgY29uc3QgaXNFbmQgPSBwYWdlID0+XG4gICAgICAgIChjdXJyUGFnZSA9PT0gbGFzdFBhZ2UgJiYgKHBhZ2UgPT09IG5leHRQYWdlVGV4dCB8fCBwYWdlID09PSBsYXN0UGFnZVRleHQpKTtcblxuICAgICAgcmV0dXJuIHBhZ2VzXG4gICAgICAgIC5maWx0ZXIoKHBhZ2UpID0+IHtcbiAgICAgICAgICBpZiAoYWx3YXlzU2hvd0FsbEJ0bnMpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gIShpc1N0YXJ0KHBhZ2UpIHx8IGlzRW5kKHBhZ2UpKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm1hcCgocGFnZSkgPT4ge1xuICAgICAgICAgIGxldCB0aXRsZTtcbiAgICAgICAgICBjb25zdCBhY3RpdmUgPSBwYWdlID09PSBjdXJyUGFnZTtcbiAgICAgICAgICBjb25zdCBkaXNhYmxlZCA9IChpc1N0YXJ0KHBhZ2UpIHx8IGlzRW5kKHBhZ2UpKTtcblxuICAgICAgICAgIGlmIChwYWdlID09PSBuZXh0UGFnZVRleHQpIHtcbiAgICAgICAgICAgIHRpdGxlID0gdGhpcy5wcm9wcy5uZXh0UGFnZVRpdGxlO1xuICAgICAgICAgIH0gZWxzZSBpZiAocGFnZSA9PT0gcHJlUGFnZVRleHQpIHtcbiAgICAgICAgICAgIHRpdGxlID0gdGhpcy5wcm9wcy5wcmVQYWdlVGl0bGU7XG4gICAgICAgICAgfSBlbHNlIGlmIChwYWdlID09PSBmaXJzdFBhZ2VUZXh0KSB7XG4gICAgICAgICAgICB0aXRsZSA9IHRoaXMucHJvcHMuZmlyc3RQYWdlVGl0bGU7XG4gICAgICAgICAgfSBlbHNlIGlmIChwYWdlID09PSBsYXN0UGFnZVRleHQpIHtcbiAgICAgICAgICAgIHRpdGxlID0gdGhpcy5wcm9wcy5sYXN0UGFnZVRpdGxlO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aXRsZSA9IGAke3BhZ2V9YDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4geyBwYWdlLCBhY3RpdmUsIGRpc2FibGVkLCB0aXRsZSB9O1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjYWxjdWxhdGVTaXplUGVyUGFnZVN0YXR1cygpIHtcbiAgICAgIGNvbnN0IHsgc2l6ZVBlclBhZ2VMaXN0IH0gPSB0aGlzLnByb3BzO1xuICAgICAgcmV0dXJuIHNpemVQZXJQYWdlTGlzdC5tYXAoKF9zaXplUGVyUGFnZSkgPT4ge1xuICAgICAgICBjb25zdCBwYWdlVGV4dCA9IF9zaXplUGVyUGFnZS50ZXh0IHx8IF9zaXplUGVyUGFnZTtcbiAgICAgICAgY29uc3QgcGFnZU51bWJlciA9IF9zaXplUGVyUGFnZS52YWx1ZSB8fCBfc2l6ZVBlclBhZ2U7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgdGV4dDogYCR7cGFnZVRleHR9YCxcbiAgICAgICAgICBwYWdlOiBwYWdlTnVtYmVyXG4gICAgICAgIH07XG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdlLXJlc29sdmVyLmpzIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBCb290c3RyYXBDb250ZXh0IH0gZnJvbSAnLi9ib290c3RyYXAnO1xuaW1wb3J0IFNpemVQZXJQYWdlT3B0aW9uIGZyb20gJy4vc2l6ZS1wZXItcGFnZS1vcHRpb24nO1xuXG5jb25zdCBzaXplUGVyUGFnZURlZmF1bHRDbGFzcyA9ICdyZWFjdC1icy10YWJsZS1zaXplUGVyUGFnZS1kcm9wZG93bic7XG5cbmNvbnN0IFNpemVQZXJQYWdlRHJvcERvd24gPSAocHJvcHMpID0+IHtcbiAgY29uc3Qge1xuICAgIG9wZW4sXG4gICAgaGlkZGVuLFxuICAgIG9uQ2xpY2ssXG4gICAgb25CbHVyLFxuICAgIG9wdGlvbnMsXG4gICAgY2xhc3NOYW1lLFxuICAgIHZhcmlhdGlvbixcbiAgICBidG5Db250ZXh0dWFsLFxuICAgIGN1cnJTaXplUGVyUGFnZSxcbiAgICBvblNpemVQZXJQYWdlQ2hhbmdlXG4gIH0gPSBwcm9wcztcblxuICBjb25zdCBkcm9wRG93blN0eWxlID0geyB2aXNpYmlsaXR5OiBoaWRkZW4gPyAnaGlkZGVuJyA6ICd2aXNpYmxlJyB9O1xuICBjb25zdCBvcGVuQ2xhc3MgPSBvcGVuID8gJ29wZW4gc2hvdycgOiAnJztcbiAgY29uc3QgZHJvcGRvd25DbGFzc2VzID0gY3MoXG4gICAgb3BlbkNsYXNzLFxuICAgIHNpemVQZXJQYWdlRGVmYXVsdENsYXNzLFxuICAgIHZhcmlhdGlvbixcbiAgICBjbGFzc05hbWUsXG4gICk7XG5cbiAgcmV0dXJuIChcbiAgICA8Qm9vdHN0cmFwQ29udGV4dC5Db25zdW1lcj5cbiAgICAgIHtcbiAgICAgICAgKHsgYm9vdHN0cmFwNCB9KSA9PiAoXG4gICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgIHN0eWxlPXsgZHJvcERvd25TdHlsZSB9XG4gICAgICAgICAgICBjbGFzc05hbWU9eyBkcm9wZG93bkNsYXNzZXMgfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgaWQ9XCJwYWdlRHJvcERvd25cIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9eyBgYnRuICR7YnRuQ29udGV4dHVhbH0gZHJvcGRvd24tdG9nZ2xlYCB9XG4gICAgICAgICAgICAgIGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIlxuICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXsgb3BlbiB9XG4gICAgICAgICAgICAgIG9uQ2xpY2s9eyBvbkNsaWNrIH1cbiAgICAgICAgICAgICAgb25CbHVyPXsgb25CbHVyIH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgeyBjdXJyU2l6ZVBlclBhZ2UgfVxuICAgICAgICAgICAgICB7ICcgJyB9XG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBib290c3RyYXA0ID8gbnVsbCA6IChcbiAgICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjYXJldFwiIC8+XG4gICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgIDx1bFxuICAgICAgICAgICAgICBjbGFzc05hbWU9eyBgZHJvcGRvd24tbWVudSAke29wZW5DbGFzc31gIH1cbiAgICAgICAgICAgICAgcm9sZT1cIm1lbnVcIlxuICAgICAgICAgICAgICBhcmlhLWxhYmVsbGVkYnk9XCJwYWdlRHJvcERvd25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgb3B0aW9ucy5tYXAob3B0aW9uID0+IChcbiAgICAgICAgICAgICAgICAgIDxTaXplUGVyUGFnZU9wdGlvblxuICAgICAgICAgICAgICAgICAgICB7IC4uLm9wdGlvbiB9XG4gICAgICAgICAgICAgICAgICAgIGtleT17IG9wdGlvbi50ZXh0IH1cbiAgICAgICAgICAgICAgICAgICAgYm9vdHN0cmFwND17IGJvb3RzdHJhcDQgfVxuICAgICAgICAgICAgICAgICAgICBvblNpemVQZXJQYWdlQ2hhbmdlPXsgb25TaXplUGVyUGFnZUNoYW5nZSB9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkpXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICApXG4gICAgICB9XG4gICAgPC9Cb290c3RyYXBDb250ZXh0LkNvbnN1bWVyPlxuICApO1xufTtcblxuU2l6ZVBlclBhZ2VEcm9wRG93bi5wcm9wVHlwZXMgPSB7XG4gIGN1cnJTaXplUGVyUGFnZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBvcHRpb25zOiBQcm9wVHlwZXMuYXJyYXkuaXNSZXF1aXJlZCxcbiAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgb25CbHVyOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvblNpemVQZXJQYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvcGVuOiBQcm9wVHlwZXMuYm9vbCxcbiAgaGlkZGVuOiBQcm9wVHlwZXMuYm9vbCxcbiAgYnRuQ29udGV4dHVhbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgdmFyaWF0aW9uOiBQcm9wVHlwZXMub25lT2YoWydkcm9wZG93bicsICdkcm9wdXAnXSksXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZ1xufTtcblNpemVQZXJQYWdlRHJvcERvd24uZGVmYXVsdFByb3BzID0ge1xuICBvcGVuOiBmYWxzZSxcbiAgaGlkZGVuOiBmYWxzZSxcbiAgYnRuQ29udGV4dHVhbDogJ2J0bi1kZWZhdWx0IGJ0bi1zZWNvbmRhcnknLFxuICB2YXJpYXRpb246ICdkcm9wZG93bicsXG4gIGNsYXNzTmFtZTogJydcbn07XG5cblxuZXhwb3J0IGRlZmF1bHQgU2l6ZVBlclBhZ2VEcm9wRG93bjtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9zaXplLXBlci1wYWdlLWRyb3Bkb3duLmpzIiwiLyogZXNsaW50IGpzeC1hMTF5L2hyZWYtbm8taGFzaDogMCAqL1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmNvbnN0IFNpemVQZXJQYWdlT3B0aW9uID0gKHtcbiAgdGV4dCxcbiAgcGFnZSxcbiAgb25TaXplUGVyUGFnZUNoYW5nZSxcbiAgYm9vdHN0cmFwNFxufSkgPT4gKGJvb3RzdHJhcDQgPyAoXG4gIDxhXG4gICAgaHJlZj1cIiNcIlxuICAgIHRhYkluZGV4PVwiLTFcIlxuICAgIHJvbGU9XCJtZW51aXRlbVwiXG4gICAgY2xhc3NOYW1lPVwiZHJvcGRvd24taXRlbVwiXG4gICAgZGF0YS1wYWdlPXsgcGFnZSB9XG4gICAgb25Nb3VzZURvd249eyAoZSkgPT4ge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgb25TaXplUGVyUGFnZUNoYW5nZShwYWdlKTtcbiAgICB9IH1cbiAgPlxuICAgIHsgdGV4dCB9XG4gIDwvYT5cbikgOiAoXG4gIDxsaVxuICAgIGtleT17IHRleHQgfVxuICAgIHJvbGU9XCJwcmVzZW50YXRpb25cIlxuICAgIGNsYXNzTmFtZT1cImRyb3Bkb3duLWl0ZW1cIlxuICA+XG4gICAgPGFcbiAgICAgIGhyZWY9XCIjXCJcbiAgICAgIHRhYkluZGV4PVwiLTFcIlxuICAgICAgcm9sZT1cIm1lbnVpdGVtXCJcbiAgICAgIGRhdGEtcGFnZT17IHBhZ2UgfVxuICAgICAgb25Nb3VzZURvd249eyAoZSkgPT4ge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIG9uU2l6ZVBlclBhZ2VDaGFuZ2UocGFnZSk7XG4gICAgICB9IH1cbiAgICA+XG4gICAgICB7IHRleHQgfVxuICAgIDwvYT5cbiAgPC9saT5cbikpO1xuXG5TaXplUGVyUGFnZU9wdGlvbi5wcm9wVHlwZXMgPSB7XG4gIHRleHQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgcGFnZTogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICBvblNpemVQZXJQYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBib290c3RyYXA0OiBQcm9wVHlwZXMuYm9vbFxufTtcblxuU2l6ZVBlclBhZ2VPcHRpb24uZGVmYXVsdFByb3BzID0ge1xuICBib290c3RyYXA0OiBmYWxzZVxufTtcblxuZXhwb3J0IGRlZmF1bHQgU2l6ZVBlclBhZ2VPcHRpb247XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9zcmMvc2l6ZS1wZXItcGFnZS1vcHRpb24uanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IFBhZ2VCdXR0b24gZnJvbSAnLi9wYWdlLWJ1dHRvbic7XG5cbmNvbnN0IFBhZ2luYXRvbkxpc3QgPSBwcm9wcyA9PiAoXG4gIDx1bCBjbGFzc05hbWU9XCJwYWdpbmF0aW9uIHJlYWN0LWJvb3RzdHJhcC10YWJsZS1wYWdlLWJ0bnMtdWxcIj5cbiAgICB7XG4gICAgICBwcm9wcy5wYWdlcy5tYXAocGFnZVByb3BzID0+IChcbiAgICAgICAgPFBhZ2VCdXR0b25cbiAgICAgICAgICBrZXk9eyBwYWdlUHJvcHMucGFnZSB9XG4gICAgICAgICAgeyAuLi5wYWdlUHJvcHMgfVxuICAgICAgICAgIG9uUGFnZUNoYW5nZT17IHByb3BzLm9uUGFnZUNoYW5nZSB9XG4gICAgICAgIC8+XG4gICAgICApKVxuICAgIH1cbiAgPC91bD5cbik7XG5cblBhZ2luYXRvbkxpc3QucHJvcFR5cGVzID0ge1xuICBwYWdlczogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBwYWdlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubnVtYmVyLCBQcm9wVHlwZXMuc3RyaW5nXSksXG4gICAgYWN0aXZlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBkaXNhYmxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZ1xuICB9KSkuaXNSZXF1aXJlZCxcbiAgb25QYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQYWdpbmF0b25MaXN0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFja2FnZXMvcmVhY3QtYm9vdHN0cmFwLXRhYmxlMi1wYWdpbmF0b3Ivc3JjL3BhZ2luYXRpb24tbGlzdC5qcyIsIi8qIGVzbGludCByZWFjdC9yZXF1aXJlLWRlZmF1bHQtcHJvcHM6IDAgKi9cbi8qIGVzbGludCBqc3gtYTExeS9ocmVmLW5vLWhhc2g6IDAgKi9cbmltcG9ydCBjcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5jbGFzcyBQYWdlQnV0dG9uIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5oYW5kbGVDbGljayA9IHRoaXMuaGFuZGxlQ2xpY2suYmluZCh0aGlzKTtcbiAgfVxuXG4gIGhhbmRsZUNsaWNrKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdGhpcy5wcm9wcy5vblBhZ2VDaGFuZ2UodGhpcy5wcm9wcy5wYWdlKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICBwYWdlLFxuICAgICAgdGl0bGUsXG4gICAgICBhY3RpdmUsXG4gICAgICBkaXNhYmxlZFxuICAgIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGNsYXNzZXMgPSBjcyh7XG4gICAgICBhY3RpdmUsXG4gICAgICBkaXNhYmxlZCxcbiAgICAgICdwYWdlLWl0ZW0nOiB0cnVlXG4gICAgfSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGxpIGNsYXNzTmFtZT17IGNsYXNzZXMgfSB0aXRsZT17IHRpdGxlIH0+XG4gICAgICAgIDxhIGhyZWY9XCIjXCIgb25DbGljaz17IHRoaXMuaGFuZGxlQ2xpY2sgfSBjbGFzc05hbWU9XCJwYWdlLWxpbmtcIj57IHBhZ2UgfTwvYT5cbiAgICAgIDwvbGk+XG4gICAgKTtcbiAgfVxufVxuXG5QYWdlQnV0dG9uLnByb3BUeXBlcyA9IHtcbiAgb25QYWdlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBwYWdlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubnVtYmVyLCBQcm9wVHlwZXMuc3RyaW5nXSkuaXNSZXF1aXJlZCxcbiAgYWN0aXZlOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmdcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFBhZ2VCdXR0b247XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9wYWNrYWdlcy9yZWFjdC1ib290c3RyYXAtdGFibGUyLXBhZ2luYXRvci9zcmMvcGFnZS1idXR0b24uanMiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgUGFnaW5hdGlvblRvdGFsID0gcHJvcHMgPT4gKFxuICA8c3BhbiBjbGFzc05hbWU9XCJyZWFjdC1ib290c3RyYXAtdGFibGUtcGFnaW5hdGlvbi10b3RhbFwiPlxuICAgICZuYnNwO1Nob3dpbmcgcm93cyB7IHByb3BzLmZyb20gfSB0byZuYnNwO3sgcHJvcHMudG8gfSBvZiZuYnNwO3sgcHJvcHMuZGF0YVNpemUgfVxuICA8L3NwYW4+XG4pO1xuXG5QYWdpbmF0aW9uVG90YWwucHJvcFR5cGVzID0ge1xuICBmcm9tOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHRvOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGRhdGFTaXplOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWRcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFBhZ2luYXRpb25Ub3RhbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdpbmF0aW9uLXRvdGFsLmpzIiwiY29uc3QgZ2V0Tm9ybWFsaXplZFBhZ2UgPSAoXG4gIHBhZ2UsXG4gIHBhZ2VTdGFydEluZGV4XG4pID0+IHtcbiAgY29uc3Qgb2Zmc2V0ID0gTWF0aC5hYnMoMSAtIHBhZ2VTdGFydEluZGV4KTtcbiAgcmV0dXJuIHBhZ2UgKyBvZmZzZXQ7XG59O1xuXG5jb25zdCBlbmRJbmRleCA9IChcbiAgcGFnZSxcbiAgc2l6ZVBlclBhZ2UsXG4gIHBhZ2VTdGFydEluZGV4XG4pID0+IChnZXROb3JtYWxpemVkUGFnZShwYWdlLCBwYWdlU3RhcnRJbmRleCkgKiBzaXplUGVyUGFnZSkgLSAxO1xuXG5jb25zdCBzdGFydEluZGV4ID0gKFxuICBlbmQsXG4gIHNpemVQZXJQYWdlLFxuKSA9PiBlbmQgLSAoc2l6ZVBlclBhZ2UgLSAxKTtcblxuZXhwb3J0IGNvbnN0IGFsaWduUGFnZSA9IChcbiAgZGF0YSxcbiAgcGFnZSxcbiAgc2l6ZVBlclBhZ2UsXG4gIHBhZ2VTdGFydEluZGV4XG4pID0+IHtcbiAgY29uc3QgZGF0YVNpemUgPSBkYXRhLmxlbmd0aDtcblxuICBpZiAocGFnZSA8IHBhZ2VTdGFydEluZGV4IHx8IHBhZ2UgPiAoTWF0aC5mbG9vcihkYXRhU2l6ZSAvIHNpemVQZXJQYWdlKSArIHBhZ2VTdGFydEluZGV4KSkge1xuICAgIHJldHVybiBwYWdlU3RhcnRJbmRleDtcbiAgfVxuICByZXR1cm4gcGFnZTtcbn07XG5cbmV4cG9ydCBjb25zdCBnZXRCeUN1cnJQYWdlID0gKFxuICBkYXRhLFxuICBwYWdlLFxuICBzaXplUGVyUGFnZSxcbiAgcGFnZVN0YXJ0SW5kZXhcbikgPT4ge1xuICBjb25zdCBkYXRhU2l6ZSA9IGRhdGEubGVuZ3RoO1xuICBpZiAoIWRhdGFTaXplKSByZXR1cm4gW107XG5cbiAgY29uc3QgZW5kID0gZW5kSW5kZXgocGFnZSwgc2l6ZVBlclBhZ2UsIHBhZ2VTdGFydEluZGV4KTtcbiAgY29uc3Qgc3RhcnQgPSBzdGFydEluZGV4KGVuZCwgc2l6ZVBlclBhZ2UpO1xuXG4gIGNvbnN0IHJlc3VsdCA9IFtdO1xuICBmb3IgKGxldCBpID0gc3RhcnQ7IGkgPD0gZW5kOyBpICs9IDEpIHtcbiAgICByZXN1bHQucHVzaChkYXRhW2ldKTtcbiAgICBpZiAoaSArIDEgPT09IGRhdGFTaXplKSBicmVhaztcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3BhY2thZ2VzL3JlYWN0LWJvb3RzdHJhcC10YWJsZTItcGFnaW5hdG9yL3NyYy9wYWdlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==
//# sourceMappingURL=react-bootstrap-table2-paginator.js.map