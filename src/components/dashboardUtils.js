import { Get } from '../utils/api-service';
import { deploymentsUrl } from '../config'

export const getData = (callback) => {
  const config = { deploymentsUrl,
    onError: function(error) {
      console.log("error fetching data!");
    }
  }
  Get(config.deploymentsUrl).then((data) => {
    callback(data);
    return data;
  });
}

export default {
  getData
}