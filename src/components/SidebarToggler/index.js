import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import { mobile } from '../../utils/common';


const toggleSidebar = (e) =>{
    e.preventDefault();
    if(document.body.classList.contains("sidebar-collapse")){
        document.body.classList.remove('sidebar-collapse')
    }else{
        document.body.classList.add('sidebar-collapse')
    }
}

const SidebarToggler = () => {
  return (
      <Fragment>
        <a
        href="#"
        className="sidebar-toggle"
        data-toggle="push-menu" role="button"
        onClick={e=>toggleSidebar(e)}>
            <span className="sr-only" >Toggle navigation</span>
        </a>  
     </Fragment>
  )
}

SidebarToggler.propTypes = {

}

export default SidebarToggler

