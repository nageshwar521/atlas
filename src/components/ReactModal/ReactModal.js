import React from 'react';
import Modal from 'react-responsive-modal';

const styles = {
  modal: {
    padding: 0
  }
};

const ReactModal = (props) => {
  return (
    <Modal
      classNames={{
        modal: styles.customModal
      }}
      open={props.openModel}
      onClose={props.onClose} 
      center
      {...props}>
      <div className="panel panel-white">
        <div className="panel-heading">
          <h4>{props.title}</h4>
        </div>
        <div className="panel-body">
          {props.children}
        </div>
        <div className="panel-footer text-right">
          <button
            className="white-button white-button--medium"
            onClick={props.onNoClick}>
            {props.noButtonText || "No"}
          </button>
          <button
            className="black-button black-button--medium"
            onClick={props.onYesClick}>
            {props.yesButtonText || "Yes"}
          </button>
        </div>
      </div>
    </Modal>
  );
}

export default ReactModal;