import React from 'react'
import { NavLink, Link } from 'react-router-dom';
import { matchPath } from 'react-router';
import Tooltip from 'rc-tooltip';

const DEFAULT_MATCH_OPTIONS = { exact: true };

const getBreadcrumbs = ({ routes, location }) => {
    const matches = [];
    const { pathname } = location;

    pathname
        .replace(/\/$/, '')
        .split('/')
        .reduce((previous, current) => {
            const pathSection = `${previous}/${current}`;
            const match = routes.find(({ matchOptions, path }) =>
                matchPath(pathSection, { ...(matchOptions || DEFAULT_MATCH_OPTIONS), path }));

            if (match) {
                matches.push({
                    label: match.label,
                    isExact: match.path === location.pathname,
                    path: pathSection,
                });
            }

            return pathSection;
        });

    return matches;
};


const Breadcrumb = ({ dividerComponent, location, routes }) => {
    const breadcrumbs = getBreadcrumbs({ location, routes });
    return (
        <div className="breadcrumb-wrapper">
            <ol className="breadcrumb">
                <li>
                    {breadcrumbs.map(({ path, label, isExact }, index) => (
                        <span key={path}>
                            <Link to={path} className={isExact ? 'active' : undefined}>{label}</Link>
                            {!!(index < breadcrumbs.length - 1) && dividerComponent()}
                        </span>
                    ))}
                </li>
                <li className="pull-right">
                    {location.pathname !== "/audit-activity" && <NavLink
                        to="/audit-activity"
                        activeClassName="active">
                        <Tooltip placement="left" trigger={['hover']} overlay="View Activity">
                            <img className="activity-black" src="../../img/activity-black.png" />
                        </Tooltip>
                    </NavLink>}
                    {location.pathname === "/audit-activity" && <NavLink
                        to="/"
                        activeClassName="active">
                        <Tooltip placement="left" trigger={['hover']} overlay="Hide Activity">
                            <img className="activity-blue" src="../../img/activity-blue.png" />
                        </Tooltip>
                    </NavLink>}
                </li>
            </ol>
        </div>
    )
}

Breadcrumb.defaultProps = {
    dividerComponent: () => ' / ',
};

export default Breadcrumb;
