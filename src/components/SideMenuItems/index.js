import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

import {NavLink} from 'react-router-dom';

const SideMenuItems = ({path, label, nested}) => {

    return (
        <li className="treeview">
            {nested? 
                (<Fragment>
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>{label}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        {
                            nested.map((route)=> SideMenuItems(route))
                        }
                    </ul>
                </Fragment>)
            :
            <NavLink to={path} activeClassName={location.pathname === path?'active':''}>
                <i className="fa fa-dashboard" /> <span>{label}</span>
            </NavLink>}
        </li>
    );
}

SideMenuItems.propTypes = {

}

export default SideMenuItems;