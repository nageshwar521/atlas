import React, { Component } from 'react';
import { Get } from '../../../utils/api-service';
import { getProjectsMetaData } from './statusBarUtils';
import { usersCountUrl } from '../../../config';

export default class StatusBars extends Component {
  constructor() {
    super();
    this.state  = {
      projectsMetaData: {
        allLocations: 0,
        allProjects: 0,
        allVnfs: 0
      },
      userData: {
      count: 0
    }
  }
}
  
  componentDidMount() {
    Get(usersCountUrl).then(data => {
        this.setState({
          userData: data
        })
    })
}

  componentWillMount() {
    const { data } = this.props;
    this.setState({
      projectsMetaData: data.metaData
    });
  }

  render() {
    const { projectsMetaData } = this.state;
    const { count } = this.state.userData;
    return (
      <div>
        {/* <div className="col-xs-12 "> */}
        <section className="status-bar-responsive tab-pane active fade in content no-padding" id="dashboard">
          <div className="col-md-2 col-sm-6 col-xs-12 no-padding-left status-bar status-bar-projects">
            <div className="info-box">
              <span className="info-box-icon">
                <img className="status-bar-image" src="../../../../img/projects.png" />
              </span>
              <div className="info-box-content">
                <span className="info-box-text">Projects</span>
                <span className="info-box-number">{projectsMetaData.allProjects}</span>
              </div>
            </div>
          </div>
          <div className="col-md-2 col-sm-6 col-xs-12 no-padding-left status-bar status-bar-vnfs">
            <div className="info-box">
              <span className="info-box-icon">
                <img className="status-bar-image" src="../../../../img/vnfs.png" />
              </span>
              <div className="info-box-content">
                <span className="info-box-text">VNF's</span>
                <span className="info-box-number">{projectsMetaData.allVnfs}</span>
              </div>
            </div>
          </div>
          <div className="col-md-2 col-sm-6 col-xs-12 no-padding-left status-bar status-bar-vms">
            <div className="info-box">
              <span className="info-box-icon">
                <img className="status-bar-image" src="../../../../img/firewall.png" />
              </span>
              <div className="info-box-content traffic-content">
                <span className="info-box-text">VM's</span>
                <span className="info-box-number">{projectsMetaData.allVms}</span>
              </div>
            </div>
          </div>
          <div className="col-md-2 col-sm-6 col-xs-12 no-padding-left status-bar status-bar-sites">
            <div className="info-box">
              <span className="info-box-icon">
                <img className="status-bar-image" src="../../../../img/sites.png" />
              </span>
              <div className="info-box-content traffic-content">
                <span className="info-box-text">Sites</span>
                <span className="info-box-number">{projectsMetaData.allLocations}</span>
              </div>
            </div>
          </div>
          <div className="clearfix visible-sm-block"></div>
          <div className="col-md-2 col-sm-6 col-xs-12 no-padding status-bar status-bar-users">
            <div className="info-box">
              <span className="info-box-icon">
                <img className="status-bar-image" src="../../../../img/users1.png" />
              </span>
              <div className="info-box-content">
                <span className="info-box-text">Users</span>
                <span className="info-box-number">{count}</span>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}