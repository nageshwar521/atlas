import { Get } from '../../../utils/api-service';
import { deploymentsUrl } from './../../../config';

const getProjectData = Get(deploymentsUrl);

export const getProjectsMetaData = () => {
  return getProjectData.then(response => {
    const { metaData } = response;
    return metaData;
  });
}

export default {
  getProjectsMetaData
}