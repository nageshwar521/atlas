import { Get } from '../../../utils/api-service';
import moment from 'moment';
import { calendarUrl } from '../../../config'

export const getCalendarData = () => {
  return Get(calendarUrl);
}

export const extractDates = (data) => {
  return data.map(item => moment(item.date).format('YYYY-MM-DD'));
}

export const extractDepolyments = (data) => {
  return data.map(item => {
    const dateKey = moment(item.date).unix();
    return {
      dateKey: item
    }
  });
}

function flatten(input) {
  const stack = [...input];
  const res = [];
  while (stack.length) {
    const next = stack.pop();
    if (Array.isArray(next)) {
      stack.push(...next);
    } else {
      res.push(next);
    }
  }
  return res.reverse();
}

export const getDepoymentsByDate = (data, date) => {
  const eventData = data.filter(item => {
    const itemDate = moment(item.date).format('YYYY-MM-DD');
    return itemDate === date ? true : false;
  });
  const mergedEvents = eventData.map((event) => {
    const { sites, ...restData } = event;

    const newSites = sites.map((site) => {
      const obj = {};
      obj.site = site.site;
      return obj;
    });

    const mergedSites = newSites.map((site) => {
      return Object.assign({}, restData, site);
    });
    
    return mergedSites;
  })
  return flatten(mergedEvents);
}

export default {
  getCalendarData,
  getDepoymentsByDate
}