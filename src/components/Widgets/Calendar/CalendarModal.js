import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import { IoIosClose } from "react-icons/io";

export default class CalendarModal extends Component {
  constructor() {
    super();
    this.state = {
      columns: [{
        dataField: 'lob',
        text: 'Business Line',
        attrs: {width: '20%'}
      }, {
        dataField: 'project',
        text: 'Project Name',
        attrs: {width: '20%'}
      }, {
        dataField: 'vnf',
        text: 'VNF Name',
        attrs: {width: '20%'}
      }, {
        dataField: 'site',
        text: 'Site Name',
        attrs: {width: '20%'}
      },
      {
        dataField: 'time',
        text: 'Planned Time',
        attrs: { width: '20%' }
      }]
    }
  }
  render() {
    const { data = [] } = this.props;
    return (
      this.props.visible ? 
        <div className="calendar-modal-overlay">
          <button
            onClick={this.props.onCloseClick}
            className="btn btn-modal-close">
            <IoIosClose color="white" size={48} />
          </button>
          <div className="calendar-modal">
            <div className="calendar-modal-header">
            </div>
            <div className="calendar-modal-body">
              <div className="calendar-table-wrapper">
                <BootstrapTable 
                  bordered={false}
                  maxHeight={500}
                  keyField='dateDeployed' 
                  data={data} 
                  columns={this.state.columns} />
              </div>
            </div>
          </div>
        </div> : null
    );
  }
}