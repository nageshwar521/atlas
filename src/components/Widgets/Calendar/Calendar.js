import React, { Component } from 'react';
import Widget from '../../Widget/Widget';
import ReactCalendar from 'react-day-picker';
import moment from "moment";
import cn from "classnames";
import CalendarModal from './CalendarModal';
import { getCalendarData, extractDates, getDepoymentsByDate } from './calendarUtils';

export default class Calendar extends Component {
  constructor() {
    super();
    this.state = {
      calendarWidget: ReactCalendar,
      date: new Date(),
      eventDates: [],
      events: [],
      eventData: [],
      minimized: false,
      maximized: false,
      visibleModal: false
    }
    this.handleDayChange = this.handleDayChange.bind(this);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
    this.handleMinimize = this.handleMinimize.bind(this);
    this.handleMaximize = this.handleMaximize.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.renderDay = this.renderDay.bind(this);
  }
  componentWillMount() {
    getCalendarData().then((response) => {
      const eventDates = extractDates(response);
      this.setState({
        events: response,
        eventDates
      });
    });
  }
  handleMinimize() {
    const minimized = this.state.minimized;
    this.setState({
      minimized: !minimized,
      calendarWidget: null
    });
    if (!minimized) {
      _this.setState({
        calendarWidget: ReactCalendar
      });
    }
  }
  handleMaximize() {
    const _this = this;
    const maximized = this.state.maximized;
    this.setState({
      maximized: !maximized,
      calendarWidget: null
    });
    window.setTimeout(function () {
      _this.setState({
        calendarWidget: ReactCalendar
      });
    }, 200);
  }
  handleRefresh() {
    const _this = this;
    this.setState({
      calendarWidget: null,
      visibleModal: false
    });
    window.setTimeout(function () {
      _this.setState({
        calendarWidget: ReactCalendar
      });
    }, 200);
  }
  handleDayChange(date) {
    this.setState({ date });
  }
  handleDayClick(day) {
    const dateString = moment(day).format('YYYY-MM-DD');
    const { eventDates } = this.state;
    const hasEvent = eventDates.includes(dateString);
    if (hasEvent) {
      const eventData = getDepoymentsByDate(this.state.events, dateString);
      this.setState({
        visibleModal: true,
        eventData
      });
    }
  }
  handleModalClose() {
    this.setState({
      visibleModal: false
    });
  }
  renderDay(day) {
    const date = day.getDate();
    const dateString = moment(day).format('YYYY-MM-DD');
    const isToday = moment(day).isSame(moment(), 'day');;
    const { eventDates } = this.state;
    const cellClass = cn("DayPicker-Day__cellStyle", 
      eventDates.includes(dateString) ? "DayPicker-Day__eventStyle" : "");
    return (
      <div className={cellClass}>
        <div className="DayPicker-Day__dateStyle">{date}</div>
        {
          isToday ? 
            <div className="DayPicker-Day__today-dot">
              <i className="fa fa-circle" />
            </div> : null
        }
      </div>
    );
  }
  render() {
    const { calendarWidget: CalendarWidget, eventData } = this.state;
    return (
      <Widget
        className="calendar-widget"
        title="Deployments Calendar"
        onMaximize={this.handleMaximize}
        onRefresh={this.handleRefresh}>
          {
            CalendarWidget ? 
              <div className="calendar-wrapper">
                <CalendarWidget
                  onDayClick={this.handleDayClick}
                  renderDay={this.renderDay}
                />
              </div> : null
          }
          <CalendarModal 
            visible={this.state.visibleModal}
            onCloseClick={this.handleModalClose}
            data={eventData} />
      </Widget>
    );
  }
}