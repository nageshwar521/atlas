import React, { Component } from "react";
import MapComponent from './MapComponent';
import Widget from "../../Widget/Widget";

export default class MapPanel extends Component {
  constructor() {
    super();
    this.state = {
      mapClass: 'mapPanelContainer mapContainer',
      mapMinimize: false,
      mapRestore: true,
      mapFullScreen: false,
      mapWidget: MapComponent,
      showMapModal: false,
      mapReset: false
    };
    this.toggleMinimize = this.toggleMinimize.bind(this);
    this.toggleMaximize = this.toggleMaximize.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
    this.loadMap = this.loadMap.bind(this);
    this.handleMapModalOpen = this.handleMapModalOpen.bind(this);
    this.handleMapModalClose = this.handleMapModalClose.bind(this);
    this.getHeaderButtons = this.getHeaderButtons.bind(this);
  }

  componentWillMount() {
    this.loadMap();
  }

  handleMapModalOpen() {
    this.setState({
      showMapModal: true
    });
  }

  handleMapModalClose() {
    this.setState({
      showMapModal: false
    });
  }

  toggleMinimize() {
    const mapMinimize = !this.state.mapMinimize;
    this.setState({
      mapWidget: null,
      mapMinimize: mapMinimize,
      mapFullScreen: false,
      mapRestore: !this.state.mapRestore,
      mapClass: mapMinimize ? 'mapPanelContainer' : 'mapPanelContainer mapContainer'
    }, () => {
      this.loadMap();
    });
  }

  toggleMaximize() {
    this.setState({
      mapWidget: null,
      mapFullScreen: !this.state.mapFullScreen,
      mapMinimize: false,
      mapRestore: !this.state.mapRestore,
      mapClass: 'mapPanelContainer mapContainer'
    }, () => {
      this.loadMap();
    });
  }

  handleRestore() {
    if (!this.state.mapMinimize) {
      localStorage.removeItem("selectedOptions");
      this.setState({
        mapWidget: null,
        mapMinimize: false,
        mapClass: 'mapPanelContainer mapContainer'
      }, () => {
        setTimeout(() => {
          this.loadMap();
        }, 100);
      });
    }
  }

  loadMap() {
    this.setState({
      mapWidget: MapComponent
    });
  }

  getHeaderButtons() {
    const { mapFullScreen } = this.state;
    return [
      {
        title: 'Settings',
        icon: 'fa-cog',
        isVisible: mapFullScreen,
        onClick: this.handleMapModalOpen
      }
    ];
  }

  render() {
    let { mapMinimize, mapRestore, mapFullScreen, mapClass, mapWidget: MapWidget } = this.state;
    const { title = "Mapbox", data } = this.props;
    const buttons = this.getHeaderButtons();
    const isMaximizable = mapMinimize ? false : true;
    return (
        <Widget
          className={mapClass}
          buttons={buttons}
          isRefreshable={true}
          maximized={mapFullScreen}
          minimized={mapMinimize}
          isMinimizable={!mapFullScreen}
          isMaximizable={isMaximizable}
          onRefresh={this.handleRestore}
          onMinimize={this.toggleMinimize}
          onMaximize={this.toggleMaximize}
          title={title}>
          {MapWidget ? <MapWidget
            data={data}
            mapFullScreen={mapFullScreen}
            mapRestore={mapRestore}
            mapMinimize={mapMinimize}
            onClose={this.handleMapModalClose}
            showMapModal={this.state.showMapModal}
            {...this.props} /> : null}
        </Widget>
    );
  }
}
