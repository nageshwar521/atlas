import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import { deepFindValuePath } from './mapUtils';

class MapIcon extends Component {
  getRows() {
    const { data, place } = this.props;
    if (!data) return <tr>No data found!</tr>;
    const selectedOptions = localStorage.getItem("selectedOptions");
    if (selectedOptions) {
      const options = JSON.parse(selectedOptions);
      const mappedLocations = deepFindValuePath(data, place[2], options.join("."));
      const mapPopupTitles = ["Business Line", "Project Name", "VNF"];
      const mappedArr = mappedLocations[0].split(".");
      return mappedArr.map((item, idx) => {
        return <tr key={idx}>
          <th>{mapPopupTitles[idx]}</th>
          <td>{item}</td>
        </tr>
      });
    } else {
      const mappedLocations = deepFindValuePath(data, place[2]);
      const mapPopupTitles = ["Business Line", "Project Name", "VNF Name"];
      const mappedArr = mappedLocations[0].split(".");
      return mappedArr.map((item, idx) => {
        return <tr key={idx}>
          <th>{mapPopupTitles[idx]}</th>
          <td>{item}</td>
        </tr>
      });
    }
  }
  renderVms(vms) {
    return vms.map((item, index) => <li key={index}>{item.hostName}</li>);
  }
  render() {
    const { data, place } = this.props;
    return (
      <div>
        <h5 className="popupTitle">{place[2]}</h5>
        <Table bordered>
          {this.getRows()}
          <tr>
            <th>Host Name</th>
            <td>
              <ul className="vms-hostname">
                {this.renderVms(place[3])}
              </ul>
            </td>
          </tr>
        </Table>
      </div>
    );
  }
}

export default MapIcon;