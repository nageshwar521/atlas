import React, { Component } from 'react';
import PieChart from "react-svg-piechart";
import { deepFindValuePath, getDuplicateCount } from './mapUtils';

const data = [
  { title: "", value: 100, color: "#2196f3" }
]

class MapIcon extends Component {
  getColorMap(allOrgs) {
    let obj = {};
    for (let key in allOrgs) {
      obj[key] = allOrgs[key];
    }
    return obj;
  }
  getColorObject(allMainOrgsMap) {
    let allOrgs = [];
    for (let mainKey in allMainOrgsMap) {
      if (mainKey === "wireless") {
        allOrgs.push({ title: "wireless", value: 20, color: '#8bc34a' });
      }
      if (mainKey === "wireline") {
        allOrgs.push({ title: "wireline", value: 70, color: '#e64a19' });
      }
      if (mainKey === "enterprise") {
        allOrgs.push({ title: "enterprise", value: 30, color: '#e64a19' });
      }
      if (mainKey === "common") {
        allOrgs.push({ title: "common", value: 40, color: '#E38627' });
      }
    }
    return allOrgs;
  }
  getOrgColors() {
    const { cluster, mapResponse } = this.props;
    if (!cluster) return [{ title: '', value: 10, color: '#ff5722' }];
    const childMarkers = cluster.getAllChildMarkers();
    const mainOrgs = childMarkers.map(marker => {
      const latlng = marker.getLatLng();
      const mappedLocations = deepFindValuePath(mapResponse, String(latlng.lat), null, "latitude");
      const mappedPathArr = mappedLocations.map(item => {
        const org = item.split(".");
        return org[0];
      });

      const orgsByCount = getDuplicateCount(mappedPathArr);
      const orgColorMap = this.getColorMap(orgsByCount);
      const orgColors = this.getColorObject(orgColorMap);
      return orgColors;
    });
    return mainOrgs;
  }
  
  render() {
    const { text } = this.props;
    return (
      <div className="svg-marker">
        <div className="svg-icon">
          <PieChart data={data} />
        </div>
        <span className="svg-text">{text}</span>
      </div>
    );
  }
}

export default MapIcon;