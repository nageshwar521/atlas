import React, { Component } from 'react';
import {
  Table
} from "react-bootstrap";;
import { getMapData, getLatLngs } from './mapUtils';
import ReactSelect from '../../ReactSelect/ReactSelect';

export default class MapModal extends Component {
  constructor() {
    super();
    this.state = {
      organizations: null,
      projects: null,
      vnfs: null,
      selectedOrg: null,
      selectedProject: null,
      selectedVnf: null,
      mapData: []
    };
    this.handleOrg = this.handleOrg.bind(this);
    this.handleProject = this.handleProject.bind(this);
    this.handleVnf = this.handleVnf.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.loadMapData = this.loadMapData.bind(this);
    this.getOptions = this.getOptions.bind(this);
    this.checkAllOptionsSelected = this.checkAllOptionsSelected.bind(this);
  }
  componentDidMount() {
    this.loadMapData();
  }
  loadMapData(options = [], callback) {
    getMapData((response) => {
      const { organizations } = response;
      let mapResponse = organizations;
      const mapData = getLatLngs(mapResponse, options);
      this.setState({
        organizations: mapResponse,
        mapData
      }, () => {
        this.props.onDataLoad(this.state.mapData);
        if (callback) {
          callback();
        }
      });
    });
  }
  handleOrg(selectedOrg) {
    const { organizations } = this.state;
    let projects = organizations[selectedOrg.value];
    this.setState({
      selectedOrg: selectedOrg.value,
      projects
    });
  }
  handleProject(selectedProject) {
    const { projects } = this.state;
    let vnfs = projects[selectedProject.value];
    this.setState({
      selectedProject: selectedProject.value,
      vnfs
    });
  }
  handleVnf(selectedVnf) {
    this.setState({
      selectedVnf: selectedVnf.value
    });
  }
  handleSubmit() {
    const _this = this;
    const { selectedOrg, selectedProject, selectedVnf } = this.state;
    if (!selectedOrg || !selectedProject || !selectedVnf) {
      return;
    }
    let selectedOptions = [selectedOrg, selectedProject, selectedVnf];
    localStorage.setItem("selectedOptions", JSON.stringify(selectedOptions));
    this.loadMapData(selectedOptions, () => {
      this.props.onClose();
    });
  }
  handleCancel() {
    this.props.onClose();
  }
  handleClose() {
    this.props.onClose();
  }
  getOptions(data) {
    if (!data) return [];
    const options = Object.keys(data);
    return options.map((option, idx) => {
      return {
        value: option,
        label: option
      };
    });
  }
  checkAllOptionsSelected() {
    const { selectedOrg, selectedProject, selectedVnf } = this.state;
    const selectedOptions = ["" + Boolean(selectedOrg), "" + Boolean(selectedProject), "" + Boolean(selectedVnf)];
    return !selectedOptions.includes("false");
  }
  getFilteredProjects(projectOptions) {
    const excludedProjects = ["totalVnfs"];
    return projectOptions.filter(project => {
      return (project.value !== "totalVnfs");
    });
  }
  render() {
    const { title } = this.props;
    const { organizations, projects, vnfs } = this.state;
    const orgOptions = this.getOptions(organizations);
    const projectOptions = this.getOptions(projects);
    const vnfOptions = this.getOptions(vnfs);
    const checkAllOptionsSelected = this.checkAllOptionsSelected();
    const disabled = !checkAllOptionsSelected ? "disabled" : "";

    return (
      <div className="inlineModal">
        <Table className="borderless">
          <tbody>
            <tr>
              <td>
                <label className="control-label">Business Line</label>
              </td>
              <td>
                <ReactSelect
                  selectedOption={this.state.selectedOrg}
                  options={orgOptions}
                  handleChange={this.handleOrg} />
              </td>
            </tr>
            <tr>
              <td>
                <label className="control-label">Project Name</label>
              </td>
              <td>
                <ReactSelect
                  selectedOption={this.state.selectedProject}
                  options={this.getFilteredProjects(projectOptions)}
                  handleChange={this.handleProject} />
              </td>
            </tr>
            <tr>
              <td>
                <label className="control-label">VNF Name</label>
              </td>
              <td>
                <ReactSelect
                  selectedOption={this.state.selectedVnf}
                  options={vnfOptions}
                  handleChange={this.handleVnf} />
              </td>
            </tr>
          </tbody>
        </Table>
        <div className="controlWrapper">
          <button
            onClick={this.handleSubmit}
            className="btn btn-primary"
            disabled={disabled}>Go</button>
          <button
            onClick={this.handleClose}
            className="btn btn-default">Close</button>
        </div>
      </div>
    );
  }
}