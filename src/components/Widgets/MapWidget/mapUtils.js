import { Get } from '../../../utils/api-service';
import { deploymentsUrl } from '../../../config';

const url = deploymentsUrl

export const getMapData = (callback) => {
  Get(url).then((data) => {
    callback(data);
  });
}

export const flattenArray = (input) => {
  const arr = [...input];
  const result = [];
  while (arr.length) {
    const item = arr.pop();
    if (item && Array.isArray(item)) {
      arr.push(...item);
    } else {
      result.push(item);
    }
  }
  return result;
}

export const flattenObject = (input) => {
  const arr = [input];
  let excludedKeys = ["status", "stash", "stage", "Jira", "totalVnfs"];
  const result = [];
  let i = 0;
  while (arr.length) {
    const item = arr.pop();
    if ((String(item) === "[object Object]") && !Array.isArray(item)) {
      let objKeys = Object.keys(item);
      for (let key in objKeys) {
        if (!excludedKeys.includes(objKeys[key])) {
          arr.push(item[objKeys[key]]);
        }
      }
    } else {
      result.push(item);
    }
  }
  return result;
}

export const deepFind = (obj, path) => {
  if (!path) return undefined;
  var paths = path.split('.')
    , current = obj
    , i;

  for (i = 0; i < paths.length; ++i) {
    if (current[paths[i]] == undefined) {
      return undefined;
    } else {
      if (Array.isArray(current[paths[i]])) {
        return [path, current[paths[i]]];
      } else {
        current = current[paths[i]];
      }
    }
  }
}

export const getDeepKeys = (obj) => {
  var keys = [];
  for (var key in obj) {
    keys.push(key);
    if ((typeof obj[key] === "object") && !Array.isArray(obj[key])) {
      var subkeys = getDeepKeys(obj[key]);
      keys = keys.concat(subkeys.map(function (subkey) {
        return key + "." + subkey;
      }));
    } else {
      keys = keys.filter((objKey) => {
        return ((objKey !== "totalVnfs") && (objKey !== "sites"))
      });
    }
  }
  return keys;
}

export const getObjectInArr = (arr, key, value) => {
  let filtered = arr.filter(obj => obj[key] === value);
  return filtered[0] ? filtered[0] : false;
}

export const uniqueArr = (arr) => {
  const obj = {};
  for (let i = 0; i < arr.length; i++) {
    obj[arr[i]] = true;
  }
  return Object.keys(obj);
}

export const getDuplicateCount = (arr) => {
  let obj = {};
  for (let i = 0; i < arr.length; i++) {
    obj[arr[i]] = obj.hasOwnProperty(arr[i]) ? ++obj[arr[i]] : 1;
  }
  return obj;
}

export const removeDuplicates = (originalArray, objKey) => {
  var trimmedArray = [];
  var values = [];
  var value;

  for (var i = 0; i < originalArray.length; i++) {
    value = originalArray[i][objKey];

    if (values.indexOf(value) === -1) {
      trimmedArray.push(originalArray[i]);
      values.push(value);
    }
  }

  return trimmedArray;
}

export const mapOrgWithLocation = (mainObj) => {
  let deepKeys = [];
  deepKeys = getDeepKeys(mainObj);
}

export const deepFindValuePath = (mainObj, value, paths, prop = "location") => {
  let deepKeys = [];
  if (paths) {
    deepKeys = [paths];
  } else {
    deepKeys = getDeepKeys(mainObj);
  }
  let allKeys = [];
  allKeys = deepKeys.filter((keyPathStr) => {
    let keyPathArr = keyPathStr.split(".");
    return keyPathArr.length === 3;
  });
  return allKeys;
}

export const mapKeysWithLocation = (mainObj) => {
  let deepKeys = getDeepKeys(mainObj);
  let allKeys = [];
  allKeys = deepKeys.map(function (item) {
    let foundArr = deepFind(mainObj, item);
    return foundArr;
  }).filter(function (item) {
    return !!item;
  });
  return allKeys;
}

export const getLatLngs = (data, targets = []) => {
  let latlngObj = [];
  let latlngs = [];
  if (targets.length > 0) {
    targets.reduceRight((prev, current) => {
      data = data[targets[prev]];
      return ++prev;
    }, 0);
    latlngs = data.sites;
  } else {
    latlngObj = flattenObject(data);
    latlngs = flattenArray(latlngObj);
  }
  return latlngs.filter(Boolean).map((({ vms, location, latitude, longitude }) => {
    return [
      longitude,
      latitude,
      location,
      vms
    ];
  }));
}

export default {
  getMapData,
  flattenArray,
  getLatLngs,
  mapKeysWithLocation,
  deepFindValuePath,
  removeDuplicates,
  uniqueArr,
  mapOrgWithLocation
}