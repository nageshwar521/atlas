import React, { Component } from 'react';
import {
  Button
} from "react-bootstrap";
import Tooltip from 'rc-tooltip';

const MapSettings = (props) => {
  return <Tooltip placement="bottom" trigger={['hover']} overlay="Settings">
    <Button
      className="mapControl"
      onClick={props.onOpen}>
      <i className="fa fa-cog settingsIcon" />
    </Button>
  </Tooltip>
}

export default MapSettings;