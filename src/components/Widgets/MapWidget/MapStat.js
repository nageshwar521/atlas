import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class MapStat extends Component {
  getStats() {
    const { stats } = this.props;
    return stats.map((stat, idx) => {
      return <tr key={idx}>
        <td><span className="statName">{stat.name}</span></td>
        <td>
          <span className="statDetails">
            <span className="statCount">{stat.count}</span>
            <span className="statSeparator">-</span>
            <span className="statPercent">({stat.percent}%)</span>
          </span>
        </td>
      </tr>
    });
  }
  render() {
    return (
      <div className="mapStatTable">
        <Table className="borderless">
          <tbody>
            {this.getStats()}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default MapStat;