import React, { Component } from 'react';
import L from "leaflet";
import { renderToString } from 'react-dom/server';
import MapModal from './MapModal';
import { getLatLngs } from './mapUtils';
import MapIcon from './MapIcon';
import MapPopup from './MapPopup';
import MapStat from './MapStat';
import $ from 'jquery';
require('leaflet.markercluster');

export default class MapComponent extends Component {
  constructor() {
    super();
    this.state = {
      places: [],
      showMapModal: false,
      modalProps: null,
      mapStyle: {},
      modalData: null,
      mapResponse: null,
      loading: true
    };
    this.handleMapModalClose = this.handleMapModalClose.bind(this);
    this.handleMapModalOpen = this.handleMapModalOpen.bind(this);
    this.handleMapData = this.handleMapData.bind(this);
  }
  componentWillMount() {

  }
  getPieChartIcon(markerText, cluster, mapResponse) {
    return renderToString(
      <MapIcon text={markerText} cluster={cluster} mapResponse={mapResponse} />
    );
  }
  getCustomMarker(markerText = '', cluster, mapResponse) {
    return L.divIcon({ html: this.getPieChartIcon(markerText, cluster, mapResponse), iconSize: L.point(40, 40) });
  }
  getHtmlMarker(markerText, className) {
    return L.divIcon({
      className: className,
      html: `<div class="marker-text">${markerText}</div><div class="marker-left"></div><div class="marker-right"></div>`,
      iconSize: new L.Point(40, 40)
    });
  }
  componentDidMount() {
    const { reset, data } = this.props;
    if (reset) {
      localStorage.removeItem("selectedOptions");
    }
    const { organizations } = data;
    let mapResponse = organizations;
    const selectedOptions = localStorage.getItem("selectedOptions");
    let places;
    if (selectedOptions) {
      const options = JSON.parse(selectedOptions);
      places = getLatLngs(mapResponse, options);
    } else {
      places = getLatLngs(mapResponse);
    }
    this.setState({
      places,
      mapResponse,
      loading: false
    }, () => {
      this.initMap();
    });
  }
  initMap() {
    const _this = this;
    const { places, mapResponse } = this.state;
    let usaBounds = [
      [25.82, -124.39],
      [49.38, -66.94] 
    ];
    if (_this.map && !this.props.showMapModal) {
      _this.map.remove();
    }
    setTimeout(() => {
      let tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 28,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
      });
      let latlng = L.latLng(-124.39, 25.82);
      _this.map = L.map('map', { center: latlng, zoom: 5, layers: [tiles] });

      const stats = this.generateStats();
      const mapStatClasses = ["leaflet-bar mapStat"];
      // if (mapFullScreen) {
      //   mapStatClasses.push("stat300");
      // } else if (mapRestore) {
      //   mapStatClasses.push("stat150");
      // } else if (mapMinimize) {
      //   mapStatClasses.push("statHide");
      // }
      L.Control.MapStat = L.Control.extend({
        onAdd: function (map) {
          var el = L.DomUtil.create('div', mapStatClasses.join(" "));

          el.innerHTML = renderToString(
            <MapStat stats={stats} />
          );

          return el;
        },

        onRemove: function (map) {
        }
      });

      L.control.mapStat = function (opts) {
        return new L.Control.MapStat(opts);
      }

      L.control.mapStat({
        position: 'bottomleft'
      }).addTo(_this.map);

      let mcg = L.markerClusterGroup({
        showCoverageOnHover: false,
        iconCreateFunction: function (cluster) {
          const childCount = cluster.getChildCount();
          const clusterMarker = _this.getCustomMarker(childCount, cluster, mapResponse);
          return clusterMarker;
        },
        spiderfyDistanceMultiplier: 2
      });

      mcg.on('clustermouseover', function (c) {
        let vehicleClusterPopup = "";
        let clusterPopup = L.popup({ closeButton: false, className: 'clusterHoverPopup' })
          .setLatLng(c.layer.getLatLng())
          .setContent('<div id="deployClusterPopupContent"><h5 class="deploy-popup-title">Locations</h5><div class="tableWrapper custom-scrollbar"><table class="deploy-popup table table-curved"><tbody></tbody></table></div></div>');

        $.each(c.layer.getAllChildMarkers(), function (index, value) {
          $('body').append('<div class="hidden" id="mapTest">' + value._popup._content + '</div>');
          let location = $("#mapTest").find('h5').html();
          vehicleClusterPopup += '<tr><td>' + (index + 1) + '</td><td>' + location + '</td></tr>';
          $("#mapTest").remove();
        });

        // clusterPopup.openOn(_this.map);
        c.layer.bindPopup(clusterPopup).addTo(_this.map).openPopup();
        // c.layer.on('popupopen', function (e) {
        //   e.popup._adjustPan();
        // });

        setTimeout(function () {
          $('#deployClusterPopupContent .table>tbody').html(vehicleClusterPopup);
          $('#deployClusterPopupContent, #map').mouseleave(function () {
            _this.map.closePopup();
          });
        }, 200);

        // _this.map.addLayer(marker);
      }).on('clusterclick', function (c) {
        _this.map.closePopup();
      });

      for (var i = 0; i < places.length; i++) {
        var place = places[i];
        var marker = L.marker(
          new L.LatLng(place[1], place[0]),
          { icon: _this.getHtmlMarker(1, 'red-marker') }
        ).bindPopup(renderToString(<MapPopup place={place} data={mapResponse} />));
        mcg.addLayer(marker);
      }
      _this.map.addLayer(mcg);
      _this.map.fitBounds(usaBounds);
    }, 100);
  }
  handleMapModalOpen() {
    this.setState({
      showMapModal: true
    });
  }
  handleMapModalClose() {
  }
  handleMapData(places) {
    if (places.length !== this.state.places.length) {
      this.setState({
        places
      }, () => {
        this.initMap();
      });
    }
  }
  getTotalVnfCount() {
    const { mapResponse } = this.state;
    return Object.keys(mapResponse).reduce((prevOrg, nextOrg) => {
      if (!mapResponse[nextOrg].totalVnfs) {
        return prevOrg;
      } else {
        return prevOrg + mapResponse[nextOrg].totalVnfs;
      }
    }, 0);
  }
  generateStats() {
    const { mapResponse } = this.state;
    const totalVnfs = this.getTotalVnfCount();
    return Object.keys(mapResponse).map((orgName) => {
      const vnfCount = mapResponse[orgName].totalVnfs;
      const percent = vnfCount ? Math.floor((vnfCount / totalVnfs) * 100) : 0;
      return { name: orgName, count: vnfCount ? vnfCount : 0, percent };
    });
  }
  render() {
    if (!this.state.mapResponse) return null;
    const {
      showMapModal,
      mapModalTitle = "Map Modal",
      mapFullScreen,
      mapRestore,
      mapMinimize
    } = this.props;
    const mapWrapperClasses = ["mapWrapper"];
    if (mapFullScreen) {
      mapWrapperClasses.push("largeMap");
    } else if (mapRestore) {
      mapWrapperClasses.push("smallMap");
    } else if (mapMinimize) {
      mapWrapperClasses.push("hiddenMap");
    }
    return (
      <div className={mapWrapperClasses.join(" ")}>
        <div id="map" className="map"></div>

        {
          (showMapModal && mapFullScreen) ? <div className="map-settings">
            <MapModal
              title={mapModalTitle}
              onDataLoad={this.handleMapData}
              show={this.props.showMapModal}
              onClose={this.props.onClose} />
          </div> : null
        }
      </div>
    );
  }
}

