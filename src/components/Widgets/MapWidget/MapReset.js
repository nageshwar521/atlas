import React, { Component } from 'react';
import {
  Button
} from "react-bootstrap";
import Tooltip from 'rc-tooltip';

const MapReset = (props) => {
  return <Tooltip placement="bottom" trigger={['hover']} overlay="Reset">
    <Button
      className="mapControl"
      onClick={props.onReset}>
      <i className="fa fa-undo resetIcon" />
    </Button>
  </Tooltip>
}

export default MapReset;