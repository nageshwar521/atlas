import React, { Component } from 'react';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import BootstrapTable from 'react-bootstrap-table-next';

export default class ProjectTableWrapper extends Component {
  render() {
    const { pagination, data, columns, overlay, tableOptions } = this.props;
    return (
      <ToolkitProvider
        keyField="id"
        data={data}
        options={tableOptions}
        columns={columns}
        loading={true}
        overlay={overlay}
        noDataIndication='No Results Found!'
        search
      >
        {
          props => (
            <div className="col-xs-12 no-padding project-table-panel">
              <div className="col-xs-12 no-padding">
                <BootstrapTable
                  {...props.baseProps}
                  options={tableOptions}
                  hover
                  striped
                  classes="projectTable"
                  bordered={false}
                  pagination={pagination}
                />
              </div>
            </div>
          )
        }
      </ToolkitProvider>
    );
  }
}