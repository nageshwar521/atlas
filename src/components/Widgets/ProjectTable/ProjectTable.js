import React, { Component } from 'react';
import cn from 'classnames';
import paginationFactory from '../../ReactBootstrapPagination/react-bootstrap-table2-paginator';
import { projectDataToTableData, addTableRows } from './projectUtils';
import ProjectTableWrapper from './ProjectTableWrapper';
import Widget from '../../Widget/Widget';

export default class ProjectTable extends Component {
  constructor() {
    super();
    this.state = {
      maximized: false,
      activePage: 1,
      columns: [
        {
          text: 'Business Line',
          dataField: 'businessLine',
          sort: true,
          formatter: (cell ) => {
            if (cell === "No Results Found!") {
              return <span className="noResultsWrapper">
                <span className="noResultsText">{cell}</span>
              </span>;
            } else {
              return cell;
            }
          }
        },
        {
          text: 'Project Name',
          dataField: 'projectName',
          sort: true,
        },
        {
          text: 'VNF Name',
          dataField: 'vnfName',
          sort: true,
        },
        {
          text: 'Status',
          dataField: 'status',
          sort: true,
          formatter: (cell) => {
            const labelClass = (cell !== "In Progress") ?
              'label label-success status-label' : 'label label-primary status-label';
            return (
              <span className={labelClass}>{cell}</span>
            );
          }
        },
        {
          text: 'Stage',
          dataField: 'stage',
          sort: true,
        },
        {
          text: 'One Jira',
          dataField: 'oneJira',
          sort: true,
          formatter: (cell) => {
            const onejiraLink = cell ? `https://onejira.verizon.com/browse/${cell.toLowerCase()}` : '#';
            return <a className="btn btn-link" target='_blank' href={onejiraLink} target="_blank">{cell}</a>;
          }
        },
        {
          text: 'One Stash',
          dataField: 'oneStash',
          sort: true,
          headerStyle: { width: 120 },
          formatter: (cell) => {
            const onestashLink = cell ? `https://onestash.verizon.com/projects/ATLAS/repos/${cell.toLowerCase()}` : '#';
            return cell ? <a className="btn btn-link" target='_blank' href={onestashLink }><i className="fa fa-bitbucket" /></a> : null;
          }
        },
        {
          text: 'Sites',
          dataField: 'sites',
          sort: true,
          formatter: (cell) => {
            return cell ? <button className="btn btn-link"><i className="fa fa-download" /></button> : null;
          }
        }
      ],
      projectData: [],
      totalRows: 0,
      page: 0,
      sizePerPage: 5,
      searchStr: '',
      tableWidget: ProjectTableWrapper,
      dropdownClass: "",
      enableSearch: true
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleMaximize = this.handleMaximize.bind(this);
    this.loadTable = this.loadTable.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleSizePerPageChange = this.handleSizePerPageChange.bind(this);
    this.handleRefreshTable = this.handleRefreshTable.bind(this);
  }
  componentWillMount() {
    const { data } = this.props;
    const projectData = projectDataToTableData(data, this.state.sizePerPage);
    this.setState({
      projectData
    });
  }
  handleSearch(searchStr) {
      const { data } = this.props;
      const projectData = projectDataToTableData(data, this.state.sizePerPage);
      let filteredData = projectData.filter(row => {
        let found = [];

        Object.keys(row).forEach((columnKey, idx, arr) => {
          let columnData = row[columnKey].toLowerCase();
          found.push(columnData.includes(searchStr) + "");
        });
        return found.includes("true");
      });
      const leftRows = filteredData.length < 1 ? this.state.sizePerPage : (filteredData.length % 5) > 0 ? (5 - (filteredData.length % 5)) : 0;
      const newFilteredData = addTableRows(filteredData, leftRows);
      this.setState({
        projectData: newFilteredData
      });
  }
  handleMaximize() {
    const _this = this;
    const maximized = this.state.maximized;
    this.setState({
      maximized: !maximized,
      tableWidget: null
    });
    window.setTimeout(function () {
      _this.loadTable();
    }, 200);
  }
  loadTable() {
    const maximized = this.state.maximized;
    this.setState({
      sizePerPage: maximized ? 10 : 5,
      tableWidget: ProjectTableWrapper
    });
  }
  handleSizePerPageChange(sizePerPage) {
    this.setState({
      sizePerPage
    });
  }
  handleDropdownToggle(paginationState) {
    const { dropdownOpen } = paginationState;
    this.setState({
      dropdownClass: dropdownOpen ? "page-size-dropdown-open" : ""
    })
  }
  handleRefreshTable() {
    this.setState({
      enableSearch: false,
      tableWidget: null
    }, () => {
      this.handleSearch("");
      window.setTimeout(() => {
        this.loadTable();
        this.setState({
          enableSearch: true
        });
      }, 100);
    })
  }
  getPagination() {
    const { projectData, maximized } = this.state;
    const customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total col-md-4 col-xs-4 col-sm-4 col-lg-2 pull-right">
        Showing {from} to {to} of {size} Results
      </span>
    );

    const options = {
      pageStartIndex: 1,
      alwaysShowAllBtns: true,
      withFirstAndLast: true,
      hideSizePerPage: !maximized,
      // hidePageListOnlyOnePage: true,
      showTotal: true,
      sizePerPage: this.state.sizePerPage,
      onSizePerPageChange: this.handleSizePerPageChange,
      onDropdownToggle: this.handleDropdownToggle,
      paginationTotalRenderer: customTotal,
      sizePerPageList: [{
        text: '5', value: 5
      }, {
        text: '10', value: 10
      }, {
        text: 'All', value: projectData.length
      }], 
    };
    return paginationFactory(options);
  }
  render() {
    const { projectData = [], columns = [], tableWidget: TableWidget, dropdownClass, loading } = this.state;
    
    return (
      <Widget
        loading={loading}
        className={cn("project-table-widget", dropdownClass)}
        title="Projects Overview"
        enableSearch={this.state.enableSearch}
        defaultSearchStr={this.state.searchStr}
        onRefresh={this.handleRefreshTable}
        onSearch={this.handleSearch}
        onMaximize={this.handleMaximize}>
        {
          TableWidget ?
            <TableWidget
              data={projectData}
              columns={columns}
              pagination={this.getPagination()} /> : null
        }
      </Widget>
    );
  }
}