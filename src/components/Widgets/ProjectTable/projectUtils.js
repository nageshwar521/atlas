const getDeepKeys = (obj) => {
  let keys = [];
  let excludedKeys = ["status", "stash", "stage", "Jira", "sites"];
  for (var key in obj) {
    keys.push(key);
    if ((typeof obj[key] === "object") && !Array.isArray(obj[key])) {
      var subkeys = getDeepKeys(obj[key]);
      if ((subkeys.includes("In Progress") || (subkeys.includes("Completed")))) {
        keys = keys.concat(subkeys.map(function (subkey, idx) {
          idx = idx + 1;
          if (idx === 1) {
            return [key].concat(subkeys);
          }
        }));
      } else {
        keys = keys.concat(subkeys.map(function (subkey) {
          return [key].concat(subkey);
        }));
      }
    } else {
      keys = keys.map((objKey) => {
        if (excludedKeys.includes(objKey) && obj[objKey]) {
          return obj[objKey];
        } else {
          return objKey;
        }
      }).filter((objKey) => {
        return ((objKey !== "totalVnfs"))
      });
    }
  }
  return keys;
}

export const addTableRows = (data, rows) => {
  let obj = {};
  obj["businessLine"] = "";
  obj["projectName"] = "";
  obj["vnfName"] = "";
  obj["status"] = "";
  obj["stage"] = "";
  obj["sites"] = "";
  obj["oneJira"] = "";
  obj["oneStash"] = "";

  for (let i = 0; i < rows; i++) {
    if ((data.length < 1) && (i < 1)) {
      data.push({
        "businessLine": "No Results Found!"
      })
    } else {
      data.push(obj);
    }
  }
  return data;
}

const generateTableData = (mainObj) => {
  const keyPaths = getDeepKeys(mainObj);
  const pathArr = keyPaths.filter((keyPathArr) => Array.isArray(keyPathArr)).filter((keyPathArr) => (keyPathArr.length === 8));
  const mainPathArr = pathArr.map((pathItem, idx) => {
    idx = idx + 1;
    let obj = {};
    obj["businessLine"] = pathItem[0];
    obj["projectName"] = pathItem[1];
    obj["vnfName"] = pathItem[2];
    obj["sites"] = "sites";
    obj["status"] = pathItem[4];
    obj["stage"] = pathItem[5];
    obj["oneJira"] = pathItem[6];
    obj["oneStash"] = pathItem[7];
    return obj;
  });

  const leftRows = mainPathArr.length < 1 ? 5 : (mainPathArr.length % 5) > 0 ? (5 - (mainPathArr.length % 5)) : 0;
  const newMainPathArr = addTableRows(mainPathArr, leftRows);
  return newMainPathArr;
};

export const projectDataToTableData = (response, sizePerPage) => {
  const { organizations } = response;
  const tableData = generateTableData(organizations, sizePerPage);
  return tableData;
}

export default {
  projectDataToTableData
}

