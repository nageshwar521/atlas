import React, { Component, Fragment } from 'react';

export default class ProjectTableSearch extends Component {
  constructor() {
    super();
    this.state = {
      searchStr: '',
    };
  }
  componentWillMount() {
    this.setState({
      searchStr: this.props.searchStr,
    });
  }
  handleClear() {
    this.setState({ searchStr: "" }, () => {
      this.props.onSearchChange("");
    });
  }
  handleSubmit() {
    this.props.onSearchChange(this.state.searchStr.toLowerCase());
  }
  render() {
    const { searchStr = '' } = this.state;
    return (
      <div className="input-group projectTable-searchBox">
        <input
          className="form-control"
          placeholder="Search..."
          type="text"
          onChange={evt =>
            this.setState({ searchStr: evt.target.value }, () => {
              this.props.onSearchChange(this.state.searchStr);
            })
          }
          value={searchStr}
        />
        {/* <div className="input-group-addon">
          <span className="fa fa-search searchIcon" />
        </div> */}
      </div>
    );
  }
}