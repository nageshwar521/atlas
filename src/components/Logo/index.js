import React from 'react'

const Logo = () => {
  return (
    <a href="#" className="logo" id="A_2">
          <span className="logo-lg" id="SPAN_5"><img src="/img/verizonlogo_desktop.png"></img><b id="B_6">ATLAS</b></span>
        </a>
  )
}

Logo.propTypes = {

}

export default Logo;