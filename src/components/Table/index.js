import React from 'react'
import TableRow from '../TableRow';
import TableHeader from '../TableHeader';

const buildRows = (data, columns, pageSize) => {
    const rows = [];
    for (let i = 0; i < pageSize; i++) {
        rows.push(<TableRow
            key={i}
            data={data[i] ? data[i] : []}
            columns={columns}></TableRow>)
    }
    return rows;
}

export default ({ columns, data, className, pageSize }) => {
    return (
        <div>
            <table className={`table dataTable ${className}`} role="grid">
                <thead>
                    <TableHeader columns={columns}></TableHeader>
                </thead>
                <tbody>
                    {
                        buildRows(data, columns, pageSize)
                    }
                </tbody>
            </table>
            <div className="row">
                <nav aria-label="Page navigation col-md-6">
                    {/* <ul className="pagination">
                    <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                    </li>
                </ul> */}
                </nav>
            </div>
        </div>
    )
}