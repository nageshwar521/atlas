import React from 'react'
import PropTypes from 'prop-types'
import SidebarToggler from '../SidebarToggler';
import Logo from '../Logo';
import Tooltip from 'rc-tooltip';

const Navbar = () => {
  return (
    <nav className="navbar navbar-static-top" id="NAV_7">
      <SidebarToggler></SidebarToggler>
      <Logo></Logo>
      <div className="navbar-custom-menu" id="DIV_10">
        <ul className="nav navbar-nav" id="UL_11">
          <li className="dropdown user user-menu">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <i className="fa fa-user-circle-o" />
              <span className="hidden-xs">admin</span>
            </a>
          </li>
          <Tooltip placement="bottom" trigger={['hover']} overlay="Settings">
            <li>
              <a href="#" data-toggle="control-sidebar" id="A_148"><i className="fa fa-cog" /></a>
            </li>
          </Tooltip>
          {/* <li>
            <a href="##" data-toggle="control-sidebar" id="A_148"><i className="fa fa-tasks" /></a>
          </li>
          <li>
            <a href="##" data-toggle="control-sidebar" id="A_148"><i className="fa fa-book" /></a>
          </li> */}
          <Tooltip placement="bottom" trigger={['hover']} overlay="Logout">
            <li>
              <a href="##" data-toggle="control-sidebar" id="A_148"><i className="fa fa-power-off" /></a>
            </li>
          </Tooltip>
        </ul>
      </div>
    </nav>
  );
}

Navbar.propTypes = {

}

export default Navbar

