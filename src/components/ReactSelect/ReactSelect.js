import React from 'react';
import Select, { components } from 'react-select';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const DropdownIndicator = props => {
  return (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...props}>
        <span className="react-select-indicator-wrapper">
          <i className={props.selectProps.menuIsOpen ? "fa fa-lg fa-caret-up" : "fa fa-lg fa-caret-down"} />
        </span>
      </components.DropdownIndicator>
    )
  );
};

export default class ReactSelect extends React.Component {
  render() {
    const { selectedOption, handleChange, options, className = "", placeholder = "Select", customStyles,  ...restProps } = this.props;
    const classes = `reactSelect ${className}`;
    const selectedValue = selectedOption ? 
            (typeof selectedOption === "string") ? 
              { value: selectedOption, label: selectedOption } : 
              selectedOption :
            { value: "Select", label: <span className="react-select-placeholder">{placeholder}</span>, isDisabled: true };

    return (
      <Select
        styles={customStyles}
        components={{ IndicatorSeparator: null, DropdownIndicator }}
        className={classes}
        classNamePrefix="reactSelect"
        value={selectedValue}
        onChange={handleChange}
        options={options}
        {...restProps}
      />
    );
  }
}