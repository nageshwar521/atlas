import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toastr } from 'react-redux-toastr';
import snapshotsActions from '../../actions/snapshotsActions';
import devicesActions from '../../actions/devicesActions';

class Notification extends React.Component {
  constructor() {
    super();
    this.createNotification = this.createNotification.bind(this);
    this.handleHideNotification = this.handleHideNotification.bind(this);
  }
  handleHideNotification(unknown, params) {
    console.log(unknown, params);
  }
  createNotification() {
    console.log(this.props);
    const {
      isSingleSnapshotDownloadProgress,
      isSingleSnapshotDownloadSuccess,
      isSingleSnapshotDownloadError,
      isMultipleSnapshotsDownloadProgress,
      isMultipleSnapshotsDownloadSuccess,
      isMultipleSnapshotsDownloadError,
      isSingleSnapshotDeleteSuccess,
      isSingleSnapshotDeleteError,
      isMultipleSnapshotsDeleteSuccess,
      isMultipleSnapshotsDeleteError,
      isAddDeviceSuccess,
      isAddDeviceError,
      isUpdateDeviceSuccess,
      isUpdateDeviceError,
      isBackupNowDataUploadSuccess,
      isBackupNowDataUploadError,
      isScheduleBackupDataUploadSuccess,
      isScheduleBackupDataUploadError,
      isSingleDeviceDeleteSuccess,
      isSingleDeviceDeleteError,
      isMultipleDevicesDeleteSuccess,
      isMultipleDevicesDeleteError,
      isAddSiteSuccess,
      isAddSiteError,
      isUpdateSiteSuccess,
      isUpdateSiteError,
      isSingleSiteDeleteSuccess,
      isSingleSiteDeleteError,
      isMultipleSitesDeleteSuccess,
      isMultipleSitesDeleteError
    } = this.props;
    let message = "";
    let type = "";
    let title = "";
    let toastrOptions = {
      transitionIn: 'fadeIn',
      transitionOut: 'fadeOut',
      onHideComplete: this.handleHideNotification
    };
    if (isAddDeviceSuccess) {
      type = "success";
      title = "Success";
      message = "added device successfully";
    } else if (isAddDeviceError) {
      type = "error";
      title = "Error";
      message = "adding device failed"
    } else if (isUpdateDeviceSuccess) {
      type = "success";
      title = "Success";
      message = "updated device successfully"
    } else if (isUpdateDeviceError) {
      type = "error";
      title = "Error";
      message = "updating device failed"
    } else if (isBackupNowDataUploadSuccess) {
      type = "success";
      title = "Success";
      message = "backup data uploaded successfully"
    } else if (isBackupNowDataUploadError) {
      type = "error";
      title = "Error";
      message = "uploading backup data failed"
    } else if (isScheduleBackupDataUploadSuccess) {
      type = "success";
      title = "Success";
      message = "schedule backup data uploaded successfully"
    } else if (isScheduleBackupDataUploadError) {
      type = "error";
      title = "Error";
      message = "schedule backup backup data failed"
    } else if (isSingleDeviceDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "device deleted successfully"
    } else if (isSingleDeviceDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting device failed"
    } else if (isMultipleDevicesDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "devices deleted successfully"
    } else if (isMultipleDevicesDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting devices failed"
    } else if (isSingleSnapshotDownloadProgress) {
      toastrOptions = {
        id: 'singleSnapshotDownloadProgress'
      }
      type = "info";
      title = "Progress";
      message = "snapshot downloading is in progress"
    } else if (isSingleSnapshotDownloadSuccess) {
      toastr.removeByType('info');
      type = "success";
      title = "Success";
      message = "snapshot downloaded successfully";
    } else if (isSingleSnapshotDownloadError) {
      toastr.removeByType('info');
      type = "error";
      title = "Error";
      message = "downloading snapshot failed";
    } else if (isMultipleSnapshotsDownloadProgress) {
      toastrOptions = {
        id: 'multipleSnapshotDownloadProgress'
      }
      type = "info";
      title = "Progress";
      message = "snapshot downloading is in progress"
    } else if (isMultipleSnapshotsDownloadSuccess) {
      toastr.removeByType('info');
      type = "success";
      title = "Success";
      message = "snapshots downloaded successfully"
    } else if (isMultipleSnapshotsDownloadError) {
      toastr.removeByType('info');
      type = "error";
      title = "Error";
      message = "downloading snapshots failed"
    } else if (isSingleSnapshotDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "snapshot deleted successfully"
    } else if (isSingleSnapshotDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting snapshot failed"
    } else if (isMultipleSnapshotsDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "snapshots deleted successfully"
    } else if (isMultipleSnapshotsDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting snapshots failed"
    } else if (isAddSiteSuccess) {
      type = "success";
      title = "Success";
      message = "added site successfully"
    } else if (isAddSiteError) {
      type = "error";
      title = "Error";
      message = "adding site failed"
    } else if (isUpdateSiteSuccess) {
      type = "success";
      title = "Success";
      message = "updated site successfully"
    } else if (isUpdateSiteError) {
      type = "error";
      title = "Error";
      message = "updating site failed"
    } else if (isSingleSiteDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "Site deleted successfully"
    } else if (isSingleSiteDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting site failed"
    } else if (isMultipleSitesDeleteSuccess) {
      type = "success";
      title = "Success";
      message = "Sites deleted successfully"
    } else if (isMultipleSitesDeleteError) {
      type = "error";
      title = "Error";
      message = "deleting sites failed"
    }

    if (message) {
      toastr[type](title, message, toastrOptions);
    }
  }
  render() {
    return (
      <div>
      </div>
    );
  }
}

const mapStateToProps = ({ snapshotsReducer, devicesReducer, sitesReducer }) => {
  return { ...snapshotsReducer, ...devicesReducer, ...sitesReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...snapshotsActions, ...devicesActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(Notification);
export default ConnectedComponent;