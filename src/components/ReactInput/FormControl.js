import React from 'react';
import cx from 'classnames';
import ReactInput from './ReactInput';
import ReactSelect from '../ReactSelect/ReactSelect';
import DatePicker from "react-datepicker";
import IconButton from '../IconButton/IconButton';

const renderForm = (props, isLocked) => {
  // console.log(props);
  switch (props.type) {
    case 'text':
      return (
        <ReactInput disabled={isLocked} type="text" {...props.inputProps} />
      )
    case 'password':
      return (
        <ReactInput disabled={isLocked} type="password" {...props.inputProps} />
      )
    case 'select':
      return (
        <ReactSelect isDisabled={isLocked} disabled={isLocked} className="configurations--form--select" {...props.selectProps} />
      )
    case 'textarea':
      return (
        <ReactInput disabled={isLocked} type="textarea" {...props.inputProps} />
      )
    case 'date':
      return (
        <DatePicker
          className="configurations--form--input"
          {...props.dateProps}
        />
      )
    case 'time':
      return (
        <DatePicker
          className="configurations--form--input"
          {...props.timeProps}
        />
      )
  }
}

const FormControl = ({ formControlClass, label, isRequired, hasError, isLocked, inputType, showPassword, onShowHide, errorText="", ...restProps }) => {
  // console.log(restProps);
  return (
    <div className={cx("configurations--form--control", formControlClass, hasError ? "has-error" : "", isLocked ? "is-locked" : "")}>
      <div className="control-label configurations--form--label clearfix">
        <div className="col-xs-8 no-padding">
          {label}
          {isRequired ? <span className="configurations--form--label--required">*</span> : null}
        </div>
        <div className="col-xs-4 no-padding text-right">
          {
            (!isLocked && inputType === "password") ?
              <IconButton 
                icon={showPassword ? "fa fa-eye" : "fa fa-eye-slash"} 
                title={showPassword ? "Hide" : "Show"} 
                onClick={onShowHide}
              /> : null
          }
        </div>
      </div>
      {renderForm(restProps, isLocked)}
      <div className={cx("configurations--form--error")}>{errorText}</div>
    </div>
  );
}

export default FormControl;