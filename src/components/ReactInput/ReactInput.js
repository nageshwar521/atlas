import React from 'react';

const getInput = (props) => {
  switch (props.type) {
    case "textarea":
      return (
        <textarea
          onChange={props.onChange}
          onBlur={props.onBlur}
          className="form-control configurations--form--textarea"
          {...props}
        >{props.value}</textarea>
      );
    default:
      return (
        <input
          onChange={props.onChange}
          onBlur={props.onBlur}
          value={props.value}
          type={props.type}
          className="form-control configurations--form--input"
          {...props}
        />
      );
  }
}

const ReactInput = (props) => {
  // console.log(props);
  return (
    <div className="configurations--form--input-wrapper">
      {getInput(props)}
    </div>
  );
}

export default ReactInput;