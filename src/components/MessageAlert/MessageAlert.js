import React from 'react';
import cn from 'classnames';

const getAlertType = type => {
  switch(type) {
    case 'success':
      return 'alert-success';
    case 'info':
      return 'alert-info';
    case 'warning':
      return 'alert-warning';
    case 'danger':
      return 'alert-danger';
    default:
      return 'alert-danger';
  }
};

const MessageAlert = (props) => {
  const type = getAlertType(props.type);
  const mainClass = cn("alert alert-dismissible message-alert", type, props.className);
  const btnClass = cn("close message-alert--close-button", btnClass);
  return (
    <div className={mainClass} role="alert">
      <button 
        type="button" 
        className={btnClass} 
        data-dismiss="alert" 
        aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div className="message-alert--message-wrapper">{props.message ? props.message : props.children}</div>
    </div>
  );
}

export default MessageAlert;