import React, { Component } from 'react';

export default class LoadingModal extends Component {
  render() {
    const {
      message = "Loading..."
    } = this.props;
    return (
      <div className="loadingModal">
        <div className="loadingMessage">{message}</div>
      </div>
    );
  }
}