import React, { Component, Fragment } from 'react';
import cn from 'classnames';

export default class WidgetSearch extends Component {
  constructor() {
    super();
    this.state = {
      searchStr: '',
    };
  }
  componentWillMount() {
    this.setState({
      searchStr: this.props.searchStr,
    });
  }
  handleClear() {
    this.setState({ searchStr: "" }, () => {
      this.props.onSearch("");
    });
  }
  render() {
    const { searchStr = '' } = this.state;
    const {
      className,
      inputClassName,
      placeholder = "Search..."
    } = this.props;
    const searchClass = cn('input-group widget-search', className);
    const inputClass = cn('form-control', inputClassName);

    return (
      <div className={searchClass}>
        <input
          className={inputClass}
          placeholder={placeholder}
          type="text"
          onChange={evt =>
            this.setState({ searchStr: evt.target.value }, () => {
              if (this.props.onSearch) {
                this.props.onSearch(this.state.searchStr.toLowerCase());
              }
            })
          }
          value={searchStr}
        />
        <div className="input-group-addon">
          {/* <span className="fa fa-search searchIcon" /> */}
        </div>
      </div>
    );
  }
}