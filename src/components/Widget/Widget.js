import React, { Component } from 'react';
import cn from 'classnames';
import WidgetHeader from './WidgetHeader';
import WidgetBody from './WidgetBody';

export default class Widget extends Component {
  constructor() {
    super();
    this.state = {
      minimized: false,
      maximized: false,
      isMinimizable: true,
      isMaximizable: true,
      isRefreshable: true
    }
    this.toggleMinimize = this.toggleMinimize.bind(this);
    this.toggleMaximize = this.toggleMaximize.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }
  
  toggleMinimize(icon, e) {
    this.setState({
      minimized: !this.state.minimized,
      maximized: false
    }, () => {
      if (this.props.onMinimize) {
        this.props.onMinimize(icon, e);
      }
    });
  }
  toggleMaximize(icon, e) {
    const maximized = !this.state.maximized;
    this.setState({
      maximized,
      minimized: false,
      isMinimizable: !maximized
    }, () => {
      if (this.props.onMaximize) {
        this.props.onMaximize(icon, e);
      }
    });
  }
  handleRefresh(icon, e) {
    if (this.props.onRefresh) {
      this.props.onRefresh(icon, e);
    }
  }
  handleSearch(searchStr) {
    if (this.props.onSearch) {
      this.props.onSearch(searchStr);
    }
  }
  render() {
    const { 
      className,
      children,
      title,
      enableSearch,
      searchComponent,
      buttons,
      loading
    } = this.props;
    const props = this.props;
    const { minimized, maximized } = this.state;
    const panelType = minimized ? "panel-minimize" : maximized ? "panel-maximize" : "";
    const panelClass = cn('panel panel-default panel-widget', panelType, className);
    const isMinimizable = props.hasOwnProperty('isMinimizable') ? this.props.isMinimizable : this.state.isMinimizable;
    const isMaximizable = props.hasOwnProperty('isMaximizable') ? this.props.isMaximizable : this.state.isMaximizable;
    const isRefreshable = props.hasOwnProperty('isRefreshable') ? this.props.isRefreshable : this.state.isRefreshable;
    return (
      <div className={panelClass}>
        <WidgetHeader 
          buttons={buttons}
          searchComponent={searchComponent}
          minimized={minimized}
          maximized={maximized}
          isRefreshable={(isRefreshable)}
          isMinimizable={isMinimizable}
          isMaximizable={isMaximizable}
          enableSearch={enableSearch}
          onSearch={this.handleSearch}
          onRefresh={this.handleRefresh}
          onMinimize={this.toggleMinimize}
          onMaximize={this.toggleMaximize}>{title}</WidgetHeader>
        {minimized ? 
          null : 
          <WidgetBody loading={loading}>
            {children}
          </WidgetBody>}
      </div>
    );
  }
}