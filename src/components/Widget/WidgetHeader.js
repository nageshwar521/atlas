import React, { Component } from 'react';
import cn from 'classnames';
import WidgetControl from './WidgetControl';
import WidgetSearch from './WidgetSearch';

class WidgetHeader extends Component {
  constructor() {
    super();
    this.handleMinimize = this.handleMinimize.bind(this);
    this.handleMaximize = this.handleMaximize.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.renderButtons = this.renderButtons.bind(this);
  }
  handleMinimize(icon, e) {
    if (this.props.onMinimize) {
      this.props.onMinimize(icon, e);
    }
  }
  handleMaximize(icon, e) {
    if (this.props.onMaximize) {
      this.props.onMaximize(icon, e);
    }
  }
  handleRefresh(icon, e) {
    if (this.props.onRefresh) {
      this.props.onRefresh(icon, e);
    }
  }
  handleSearch(searchStr) {
    if (this.props.onSearch) {
      this.props.onSearch(searchStr);
    }
  }
  renderButtons() {
    const { buttons = [] } = this.props;
    return buttons.map((button, idx) => {
      const { isVisible, icon, title, onClick } = button;
      return (
        <WidgetControl
          disabled={!isVisible}
          key={idx}
          icon={icon}
          title={title}
          onClick={onClick} /> 
      );
    });
  }
  render() {
    const {
      className,
      titleClassName,
      controlsClassName,
      title,
      children,
      minimized,
      maximized,
      isMinimizable = true,
      isMaximizable = true,
      isRefreshable = true,
      enableSearch = false,
      searchComponent: SearchComponent
    } = this.props;
    const headingClass = cn('panel-heading clearfix', className);
    const titleClass = cn('panel-title col-xs-12 col-sm-6 no-padding', titleClassName);
    const controlsClass = cn('panel-controls col-xs-12 col-sm-6 no-padding clearfix', controlsClassName);
    return (
      <div className={headingClass}>
        <div className={titleClass}>
          {children ? children : title}
        </div>
        <div className={controlsClass}>
          <div className="pull-right controls-wrapper clearfix">
            <div className="btn-group pull-right">
              {
                this.renderButtons()
              }
              {
                <WidgetControl
                  disabled={!isRefreshable || minimized}
                  icon="fa-undo"
                  title="Reset"
                  onClick={this.handleRefresh} />
              }
              {
                <WidgetControl
                  disabled={!isMinimizable}
                  icon={minimized ? "fa-plus" : "fa-minus"}
                  title="Minimize"
                  onClick={this.handleMinimize} />
              }
              {
                <WidgetControl
                  disabled={!isMaximizable}
                  icon={maximized ? "fa-times" : "fa-expand"}
                  title="Maximize"
                  onClick={this.handleMaximize} />
              }
            </div>
            <div className="pull-right">
              {
                enableSearch ?
                  SearchComponent ? SearchComponent : <WidgetSearch onSearch={this.handleSearch} /> : null
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

WidgetHeader.defaultProps = {
  
};

export default WidgetHeader;