import React, { Component } from "react";
import {
  Button
} from "react-bootstrap";
import Tooltip from 'rc-tooltip';
import cn from 'classnames';

class WidgetControl extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    const { icon } = this.props;
    if (this.props.onClick) {
      this.props.onClick(icon, e);
    }
  }
  render() {
    const {
      placement,
      trigger,
      title,
      btnClassName,
      iconClassName,
      icon,
      disabled
    } = this.props;
    const btnClass = cn('btn btn-default', btnClassName);
    const iconClass = cn('fa', icon, iconClassName);

    return (
      disabled ? 
        <button
          disabled={disabled}
          className={btnClass}
          onClick={this.handleClick}
        >
          <i className={iconClass} />
        </button> : 
        <Tooltip placement={placement} trigger={trigger} overlay={title}>
          <button
            disabled={disabled}
            className={btnClass}
            onClick={this.handleClick}
          >
            <i className={iconClass} />
          </button>
        </Tooltip>
    );
  }
}

WidgetControl.defaultProps = {
  placement: "bottom",
  trigger: ["hover"],
  overlayText: "overlay"
};

export default WidgetControl;