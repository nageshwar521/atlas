import React, { Component } from 'react';
import cn from 'classnames';

export default class WidgetBody extends Component {
  render() {
    const { 
      className,
      children,
      loading = false
    } = this.props;
    const bodyClass = cn('panel-body', className);

    return (
      loading ? 
        <div className="widget-loading">loading...</div> : 
        <div className={bodyClass}>
          {children}
        </div>
    );
  }
}