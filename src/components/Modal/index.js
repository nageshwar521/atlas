import React from 'react'

export default ({ title, children, cancelLabel, saveLabel, onSave, onCancel, onClose, isDisabled }) => {
    return (
        <div className="modal fade in modal-open" id="modal-default" data-component="Modal">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={onClose}>
                            <span aria-hidden="true">×</span></button>
                        <h4 className="modal-title">{title}</h4>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer">
                        {cancelLabel && <button type="button" className="btn btn-default pull-left"
                            data-dismiss="modal" onClick={onCancel}>{cancelLabel}</button>}
                        <button disabled={isDisabled} type="button" className="btn btn-primary" onClick={onSave}>{saveLabel}</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
