import React from 'react'

const TableRow = ({data, columns}) => {
  return (
    <tr role="row">
    {
        columns.map((column,index)=><td key={index}>
            {
              column.cell?
                data[column.accessor] && column.cell(data[column.accessor])
                :data[column.accessor]}
        </td>)
    } 
    </tr>
  )
}

export default TableRow;