import React, { Component } from "react";
import Tooltip from 'rc-tooltip';
import cn from 'classnames';

class IconButton extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    const { icon } = this.props;
    if (this.props.onClick) {
      this.props.onClick(icon, e);
    }
  }
  render() {
    const {
      placement,
      trigger,
      title,
      className,
      iconClassName,
      icon,
      disabled
    } = this.props;
    const btnClass = cn('btn btn-icon', className);
    const iconClass = cn('fa', icon, iconClassName);

    return (
      disabled ?
        <button
          disabled={disabled}
          className={btnClass}
          onClick={this.handleClick}
        >
          <i className={iconClass} />
        </button> :
        <Tooltip placement={placement} trigger={trigger} overlay={title}>
          <button
            disabled={disabled}
            className={btnClass}
            onClick={this.handleClick}
          >
            <i className={iconClass} />
          </button>
        </Tooltip>
    );
  }
}

IconButton.defaultProps = {
  placement: "bottom",
  trigger: ["hover"],
  title: "overlay"
};

export default IconButton;