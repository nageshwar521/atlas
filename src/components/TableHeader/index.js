import React from 'react'
import PropTypes from 'prop-types'

const TableHeader = ({ columns }) => {
  return (
    <tr className="table-header" role="row">
      {
        columns.map((column, index) => <th key={index}
          colSpan={column.colSpan ? colSpan : 1} >
          {column.header}
        </th>)
      }
    </tr>
  )
}

export default TableHeader;
