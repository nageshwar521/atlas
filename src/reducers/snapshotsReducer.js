import * as types from '../actions/actionTypes';

const initialState = {
  snapshots: [],
  totalSnapshots: 0,
  error: null,
  fetching: false,
  countMatchingRequest: 0,
  selectedSnapshots: [],
  selectedSnapshotIds: [],
  filteredSnapshots: [],
  isSingleSnapshotDownloadProgress: false,
  isSingleSnapshotDownloadSuccess: false,
  isSingleSnapshotDownloadError: false,
  isMultipleSnapshotsDownloadProgress: false,
  isMultipleSnapshotsDownloadSuccess: false,
  isMultipleSnapshotsDownloadError: false,
  isSingleSnapshotDeleteSuccess: false,
  isSingleSnapshotDeleteError: false,
  isMultipleSnapshotsDeleteSuccess: false,
  isMultipleSnapshotsDeleteError: false
};
const snapshotsReducer = (state = initialState, action) => {
  // console.log("snapshotsReducer=", action.type);
  // console.log(state);
  switch (action.type) {
    case types.FETCH_SNAPSHOTS_REQUEST:
      return Object.assign({}, state, {
        fetching: true
      });
      break;
    case types.FETCH_SNAPSHOTS_SUCCESS: {
      let { snapshots, totalCount, countMatchingRequest} = action.payload;
      return Object.assign({}, state, {
        fetching: false,
        totalSnapshots: totalCount,
        countMatchingRequest,
        snapshots: snapshots.map((snapshotName, id) => Object.assign({}, snapshotName, { id }))
      });
      break;
    }
    case types.FETCH_SNAPSHOTS_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error
      });
      break;
    case types.SELECTED_SNAPSHOTS: {
      const { id, isSelect, selectedSnapshots } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedSnapshotIds: [...state.selectedSnapshotIds, id],
          selectedSnapshots: [...state.selectedSnapshots, ...selectedSnapshots]
        });
      } else {
        return Object.assign({}, state, {
          selectedSnapshotIds: state.selectedSnapshotIds.filter(x => x !== id),
          selectedSnapshots: state.selectedSnapshots.filter(x => x.id !== id)
        });
      }
      break;
    }
    case types.SELECTED_ALL_SNAPSHOTS: {
      const { ids, isSelect, selectedSnapshots } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedSnapshotIds: [...ids],
          selectedSnapshots: [...selectedSnapshots]
        });
      } else {
        return Object.assign({}, state, {
          selectedSnapshotIds: [],
          selectedSnapshots: []
        });
      }
      break;
    }
    case types.FILTER_SNAPSHOTS:
      return Object.assign({}, state, {
        snapshots: state.snapshots.filter(x => state.selectedSnapshotIds.includes(x.id))
      });
      break;
    case types.CLEAR_SNAPSHOTS:
      return Object.assign({}, state, {
        selectedSnapshotIds: [],
        selectedSnapshots: []
      });
      break;
    case types.DOWNLOAD_SINGLE_SNAPSHOT_REQUEST:
      return Object.assign({}, state, {
        isSingleSnapshotDownloadProgress: true,
        isSingleSnapshotDownloadSuccess: false,
        isSingleSnapshotDownloadError: false
      });
    case types.DOWNLOAD_SINGLE_SNAPSHOT_SUCCESS:
      return Object.assign({}, state, {
        isSingleSnapshotDownloadProgress: false,
        isSingleSnapshotDownloadSuccess: true,
        isSingleSnapshotDownloadError: false
      });
    case types.DOWNLOAD_SINGLE_SNAPSHOT_FAILURE:
      return Object.assign({}, state, {
        isSingleSnapshotDownloadProgress: false,
        isSingleSnapshotDownloadSuccess: false,
        isSingleSnapshotDownloadError: true
      });
    case types.DOWNLOAD_MULTIPLE_SNAPSHOTS_REQUEST:
      return Object.assign({}, state, {
        isMultipleSnapshotsDownloadProgress: true,
        isMultipleSnapshotsDownloadSuccess: false,
        isMultipleSnapshotsDownloadError: false
      });
    case types.DOWNLOAD_MULTIPLE_SNAPSHOTS_SUCCESS:
      return Object.assign({}, state, {
        isMultipleSnapshotsDownloadProgress: false,
        isMultipleSnapshotsDownloadSuccess: true,
        isMultipleSnapshotsDownloadError: false
      });
    case types.DOWNLOAD_MULTIPLE_SNAPSHOTS_FAILURE:
      return Object.assign({}, state, {
        isMultipleSnapshotsDownloadProgress: false,
        isMultipleSnapshotsDownloadSuccess: false,
        isMultipleSnapshotsDownloadError: true
      });
    case types.DELETE_SINGLE_SNAPSHOT_REQUEST:
      return Object.assign({}, state, {
        isSingleSnapshotDeleteSuccess: false,
        isSingleSnapshotDeleteError: false
      });
    case types.DELETE_SINGLE_SNAPSHOT_SUCCESS:
      return Object.assign({}, state, {
        isSingleSnapshotDeleteSuccess: true,
        isSingleSnapshotDeleteError: false
      });
    case types.DELETE_SINGLE_SNAPSHOT_FAILURE:
      return Object.assign({}, state, {
        isSingleSnapshotDeleteSuccess: false,
        isSingleSnapshotDeleteError: true
      });
    case types.DELETE_MULTIPLE_SNAPSHOTS_REQUEST:
      return Object.assign({}, state, {
        isMultipleSnapshotsDeleteSuccess: false,
        isMultipleSnapshotsDeleteError: false
      });
    case types.DELETE_MULTIPLE_SNAPSHOTS_SUCCESS:
      return Object.assign({}, state, {
        isMultipleSnapshotsDeleteSuccess: true,
        isMultipleSnapshotsDeleteError: false
      });
    case types.DELETE_MULTIPLE_SNAPSHOTS_FAILURE:
      return Object.assign({}, state, {
        isMultipleSnapshotsDeleteSuccess: false,
        isMultipleSnapshotsDeleteError: true
      });
    case types.RESET_SNAPSHOT_TAB: {
      return Object.assign({}, state, {
        selectedSnapshots: [],
        selectedSnapshotIds: [],
        filteredSnapshots: [],
        isSingleSnapshotDownloadSuccess: false,
        isSingleSnapshotDownloadError: false,
        isMultipleSnapshotsDownloadSuccess: false,
        isMultipleSnapshotsDownloadError: false,
        isSingleSnapshotDeleteSuccess: false,
        isSingleSnapshotDeleteError: false,
        isMultipleSnapshotsDeleteSuccess: false,
        isMultipleSnapshotsDeleteError: false
      });
    }
    case types.HANDLE_SEARCH:
      return Object.assign({}, state, {
        filteredSnapshots: [...action.payload]
      });
      break;
    default:
  }
  return state;
}

export default snapshotsReducer;