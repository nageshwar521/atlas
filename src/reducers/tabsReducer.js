import * as types from '../actions/actionTypes';

const initialState = {
  activeTab: 'Snapshots'
};

const tabsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_TAB:
      return Object.assign({}, state, {
        activeTab: action.tabName
      });
    default:
      return {...state};
  }
}

export default tabsReducer;