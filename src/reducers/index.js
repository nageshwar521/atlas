import { combineReducers } from 'redux';
// Reducers
import deploymentsReducer from './deploymentsReducer';
// Snapshots
import snapshotsReducer from './snapshotsReducer';
// Modal
import modalReducer from './modalReducer';
// Tabs
import tabsReducer from './tabsReducer';
// Sites
import sitesReducer from './sitesReducer';
// Devices
import devicesReducer from './devicesReducer';
// Toastr
import { reducer as toastr } from 'react-redux-toastr';

const rootReducer = combineReducers({
  deploymentsReducer,
  snapshotsReducer,
  modalReducer,
  tabsReducer,
  sitesReducer,
  devicesReducer,
  toastr
});

export default rootReducer;