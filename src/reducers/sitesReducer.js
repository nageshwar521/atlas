import * as types from '../actions/actionTypes';

const initialState = {
  isOpenNewSiteForm: false,
  isOpenSiteDetails: false,
  isEditable: false,
  siteData: null,
  isFetching: false,
  sites: [],
  error: null,
  totalCount: 0,
  countMatchingRequest: 0,
  selectedSites: [],
  selectedSiteIds: [],
  filteredSites: [],
  isAddSiteSuccess: false,
  isAddSiteError: false,
  isUpdateSiteSuccess: false,
  isUpdateSiteError: false,
  isSingleSiteDeleteSuccess: false,
  isSingleSiteDeleteError: false,
  isMultipleSitesDeleteSuccess: false,
  isMultipleSitesDeleteError: false
};

const sitesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SHOW_NEW_SITE_FORM:
      return Object.assign({}, state, {
        isOpenNewSiteForm: true
      });
    case types.HIDE_NEW_SITE_FORM:
      return Object.assign({}, state, {
        isOpenNewSiteForm: false
      });
    case types.SHOW_SITE_DETAILS:
      return Object.assign({}, state, {
        isOpenSiteDetails: true,
        siteData: { ...action.payload.siteData },
        isEditable: action.payload.isEditable
      });
    case types.HIDE_SITE_DETAILS:
      return Object.assign({}, state, {
        isOpenSiteDetails: false,
        siteData: null
      });
    case types.FETCH_SITES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    // case types.FETCH_SITES_SUCCESS:
      // // const { sites, totalCount, countMatchingRequest } = action.payload;
      // const sites = action.payload;
      // const totalCount = sites.length;
      // return Object.assign({}, state, {
      //   isFetching: false,
      //   sites,
      //   totalCount: totalCount,
      //   countMatchingRequest: 0
      // });
      case types.FETCH_SITES_SUCCESS:
      // const { devices, totalCount, countMatchingRequest } = action.payload;
      const sites = action.payload;
      const totalCount = sites.length;
      return Object.assign({}, state, {
        isFetching: false,
        sites: sites.map((site, idx) => {
          site.id = idx;
          return site;
        }), 
        totalCount: totalCount, 
        countMatchingRequest: 0
      });
    case types.FETCH_SITES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.SELECTED_SITES: {
      const { id, isSelect, selectedSites } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedSiteIds: [...state.selectedSiteIds, id],
          selectedSites: [...state.selectedSites, ...selectedSites]
        });
      } else {
        return Object.assign({}, state, {
          selectedSiteIds: state.selectedSiteIds.filter(x => x !== id),
          selectedSites: state.selectedSites.filter(x => x.id !== id)
        });
      }
    }
    case types.SELECTED_ALL_SITES: {
      const { ids, isSelect, selectedSites } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedSiteIds: [...ids],
          selectedSites: [...selectedSites]
        });
      } else {
        return Object.assign({}, state, {
          selectedSiteIds: [],
          selectedSites: []
        });
      }
    }
    case types.FILTER_SITES:
      return Object.assign({}, state, {
        sites: state.sites.filter(x => state.selectedSiteIds.includes(x.id))
      });
    case types.CLEAR_SITES:
      return Object.assign({}, state, {
        selectedSiteIds: [],
        selectedSites: [],
        filteredSites: []
      });
    case types.HANDLE_SEARCH:
      return Object.assign({}, state, {
        filteredSites: [...action.payload]
      });
    case types.ADD_SITE_REQUEST:
      return Object.assign({}, state, {
        isAddSiteSuccess: false,
        isAddSiteError: false
      });
    case types.ADD_SITE_SUCCESS:
      return Object.assign({}, state, {
        isAddSiteSuccess: true,
        isAddSiteError: false,
        isOpenNewSiteForm: false,
        selectedSiteIds: [],
        selectedSites: [],
        filteredSites: []
      });
    case types.ADD_SITE_FAILURE:
      return Object.assign({}, state, {
        isAddSiteSuccess: false,
        isAddSiteError: true,
        error: action.error,
        isOpenNewSiteForm: true
      });
    case types.UPDATE_SITE_DETAILS:
      return Object.assign({}, state, {
        siteData: { ...action.payload }
      });
    case types.UPDATE_SITE_REQUEST:
      return Object.assign({}, state, {
        isUpdateSiteSuccess: false,
        isUpdateSiteError: false
      });
    case types.UPDATE_SITE_SUCCESS:
      return Object.assign({}, state, {
        isUpdateSiteSuccess: true,
        isUpdateSiteError: false,
        isOpenSiteDetails: false,
        siteData: null,
        selectedSiteIds: [],
        selectedSites: [],
        filteredSites: []
      });
    case types.UPDATE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        isUpdateSiteSuccess: false,
        isUpdateSiteError: true,
        isOpenDeviceDetails: false,
        siteData: null,
        error: action.error
      });
    case types.DELETE_SINGLE_SITE_REQUEST:
      return Object.assign({}, state, {
        isSingleSiteDeleteSuccess: false,
        isSingleSiteDeleteError: false
      });
    case types.DELETE_SINGLE_SITE_SUCCESS:
      return Object.assign({}, state, {
        isSingleSiteDeleteSuccess: true,
        isSingleSiteDeleteError: false
      });
    case types.DELETE_SINGLE_SITE_FAILURE:
      return Object.assign({}, state, {
        isSingleSiteDeleteSuccess: false,
        isSingleSiteDeleteError: true
      });
    case types.DELETE_MULTIPLE_SITES_REQUEST:
      return Object.assign({}, state, {
        isMultipleSitesDeleteSuccess: false,
        isMultipleSitesDeleteError: false
      });
    case types.DELETE_MULTIPLE_SITES_SUCCESS:
      return Object.assign({}, state, {
        isMultipleSitesDeleteSuccess: true,
        isMultipleSitesDeleteError: false
      });
    case types.DELETE_MULTIPLE_SITES_FAILURE:
      return Object.assign({}, state, {
        isMultipleSitesDeleteSuccess: false,
        isMultipleSitesDeleteError: true
      });
    case types.RESET_SITE_TAB: {
      return Object.assign({}, state, {
        selectedSites: [],
        selectedSiteIds: [],
        filteredSites: [],
        isOpenNewSiteForm: false,
        isOpenSiteDetails: false,
        isEditable: false,
        siteData: null,
        isAddSiteSuccess: false,
        isAddSiteError: false,
        isUpdateSiteSuccess: false,
        isUpdateSiteError: false,
        isSingleSiteDeleteSuccess: false,
        isSingleSiteDeleteError: false,
        isMultipleSitesDeleteSuccess: false,
        isMultipleSitesDeleteError: false
      });
    }
    default:
      return {...state};
  }
}

export default sitesReducer;