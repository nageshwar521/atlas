import * as types from '../actions/actionTypes';

const initialState = {
  deployments: null,
  error: null,
  fetching: false
};
const deploymentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_DEPLOYMENTS_DATA_REQUEST:
      state = Object.assign({}, state, {
        fetching: true
      });
      break;
    case types.FETCH_DEPLOYMENTS_DATA_SUCCESS:
      state = Object.assign({}, state, {
        fetching: false,
        deployments: action.payload
      });
      break;
    case types.FETCH_DEPLOYMENTS_DATA_FAILURE:
      state = Object.assign({}, state, {
        fetching: false,
        error: action.error
      });
      break;
    default:
  }
  return state;
}

export default deploymentsReducer;