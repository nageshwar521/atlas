import * as types from '../actions/actionTypes';

const initialState = {
  isOpenModal: false,
  modalTitle: '',
  yesButtonText: 'Yes',
  noButtonText: 'No'
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.OPEN_MODAL:
      return Object.assign({}, state, {
        isOpenModal: true,
        ...action.payload
      });
    case types.CLOSE_MODAL:
      return Object.assign({}, state, {
        isOpenModal: false
      });
    default:
      return Object.assign({}, state, {
        isOpenModal: false
      });
  }
}

export default modalReducer;