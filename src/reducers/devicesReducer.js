import * as types from '../actions/actionTypes';

const initialState = {
  isOpenNewDeviceForm: false,
  isOpenDeviceDetails: false,
  isEditable: false,
  deviceData: null,
  isFetching: false,
  devices: [],
  error: null,
  totalCount: 0,
  countMatchingRequest: 0,
  selectedDevices: [],
  selectedDeviceIds: [],
  filteredDevices: [],
  scheduleBackupData: null,
  lobs: [],
  projects: [],
  vnfs: [],
  siteIds: [],
  isScheduleBackupDataUploadSuccess: false,
  isScheduleBackupDataUploadError: false,
  isScheduleBackupModalOpen: false,
  isBackupNowDataUploadSuccess: false,
  isBackupNowDataUploadError: false,
  isBackupNowModalOpen: false,
  isAddDeviceSuccess: false,
  isAddDeviceError: false,
  isUpdateDeviceSuccess: false,
  isUpdateDeviceError: false,
  isSingleDeviceDeleteSuccess: false,
  isSingleDeviceDeleteError: false,
  isMultipleDevicesDeleteSuccess: false,
  isMultipleDevicesDeleteError: false
};

const devicesReducer = (state = initialState, action) => {
  // console.log("devicesReducer=", action.type);
  // console.log(state);
  switch (action.type) {
    case types.SHOW_NEW_DEVICE_FORM:
      return Object.assign({}, state, {
        isOpenNewDeviceForm: true
      });
    case types.HIDE_NEW_DEVICE_FORM:
      return Object.assign({}, state, {
        isOpenNewDeviceForm: false
      });
    case types.SHOW_DEVICE_DETAILS:
      return Object.assign({}, state, {
        isOpenDeviceDetails: true,
        deviceData: {...action.payload.deviceData},
        isEditable: action.payload.isEditable
      });
    case types.HIDE_DEVICE_DETAILS:
      return Object.assign({}, state, {
        isOpenDeviceDetails: false,
        deviceData: null
      });
    case types.FETCH_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_DEVICES_SUCCESS:
      const devices = action.payload;
      const totalCount = devices.length;
      return Object.assign({}, state, {
        isFetching: false,
        devices: devices.map((device, idx) => {
          device.id = idx;
          return device;
        }),
        totalCount: totalCount, 
        countMatchingRequest: 0
      });
    case types.FETCH_DEVICES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.SELECTED_DEVICES: {
      const { id, isSelect, selectedDevices } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedDeviceIds: [...state.selectedDeviceIds, id],
          selectedDevices: [...state.selectedDevices, ...selectedDevices]
        });
      } else {
        return Object.assign({}, state, {
          selectedDeviceIds: state.selectedDeviceIds.filter(x => x !== id),
          selectedDevices: state.selectedDevices.filter(x => x.id !== id)
        });
      }
    }
    case types.SELECTED_ALL_DEVICES: {
      const { ids, isSelect, selectedDevices } = action.payload;
      if (isSelect) {
        return Object.assign({}, state, {
          selectedDeviceIds: [...ids],
          selectedDevices: [...selectedDevices]
        });
      } else {
        return Object.assign({}, state, {
          selectedDeviceIds: [],
          selectedDevices: []
        });
      }
    }
    case types.FILTER_DEVICES:
      return Object.assign({}, state, {
        devices: state.devices.filter(x => state.selectedDeviceIds.includes(x.id))
      });
    case types.CLEAR_DEVICES:
      return Object.assign({}, state, {
        selectedDeviceIds: [],
        selectedDevices: [],
        filteredDevices: []
      });
    case types.HANDLE_SEARCH:
      return Object.assign({}, state, {
        filteredDevices: [...action.payload]
      });
    case types.UPDATE_SCHEDULE_BACKUP_DATA:
      return Object.assign({}, state, {
        scheduleBackupData: { ...action.payload }
      });
    case types.FETCH_SCHEDULE_BACKUP_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_SCHEDULE_BACKUP_DATA_SUCCESS:
      const scheduleBackupData = action.payload;
      return Object.assign({}, state, {
        isFetching: false,
        isScheduleBackupModalOpen: true,
        scheduleBackupData: { ...scheduleBackupData }
      });
    case types.FETCH_SCHEDULE_BACKUP_DATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        isScheduleBackupModalOpen: false,
        error: action.error
      });
    case types.FETCH_LOBS_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_LOBS_DATA_SUCCESS:
      const lobs = action.payload;
      return Object.assign({}, state, {
        isFetching: false,
        lobs: [...lobs]
      });
    case types.FETCH_LOBS_DATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.FETCH_PROJECTS_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_PROJECTS_DATA_SUCCESS:
      const projects = action.payload;
      return Object.assign({}, state, {
        isFetching: false,
        projects: [...projects]
      });
    case types.FETCH_PROJECTS_DATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.FETCH_VNFS_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_VNFS_DATA_SUCCESS:
      const vnfs = action.payload;
      return Object.assign({}, state, {
        isFetching: false,
        vnfs: [...vnfs]
      });
    case types.FETCH_VNFS_DATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.FETCH_SITES_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.FETCH_SITES_DATA_SUCCESS:
      const siteIds = action.payload;
      return Object.assign({}, state, {
        isFetching: false,
        siteIds: [...siteIds]
      });
    case types.FETCH_SITES_DATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case types.POST_SCHEDULE_BACKUP_DATA_REQUEST:
      return Object.assign({}, state, {
        isScheduleBackupDataUploadSuccess: false,
        isScheduleBackupDataUploadError: false
      });
    case types.POST_SCHEDULE_BACKUP_DATA_SUCCESS:
      return Object.assign({}, state, {
        isScheduleBackupDataUploadSuccess: true,
        isScheduleBackupDataUploadError: false,
        isScheduleBackupModalOpen: false
      });
    case types.POST_SCHEDULE_BACKUP_DATA_FAILURE:
      return Object.assign({}, state, {
        isScheduleBackupDataUploadSuccess: false,
        isScheduleBackupDataUploadError: true,
        error: action.error,
        isScheduleBackupModalOpen: false
      });
    case types.CLOSE_SCHEDULE_BACKUP_MODAL:
      return Object.assign({}, state, {
        isScheduleBackupModalOpen: false
      });
    case types.ADD_DEVICE_REQUEST:
      return Object.assign({}, state, {
        isAddDeviceSuccess: false,
        isAddDeviceError: false
      });
    case types.ADD_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        isAddDeviceSuccess: true,
        isAddDeviceError: false,
        isOpenNewDeviceForm: false,
        selectedDeviceIds: [],
        selectedDevices: [],
        filteredDevices: []
      });
    case types.ADD_DEVICE_FAILURE:
      return Object.assign({}, state, {
        isAddDeviceSuccess: false,
        isAddDeviceError: true,
        error: action.error,
        isOpenNewDeviceForm: false
      });
    case types.UPDATE_DEVICE_DETAILS: 
      return Object.assign({}, state, {
        deviceData: {...action.payload}
      });
    case types.UPDATE_DEVICE_REQUEST:
      return Object.assign({}, state, {
        isUpdateDeviceSuccess: false,
        isUpdateDeviceError: false
      });
    case types.UPDATE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        isUpdateDeviceSuccess: true,
        isUpdateDeviceError: false,
        isOpenDeviceDetails: false,
        deviceData: null,
        selectedDeviceIds: [],
        selectedDevices: [],
        filteredDevices: []
      });
    case types.UPDATE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        isUpdateDeviceSuccess: false,
        isUpdateDeviceError: true,
        deviceData: null,
        error: action.error,
        isOpenDeviceDetails: false
      });
    case types.POST_BACKUP_NOW_DATA_REQUEST:
      return Object.assign({}, state, {
        isBackupNowDataUploadSuccess: false,
        isBackupNowDataUploadError: false
      });
    case types.POST_BACKUP_NOW_DATA_SUCCESS:
      return Object.assign({}, state, {
        isBackupNowDataUploadSuccess: true,
        isBackupNowDataUploadError: false,
        isBackupNowModalOpen: false
      });
    case types.POST_BACKUP_NOW_DATA_FAILURE:
      return Object.assign({}, state, {
        isBackupNowDataUploadSuccess: false,
        isBackupNowDataUploadError: true,
        error: action.error,
        isBackupNowModalOpen: false
      });
    case types.DELETE_SINGLE_DEVICE_REQUEST:
      return Object.assign({}, state, {
        isSingleDeviceDeleteSuccess: false,
        isSingleDeviceDeleteError: false
      });
    case types.DELETE_SINGLE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        isSingleDeviceDeleteSuccess: true,
        isSingleDeviceDeleteError: false
      });
    case types.DELETE_SINGLE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        isSingleDeviceDeleteSuccess: false,
        isSingleDeviceDeleteError: true
      });
    case types.DELETE_MULTIPLE_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isMultipleDevicesDeleteSuccess: false,
        isMultipleDevicesDeleteError: false
      });
    case types.DELETE_MULTIPLE_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        isMultipleDevicesDeleteSuccess: true,
        isMultipleDevicesDeleteError: false
      });
    case types.DELETE_MULTIPLE_DEVICES_FAILURE:
      return Object.assign({}, state, {
        isMultipleDevicesDeleteSuccess: false,
        isMultipleDevicesDeleteError: true
      });
    case types.OPEN_BACKUP_NOW_MODAL:
      return Object.assign({}, state, {
        isBackupNowModalOpen: true
      });
    case types.CLOSE_BACKUP_NOW_MODAL:
      return Object.assign({}, state, {
        isBackupNowModalOpen: false
      });
    case types.RESET_DEVICE_TAB: {
      return Object.assign({}, state, {
        selectedDevices: [],
        selectedDeviceIds: [],
        filteredDevices: [],
        isOpenNewDeviceForm: false,
        isOpenDeviceDetails: false,
        isEditable: false,
        deviceData: null,
        isScheduleBackupDataUploadSuccess: false,
        isScheduleBackupDataUploadError: false,
        isScheduleBackupModalOpen: false,
        isBackupNowDataUploadSuccess: false,
        isBackupNowDataUploadError: false,
        isBackupNowModalOpen: false,
        isAddDeviceSuccess: false,
        isAddDeviceError: false,
        isUpdateDeviceSuccess: false,
        isUpdateDeviceError: false,
        isSingleDeviceDeleteSuccess: false,
        isSingleDeviceDeleteError: false,
        isMultipleDevicesDeleteSuccess: false,
        isMultipleDevicesDeleteError: false
      });
    }
    default:
      return {...state};
  }
}

export default devicesReducer;