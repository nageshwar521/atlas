import React, { Component, Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Header from './containers/Header';
import Sidebar from './containers/Sidebar';
import Breadcrumb from './components/Breadcrumb';
import Dashboard from './containers/Dashboard';
import ConfigManagement from './containers/ConfigManagement';
import AuditActivities from './containers/AuditActivities';
import AuditEvents from './containers/AuditEvents';

const sidebarRoutes = [
    {
        path: '/',
        exact: true,
        label: 'Dashboard',
        component: () => <Dashboard />
    },
    {
        path: '/snapshot-management',
        exact: true,
        label: 'Snapshot Management',
        component: () => <ConfigManagement />,
    }
];

const activityRoutes = [
    {
        path: "/audit-activity",
        label: "Activities",
        exact: true,
        component: () => <AuditActivities />
    }
]

const explicitRoutes = [
    {
        path: "/audit-activity/:id",
        label: "Activity Detail",
        component: () => <AuditEvents />
    }
]

export default class Routes extends Component {
    constructor(props) {
        super(props);
        this.previousLocation = this.props.location;
        this.allRoutes = [...sidebarRoutes, ...activityRoutes];
    }

    componentWillUpdate(nextProps) {
        const { location } = this.props;
        if (
            nextProps.history.action !== "POP" &&
            (!location.state || !location.state.modal)
        ) {
            this.previousLocation = this.props.location;
        }
    }

    render() {
        const { location } = this.props;
        const isModal = !!(
            location.state &&
            location.state.modal &&
            this.previousLocation !== location
        );
        return (
            <Fragment>
                <Header />
                <Sidebar sidebarRoutes={sidebarRoutes}></Sidebar>
                <div className="content-wrapper clearfix">
                    <Breadcrumb location={location} routes={[...this.allRoutes, ...explicitRoutes]}></Breadcrumb>
                    <Switch location={isModal ? this.previousLocation : location}>
                        {this.allRoutes.map((route, index) => (
                            <Route
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                component={route.component}
                            />
                        ))}
                        <Redirect
                            to={{
                                pathname: "/",
                            }} />
                    </Switch>
                    {explicitRoutes.map((route, index) => {
                        return isModal ? <Route
                            key={index}
                            path={route.path}
                            component={route.component}
                        /> : null
                    })}
                </div>
            </Fragment>
        )
    }
}