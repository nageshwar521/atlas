<div class="panel border-primary no-border border-3-top lobipanel" data-panel-control="" data-inner-id="oTcSVWTuUm" data-index="0">
  <div class="panel-heading">
    <div class="panel-title" style="max-width: calc(100% - 180px);">
      <h5>Production Change <small>over years</small></h5>
    </div>
    <div class="dropdown"><ul class="dropdown-menu dropdown-menu-right">
      <li>
        <a data-func="editTitle"
          data-tooltip="Edit title"
          data-toggle="tooltip"
          data-title="Edit title"
          data-placement="bottom"
          data-original-title=""
          title="">
          <i class="panel-control-icon glyphicon glyphicon-pencil"></i>
          <span class="control-title">Edit title</span>
        </a></li>
      <li>
        <a data-func="unpin"
          data-tooltip="Unpin"
          data-toggle="tooltip"
          data-title="Unpin"
          data-placement="bottom"
          data-original-title=""
          title="">
          <i class="panel-control-icon glyphicon glyphicon-move"></i>
          <span class="control-title">Unpin</span>
        </a></li>
      <li>
        <a data-func="reload"
          data-tooltip="Reload"
          data-toggle="tooltip"
          data-title="Reload"
          data-placement="bottom"
          data-original-title=""
          title="">
          <i class="panel-control-icon glyphicon glyphicon-refresh"></i>
          <span class="control-title">Reload</span>
        </a></li>
      <li><a data-func="minimize"
        data-tooltip="Minimize"
        data-toggle="tooltip"
        data-title="Minimize"
        data-placement="bottom"
        data-original-title=""
        title="">
        <i class="panel-control-icon glyphicon glyphicon-minus"></i>
        <span class="control-title">Minimize</span>
      </a></li>
      <li>
        <a data-func="expand"
          data-tooltip="Fullscreen"
          data-toggle="tooltip"
          data-title="Fullscreen"
          data-placement="bottom"
          data-original-title=""
          title="">
          <i class="panel-control-icon glyphicon glyphicon-resize-full"></i>
          <span class="control-title">Fullscreen</span>
        </a></li>
      <li>
        <a data-func="close"
          data-tooltip="Close"
          data-toggle="tooltip"
          data-title="Close"
          data-placement="bottom"
          data-original-title=""
          title="">
          <i class="panel-control-icon glyphicon glyphicon-remove"></i>
          <span class="control-title">Close</span>
        </a></li></ul>
      <div class="dropdown-toggle" data-toggle="dropdown">
        <span class="panel-control-icon glyphicon glyphicon-cog"></span>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="src-code" style="display: none;">
      <pre class=" language-html"><code class=" language-html">
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>production-chart<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>op-chart<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
        <span class="token comment" spellcheck="true">&lt;!-- ========== JS ========== --&gt;</span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>script</span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>js/production-chart.js<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token script language-javascript"></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>script</span><span class="token punctuation">&gt;</span></span>
      </code></pre>
    </div>
  </div >
</div >