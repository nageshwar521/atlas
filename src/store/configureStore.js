import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from '../reducers';

const loggerMiddleware = createLogger();

const configureStore = () => {
  return createStore(
    reducers,
    {},
    applyMiddleware(thunkMiddleware)
  );
};

export default configureStore;