import React, { Component } from 'react'
import MapPanel from '../../components/Widgets/MapWidget/MapPanel';
import ProjectTable from '../../components/Widgets/ProjectTable/ProjectTable';
import StatusBar from '../../components/Widgets/StatusBars';
import Calendar from '../../components/Widgets/Calendar/Calendar';
import LoadingModal from '../../components/LoadingModal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import { getData } from '../../components/dashboardUtils';
import deploymentActions from '../../actions/deploymentActions';
import MessageAlert from '../../components/MessageAlert/MessageAlert';

class Dashboard extends Component {
    constructor() {
        super();
        this.state = {
            data: null
        };
    }
    componentDidMount() {
        // getData(data => {
        //     this.setState({
        //         data
        //     });
        // });
        this.props.fetchDeployments();
    }
    render() {
        const { deployments, error } = this.props;
        return (
            <div className="col-xs-12">
                <div className="col-xs-12 no-padding">
                    <section className="tab-pane active fade in content no-padding" id="dashboard">
                        {
                            // !deployments ? 
                            //     <div className="col-xs-12 no-padding">
                            //         {/* <LoadingModal /> */}
                            //         <MessageAlert>{error && error.message}</MessageAlert>
                            //     </div> : 
                            !deployments ? 
                                <div className="col-xs-12 no-padding">
                                    {/* <LoadingModal /> */}
                                    {error ? <MessageAlert>Something went wrong! Please refresh the page and try again.</MessageAlert> : <LoadingModal />}
                                </div> : 
                                <div className="col-xs-12 no-padding">
                                    <div>
                                        <StatusBar data={deployments} />
                                    </div>
                                    <div className="col-md-7 col-sm-7 col-xs-12 no-padding-left">
                                        <MapPanel data={deployments} title="Deployments Map" mapModalTitle="Cofigurations" />
                                    </div>
                                    <div className="col-md-5 col-sm-5 col-xs-12 no-padding">
                                        <Calendar data={deployments} />
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <ProjectTable data={deployments} />
                                    </div>
                                </div>
                        }
                    </section>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.deploymentsReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators(deploymentActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);