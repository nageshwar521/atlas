import React, { Component, Fragment } from 'react'
import Logo from '../../components/Logo';
import Navbar from '../../components/Navbar';

export default class Header extends Component {
  render() {
    return (
      <Fragment>
        <header className="main-header" id="HEADER_1">
          { /* Logo */ }
          {/* <Logo></Logo> */}
          { /* Header Navbar: style can be found in header.less */ }
          <Navbar></Navbar>
        </header>
      </Fragment>
    )
  }
}