
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
// import UserPanel from '../../components/UserPanel';
// import SidebarSearch from '../../components/SidebarSearch';
import SideMenuItems from '../../components/SideMenuItems';

class Sidebar extends Component {
  render() {
    return (<Fragment>
      <aside className="main-sidebar" id="ASIDE_1">
        <section className="sidebar">
          {/* <UserPanel></UserPanel>
          <SidebarSearch></SidebarSearch> */}
          <div>
            <ul className="sidebar-menu tree" data-widget="tree">
              {this.props.sidebarRoutes.map((route, index)=> <SideMenuItems key={index} {...route}></SideMenuItems>)}
            </ul>
          </div>
          <div id="DIV_310" />
        </section>
      </aside>
      </Fragment>);
  }
}

export default Sidebar;
  