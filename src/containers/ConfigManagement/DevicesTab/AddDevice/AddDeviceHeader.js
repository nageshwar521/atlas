import React from 'react';

const AddDeviceHeader = (props) => {
  return (
    <div className="configurations--form--header">
      <button
        className="configurations--form--header--button"
        onClick={props.onBackClick}
      >
        <i className="fa fa-arrow-back" />
      </button>
      <div className="configurations--form--header--title">
        {props.title}
      </div>
    </div>
  );
}

export default AddDeviceHeader;