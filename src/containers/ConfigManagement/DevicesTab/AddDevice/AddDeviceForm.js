import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AddDeviceHeader from './AddDeviceHeader';
import FormControl from '../../../../components/ReactInput/FormControl';
import devicesActions from '../../../../actions/devicesActions';
import validator from 'validator';

class AddDeviceForm extends React.Component {
  constructor() {
    super();
    this.state = {
      passwordType: {
        label: '',
        value: ''
      },
      formErrors: [],
      showPassword: false,
      formData: {
        ip: "",
        hostName: "",
        user: "",
        password: "",
        ssh_key_data: "",
        siteId: ""
      },
      formFields: {
        siteId: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        hostName: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        ip: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        user: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        passwordType: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        password: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        ssh_key_data: {
          className: "col-xs-4 no-padding",
          hasError: false
        }
      }
    };
    this.handlePasswordTypeChange = this.handlePasswordTypeChange.bind(this);
    this.handlePasswordTypeBlur = this.handlePasswordTypeBlur.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.getFormControlClasses = this.getFormControlClasses.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getLobsOptions = this.getLobsOptions.bind(this);
    this.getProjectsOptions = this.getProjectsOptions.bind(this);
    this.getVnfsOptions = this.getVnfsOptions.bind(this);
    this.getSitesOptions = this.getSitesOptions.bind(this);
    this.handlePasswordShowHide = this.handlePasswordShowHide.bind(this);
  }
  componentDidMount() {
    this.props.fetchSitesData();
  }
  getPasswordTypeOptions() {
    return [
      { label: 'Password', value: 'Password' },
      { label: 'SSH Key (Optional)', value: 'SSH Key (Optional)' },
    ];
  }
  handlePasswordTypeChange(selectedOption) {
    const { formFields } = this.state;
    if (selectedOption.value) {
      formFields['passwordType'].hasError = false;
    } else {
      formFields['passwordType'].hasError = true;
    }
    this.setState({
      passwordType: selectedOption,
      formFields
    });
  }
  handlePasswordTypeBlur() {
    const { formFields, passwordType } = this.state;
    if (passwordType.value) {
      formFields['passwordType'].hasError = false;
    } else {
      formFields['passwordType'].hasError = true;
    }
    this.setState({
      formFields
    });
  }
  handleCancel() {
    // console.log('handleCancel');
    this.props.onCancel();
  }
  handleSubmit() {
    // console.log('handleSubmit');
    let formErrors = this.validateFields();
    // console.log(formErrors);
    if (!formErrors.includes("false")) {
      this.props.addDevice(this.state.formData);
      this.props.onRefresh();
      setTimeout(() => {
        this.props.addDeviceRequest();
      }, 100);
    }
  }
  handleChange(field, event) {
    // console.log("handleChange");
    // console.log(field);
    // console.log(event);
    const { formData } = this.state;
    formData[field] = event.target.value;
    this.setState({
      formData
    });
  }
  handleSelectChange(field, selectedOption) {
    const { formData } = this.state;
    formData[field] = selectedOption.value;
    /* if (field === "lob") {
      const params = `?lob=${selectedOption.value}`;
      this.props.fetchProjectsData(params);
    } else if (field === "project") {
      const params = `?lob=${formData["lob"]}&project=${selectedOption.value}`;
      this.props.fetchVnfsData(params);
    } else if (field === "vnf") {
      const params = `?lob=${formData["lob"]}&project=${formData["project"]}&vnf=${selectedOption.value}`;
      this.props.fetchSitesData(params);
    } */
    this.setState({
      formData
    });
  }
  getFormControlClasses(classes, field) {
    const { formData } = this.state;
    return cx(classes, field ? formData[field] ? "has-error" : "" : "");
  }
  validateFields(field) {
    let { formData, formFields, passwordType } = this.state;
    let hasErrors = [];
    if (field) {
      if (!formData[field]) {
        formFields[field].hasError = true;
        formFields[field].errorText = `${field} should not be empty`;
      } else if (field === "ip") {
        formFields[field].hasError = !validator.isIP(formData[field]);
        formFields[field].errorText = !validator.isIP(formData[field]) ? `${field} is not valid ipaddress` : '';
      } else {
        formFields[field].hasError = false;
        formFields[field].errorText = '';
      }
    } else {
      for (let field in formFields) {
        if ((field !== "passwordType") && (field !== "password") && (field !== "ssh_key_data") && !formData[field]) {
          formFields[field].hasError = true;
          formFields[field].errorText = `${field} should not be empty`;
        } else if ((field === "password") || (field === "ssh_key_data")) {
          if ((field === "password") && (passwordType.value === "Password") && !formData["password"]) {
            formFields["password"].hasError = true;
            formFields["password"].errorText = `${field} should not be empty`;
            formFields["ssh_key_data"].hasError = false;
            formFields["ssh_key_data"].errorText = "";
          } else if ((field === "ssh_key_data") && (passwordType.value === "SSH Key (Optional)") && !formData["ssh_key_data"]) {
            formFields["ssh_key_data"].hasError = true;
            formFields["ssh_key_data"].errorText = `${field} should not be empty`;
            formFields["password"].hasError = false;
            formFields["password"].errorText = "";
          }
        } else if (field === "ip") {
          formFields[field].hasError = !validator.isIP(formData[field]);
          formFields[field].errorText = !validator.isIP(formData[field]) ? `${field} is not valid ipaddress` : '';
        } else if (field === "passwordType") {
          formFields[field].hasError = !passwordType.value;
          formFields[field].errorText = !passwordType.value ? `${field} is not selected` : '';
        } else {
          formFields[field].hasError = false;
          formFields[field].errorText = '';
        }
      }
    }

    for (let field in formFields) {
      hasErrors.push((!formFields[field].hasError).toString());
    }

    this.setState({
      formFields,
      formErrors: hasErrors
    });

    return hasErrors;
  }
  getLobsOptions() {
    const { lobs } = this.props;
    return lobs.map(lob => ({ label: lob, value: lob }));
  }
  getProjectsOptions() {
    const { projects } = this.props;
    return projects.map(project => ({ label: project, value: project }));
  }
  getVnfsOptions() {
    const { vnfs } = this.props;
    return vnfs.map(vnf => ({ label: vnf, value: vnf }));
  }
  getSitesOptions() {
    const { siteIds } = this.props;
    return siteIds.map(site => ({ label: site, value: site }));
  }
  handlePasswordShowHide() {
    this.setState({
      showPassword: !this.state.showPassword
    })
  }
  render() {
    const { passwordType, formFields, formData, showPassword } = this.state;
    const sitesOptions = this.getSitesOptions();
    const passwordInputType = showPassword ? "text" : "password";
    return (
      <div className="configurations">
        <div className="configurations--row">
          <h4 className="configurations--form--title no-margin">
            Add New Device
          </h4>
        </div>
        <div className="configurations--row">
          <h4 className="configurations--form--title no-margin">
          </h4>
          <div className="configurations--form clearfix">
            <FormControl
              label="Host Name"
              type="text"
              isRequired
              formControlClass={formFields["hostName"].className}
              hasError={formFields["hostName"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "hostName"),
                onBlur: this.validateFields.bind(this, "hostName"),
                value: formData["hostName"]
              }}
            />
            <FormControl
              label="IP Address"
              type="text"
              isRequired
              formControlClass={formFields["ip"].className}
              hasError={formFields["ip"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "ip"),
                onBlur: this.validateFields.bind(this, "ip"),
                value: formData["ip"]
              }}
            />
            <FormControl
              label="Sites"
              type="select"
              isRequired
              formControlClass={formFields["siteId"].className}
              hasError={formFields["siteId"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "siteId"),
                onBlur: this.validateFields.bind(this, "siteId"),
                placeholder: 'Select',
                options: sitesOptions,
                selectedOption: formData["siteId"]
              }}
            />
            <FormControl
              label="User Name"
              type="text"
              isRequired
              formControlClass={formFields["user"].className}
              hasError={formFields["user"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "user"),
                onBlur: this.validateFields.bind(this, "user"),
                value: formData["user"]
              }}
            />
            <FormControl
              label="Password Type"
              type="select"
              isRequired
              formControlClass={formFields["passwordType"].className}
              hasError={formFields["passwordType"].hasError}
              selectProps={{
                options: this.getPasswordTypeOptions(),
                selectedOption: passwordType,
                onChange: this.handlePasswordTypeChange,
                onBlur: this.handlePasswordTypeBlur
              }}
            />
            {
              passwordType.value === "Password" ?
                <FormControl
                  label="Password"
                  showPassword={showPassword}
                  inputType="password"
                  type={passwordInputType}
                  onShowHide={this.handlePasswordShowHide}
                  formControlClass={formFields["password"].className}
                  hasError={formFields["password"].hasError}
                  inputProps={{
                    onChange: this.handleChange.bind(this, "password"),
                    onBlur: this.validateFields.bind(this, "password"),
                    value: formData["password"]
                  }}
                /> :
                passwordType.value === "SSH Key (Optional)" ? 
                <FormControl
                  label="SSH Key (Optional)"
                  type="textarea"
                  formControlClass={formFields["ssh_key_data"].className}
                  hasError={formFields["ssh_key_data"].hasError}
                  inputProps={{
                    onChange: this.handleChange.bind(this, "ssh_key_data"),
                    onBlur: this.validateFields.bind(this, "ssh_key_data"),
                    value: formData["ssh_key_data"]
                  }}
                /> : null
            }
          </div>
        </div>
        <div className="configurations--row clearfix">
          <div className="configurations--buttons clearfix pull-right">
            <button
              className="btn white-button medium configurations--form--button"
              onClick={this.handleCancel}>Cancel</button>
            <button
              className="btn black-button medium configurations--form--button"
              onClick={this.handleSubmit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ devicesReducer }) => {
  return { ...devicesReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...devicesActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(AddDeviceForm);
export default ConnectedComponent;