import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactDataTable from '../../../components/ReactDataTable/ReactDataTable';
import snapshotsActions from '../../../actions/snapshotsActions';
import Tooltip from 'rc-tooltip';
import moment from 'moment';
import { filterSelectedRows } from '../configUtils';
import WidgetControl from '../../../components/Widget/WidgetControl';
import AddDeviceForm from './AddDevice/AddDeviceForm';
import tabsActions from '../../../actions/tabsActions';
import devicesActions from '../../../actions/devicesActions';
import modalActions from '../../../actions/modalActions';
import ReactModal from '../../../components/ReactModal/ReactModal';
import ScheduleBackupForm from './ScheduleBackupForm';
import DeviceDetails from './DeviceDetails/DeviceDetails';


class DeviceTable extends Component {
  constructor() {
    super();
    this.state = {
      columns: [{
        dataField: 'hostName',
        text: 'Host Name',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      }, {
        dataField: 'ip',
        text: 'IP Address',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'ipaddress-table-header-cell',
        classes: 'ipaddress-table-cell'
      },
      {
        dataField: 'siteId',
        text: 'Site ID',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'checksum-table-header-cell',
        classes: 'checksum-table-cell'
      }, 
       {
        dataField: 'time',
        text: 'Last Updated',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'timestamp-table-header-cell',
        classes: 'timestamp-table-cell'
      }, 
      
      {
        dataField: 'frequency',
        text: 'Frequency',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      }],
      sizePerPage: 10,
      currentPage: 1,
      addNewDevice: false,
      searchText: "",
      selectedAll: false,
      selectedFew: false,
      startDate: null,
      endDate: null,
      time: null,
      frequency: null,
    };
    this.toolTipEnabled = this.toolTipEnabled.bind(this);
    this.handleOnSelect = this.handleOnSelect.bind(this);
    this.handleOnSelectAll = this.handleOnSelectAll.bind(this);
    this.handleClearSelect = this.handleClearSelect.bind(this);
    this.handleShowSelected = this.handleShowSelected.bind(this);
    this.handleActionChange = this.handleActionChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleAddDevice = this.handleAddDevice.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleDeleteModalAccept = this.handleDeleteModalAccept.bind(this);
    this.handleDeleteModalReject = this.handleDeleteModalReject.bind(this);
    this.handleBackupNowModalAccept = this.handleBackupNowModalAccept.bind(this);
    this.handleBackupNowModalReject = this.handleBackupNowModalReject.bind(this);
    this.handleScheduleBackupModalSubmit = this.handleScheduleBackupModalSubmit.bind(this);
    this.handleScheduleBackupModalCancel = this.handleScheduleBackupModalCancel.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleFrequencyChange = this.handleFrequencyChange.bind(this);
    this.handleViewDeviceClose = this.handleViewDeviceClose.bind(this);
  }
  componentDidMount() {
    this.props.fetchDevices(this.state.currentPage, this.state.sizePerPage);
    // console.log("componentDidMount");
    // console.log(this.props);
    // const { isAddDeviceSuccess, isAddDeviceError } = this.props;
    // if (isAddDeviceSuccess) {
    //   this.renderNotification('success');
    // } else if (isAddDeviceError) {
    //   this.renderNotification('error');
    // }
  }
  componentDidUpdate() {
    // console.log("componentDidUpdate");
    // console.log(this.props);
    // const { isAddDeviceSuccess, isAddDeviceError } = this.props;
    // if (isAddDeviceSuccess) {
    //   this.renderNotification('success');
    // } else if (isAddDeviceError) {
    //   this.renderNotification('error');
    // }
  }
  toolTipEnabled(cell) {
    return (
      <Tooltip placement="left" overlay={cell}><span>{cell}</span></Tooltip>
    );
  }
  handleClearSelect() {
    this.setState({
      sizePerPage: 10,
      searchText: '',
      selectedAll: false,
      selectedFew: false
    }, () => {
      this.props.clearSelectedDevices();
      this.props.fetchDevices();
    });
  }
  handleShowSelected() {
    const { selectedDeviceIds = [] } = this.props;
    if (selectedDeviceIds.length > 0) {
      const filteredDevices = filterSelectedRows(this.props.devices, selectedDeviceIds);
      this.props.filterDevices(filteredDevices);
      this.setState({ selectedFew: true, selectedAll: false });
    }
    if (this.state.selectedFew && selectedDeviceIds.length == 1) {
      this.handleClearSelect();
    }
  }
  handleOnSelect(row, isSelect) {
    this.props.handleSelectedDevices(row.id, isSelect, [row]);
    if (this.state.selectedFew == true) {
      this.handleShowSelected();
    }
  }
  handleOnSelectAll(isSelect, rows) {
    const ids = rows.map(r => r.id);
    this.props.handleSelectedAllDevices(ids, isSelect, rows);
    this.setState({ selectedAll: !this.state.selectedAll });
    if ((this.state.selectedAll == false) && (this.state.selectedFew == true)) {
      this.handleClearSelect();
    }
  }
  handleActionChange(selectedAction) {
    const { selectedDevices } = this.props;
    this.setState({
      selectedAction: selectedAction.value
    }, () => {
      if (selectedAction.value === "Delete") {
        this.props.openModal({
          modalTitle: "Delete"
        });
      } else if (selectedAction.value === "Schedule Backup") {
        this.props.fetchScheduleBackupData(selectedDevices[0].deviceId);
      } else if (selectedAction.value === "Backup Now") {
        this.props.openBackupNowModal();
      } else if (selectedAction.value === "Properties") {
        this.props.showDeviceDetails({ deviceData: selectedDevices[0], isEditable: false });
      } else if (selectedAction.value === "Edit") {
        this.props.showDeviceDetails({ deviceData: selectedDevices[0], isEditable: true });
      }
    });
  }
  handleViewDeviceClose() {
    this.props.clearSelectedDevices();
    this.props.hideDeviceDetails();
  }
  handlePageChange(page) {
    this.setState({
      currentPage: page
    }, () => {
      this.props.clearSelectedDevices();
      this.props.fetchDevices(page, this.state.sizePerPage);
    });
  }
  getOptions() {
    const options = [
      { value: 'Backup Now', label: 'Backup Now', iconClass: 'fa fa-hdd-o', isOptionDisabled: true },
      { value: 'Schedule Backup', label: 'Schedule Backup', iconClass: 'fa fa-clock-o', isOptionDisabled: true },
      { value: 'Clone', label: 'Clone', iconClass: 'fa fa-copy', isOptionDisabled: true },
      { value: 'Edit', label: 'Edit', iconClass: 'fa fa-edit', isOptionDisabled: true },
      { value: 'Delete', label: 'Delete', iconClass: 'fa fa-trash', isOptionDisabled: true },
      { value: 'Properties', label: 'Properties', iconClass: 'fa fa-eye', isOptionDisabled: true }
    ];
    const { selectedDevices } = this.props;
    return options.map((option) => {
      if (selectedDevices.length === 1) {
        option.isOptionDisabled = false;
      } else if (selectedDevices.length > 1) {
        if ((option.value === "Delete")) {
          option.isOptionDisabled = false;
        } else {
          option.isOptionDisabled = true;
        }
      } else {
        option.isOptionDisabled = true;
      }
      return option;
    });
  }
  getTableControls() {
    const { selectedDevices } = this.props;
    const isDisabled = selectedDevices.length > 0 ? false : true;
    return (
      <React.Fragment>
        <button
          className="btn black-button pull-right"
          onClick={this.handleAddDevice}>Add Device</button>
        <button
          className="btn black-button pull-right"
          onClick={this.handleShowSelected}
          disabled={isDisabled}>Show Selected Only</button>
        <WidgetControl
          btnClassName="sortby-reset table-reset-button"
          icon="fa-repeat"
          title="Reset"
          onClick={this.handleClearSelect} />
      </React.Fragment>
    );
  }
  handleAddDevice() {
    this.props.showNewDeviceForm();
  }
  setAddDeviceState(){
    this.setState({
      addNewDevice: false
    })
  }
  handleCancel() {
    this.props.setActiveTab("Devices");
    this.props.hideNewDeviceForm();
  }
  handleSearchChange(searchText) {
    this.setState({
      searchText
    }, () => {
      this.props.handleSearch(searchText);
    });
  }
  handleBackupNowModalAccept() {
    const { selectedDevices } = this.props;
    if (selectedDevices.length === 1) {
      this.props.postBackupNowData(selectedDevices[0].deviceId);
      this.props.closeBackupNowModal();
      this.props.clearSelectedDevices();
      setTimeout(() => {
        this.props.postBackupNowDataRequest();
      }, 100);
    }
  }
  handleBackupNowModalReject() {
    this.props.closeBackupNowModal();
    this.props.clearSelectedDevices();
  }
  renderBackupNowModal() {
    const {
      isBackupNowModalOpen
    } = this.props;
    return (
      <ReactModal
        title="Backup Now"
        open={isBackupNowModalOpen}
        onClose={this.handleBackupNowModalReject}
        onYesClick={this.handleBackupNowModalAccept}
        onNoClick={this.handleBackupNowModalReject}
      >
        Do you you want to backup now?
      </ReactModal>
    );
  }
  handleDeleteModalAccept() {
    const { selectedDevices } = this.props;
    if (selectedDevices.length === 1) {
      this.props.deleteSingleDevice(selectedDevices);
      this.props.closeModal();
      this.props.clearSelectedDevices();
      setTimeout(() => {
        this.props.deleteSingleDeviceRequest();
      }, 100);
    } else {
      this.props.deleteMultipleDevices(selectedDevices);
      this.props.closeModal();
      this.props.clearSelectedDevices();
      setTimeout(() => {
        this.props.deleteMultipleDevicesRequest();
      }, 100);
    }
  }
  handleDeleteModalReject() {
    this.props.closeModal();
    this.props.clearSelectedDevices();
  }
  renderDeleteModal() {
    const {
      isOpenModal
    } = this.props;
    return (
      <ReactModal
        title="Delete Device"
        open={isOpenModal}
        onClose={this.handleDeleteModalReject}
        onYesClick={this.handleDeleteModalAccept}
        onNoClick={this.handleDeleteModalReject}
      >
        Are you sure you want to delete?
      </ReactModal>
    );
  }
  getMomentFromTimeString(str) {
    var t = moment(str, 'HH:mm');
    // Now t is a moment.js object of today's date at the time given in str
  
    if (t.get('hour') < 22) // If it's before 9 pm
      t.add('d', 1); // Add 1 day, so that t is tomorrow's date at the same time
  
    return t;
  }
  getFormattedScheduleBackupData(data) {
    const startDate = moment(data.startDate).format("YYYY-MM-DD");
    const endDate = moment(data.endDate).format("YYYY-MM-DD");
    const time = this.getMomentFromTimeString(data.time).format("HH:mm");
    const isSameDay = moment(startDate).isSame(endDate);
    const frequency = isSameDay ? "" : data.frequency;
    return {
      ...data,
      startDate,
      endDate,
      time,
      frequency
    };
  }
  handleScheduleBackupModalSubmit() {
    const { selectedDevices, scheduleBackupData } = this.props;
    const data = Object.assign({}, scheduleBackupData, {
      deviceId: selectedDevices[0].deviceId
    })
    const newData = this.getFormattedScheduleBackupData(data);
    if (selectedDevices.length === 1) {
      this.props.postScheduleBackupData(newData);
      this.props.closeScheculeBackupModal();
      this.props.clearSelectedDevices();
      setTimeout(() => {
        this.props.postScheduleBackupDataRequest();
      }, 100);
    }
  }
  handleScheduleBackupModalCancel() {
    this.props.closeScheculeBackupModal();
    this.props.clearSelectedDevices();
  }
  renderScheduleBackupModal() {
    const {
      scheduleBackupData,
      isScheduleBackupModalOpen
    } = this.props;
    return (
      <ReactModal
        title="Schedule Backup"
        open={isScheduleBackupModalOpen}
        onClose={this.handleScheduleBackupModalCancel}
        onYesClick={this.handleScheduleBackupModalSubmit}
        onNoClick={this.handleScheduleBackupModalCancel}
        yesButtonText="Schedule"
        noButtonText="Cancel"
      >
        <ScheduleBackupForm 
          {...scheduleBackupData}
          onStartDateChange={this.handleStartDateChange}
          onEndDateChange={this.handleEndDateChange}
          onTimeChange={this.handleTimeChange}
          onFrequencyChange={this.handleFrequencyChange}
        />
      </ReactModal>
    );
  }
  handleStartDateChange(startDate) {
    // console.log('handleStartDateChange');
    const {
      scheduleBackupData
    } = this.props;
    const newData = {...scheduleBackupData, startDate};
    this.props.updateScheduleBackupData(newData);
  }
  handleEndDateChange(endDate) {
    const {
      scheduleBackupData
    } = this.props;
    const newData = {...scheduleBackupData, endDate};
    this.props.updateScheduleBackupData(newData);
  }
  handleTimeChange(time) {
    const {
      scheduleBackupData
    } = this.props;
    const newData = {...scheduleBackupData, time};
    this.props.updateScheduleBackupData(newData);
  }
  handleFrequencyChange(frequency) {
    const {
      scheduleBackupData
    } = this.props;
    const newData = {...scheduleBackupData, frequency: frequency.value};
    this.props.updateScheduleBackupData(newData);
  }
  render() {
    console.log('rendering!!', this.props);
    const { 
      columns, 
      currentPage, 
      sizePerPage, 
      selectedAction, 
      searchText
    } = this.state;
    const { 
      devices, 
      isOpenNewDeviceForm, 
      isOpenDeviceDetails,
      deviceData,
      isEditable,
      totalCount, 
      selectedDevices, 
      filteredDevices, 
      countMatchingRequest
    } = this.props;
    return (
      <div>
        {
          isOpenNewDeviceForm ? 
              <AddDeviceForm 
                onRefresh={this.handleClearSelect}
                onCancel={this.handleCancel} /> :
          isOpenDeviceDetails ? 
              <DeviceDetails
                onRefresh={this.handleClearSelect}
                isLocked={!isEditable}
                onCancel={this.handleViewDeviceClose} deviceData={deviceData} /> :
          <div className="config-management--config-table">
            <ReactDataTable
              search={true}
              columns={columns}
              options={this.getOptions()}
              currentPage={currentPage}
              sizePerPage={sizePerPage}
              header={this.getTableControls()}
              data={searchText ? filteredDevices : devices}
              totalRows={searchText ? countMatchingRequest : totalCount}
              rowSelectable
              selectedRows={selectedDevices}
              titleText="node(s) selected"
              selectedAction={selectedAction}
              onActionChange={this.handleActionChange}
              onSelectRow={this.handleOnSelect}
              onSelectAllRows={this.handleOnSelectAll}
              onPageChange={this.handlePageChange}
              onSearch={this.handleSearchChange}
            />
            {this.renderDeleteModal()}
            {this.renderScheduleBackupModal()}
            {this.renderBackupNowModal()}
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({ snapshotsReducer, devicesReducer, modalReducer }) => {
  return { ...snapshotsReducer, ...devicesReducer, ...modalReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...snapshotsActions, ...tabsActions, ...devicesActions, ...modalActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(DeviceTable);
export default ConnectedComponent;