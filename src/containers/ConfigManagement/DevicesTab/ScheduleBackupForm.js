import React from 'react';
import moment from 'moment';
import FormControl from '../../../components/ReactInput/FormControl';

const frequencyOptions = () => {
  return [
    { label: 'Daily', value: 'Daily' },
    { label: 'Weekly', value: 'Weekly' },
    { label: 'Monthly', value: 'Monthly'},
    { label: 'Yearly', value: 'Yearly'}
  ];
};

function getMomentFromTimeString(str) {
  var t = moment(str, 'HH:mm');
  // Now t is a moment.js object of today's date at the time given in str

  if (t.get('hour') < 22) // If it's before 9 pm
    t.add('d', 1); // Add 1 day, so that t is tomorrow's date at the same time

  return t;
}

const ScheduleBackupForm = (props) => {
  console.log("ScheduleBackupForm props");
  console.log(props);
  const time = getMomentFromTimeString(props.time).format("HH:mm");
  const isSameDay = moment(props.startDate).isSame(props.endDate);
  return (
    <div className="schedule-backup-form">
      <div className="col-xs-3 schedule-backup-form--date-wrapper full-width no-padding">
        <FormControl
          type="date"
          label="Start Date"
          isRequired
          dateProps={{
            minDate: new Date(),
            value: moment(props.startDate).format("YYYY-MM-DD"),
            onSelect: props.onStartDateChange
          }}
        />
      </div>
      <div className="col-xs-3 schedule-backup-form--date-wrapper full-width no-padding">
        <FormControl
          type="date"
          label="End Date"
          isRequired
          dateProps={{
            minDate: new Date(),
            value: moment(props.endDate).format("YYYY-MM-DD"),
            onSelect: props.onEndDateChange
          }}
        />
      </div>
      <div className="col-xs-3 schedule-backup-form--time-wrapper full-width no-padding">
        <FormControl
          type="time"
          label="Time (CST)"
          placeholder="Select Time"
          isRequired
          timeProps={{
            value: time,
            onChange: props.onTimeChange,
            showTimeSelect: true,
            showTimeSelectOnly: true,
            timeIntervals: 5,
            dateFormat: "HH:mm",
            timeCaption: "Time"
          }}
        />
      </div>
      <div className="col-xs-3 schedule-backup-form--frequency-wrapper full-width no-padding">
        <FormControl
          type="select"
          label="Frequency"
          selectProps={{
            placeholder: 'Frequency',
            onChange: props.onFrequencyChange,
            options: frequencyOptions(),
            selectedOption: props.frequency,
            isDisabled: isSameDay
          }}
        />
      </div>
    </div>
  );
}

export default ScheduleBackupForm;