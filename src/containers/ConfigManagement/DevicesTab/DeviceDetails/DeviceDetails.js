import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import devicesActions from '../../../../actions/devicesActions';
import FormControl from '../../../../components/ReactInput/FormControl';
import { cx } from 'emotion';
import validator from 'validator';

class DeviceDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      passwordType: {
        label: '',
        value: ''
      },
      formErrors: [],
      showPassword: false,
      formFields: {
        siteId: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        hostName: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        ip: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        user: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        passwordType: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        password: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        ssh_key_data: {
          className: "col-xs-12 no-padding",
          hasError: false
        }
      }
    };
    this.handlePasswordTypeChange = this.handlePasswordTypeChange.bind(this);
    this.handlePasswordTypeBlur = this.handlePasswordTypeBlur.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.getFormControlClasses = this.getFormControlClasses.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getSitesOptions = this.getSitesOptions.bind(this);
    this.handlePasswordShowHide = this.handlePasswordShowHide.bind(this);
  }
  componentDidMount() {
    this.props.fetchSitesData();
  }
  getPasswordTypeOptions() {
    return [
      { label: 'Password', value: 'Password' },
      { label: 'SSH Key (Optional)', value: 'SSH Key' },
    ];
  }
  handlePasswordTypeChange(selectedOption) {
    const { formFields } = this.state;
    const { deviceData } = this.props;
    if (selectedOption.value) {
      formFields['passwordType'].hasError = false;
    } else {
      formFields['passwordType'].hasError = true;
    }
    if (selectedOption.value === "Password") {
      deviceData["ssh_key_data"] = "";
    } else if (selectedOption.value === "SSH Key") {
      deviceData["password"] = "";
    }
    this.props.updateDeviceDetails(deviceData);
    this.setState({
      passwordType: selectedOption,
      formFields
    });
  }
  handlePasswordTypeBlur() {
    const { formFields, passwordType } = this.state;
    if (passwordType.value) {
      formFields['passwordType'].hasError = false;
    } else {
      formFields['passwordType'].hasError = true;
    }
    this.setState({
      formFields
    });
  }
  handleCancel() {
    console.log('handleCancel');
    this.props.onCancel();
  }
  handleSubmit() {
    console.log('handleSubmit');
    let formErrors = this.validateFields();
    console.log(formErrors);
    if (!formErrors.includes("false")) {
      this.props.updateDevice(this.props.deviceData);
      this.props.onRefresh();
      setTimeout(() => {
        this.props.updateDeviceRequest();
      }, 100);
    }
  }
  handleChange(field, event) {
    const { deviceData } = this.props;
    deviceData[field] = event.target.value;
    this.props.updateDeviceDetails(deviceData);
  }
  handleSelectChange(field, selectedOption) {
    const { deviceData } = this.props;
    deviceData[field] = selectedOption.value;
    this.props.updateDeviceDetails(deviceData);
  }
  getFormControlClasses(classes, field) {
    const { deviceData } = this.props;
    return cx(classes, field ? deviceData[field] ? "has-error" : "" : "");
  }
  validateFields(field) {
    let { formFields } = this.state;
    const { deviceData } = this.props;
    const passwordType =  this.state.passwordType.value ? 
                            this.state.passwordType :
                            deviceData.password ? 
                              { label: 'Password', value: 'Password' } : 
                              { label: 'SSH Key (Optional)', value: 'SSH Key' };
    let hasErrors = [];
    if (field) {
      if (!deviceData[field]) {
        formFields[field].hasError = true;
        formFields[field].errorText = `${field} should not be empty`;
      } else if (field === "ip") {
        formFields[field].hasError = !validator.isIP(deviceData[field]);
        formFields[field].errorText = !validator.isIP(deviceData[field]) ? `${field} is not valid ipaddress` : '';
      } else {
        formFields[field].hasError = false;
        formFields[field].errorText = '';
      }
    } else {
      for (let field in formFields) {
        if ((field !== "passwordType") && (field !== "password") && (field !== "ssh_key_data") && !deviceData[field]) {
          formFields[field].hasError = true;
          formFields[field].errorText = `${field} should not be empty`;
        } else if ((field === "password") || (field === "ssh_key_data")) {
          if ((field === "password") && (passwordType.value === "Password") && !deviceData["password"]) {
            formFields["password"].hasError = true;
            formFields["password"].errorText = `${field} should not be empty`;
            formFields["ssh_key_data"].hasError = false;
            formFields["ssh_key_data"].errorText = "";
          } else if ((field === "ssh_key_data") && (passwordType.value === "SSH Key") && !deviceData["ssh_key_data"]) {
            formFields["ssh_key_data"].hasError = true;
            formFields["ssh_key_data"].errorText = `${field} should not be empty`;
            formFields["password"].hasError = false;
            formFields["password"].errorText = "";
          }
        } else if (field === "ip") {
          formFields[field].hasError = !validator.isIP(deviceData[field]);
          formFields[field].errorText = !validator.isIP(deviceData[field]) ? `${field} is not valid ipaddress` : '';
        } else if (field === "passwordType") {
          formFields[field].hasError = !passwordType.value;
          formFields[field].errorText = !passwordType.value ? `${field} is not selected` : '';
        } else {
          formFields[field].hasError = false;
          formFields[field].errorText = '';
        }
      }
    }

    for (let field in formFields) {
      hasErrors.push((!formFields[field].hasError).toString());
    }

    this.setState({
      formFields,
      formErrors: hasErrors
    });

    return hasErrors;
  }
  getSitesOptions() {
    const { siteIds } = this.props;
    return siteIds.map(site => ({ label: site, value: site }));
  }
  handlePasswordShowHide() {
    this.setState({
      showPassword: !this.state.showPassword
    })
  }
  render() {
    console.log(this.props);
    const { formFields, showPassword } = this.state;
    const { deviceData, isLocked } = this.props;
    const sitesOptions = this.getSitesOptions();
    const passwordInputType = showPassword ? "text" : "password";
    const passwordType =  this.state.passwordType.value ? 
                            this.state.passwordType :
                            deviceData.password ? 
                              { label: 'Password', value: 'Password' } : 
                              { label: 'SSH Key (Optional)', value: 'SSH Key' };
    console.log(passwordType);
    return (
      <div className="configurations">
        <div className="configurations--row">
          <h4 className="configurations--form--title no-margin">
            Device Information
          </h4>
          <div className="configurations--form clearfix">
            <FormControl
              label="Host Name"
              type="text"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["hostName"].className}
              hasError={formFields["hostName"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "hostName"),
                onBlur: this.validateFields.bind(this, "hostName"),
                value: deviceData["hostName"],
                rows: 2
              }}
            />
            <FormControl
              label="IP Address"
              type="text"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["ip"].className}
              hasError={formFields["ip"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "ip"),
                onBlur: this.validateFields.bind(this, "ip"),
                value: deviceData["ip"]
              }}
            />
            <FormControl
              label="Sites"
              type="select"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["siteId"].className}
              hasError={formFields["siteId"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "siteId"),
                onBlur: this.validateFields.bind(this, "siteId"),
                placeholder: 'Select',
                options: sitesOptions,
                selectedOption: deviceData["siteId"]
              }}
            />
          </div>
          <div className="configurations--form clearfix">
            <FormControl
              label="User Name"
              type="text"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["user"].className}
              hasError={formFields["user"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "user"),
                onBlur: this.validateFields.bind(this, "user"),
                value: deviceData["user"]
              }}
            />
            {!isLocked ? <FormControl
              label="Password Type"
              type="select"
              isRequired
              formControlClass={formFields["passwordType"].className}
              hasError={formFields["passwordType"].hasError}
              selectProps={{
                options: this.getPasswordTypeOptions(),
                selectedOption: passwordType,
                onChange: this.handlePasswordTypeChange,
                onBlur: this.handlePasswordTypeBlur
              }}
            /> : null}
            {
              passwordType.value === "Password" ?
                <FormControl
                  label="Password"
                  showPassword={showPassword}
                  inputType="password"
                  type={passwordInputType}
                  isLocked={isLocked}
                  onShowHide={this.handlePasswordShowHide}
                  formControlClass={formFields["password"].className}
                  hasError={formFields["password"].hasError}
                  inputProps={{
                    onChange: this.handleChange.bind(this, "password"),
                    onBlur: this.validateFields.bind(this, "password"),
                    value: deviceData["password"]
                  }}
                /> : null
            }
            {
              passwordType.value === "SSH Key" ?
                  <FormControl
                    label="SSH Key (Optional)"
                    type="textarea"
                    isLocked={isLocked}
                    formControlClass={formFields["ssh_key_data"].className}
                    hasError={formFields["ssh_key_data"].hasError}
                    inputProps={{
                      onChange: this.handleChange.bind(this, "ssh_key_data"),
                      onBlur: this.validateFields.bind(this, "ssh_key_data"),
                      value: deviceData["ssh_key_data"]
                    }}
                  /> : null
            }
          </div>
        </div>
        <div className="configurations--row clearfix">
          <div className="configurations--buttons clearfix pull-right">
            <button
              className={cx("btn medium configurations--form--button", isLocked ? "black-button" : "white-button")}
              onClick={this.handleCancel}>{isLocked ? "Close" : "Cancel"}</button>
            {
              !isLocked ? 
                <button
                  className="btn black-button medium configurations--form--button"
                  onClick={this.handleSubmit}>Update</button> : null
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ devicesReducer }) => {
  return { ...devicesReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...devicesActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(DeviceDetails);
export default ConnectedComponent;