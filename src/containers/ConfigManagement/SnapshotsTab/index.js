import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SortBy from './SortBy';
import snapshotsActions from '../../../actions/snapshotsActions';
import ReactTree from './ReactTree/ReactTree';
import { generateTreeData, filterSelectedRows } from '../configUtils';
import ReactDataTable from '../../../components/ReactDataTable/ReactDataTable';
import Tooltip from 'rc-tooltip';
import moment from 'moment';
import ReactModal from '../../../components/ReactModal/ReactModal';
import modalActions from '../../../actions/modalActions';
import WidgetControl from '../../../components/Widget/WidgetControl';

class SnapshotsTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
      treeData: [],
      columns: [ {
        dataField: 'snapshotName',
        text: 'Snapshot Name',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      },{
        dataField: 'checksum',
        text: 'Checksum',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'checksum-table-header-cell',
        classes: 'checksum-table-cell'
      },  {
        dataField: 'size',
        text: 'Size (KB)',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'size-table-header-cell',
        classes: 'size-table-cell',
        headerStyle: {
          width: '80px'
        }
      }, {
        dataField: 'lastUpdatedTime',
        text: 'Last Updated',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(moment(cell).format('LLL')),
        headerClasses: 'timestamp-table-header-cell',
        classes: 'timestamp-table-cell'
      },{
        dataField: 'siteName',
        text: 'Site Name',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      },{
        dataField: 'hostName',
        text: 'Host Name',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      }, {
        dataField: 'ipAddress',
        text: 'IP Address',
        sort: true,
        formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'ipaddress-table-header-cell',
        classes: 'ipaddress-table-cell'
      }],
      sizePerPage: 10,
      currentPage: 1,
      selectedSnapshots: [],
      searchText: ''
    };
    this.handleSortData = this.handleSortData.bind(this);
    this.handleTreeNodeClick = this.handleTreeNodeClick.bind(this);
    this.handlePathToParams = this.handlePathToParams.bind(this);
    this.fillMissingParams = this.fillMissingParams.bind(this);
    this.toolTipEnabled = this.toolTipEnabled.bind(this);
    this.handleOnSelect = this.handleOnSelect.bind(this);
    this.handleOnSelectAll = this.handleOnSelectAll.bind(this);
    this.handleClearSelect = this.handleClearSelect.bind(this);
    this.handleShowSelected = this.handleShowSelected.bind(this);
    this.handleActionChange = this.handleActionChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
    this.handleYesClick = this.handleYesClick.bind(this);
    this.handleNoClick = this.handleNoClick.bind(this);
    this.handleDownwloadClick = this.handleDownwloadClick.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }
  componentDidMount() {
    this.props.fetchSnapshots(this.state.currentPage, this.state.sizePerPage);
  }
  fillMissingParams(arr) {
    let missingParamsLength = 4 - arr.length;
    return arr.concat(Array.from({ length: missingParamsLength }).map(() => ''));
  }
  handlePathToParams(path) {
    const pathArr = this.fillMissingParams(path.split(","));
    return pathArr.reduce((params, item, idx) => {
      if (idx === 0) return params += `&lob=${item}`;
      if (idx === 1) return params += `&projectName=${item}`;
      if (idx === 2) return params += `&vnf=${item}`;
      if (idx === 3) return params += `&hostName=${item}`;
    }, "");
  }
  handleTreeNodeClick(path) {
    const selectedNode = path.split(",").reverse()[0];
    this.setState({
      selectedNode
    }, () => {
      const params = this.handlePathToParams(path);
      this.props.fetchSnapshots(undefined, undefined, params);
    });
  }
  handleSortData(data, { selectedOrg, selectedProject, selectedVnf }) {
    const tree = generateTreeData(data, this.handleTreeNodeClick);
    let treeData = tree;
    if (selectedVnf) {
      let nodeObj = {};
      nodeObj["label"] = selectedVnf;
      nodeObj["open"] = true;
      nodeObj["children"] = tree[0];
      treeData = [nodeObj];
    }

    if (selectedProject) {
      let nodeObj = {};
      nodeObj["label"] = selectedProject;
      nodeObj["open"] = true;
      nodeObj["children"] = treeData;
      treeData = [nodeObj];
    }

    if (selectedOrg) {
      let nodeObj = {};
      nodeObj["label"] = selectedOrg;
      nodeObj["open"] = true;
      nodeObj["children"] = treeData;
      treeData = [nodeObj];
    }
    this.setState({
      treeData
    });
  }
  toolTipEnabled(cell) {
    return (
      <Tooltip placement="left" overlay={cell}><span>{cell}</span></Tooltip>
    );
  }
  handleClearSelect() {
    this.setState({
      sizePerPage: 10,
      searchText: ''
    }, () => {
      this.props.clearSelectedSnapshots();
      this.props.fetchSnapshots(this.state.currentPage, this.state.sizePerPage);
    });
  }
  handleShowSelected() {
    const { selectedSnapshotIds = [] } = this.props;
    if (selectedSnapshotIds.length > 0) {
      const filteredRows = filterSelectedRows(this.props.snapshots, selectedSnapshotIds);
      this.props.filterSnapshots(filteredRows);
      // this.setState({ selectedAll: false });
    }
    // if (selectedSnapshotIds.length == 1) {
    //   this.handleClearSelect();
    // }
  }
  handleOnSelect(row, isSelect) {
    this.props.handleSelectedSnapshots(row.id, isSelect, [row]);
    // if (this.state.selectedFew == true) {
    //   this.handleShowSelected();
    // }
  }
  handleOnSelectAll(isSelect, rows) {
    const ids = rows.map(r => r.id);
    this.props.handleSelectedAllSnapshots(ids, isSelect, rows);
    // this.setState({ selectedAll: !this.state.selectedAll });
    // if ((this.state.selectedAll == false) && (this.state.selectedFew == true)) {
    //   this.handleClearSelect();
    // }
  }
  handleActionChange(selectedAction) {
    const { selectedSnapshots } = this.props;
    // console.log(selectedAction);
    this.setState({
      selectedAction: selectedAction.value,
      selectedSnapshots
    }, () => {
      // console.log("actions changed!");
      if (selectedAction.value === "Delete") {
        this.props.openModal();
      } else if (selectedAction.value === "Download") {
         this.handleDownwloadClick();
      }
    });
  }
  handlePageChange(page) {
    this.setState({
      currentPage: page
    }, () => {
      this.props.clearSelectedSnapshots();
      this.props.fetchSnapshots(page, this.state.sizePerPage);
    });
  }
  getTableControls() {
    const { selectedSnapshots } = this.props;
    const isDisabled = selectedSnapshots.length > 0 ? false : true;
    return (
      <React.Fragment>
        <button
          className="btn black-button pull-right"
          onClick={this.handleShowSelected}
          disabled={isDisabled}>Show Selected Only</button>
        <WidgetControl
          btnClassName="sortby-reset table-reset-button"
          icon="fa-repeat"
          title="Reset"
          onClick={this.handleClearSelect} />
      </React.Fragment>
    );
  }
  handleModalClose() {
    this.props.closeModal();
    this.props.clearSelectedSnapshots();
  }
  handleYesClick() {
    const { selectedSnapshots } = this.props;
    if (selectedSnapshots.length === 1) {
      this.props.deleteSingleSnapshot(selectedSnapshots);
      this.props.closeModal();
      this.props.clearSelectedSnapshots();
      setTimeout(() => {
        this.props.deleteSingleSnapshotRequest();
      }, 100);
    } else {
      this.props.deleteMultipleSnapshots(selectedSnapshots);
      this.props.closeModal();
      this.props.clearSelectedSnapshots();
      setTimeout(() => {
        this.props.deleteMultipleSnapshotsRequest();
      }, 100);
    }
  }
  handleNoClick() {
    this.props.closeModal();
    this.props.clearSelectedSnapshots();
  }
  handleDownwloadClick(){
    const { selectedSnapshots } = this.props;
    if (selectedSnapshots.length === 1) {
      this.props.downloadSingleSnapshot(selectedSnapshots);
      this.props.clearSelectedSnapshots();
      setTimeout(() => {
        this.props.downloadSingleSnapshotRequest();
      }, 100);
    } else {
      this.props.downloadMultipleSnapshots(selectedSnapshots);
      this.props.clearSelectedSnapshots();
      setTimeout(() => {
        this.props.downloadMultipleSnapshotsRequest();
      }, 100);
    }
  }
  getOptions() {
    const options = [
      { value: 'Download', label: 'Download', iconClass: 'fa fa-download', isOptionDisabled: true },
      { value: 'Delete', label: 'Delete', iconClass: 'fa fa-trash', isOptionDisabled: true },
      { value: 'Compare', label: 'Compare', iconClass: 'fa fa-exchange', isOptionDisabled: true },
      { value: 'Properties', label: 'Properties', iconClass: 'fa fa-eye', isOptionDisabled: true }
    ];
    const { selectedSnapshots } = this.props;
    return options.map((option) => {
      if (selectedSnapshots.length >= 1) {
        if ((option.value === "Download") || (option.value === "Delete")) {
          option.isOptionDisabled = false;
        } else {
          option.isOptionDisabled = true;
        }
      } else {
        option.isOptionDisabled = true;
      }
      return option;
    });
  }
  handleSearchChange(searchText) {
    // console.log("handleSearchChange");
    // console.log(searchText);
    this.setState({
      searchText
    }, () => {
      this.props.handleSearch(searchText);
    });
  }
  render() {
    // console.log("SnapshotsTab this.props");
    // console.log(this.props);
    const { columns, currentPage, sizePerPage, selectedAction, searchText } = this.state;
    const { snapshots, totalSnapshots, selectedSnapshots, countMatchingRequest, isOpenModal, filteredSnapshots } = this.props;
    return (
      <div className="config-management--snapshots">
        <div className="col-xs-3 config-management--snapshots--sidebar">
          <SortBy onReset={this.handleReset} onDataLoad={this.handleSortData} />
          <div className="treeview-wrapper">
            <ReactTree data={this.state.treeData} />
          </div>
        </div>
        <div className="col-xs-9 config-management--snapshots--table">
          <ReactDataTable
            search={true}
            options={this.getOptions()}
            columns={columns}
            currentPage={currentPage}
            sizePerPage={sizePerPage}
            header={this.getTableControls()}
            data={searchText ? filteredSnapshots : snapshots}
            totalRows={searchText ? countMatchingRequest : totalSnapshots}
            rowSelectable
            selectedRows={selectedSnapshots}
            titleText="node(s) selected"
            selectedAction={selectedAction}
            onActionChange={this.handleActionChange}
            onSelectRow={this.handleOnSelect}
            onSelectAllRows={this.handleOnSelectAll}
            onPageChange={this.handlePageChange}
            onSearch={this.handleSearchChange}
          />
          <ReactModal
            title="Delete Snapshots"
            open={isOpenModal}
            onClose={this.handleModalClose}
            onYesClick={this.handleYesClick}
            onNoClick={this.handleNoClick}
          >
            Are you sure you want to delete?
          </ReactModal>
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({ snapshotsReducer, modalReducer }) => {
  // console.log("snapshotsReducer");
  // console.log(snapshotsReducer);
  return {...snapshotsReducer, ...modalReducer};
}
const mapDispatchToProps = dispatch => {
  return { ...bindActionCreators({ ...snapshotsActions, ...modalActions}, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(SnapshotsTab);
export default ConnectedComponent;