import React, { Component } from 'react';
import {
  Table
} from "react-bootstrap";;
import { getMapData, getLatLngs } from '../../../components/Widgets/MapWidget/mapUtils';
import ReactSelect from '../../../components/ReactSelect/ReactSelect';
import WidgetControl from '../../../components/Widget/WidgetControl';

export default class SortBy extends Component {
  constructor() {
    super();
    this.state = {
      organizations: null,
      projects: null,
      vnfs: null,
      sites: null,
      selectedOrg: null,
      selectedProject: null,
      selectedVnf: null,
      mapData: []
    };
    this.handleOrg = this.handleOrg.bind(this);
    this.handleProject = this.handleProject.bind(this);
    this.handleVnf = this.handleVnf.bind(this);
    this.loadMapData = this.loadMapData.bind(this);
    this.getOptions = this.getOptions.bind(this);
    this.checkAllOptionsSelected = this.checkAllOptionsSelected.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }
  componentDidMount() {
    this.loadMapData();
  }
  handleReset() {
    this.setState({
      organizations: null,
      projects: null,
      vnfs: null,
      sites: null,
      selectedOrg: null,
      selectedProject: null,
      selectedVnf: null
    }, () => {
      this.loadMapData();
      if (this.props.onReset) {
        this.props.onReset();
      }
    });
  }
  loadMapData() {
    getMapData((response) => {
      const { organizations } = response;
      this.setState({
        organizations
      }, () => {
        if (this.props.onDataLoad) {
          this.props.onDataLoad(organizations, this.state);
        }
      });
    });
  }
  handleOrg(selectedOrg) {
    const { organizations } = this.state;
    let projects = organizations[selectedOrg.value];
    this.setState({
      selectedOrg: selectedOrg.value,
      projects,
      selectedProject: null,
      selectedVnf: null,
      vnfs: null,
      sites: null
    }, () => {
      let obj = {
        selectedOrg: selectedOrg.value,
        selectedProject: null,
        selectedVnf: null
      };
      if (this.props.onDataLoad) {
        this.props.onDataLoad(projects, obj);
      }
    });
  }
  handleProject(selectedProject) {
    const { projects } = this.state;
    let vnfs = projects[selectedProject.value];
    this.setState({
      selectedProject: selectedProject.value,
      selectedVnf: null,
      vnfs
    }, () => {
      let obj = {
        selectedOrg: this.state.selectedOrg,
        selectedProject: selectedProject.value,
        selectedVnf: null,
        sites: null
      };
      if (this.props.onDataLoad) {
        this.props.onDataLoad(vnfs, obj);
      }
    });
  }
  handleVnf(selectedVnf) {
    const { vnfs } = this.state;
    let sites = vnfs[selectedVnf.value];
    this.setState({
      selectedVnf: selectedVnf.value,
      sites
    }, () => {
      let obj = {
        selectedOrg: this.state.selectedOrg,
        selectedProject: this.state.selectedProject,
        selectedVnf: selectedVnf.value
      };
      if (this.props.onDataLoad) {
        this.props.onDataLoad(sites, obj);
      }
    });
  }
  getOptions(data) {
    if (!data) return [];
    const options = Object.keys(data);
    return options.map((option, idx) => {
      return {
        value: option,
        label: option
      };
    });
  }
  checkAllOptionsSelected() {
    const { selectedOrg, selectedProject, selectedVnf } = this.state;
    const selectedOptions = ["" + Boolean(selectedOrg), "" + Boolean(selectedProject), "" + Boolean(selectedVnf)];
    return !selectedOptions.includes("false");
  }
  getFilteredProjects(projectOptions) {
    const excludedProjects = ["totalVnfs"];
    return projectOptions.filter(project => {
      return (project.value !== "totalVnfs");
    });
  }
  render() {
    const { organizations, projects, vnfs } = this.state;
    const orgOptions = this.getOptions(organizations);
    const projectOptions = this.getOptions(projects);
    const vnfOptions = this.getOptions(vnfs);
    const customStyles = {
      option: (provided) => ({
        ...provided,
        fontWeight: 'normal'
      }),
    };

    return (
      <div className="config-management--groupby">
        <h5 className="config-management--groupby-title">
          Sort By
          <span className="pull-right">
            <WidgetControl
              btnClassName="sortby-reset"
              icon="fa-repeat"
              title="Reset"
              onClick={this.handleReset} />
          </span>
        </h5>
        <Table className="borderless">
          <tbody>
            <tr>
              <td>
                <ReactSelect
                  customStyles={customStyles}
                  className= "placeholder-style"
                  placeholder="Business Line"
                  selectedOption={this.state.selectedOrg}
                  options={orgOptions}
                  handleChange={this.handleOrg} />
              </td>
            </tr>
            <tr>
              <td>
                <ReactSelect
                  customStyles={customStyles}
                  placeholder="Project Name"
                  className= "placeholder-style"
                  selectedOption={this.state.selectedProject}
                  options={this.getFilteredProjects(projectOptions)}
                  handleChange={this.handleProject} />
              </td>
            </tr>
            <tr>
              <td>
                <ReactSelect
                  customStyles={customStyles}
                  className= "placeholder-style"
                  placeholder="VNF Name"
                  selectedOption={this.state.selectedVnf}
                  options={vnfOptions}
                  handleChange={this.handleVnf} />
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}