import React from 'react';
import Tree from './Tree';
import { generateTree } from '../../configUtils';

const typeStyles = {
  fontSize: '1em',
  verticalAlign: 'middle'
};

class ReactTree extends React.PureComponent {
  constructor() {
    super();
    this.renderTree = this.renderTree.bind(this);
  }
  renderTree() {
    const { data } = this.props;
    const tree = generateTree(Tree, data);
    return tree;
  }
  render() {
    return (
      <div className="treeview-main">
        {this.renderTree()}
      </div>
    )
  }
}

export default ReactTree;