import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FormControl from '../../../../components/ReactInput/FormControl';
import cx from 'classnames';
import sitesActions from '../../../../actions/sitesActions';
import devicesActions from '../../../../actions/devicesActions';

class AddSitesForm extends React.Component {
  constructor() {
    super();
    this.state = {
      formErrors: [],
      formData: {
        lob: "",
        project: "",
        vnf: "",
        siteId: "",
        siteName: "",
        latitude: "",
        longitude: ""
      },
      formFields: {
        lob: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        project: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        vnf: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        siteId: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        siteName: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        latitude: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        longitude: {
          className: "col-xs-4 no-padding",
          hasError: false
        }
      }
    };
    this.handleBackClick = this.handleBackClick.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getFormControlClasses = this.getFormControlClasses.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getLobsOptions = this.getLobsOptions.bind(this);
    this.getProjectsOptions = this.getProjectsOptions.bind(this);
    this.getVnfsOptions = this.getVnfsOptions.bind(this);
  }
  componentDidMount() {
    this.props.fetchLobsData();
  }
  handleBackClick() {
    console.log('handleBackClick');
  }
  handleCancel() {
    console.log('handleCancel');
    this.props.onCancel();
  }
  handleSubmit() {
    console.log('handleSubmit');
    this.validateFields();
    let formErrors = this.validateFields();
    console.log(formErrors);
    if (!formErrors.includes("false")) {
      this.props.addSite(this.state.formData);
      this.props.onRefresh();
      setTimeout(() => {
        this.props.addSiteRequest();
      }, 100);
    }
  }
  handleChange(field, event) {
    const { formData } = this.state;
    formData[field] = event.target.value;
    this.setState({
      formData
    });
  }
  handleSelectChange(field, selectedOption) {
    const { formData } = this.state;
    formData[field] = selectedOption.value;
    if (field === "lob") {
      const params = `?lob=${selectedOption.value}`;
      this.props.fetchProjectsData(params);
    } else if (field === "project") {
      const params = `?lob=${formData["lob"]}&project=${selectedOption.value}`;
      this.props.fetchVnfsData(params);
    } else if (field === "vnf") {
      const params = `?lob=${formData["lob"]}&project=${formData["project"]}&vnf=${selectedOption.value}`;
      this.props.fetchSitesData(params);
    }
    this.setState({
      formData
    });
  }
  getFormControlClasses(classes, field) {
    const { formData } = this.state;
    return cx(classes, field ? formData[field] ? "has-error" : "" : "");
  }
  validateFields(field) {
    let { formData, formFields, passwordType } = this.state;
    let hasErrors = [];
    if (field) {
      if (!formData[field]) {
        formFields[field].hasError = true;
        formFields[field].errorText = `${field} should not be empty`;
      } else if (field === "latitude") {
        formFields[field].hasError = isNaN(formData[field]);
        formFields[field].errorText = isNaN(formData[field]) ? `${field} is not valid` : '';
      } else if (field === "longitude") {
        formFields[field].hasError = isNaN(formData[field]);
        formFields[field].errorText = isNaN(formData[field]) ? `${field} is not valid` : '';
      } else {
        formFields[field].hasError = false;
        formFields[field].errorText = '';
      }
    } else {
      for (let field in formFields) {
        if (!formData[field]) {
          formFields[field].hasError = true;
          formFields[field].errorText = `${field} should not be empty`;
        } else if (field === "latitude") {
          formFields[field].hasError = isNaN(formData[field]);
          formFields[field].errorText = isNaN(formData[field]) ? `${field} is not valid` : '';
        } else if (field === "longitude") {
          formFields[field].hasError = isNaN(formData[field]);
          formFields[field].errorText = isNaN(formData[field]) ? `${field} is not valid` : '';
        } else {
          formFields[field].hasError = false;
          formFields[field].errorText = '';
        }
      }
    }

    for (let field in formFields) {
      hasErrors.push((!formFields[field].hasError).toString());
    }

    this.setState({
      formFields,
      formErrors: hasErrors
    });

    return hasErrors;
  }
  getLobsOptions() {
    const { lobs } = this.props;
    return lobs.map(lob => ({ label: lob, value: lob }));
  }
  getProjectsOptions() {
    const { projects } = this.props;
    return projects.map(project => ({ label: project, value: project }));
  }
  getVnfsOptions() {
    const { vnfs } = this.props;
    return vnfs.map(vnf => ({ label: vnf, value: vnf }));
  }
  render() {
    const { formFields, formData } = this.state;
    const lobsOptions = this.getLobsOptions();
    const projectsOptions = this.getProjectsOptions();
    const vnfsOptions = this.getVnfsOptions();
    return (
      <div className="configurations">
        <div className="configurations--row">
          <h4 className="configurations--form--title no-margin">
            Add New Site
          </h4>
          <div className="configurations--form clearfix">
            <FormControl
              label="Line of Business"
              type="select"
              isRequired
              formControlClass={formFields["lob"].className}
              hasError={formFields["lob"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "lob"),
                onBlur: this.validateFields.bind(this, "lob"),
                placeholder: 'Select',
                options: lobsOptions,
                selectedOption: formData["lob"]
              }}
            />
            <FormControl
              label="Project Name"
              type="select"
              isRequired
              formControlClass={formFields["project"].className}
              hasError={formFields["project"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "project"),
                onBlur: this.validateFields.bind(this, "project"),
                placeholder: 'Select',
                options: projectsOptions,
                selectedOption: formData["project"]
              }}
            />
            <FormControl
              label="VNF Name"
              type="select"
              isRequired
              formControlClass={formFields["vnf"].className}
              hasError={formFields["vnf"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "vnf"),
                onBlur: this.validateFields.bind(this, "vnf"),
                placeholder: 'Select',
                options: vnfsOptions,
                selectedOption: formData["vnf"]
              }}
            />
          </div>
        </div>
        <div className="configurations--row">
          {/* <h4 className="configurations--form--title no-margin">
            Sites Information
          </h4> */}
          <div className="configurations--form clearfix">
            <FormControl
              label="Site Id"
              type="text"
              isRequired
              formControlClass={formFields["siteId"].className}
              hasError={formFields["siteId"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "siteId"),
                onBlur: this.validateFields.bind(this, "siteId"),
                value: formData["siteId"]
              }}
            />
            <FormControl
              label="Site Name"
              type="text"
              isRequired
              formControlClass={formFields["siteName"].className}
              hasError={formFields["siteName"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "siteName"),
                onBlur: this.validateFields.bind(this, "siteName"),
                value: formData["siteName"]
              }}
            />
          </div>
          <div className="configurations--form">
            <FormControl
              label="Latitude"
              type="text"
              formControlClass={formFields["latitude"].className}
              hasError={formFields["latitude"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "latitude"),
                onBlur: this.validateFields.bind(this, "latitude"),
                value: formData["latitude"]
              }}
            />
            <FormControl
              label="Longitude"
              type="text"
              formControlClass={formFields["longitude"].className}
              hasError={formFields["longitude"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "longitude"),
                onBlur: this.validateFields.bind(this, "longitude"),
                value: formData["longitude"]
              }}
            />
          </div>
        </div>
        <div className="configurations--row clearfix">
          <div className="configurations--buttons clearfix pull-right">
            <button
              className="btn white-button medium configurations--form--button"
              onClick={this.handleCancel}>Cancel</button>
            <button
              className="btn black-button medium configurations--form--button"
              onClick={this.handleSubmit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ devicesReducer, sitesReducer }) => {
  return { ...devicesReducer, ...sitesReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...devicesActions, ...sitesActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(AddSitesForm);
export default ConnectedComponent;