import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactDataTable from '../../../components/ReactDataTable/ReactDataTable';
import snapshotsActions from '../../../actions/snapshotsActions';
import Tooltip from 'rc-tooltip';
import moment from 'moment';
import { filterSelectedRows } from '../configUtils';
import WidgetControl from '../../../components/Widget/WidgetControl';
import AddSitesForm from './AddSites/AddSitesForm';
import tabsActions from '../../../actions/tabsActions';
import sitesActions from '../../../actions/sitesActions';
import ReactModal from '../../../components/ReactModal/ReactModal';
import modalActions from '../../../actions/modalActions';
import SiteDetails from './SiteDetails/SiteDetails';
import Notification from '../../../components/Notification/Notification';


class SitesTable extends Component {
  constructor() {
    super();
    this.state = {
      columns: [{
        dataField: 'siteId',
        text: 'Site ID',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'checksum-table-header-cell',
        classes: 'checksum-table-cell'
      }, {
        dataField: 'siteName',
        text: 'Site Name',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      }, {
        dataField: 'lob',
        text: 'LOB',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'snapshot-table-header-cell',
        classes: 'snapshot-table-cell'
      }, {
        dataField: 'project',
        text: 'Project Name',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'ipaddress-table-header-cell',
        classes: 'ipaddress-table-cell'
      }, {
        dataField: 'vnf',
        text: 'VNF Name',
        sort: true,
        //formatter: (cell) => this.toolTipEnabled(cell),
        headerClasses: 'ipaddress-table-header-cell',
        classes: 'ipaddress-table-cell'
      }],
      sizePerPage: 10,
      currentPage: 1,
      addNewSite: false,
      searchText: '',
      selectedAll: false,
      selectedFew: false
    };
    this.toolTipEnabled = this.toolTipEnabled.bind(this);
    this.handleOnSelect = this.handleOnSelect.bind(this);
    this.handleOnSelectAll = this.handleOnSelectAll.bind(this);
    this.handleClearSelect = this.handleClearSelect.bind(this);
    this.handleShowSelected = this.handleShowSelected.bind(this);
    this.handleActionChange = this.handleActionChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleAddSite = this.handleAddSite.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
    this.handleYesClick = this.handleYesClick.bind(this);
    this.handleNoClick = this.handleNoClick.bind(this);
    this.handleViewSiteClose = this.handleViewSiteClose.bind(this);
  }
  componentDidMount() {
    this.props.fetchSites(this.state.currentPage, this.state.sizePerPage);
  }
  toolTipEnabled(cell) {
    return (
      <Tooltip placement="left" overlay={cell}><span>{cell}</span></Tooltip>
    );
  }
  handleClearSelect() {
    this.setState({
      sizePerPage: 10,
      searchText: '',
      selectedAll: false,
      selectedFew: false
    }, () => {
      this.props.clearSelectedSites();
      this.props.fetchSites(this.state.currentPage, this.state.sizePerPage);
    });
  }
  handleShowSelected() {
    const { selectedSiteIds = [] } = this.props;
    if (selectedSiteIds.length > 0) {
      const filteredSites = filterSelectedRows(this.props.sites, selectedSiteIds);
      this.props.filterSites(filteredSites);
      this.setState({ selectedFew: true, selectedAll: false });
    }
    if (this.state.selectedFew && selectedSiteIds.length == 1) {
      this.handleClearSelect();
    }
  }
  handleOnSelect(row, isSelect) {
    this.props.handleSelectedSites(row.id, isSelect, [row]);
    if (this.state.selectedFew == true) {
      this.handleShowSelected();
    }
  }
  handleOnSelectAll(isSelect, rows) {
    const ids = rows.map(r => r.id);
    this.props.handleSelectedAllSites(ids, isSelect, rows);
    this.setState({ selectedAll: !this.state.selectedAll });
    if ((this.state.selectedAll == false) && (this.state.selectedFew == true)) {
      this.handleClearSelect();
    }
  }
  handleActionChange(selectedAction) {
    const { selectedSites } = this.props;
    // console.log(selectedAction);
    this.setState({
      selectedAction: selectedAction.value
    }, () => {
      if (selectedAction.value === "Delete") {
        this.props.openModal();
      } else if (selectedAction.value === "Properties") {
        this.props.showSiteDetails({ siteData: selectedSites[0], isEditable: false });
      } else if (selectedAction.value === "Edit") {
        this.props.showSiteDetails({ siteData: selectedSites[0], isEditable: true });
      }
    });
  }
  handleViewSiteClose() {
    this.props.clearSelectedSites();
    this.props.hideSiteDetails();
  }
  handlePageChange(page) {
    this.setState({
      currentPage: page
    }, () => {
      this.props.clearSelectedSites();
      this.props.fetchSites(page, this.state.sizePerPage);
    });
  }
  getOptions() {
    const options = [
      { value: 'Edit', label: 'Edit', iconClass: 'fa fa-edit', isOptionDisabled: true },
      { value: 'Delete', label: 'Delete', iconClass: 'fa fa-trash', isOptionDisabled: true },
      { value: 'Properties', label: 'Properties', iconClass: 'fa fa-eye', isOptionDisabled: true }
    ];
    const { selectedSites } = this.props;
    return options.map((option) => {
      if (selectedSites.length === 1) {
        option.isOptionDisabled = false;
      } else if (selectedSites.length > 1) {
        if ((option.value === "Delete")) {
          option.isOptionDisabled = false;
        } else {
          option.isOptionDisabled = true;
        }
      } else {
        option.isOptionDisabled = true;
      }
      return option;
    });
  }
  getTableControls() {
    const { selectedSites } = this.props;
    const isDisabled = selectedSites.length > 0 ? false : true;
    return (
      <React.Fragment>
        <button
          className="btn black-button pull-right"
          onClick={this.handleAddSite}>Add Sites</button>
        <button
          className="btn black-button pull-right"
          onClick={this.handleShowSelected}
          disabled={isDisabled}>Show Selected Only</button>
        <WidgetControl
          btnClassName="sortby-reset table-reset-button"
          icon="fa-repeat"
          title="Reset"
          onClick={this.handleClearSelect} />
      </React.Fragment>
    );
  }
  handleModalClose() {
    this.props.closeModal();
    this.props.clearSelectedSites();
  }
  handleYesClick() {
    const { selectedSites } = this.props;
    if (selectedSites.length === 1) {
      this.props.deleteSingleSite(selectedSites);
      this.props.closeModal();
      this.props.clearSelectedSites();
      setTimeout(() => {
        this.props.deleteSingleSiteRequest();
      }, 100);
    } else {
      this.props.deleteMultipleSites(selectedSites);
      this.props.closeModal();
      this.props.clearSelectedSites();
      setTimeout(() => {
        this.props.deleteMultipleSitesRequest();
      }, 100);
    }
  }
  handleNoClick() {
    this.props.closeModal();
    this.props.clearSelectedSites();
  }
  handleAddSite() {
    this.props.showNewSiteForm();
  }
  // Dont try to handle parent state from children
  // The better way to handle parent props is through redux
  setAddSiteState(){
    this.setState({
      addNewSite: false
    })
  }
  handleCancel() {
    this.props.setActiveTab("Sites");
    this.props.hideNewSiteForm();
  }
  handleSearchChange(searchText) {
    this.setState({
      searchText
    }, () => {
      this.props.handleSearch(searchText);
    });
  }
  render() {
    // console.log('rendering!!');
    const { columns, currentPage, sizePerPage, selectedAction, searchText } = this.state;
    const { sites, isOpenNewSiteForm, totalCount, selectedSites, filteredSites, isOpenModal, countMatchingRequest, isOpenSiteDetails, isEditable, siteData } = this.props;
    // const { snapshots, totalSnapshots, selectedSnapshots, selectedSnapshotIds } = this.props;
    return (
      <div>
        {
          isOpenNewSiteForm ? 
            <AddSitesForm
              onRefresh={this.handleClearSelect}
              onCancel={this.handleCancel}
            /> :
            isOpenSiteDetails ? 
              <SiteDetails
                onRefresh={this.handleClearSelect}
                isLocked={!isEditable}
                onCancel={this.handleViewSiteClose} siteData={siteData} /> :
              <div className="config-management--config-table">
                <ReactDataTable
                  search={true}
                  columns={columns}
                  options={this.getOptions()}
                  currentPage={currentPage}
                  sizePerPage={sizePerPage}
                  header={this.getTableControls()}
                  data={searchText ? filteredSites : sites}
                  totalRows={searchText ? countMatchingRequest : totalCount}
                  rowSelectable
                  selectedRows={selectedSites}
                  titleText="node(s) selected"
                  selectedAction={selectedAction}
                  onActionChange={this.handleActionChange}
                  onSelectRow={this.handleOnSelect}
                  onSelectAllRows={this.handleOnSelectAll}
                  onPageChange={this.handlePageChange}
                  onSearch={this.handleSearchChange}
                />
                <ReactModal
                  title="Delete Sites"
                  open={isOpenModal}
                  onClose={this.handleModalClose}
                  onYesClick={this.handleYesClick}
                  onNoClick={this.handleNoClick}
                >
                  Are you sure you want to delete?
                </ReactModal>
              </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({ snapshotsReducer, sitesReducer, modalReducer }) => {
  return { ...snapshotsReducer, ...sitesReducer, ...modalReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...snapshotsActions, ...tabsActions, ...sitesActions, ...modalActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(SitesTable);
export default ConnectedComponent;