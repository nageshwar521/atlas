import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FormControl from '../../../../components/ReactInput/FormControl';
import cx from 'classnames';
import sitesActions from '../../../../actions/sitesActions';
import devicesActions from '../../../../actions/devicesActions';

class SiteDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      formErrors: [],
      formFields: {
        lob: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        project: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        vnf: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        siteId: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        siteName: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        latitude: {
          className: "col-xs-4 no-padding",
          hasError: false
        },
        longitude: {
          className: "col-xs-4 no-padding",
          hasError: false
        }
      }
    };
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getFormControlClasses = this.getFormControlClasses.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getLobsOptions = this.getLobsOptions.bind(this);
    this.getProjectsOptions = this.getProjectsOptions.bind(this);
    this.getVnfsOptions = this.getVnfsOptions.bind(this);
  }
  componentDidMount() {
    this.props.fetchLobsData();
  }
  handleCancel() {
    // console.log('handleCancel');
    this.props.onCancel();
  }
  handleSubmit() {
    // console.log('handleSubmit');
    this.validateFields();
    let formErrors = this.validateFields();
    // console.log(formErrors);
    if (!formErrors.includes("false")) {
      this.props.updateSite(this.props.siteData);
      this.props.onRefresh();
      setTimeout(() => {
        this.props.updateSiteRequest();
      }, 100);
    }
  }
  handleChange(field, event) {
    const { siteData } = this.props;
    siteData[field] = event.target.value;
    this.props.updateSiteDetails(siteData);
  }
  handleSelectChange(field, selectedOption) {
    const { siteData } = this.props;
    siteData[field] = selectedOption.value;
    if (field === "lob") {
      siteData["project"] = "";
      siteData["vnf"] = "";
      const params = `?lob=${selectedOption.value}`;
      this.props.fetchProjectsData(params);
    } else if (field === "project") {
      siteData["vnf"] = "";
      const params = `?lob=${siteData["lob"]}&project=${selectedOption.value}`;
      this.props.fetchVnfsData(params);
    } else if (field === "vnf") {
      const params = `?lob=${siteData["lob"]}&project=${siteData["project"]}&vnf=${selectedOption.value}`;
      this.props.fetchSitesData(params);
    }
    this.props.updateSiteDetails(siteData);
  }
  getFormControlClasses(classes, field) {
    const { siteData } = this.props;
    return cx(classes, field ? siteData[field] ? "has-error" : "" : "");
  }
  validateFields(field) {
    let { formFields } = this.state;
    const { siteData } = this.props;
    let hasErrors = [];
    if (field) {
      if (!siteData[field]) {
        formFields[field].hasError = true;
        formFields[field].errorText = `${field} should not be empty`;
      } else if (field === "latitude") {
        formFields[field].hasError = isNaN(siteData[field]);
        formFields[field].errorText = isNaN(siteData[field]) ? `${field} is not valid` : '';
      } else if (field === "longitude") {
        formFields[field].hasError = isNaN(siteData[field]);
        formFields[field].errorText = isNaN(siteData[field]) ? `${field} is not valid` : '';
      } else {
        formFields[field].hasError = false;
        formFields[field].errorText = '';
      }
    } else {
      for (let field in formFields) {
        if (!siteData[field]) {
          formFields[field].hasError = true;
          formFields[field].errorText = `${field} should not be empty`;
        } else if (field === "latitude") {
          formFields[field].hasError = isNaN(siteData[field]);
          formFields[field].errorText = isNaN(siteData[field]) ? `${field} is not valid` : '';
        } else if (field === "longitude") {
          formFields[field].hasError = isNaN(siteData[field]);
          formFields[field].errorText = isNaN(siteData[field]) ? `${field} is not valid` : '';
        } else {
          formFields[field].hasError = false;
          formFields[field].errorText = '';
        }
      }
    }

    for (let field in formFields) {
      hasErrors.push((!formFields[field].hasError).toString());
    }

    this.setState({
      formFields,
      formErrors: hasErrors
    });

    return hasErrors;
  }
  getLobsOptions() {
    const { lobs } = this.props;
    return lobs.map(lob => ({ label: lob, value: lob }));
  }
  getProjectsOptions() {
    const { projects } = this.props;
    return projects.map(project => ({ label: project, value: project }));
  }
  getVnfsOptions() {
    const { vnfs } = this.props;
    return vnfs.map(vnf => ({ label: vnf, value: vnf }));
  }
  render() {
    const { formFields } = this.state;
    const { siteData, isLocked } = this.props;
    const lobsOptions = this.getLobsOptions();
    const projectsOptions = this.getProjectsOptions();
    const vnfsOptions = this.getVnfsOptions();
    return (
      <div className="configurations">
        <div className="configurations--row">
          <h4 className="configurations--form--title no-margin">
            Site Information
          </h4>
          <div className="configurations--form clearfix">
            <FormControl
              label="Line of Business"
              type="select"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["lob"].className}
              hasError={formFields["lob"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "lob"),
                onBlur: this.validateFields.bind(this, "lob"),
                placeholder: 'Select',
                options: lobsOptions,
                selectedOption: siteData["lob"]
              }}
            />
            <FormControl
              label="Project Name"
              type="select"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["project"].className}
              hasError={formFields["project"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "project"),
                onBlur: this.validateFields.bind(this, "project"),
                placeholder: 'Select',
                options: projectsOptions,
                selectedOption: siteData["project"]
              }}
            />
            <FormControl
              label="VNF Name"
              type="select"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["vnf"].className}
              hasError={formFields["vnf"].hasError}
              selectProps={{
                onChange: this.handleSelectChange.bind(this, "vnf"),
                onBlur: this.validateFields.bind(this, "vnf"),
                placeholder: 'Select',
                options: vnfsOptions,
                selectedOption: siteData["vnf"]
              }}
            />
          </div>
        </div>
        <div className="configurations--row clearfix">
          {/* <h4 className="configurations--form--title no-margin">
            Sites Information
          </h4> */}
          <div className="configurations--form clearfix">
            <FormControl
              label="Site Id"
              type="text"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["siteId"].className}
              hasError={formFields["siteId"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "siteId"),
                onBlur: this.validateFields.bind(this, "siteId"),
                value: siteData["siteId"]
              }}
            />
            <FormControl
              label="Site Name"
              type="text"
              isRequired
              isLocked={isLocked}
              formControlClass={formFields["siteName"].className}
              hasError={formFields["siteName"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "siteName"),
                onBlur: this.validateFields.bind(this, "siteName"),
                value: siteData["siteName"]
              }}
            />
          </div>
          <div className="configurations--form">
            <FormControl
              label="Latitude"
              type="text"
              isLocked={isLocked}
              formControlClass={formFields["latitude"].className}
              hasError={formFields["latitude"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "latitude"),
                onBlur: this.validateFields.bind(this, "latitude"),
                value: siteData["latitude"]
              }}
            />
            <FormControl
              label="Longitude"
              type="text"
              isLocked={isLocked}
              formControlClass={formFields["longitude"].className}
              hasError={formFields["longitude"].hasError}
              inputProps={{
                onChange: this.handleChange.bind(this, "longitude"),
                onBlur: this.validateFields.bind(this, "longitude"),
                value: siteData["longitude"]
              }}
            />
          </div>
        </div>
        <div className="configurations--row clearfix">
          <div className="configurations--buttons clearfix pull-right">
            <button
              className={cx("btn medium configurations--form--button", isLocked ? "black-button" : "white-button")}
              onClick={this.handleCancel}>{isLocked ? "Close" : "Cancel"}</button>
            {
              !isLocked ?
                <button
                  className="btn black-button medium configurations--form--button"
                  onClick={this.handleSubmit}>Update</button> : null
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ devicesReducer, sitesReducer }) => {
  return { ...devicesReducer, ...sitesReducer };
}
const mapDispatchToProps = dispatch => {
  const allActions = { ...devicesActions, ...sitesActions };
  return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(SiteDetails);
export default ConnectedComponent;