import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tabs from 'react-responsive-tabs';
import { generateTreeData } from './configUtils';
import SnapshotsTab from './SnapshotsTab';
import DeviceTab from './DevicesTab/index';
import SitesTable from './SitesTab/index';
import devicesActions from '../../actions/devicesActions';
import sitesActions from '../../actions/sitesActions';
import tabsActions from '../../actions/tabsActions';
import snapshotsActions from '../../actions/snapshotsActions';

const tabsList = [
	{
		name: 'Snapshots'
	},
	{
		name: 'Devices'
	},
	{
		name: 'Sites'
	},
	{
		name: 'Transfer Status'
	},
	{
		name: 'Inventory Status'
	},
	{
		name: 'Script Management'
	},
	{
		name: 'Live Test Reports'
	}
];

class ConfigManagement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [],
			treeData: []
		};
		this.getTabs = this.getTabs.bind(this);
		this.getTabContent = this.getTabContent.bind(this);
		this.handleTabChange = this.handleTabChange.bind(this);
	}

	componentDidMount() {
		this.props.setActiveTab("Snapshots");
		this.props.resetSnapshots();
		this.props.resetDevices();
		this.props.resetSites();
	}

	getTabContent(name) {
		if (name === "Snapshots") {
			return (
				<div>
					<SnapshotsTab />
				</div>
			);
		} else if (name === "Devices") {
			return (
				<div>
					<DeviceTab />
				</div>
			);
		} else if (name === "Sites") {
			return (
				<div>
					<SitesTable />
				</div>
			);
		} else {
			return null;
		}
	}
	
	getTabs() {
		// console.log("getTabs");
		// console.log(this.props);
		// const { activeTab } = this.props;
		return tabsList.map((tabObj, index) => {
			return {
				title: tabObj.name,
				getContent: this.getTabContent.bind(null, tabObj.name),
				key: index,
				tabClassName: '',
				panelClassName: 'clearfix config-management--tab-content',
			};
		});
	}

	getActiveTab() {
		const { activeTab } = this.props;
		const activeTabIndex = tabsList.findIndex((tabObj) => {
			return tabObj.name === activeTab;
		});
		// console.log('activeTabIndex');
		// console.log(activeTabIndex);
		return activeTabIndex;
	}

	handleTabChange(activeTabIndex) {
		// console.log('handleTabChange', tabsList[activeTab].name);
		// this.props.clearSelectedHostnames();
		// this.props.clearSelectedDevices();
		// this.props.clearSelectedSites();
		// this.props.hideNewDeviceForm();
		// this.props.hideNewSiteForm();
		this.props.setActiveTab(tabsList[activeTabIndex].name);
		this.props.resetSnapshots();
		this.props.resetDevices();
		this.props.resetSites();
	}

	render() {
		const activeTabIndex = this.getActiveTab();
		return (
			<Fragment>
				<div className="panel panel-default component-wrapper">
					<div className="panel-body">
						<div className="config-management">
							<div className="col-xs-12 config-management--header">
								<div className="col-xs-6 no-padding config-management--header--search">
									
								</div>
							</div>
							<div className="col-xs-12 clearfix config-management--content">
								<Tabs 
									selectedTabKey={activeTabIndex} 
									items={this.getTabs()} 
									transformWidth={400}
									onChange={this.handleTabChange} />
							</div>
						</div>

					</div>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = ({ snapshotsReducer, tabsReducer }) => {
	return { ...snapshotsReducer, ...tabsReducer };
}
const mapDispatchToProps = dispatch => {
	const allActions = { ...devicesActions, ...sitesActions, ...snapshotsActions, ...tabsActions };
	return { ...bindActionCreators(allActions, dispatch) };
}
const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(ConfigManagement);
export default ConnectedComponent;