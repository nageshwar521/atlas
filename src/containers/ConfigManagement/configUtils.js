import React from 'react';

export const productsGenerator = (quantity = 5, callback) => {
  if (callback) return Array.from({ length: quantity }, callback);

  // if no given callback, retrun default product format.
  return (
    Array.from({ length: quantity }, (value, index) => ({
      id: index,
      name: `Item name ${index}`,
      price: 2100 + index,
      expand: [{
        fieldA: 'test1',
        fieldB: (i + 1) * 99,
        fieldC: (i + 1) * Math.random() * 100,
        fieldD: '123eedd' + i
      }, {
        fieldA: 'test2',
        fieldB: i * 99,
        fieldC: i * Math.random() * 100,
        fieldD: '123eedd' + i
      }]
    }))
  );
};

export const productsQualityGenerator = (quantity = 5) =>
  Array.from({ length: quantity }, (value, index) => ({
    id: index,
    name: `Item name ${index}`,
    quality: index % 3
  }));

export const productsExpandRowsGenerator = (quantity = 5, callback) => {
  if (callback) return Array.from({ length: quantity }, callback);

  // if no given callback, retrun default product format.
  return (
    Array.from({ length: quantity }, (value, index) => ({
      id: index,
      name: `Item name ${index}`,
      price: 2100 + index,
      expand: [{
        fieldA: 'test1',
        fieldB: (index + 1) * 99,
        fieldC: (index + 1) * Math.random() * 100,
        fieldD: '123eedd' + index
      }, {
        fieldA: 'test2',
        fieldB: index * 99,
        fieldC: index * Math.random() * 100,
        fieldD: '123eedd' + index
      }]
    }))
  );
};

export const generateTreeData = (data, callback) => {
  const getTreeData = (data, path = []) => {
    const excludedKeys = ["status", "stage", "Jira", "stash", "location", "latitude", "longitude", "totalVnfs"];
    const dataKeys = Object.keys(data);
    return dataKeys.map((dataKey) => {
      if (excludedKeys.includes(dataKey)) {
        return null;
      }
      let obj = {};
      if (!excludedKeys.includes(dataKey)) {
        if (dataKey === "sites") {
          data[dataKey].forEach((dataItem) => {
            let arr = [];
            const children = getTreeData(dataItem, path.concat(dataItem));
            const anotherChildren = children.map(childObj => childObj.children);
            const newChildren = anotherChildren[0].map(childObj => childObj.hostName);
            obj["children"] = newChildren.map((childItem) => {
              return { "label": <a className="treeview-hostname" onClick={callback.bind(null, path.concat(childItem).join(","))}>{childItem}</a> };
            });
          });
        } else if (String(data[dataKey]) === "[object Object]") {
          obj["label"] = <a className="treeview-hostname" onClick={callback.bind(null, path.concat(dataKey).join(","))}>{dataKey}</a>;
          obj["children"] = getTreeData(data[dataKey], path.concat(dataKey));
        } else {
          obj["label"] = <a className="treeview-hostname" onClick={callback.bind(null, path.concat(dataKey).join(","))}>{dataKey}</a>;
          obj["children"] = data[dataKey];
        }
      }
      if ((dataKey === "Wireless") || (dataKey === "Wireline") || (dataKey === "Enterprise")) {
        obj["open"] = true;
      }
      return obj;
    })
      .filter(item => item !== null)
      .map(itemObj => {
        return itemObj.label ? itemObj : itemObj.children;
      })
      .filter(item => item !== null);
  }
  const newData = getTreeData(data);
  const formatedData = (data) => {
    return data.map(dataObj => {
      if (dataObj.children) {
        if (!Array.isArray(dataObj.children[0])) {
          dataObj.children = formatedData(dataObj.children);
        } else {
          dataObj.children = dataObj.children[0];
        }
      }
      return dataObj;
    });
  }
  return formatedData(newData);
}
export const generateTree = (Tree, data) => {
  return data.map(({ children, label, open, type }, index) => {
    if (children) {
      return <Tree key={index} content={label} open={open} type={type}>
        {Array.isArray(children) ? generateTree(Tree, children) : children}
      </Tree>
    } else {
      return <Tree key={index} content={label} open={open} type={type} />
    }
  }).filter(item => item !== null);
}
export const filterSelectedRows = (data, selectedKeys) => {
  return data.filter((obj, idx) => selectedKeys.includes(idx));
}
export const searchHostnames = (data, searchStr) => {
  return data.filter(obj => {
    return Object.values(obj).join(" ").toLowerCase().includes(searchStr);
  });
}