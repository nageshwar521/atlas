import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Modal from '../../components/auditModal';
import { Get } from '../../utils/api-service';
import ReactJson from 'react-json-view';

class AuditEvents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eventId: this.props.match.params.id,
            eventData: {
                initiatedBy: null,
                time: null,
                event: null,
                requestUri: null,
                requestMethod: null,
                requestHeaders: null,
                requestBody: null,
                responseBody: null,
            },
        };
        this.JsonViewerConfig = {
            lineNumbers: true,
            name: false,
            iconStyle: 'square',
            enableClipboard: false,
            displayObjectSize: false,
            displayDataTypes: false,
        };
        this.back = this.back.bind(this);
    }
    componentDidMount() {
        //https://10.75.38.114:8445/api/v1/platform/audit/${this.state.eventId}
        Get(`https://atlas.ashs2.vnf.vzwnet.com:8445/api/v1/platform/audit/${this.state.eventId}`).then(data => {
            this.setState({
                eventData: data
            })
        })
    }

    back(e) {
        e.stopPropagation();
        this.props.history.goBack();
    }

    render() {
        if (!this.state.eventId) {
            return null;
        }
        const {
            initiatedBy,
            time,
            event,
            requestUri,
            requestMethod,
            requestHeaders,
            requestBody,
            responseBody,
        } = this.state.eventData;
        return (
            <Modal
                title={`Event ${this.state.eventId}`}
                saveLabel="Ok"
                back={this.back}
            >
                <div className="activity-modal">
                    <div className="row">
                        <label className="col-sm-3">INITIATED BY</label>
                        <span className="col-sm-9">
                            {initiatedBy} on {time}
                        </span>
                    </div>
                    <div className="row">
                        <label className="col-sm-3">ACTION</label>
                        <span className="col-sm-9">{event}</span>
                    </div>
                    <div className="row">
                        <label className="col-sm-3">REQUEST URI</label>
                        <span className="col-sm-9">
                            {requestMethod} - {requestUri}
                        </span>
                    </div>
                    <div className="row">
                        <label className="col-sm-12">REQUEST HEADERS</label>
                        <div className="col-sm-12 json-container">
                            <ReactJson
                                src={requestHeaders}
                                {...this.JsonViewerConfig}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <label className="col-sm-12">REQUEST BODY</label>
                        <div className="col-sm-12 json-container">
                            <ReactJson
                                src={requestBody}
                                {...this.JsonViewerConfig}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <label className="col-sm-12">RESPONSE BODY</label>
                        <div className="col-sm-12 json-container">
                            <ReactJson
                                src={JSON.parse(responseBody)}
                                {...this.JsonViewerConfig}
                            />
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

export default withRouter(AuditEvents);
