import React, { Component, Fragment } from 'react';
import { Get } from '../../utils/api-service';
import { auditActivityUrl } from "../../config";
import { Link } from 'react-router-dom';
import Pagination from 'rc-pagination';
import Select from 'rc-select';
import localeInfo from 'rc-pagination/lib/locale/en_US';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
const { SearchBar } = Search;

class TableSearch extends Component {
    constructor() {
        super();
        this.state = {
            searchStr: '',
        };
    }
    componentWillMount() {
        this.setState({
            searchStr: this.props.searchStr,
        });
    }
    handleClear() {
        this.setState({ searchStr: "" }, () => {
            this.props.onSearchChange("");
        });
    }
    handleSubmit() {
        this.props.onSearchChange(this.state.searchStr);
    }
    render() {
        const { searchStr = '' } = this.state;
        return (
            <div className="input-group auditTable-searchBox">
                <input
                    className="form-control"
                    type="text"
                    onChange={evt =>
                        this.setState({ searchStr: evt.target.value })
                    }
                    value={searchStr}
                />
                <div className="input-group-btn">
                    {
                        searchStr ? <button
                            className="btn clearBtn"
                            type="button"
                            onClick={this.handleClear.bind(this)}
                        >
                            <span className="fa fa-times searchIcon" />
                        </button> : null
                    }
                    <button
                        className="btn searchBtn"
                        type="button"
                        onClick={this.handleSubmit.bind(this)}
                    >
                        <span className="fa fa-search searchIcon" />
                    </button>
                </div>
            </div>
        );
    }
}

export default class AuditActivities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            activePage: 1,
            columns: [
                {
                    text: 'TIME',
                    dataField: 'time',
                    sort: true,
                },
                {
                    text: 'INITIATED BY',
                    dataField: 'initiatedBy',
                    sort: true,
                },
                {
                    text: 'EVENT',
                    dataField: 'event',
                    sort: true,
                },
                {
                    text: 'ACTIONS',
                    dataField: 'id',
                    formatter: id => (
                        <Link
                            className="audit-link"
                            to={{
                                pathname: `/audit-activity/${id}`,
                                state: { modal: true },
                            }}
                        >
                            <i className="fa fa-search-plus" />
                        </Link>
                    ),
                },
            ],
            tableData: [],
            totalRows: 0,
            page: 0,
            sizePerPage: 10,
            searchStr: '',
        };

        this.handleSearch = this.handleSearch.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSizePerPageChange = this.handleSizePerPageChange.bind(this);
        this.renderShowTotal = this.renderShowTotal.bind(this);
    }

    componentDidMount() {
        this.fetchTotalRowsCount();
        this.fetchData();
    }

    getFetchUrlWithParams(params = []) {
        const baseUrl = auditActivityUrl;
        const paramArr = params.map(param => `${param.name}=${param.value}`);
        const queryString =
            paramArr.length > 0 ? '?' + paramArr.join('&') : paramArr.join('&');
        return `${baseUrl}${queryString}`;
    }

    fetchTotalRowsCount() {
        const params = [];
        const fetchUrl = this.getFetchUrlWithParams(params);

        Get(fetchUrl).then(data => {
            const { totalCount } = data;
            this.setState({
                totalRows: totalCount,
            });
        });
    }

    fetchData(
        page = this.state.page,
        sizePerPage = this.state.sizePerPage,
        searchStr = this.state.searchStr
    ) {
        const params = [
            { name: 'page', value: page },
            { name: 'sizePerPage', value: sizePerPage },
            { name: 'search', value: searchStr },
        ];
        const fetchUrl = this.getFetchUrlWithParams(params);

        Get(fetchUrl)
            .then(data => {
                const { activities = [] } = data;

                this.setState({
                    tableData: activities,
                    page,
                    sizePerPage,
                    searchStr,
                });
            })
            .catch(error => {
                console.error(error);
            });
    }

    handlePageChange(page = this.state.page) {
        this.fetchData(page - 1, this.state.sizePerPage);
    }

    handleSizePerPageChange(sizePerPage = this.state.sizePerPage) {
        this.fetchData(this.state.page, sizePerPage);
    }

    renderShowTotal(total, range) {
        return (
            <span className="react-bootstrap-table-pagination-total">
                Showing {range[0]} to {range[1]} of {total} Results
            </span>
        );
    }

    handleSearch(searchStr) {
        this.fetchData(this.state.page, this.state.sizePerPage, searchStr);
    }

    clearSearch() {
        this.fetchData(this.state.page, this.state.sizePerPage, "");
    }

    render() {
        const { tableData = [], columns = [], totalRows } = this.state;

        return (
            <Fragment>
                <div className="panel panel-default component-wrapper">
                    <div className="panel-body">
                        <h5 />
                        <ToolkitProvider
                            keyField="id"
                            data={tableData}
                            columns={columns}
                            search
                        >
                            {props => (
                                <div>
                                    <span className="heading-text">
                                        AUDIT | ALL ACTIVITY
                                    </span>

                                    <TableSearch
                                        {...props.searchProps}
                                        searchStr={this.state.searchStr}
                                        onSearchChange={this.handleSearch}
                                        onClear={this.clearSearch}
                                    />
                                    <hr />
                                    <BootstrapTable
                                        {...props.baseProps}
                                        hover
                                        classes="auditTable"
                                        bordered={false}
                                    />
                                </div>
                            )}
                        </ToolkitProvider>
                        <Pagination
                            selectComponentClass={Select}
                            pageSize={this.state.sizePerPage}
                            showSizeChanger={true}
                            onShowSizeChange={this.handleSizePerPageChange}
                            pageSizeOptions={["10", "25", "50"]}
                            onChange={this.handlePageChange}
                            current={this.state.page + 1}
                            total={totalRows}
                            showTotal={this.renderShowTotal}
                            locale={localeInfo} />
                    </div>
                </div>
            </Fragment>
        );
    }
}
