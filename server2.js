const jsonServer = require('json-server');
const cors = require('cors');
const server = jsonServer.create();
const router = jsonServer.router('./db.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 3002;
const db = require('./db.json');

server.use(cors());
server.get('/hostnames', (req, res) => {
  // console.log('/hostnames');
  // console.log(server);
  let page = req.query['page'] || 0;
  let sizePerPage = req.query['sizePerPage'] || 10;
  let hostnames = db.hostnames;
  let { snapshots, totalCount, countMatchingRequest} = hostnames;
  let results = [];
  // console.log(page, sizePerPage);
  // console.log(snapshots);

  if (!isNaN(page) && !isNaN(sizePerPage)) {
    let from = ((page - 1) * sizePerPage) === 0 ? 1 : (page - 1) * sizePerPage;
    results = snapshots.slice(from, sizePerPage * page);
    // console.log(results);
    const data = {
      totalCount,
      countMatchingRequest,
      snapshots: results
    };
    res.status(200).json(data);
  } else {
    const data = {
      totalCount,
      countMatchingRequest,
      snapshots: results
    };
    res.status(200).json(data);
  }
});
server.get('/devicesMock', (req, res) => {
  // console.log('/devicesMock');
  // console.log(server);
  let page = req.query['page'] || 0;
  let sizePerPage = req.query['sizePerPage'] || 10;
  let devicesMock = db.devicesMock;
  let { devices, totalCount, countMatchingRequest } = devicesMock;
  let results = [];
  console.log(page, sizePerPage);
  console.log(devices);

  if (!isNaN(page) && !isNaN(sizePerPage)) {
    let from = page * sizePerPage;
    let to = sizePerPage * (page+1);
    results = devices.slice(from, to);
    console.log(from, to);
    console.log(results);
    const data = {
      totalCount,
      countMatchingRequest,
      devices: results
    };
    res.status(200).json(data);
  } else {
    const data = {
      totalCount,
      countMatchingRequest,
      devices: results
    };
    res.status(200).json(data);
  }
});
server.get('/sitesMock', (req, res) => {
  // console.log('/sitesMock');
  // console.log(server);
  let page = req.query['page'] || 0;
  let sizePerPage = req.query['sizePerPage'] || 10;
  let sitesMock = db.sitesMock;
  let { sites, totalCount, countMatchingRequest } = sitesMock;
  let results = [];
  // console.log(page, sizePerPage);
  // console.log(sites);

  if (!isNaN(page) && !isNaN(sizePerPage)) {
    let from = page * sizePerPage;
    let to = sizePerPage * (page + 1);
    results = sites.slice(from, to);
    console.log(from, to);
    console.log(results);
    const data = {
      totalCount,
      countMatchingRequest,
      sites: results
    };
    res.status(200).json(data);
  } else {
    const data = {
      totalCount,
      countMatchingRequest,
      sites: results
    };
    res.status(200).json(data);
  }
});

server.use(middlewares);
server.use(router);
server.listen(port);